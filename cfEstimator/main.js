﻿//set bluronly to true to simply remove focus from the field
//the user was currently in
function submitenter(myfield,e,btn,bluronly)
{
  var keycode;
  if (window.event) keycode = window.event.keyCode;
  else if (e) keycode = e.which;
  else return true;

  if (keycode == 13)
  {
    if (bluronly)
    {
      myfield.blur();
      return false;
    }
    //alert("field id: " + myfield.id);
    //myfield.form.submit();
    var arr = myfield.id.split("_");
    if (arr.length == 2) {
      myfield.form[arr[0] + "_" + btn].click();
    } else if (arr.length == 3) {
      myfield.form[arr[0] + "_" + arr[1] + "_" + btn].click();
    }
    else {
      myfield.form[btn].click();
    }
    return false;
  }
  else {
    return true;
  }
}

//Returns an associative array of key/value pairs
//split based on the delimiter passed in.  Assumes
//string contains paramaters of the form key=value.
//Set convertToLower to true to set key to lowercase.
function parseParams(paramString, delimiter, convertToLower) {
  var s = paramString.toString();
  if (s.charAt(s.length-1) == delimiter)
    s.substr(0,s.length-1)
  if (s.charAt(0) == delimiter)
    s = s.substr(1);
  var myParams = new Array();
  var vars = s.split(delimiter);
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
    if (convertToLower)
      myParams[pair[0].toLowerCase()] = pair[1];
    else
      myParams[pair[0]] = pair[1];
  }
  return myParams;
}

//Number Formating Functions

//Formats to two decimal places or returns as whole number if decimal not required
function formatNumber(num) {
  num = parseFloat(num.toString().replace(",",""));
  //alert(num);
  if (num == 0)
    return "";
  var fNum = num.toFixed(2);
  var decPos=fNum.indexOf(".");
  if (decPos>-1)
  {
    first=fNum.substring(0,decPos);
    second=fNum.substring(decPos,fNum.length);
    while (second.charAt(second.length-1)=="0")
      second=second.substring(0,second.length-1);
    if (second.length>1)
      return first+second;
    else
      return first;
  }

  return fNum;
}

//AJAX Functions

/*first ajax example*/
function createRequestObject() {
    var ro;
    var browser = navigator.appName;
    if(browser == "Microsoft Internet Explorer"){
        ro = new ActiveXObject("Microsoft.XMLHTTP");
    }else{
        ro = new XMLHttpRequest();
    }
    return ro;
}

var http = createRequestObject();

function sndReq(qs) {
    http.open('get', 'ajaxresponse.aspx?'+qs+'&t='+ (new Date().valueOf()), true);
    http.onreadystatechange = handleResponse;
    http.send(null);
}

function handleResponse() {
    if (http.readyState == 4 || http.readyState == 'complete'){
        var response = http.responseText;
        //if(response != "success")
        if(response.indexOf("=") != 0)
          alert("An error occurred while performing this task!  The most likely cause is that your session has expired.");
        else
        {
          if(typeof window.processResponse == 'function') {
            // function exists, so we can now call it
            processResponse(response.substring(1));
          }
        }
        /*
        var update = new Array();

        if(response.indexOf('|' != -1)) {
            update = response.split('|');
            //alert(update[0]);
            document.getElementById(update[0]).innerHTML = update[1];
        }
        */
    }
}

/*second ajax example*/
/*
function GetXmlHttpObject(handler)
{ 
    var objXmlHttp = null;
    if (!window.XMLHttpRequest)
    {
        // Microsoft
        objXmlHttp = GetMSXmlHttp();
        if (objXmlHttp != null)
        {
            objXmlHttp.onreadystatechange = handler;
        }
    } 
    else
    {
        // Mozilla | Netscape | Safari
        objXmlHttp = new XMLHttpRequest();
        if (objXmlHttp != null)
        {
            objXmlHttp.onload = handler;
            objXmlHttp.onerror = handler;
        }
    } 
    return objXmlHttp; 
} 

function GetMSXmlHttp() {
    var xmlHttp = null;
    var clsids = ["Msxml2.XMLHTTP.6.0",
                  "Msxml2.XMLHTTP.4.0",
                  "Msxml2.XMLHTTP.3.0"];
    for(var i=0; i<clsids.length && xmlHttp == null; i++) {
        xmlHttp = CreateXmlHttp(clsids[i]);
    }
    return xmlHttp;
}

function CreateXmlHttp(clsid) {
    var xmlHttp = null;
    try {
        xmlHttp = new ActiveXObject(clsid);
        lastclsid = clsid;
        return xmlHttp;
    }
    catch(e) {}
}

function SendXmlHttpRequest(xmlhttp, url) { 
    xmlhttp.open('GET', url, true); 
    xmlhttp.send(null); 
}

var xmlHttp; 

function ExecuteCall(url)
{ 
    try 
    { 
        xmlHttp = GetXmlHttpObject(CallbackMethod); 
        SendXmlHttpRequest(xmlHttp, url); 
    }
    catch(e){} 
} 
    
//CallbackMethod will fire when the state 
//has changed, i.e. data is received back 
function CallbackMethod() 
{ 
    try
    {
        //readyState of 4 or 'complete' represents 
        //that data has been returned 
        if (xmlHttp.readyState == 4 || 
            xmlHttp.readyState == 'complete')
        {
            var response = xmlHttp.responseText; 
            if (response.length > 0)
            {
                //update page
                document.getElementById("foo").innerHTML 
                                                   = response; 
            } 
        }
    }
    catch(e){}
}
*/