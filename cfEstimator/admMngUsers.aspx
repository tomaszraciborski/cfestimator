﻿﻿<%@ page language="C#" enableeventvalidation="false" masterpagefile="~/cfestimator.master" autoeventwireup="true" CodeBehind="admMngUsers.aspx.cs" Inherits="cfEstimator.admMngUsers" title="Manage Users" %>
<%@ Register src="admMenu.ascx" tagname="admMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<uc1:admMenu ID="admMenu1" runat="server" TagID="hrefUsers" />
<div style="text-align: center"><asp:Label runat="server" ID="lblMsg" CssClass="infoMsg" EnableViewState="false"></asp:Label></div>
    <div align=right>
        <asp:LinkButton ID="lbExport" runat="server" onclick="lbExport_Click"><img 
                src="img/button_excel.gif" border="0" width="24" height="24" 
                alt="Export Users to Excel" name="exportexcel" id="exportexcel" /></asp:LinkButton>
    </div>
<div class="gridActions">
<strong>Actions:</strong>
<a href="userProfile.aspx">Add new user</a>
</div>
<asp:GridView ID="gvUsers" runat="server" CssClass="gridTable" AllowPaging="True" AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="UserID" OnRowDeleting="DeleteRow" OnSelectedIndexChanging="SelectRow" CellPadding="0" PageSize="20" OnPageIndexChanging="ChangePage" GridLines="None" BorderStyle="None" Width="100%">
    <Columns>
        <asp:CommandField ShowSelectButton="True" SelectImageUrl="~/img/edit.gif" SelectText="View" ButtonType="Image" ItemStyle-Width="16px" />
        <asp:TemplateField>
            <ItemStyle Width="16px" />
            <ItemTemplate>
                <a href="admMngUsers.aspx?fn=delete&id=<%# DataBinder.Eval(Container.DataItem, "UserID").ToString() %>&username=<%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem, "Username").ToString()) %>" onclick="javascript:return confirm('Are you sure you want to delete user \'<%# DataBinder.Eval(Container.DataItem, "Username").ToString() %>\'?');"><img alt="Delete" src="img/delete.gif" border="0" width="16px" height="16px" /></a>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="UserID" HeaderText="UserID" InsertVisible="False"
            ReadOnly="True" SortExpression="UserID" Visible="False" />
        <asp:BoundField DataField="Username" HeaderText="Username" SortExpression="Username" />
        <asp:BoundField DataField="LastName" HeaderText="Last Name" SortExpression="LastName" />
        <asp:BoundField DataField="FirstName" HeaderText="First Name" SortExpression="FirstName" />
        <asp:CheckBoxField DataField="IsActive" HeaderText="Is Active" SortExpression="IsActive" />
        <asp:CheckBoxField DataField="IsAdmin" HeaderText="Is Admin" SortExpression="IsAdmin" />
        <asp:CheckBoxField DataField="IsGDEmployee" HeaderText="Is GD Employee" SortExpression="IsGDEmployee" />
        <asp:BoundField DataField="LastLoginDate" HeaderText="Last Login" SortExpression="LastLoginDate" />
    </Columns>
    <HeaderStyle CssClass="gridHeader" />
    <RowStyle CssClass="gridRow" VerticalAlign="Top" />
    <AlternatingRowStyle CssClass="gridAltRow" VerticalAlign="Top" />
    <PagerStyle CssClass="gridPager" />
</asp:GridView>
<asp:Panel ID="pnlData" runat="server" Visible="true">
    <asp:GridView ID="gvData" runat="server" EnableViewState="false" 
    AutoGenerateColumns="True">
    </asp:GridView>
</asp:Panel>
</asp:Content>

