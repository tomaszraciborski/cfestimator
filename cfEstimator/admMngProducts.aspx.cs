﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using static cfEstimator.App_Code;

namespace cfEstimator
{      
    public class admMngProducts : Page, IRequiresSessionState
    {
        // Fields
        protected admMenu admMenu1;
        protected Label lblMsg;
        protected FileUpload fuProducts;
        protected Button btUploadProducts;
        protected Button btAdd;
        protected LinkButton lbExport;
        protected TextBox tbPartNumber;
        protected Button btFind;
        protected Label lblErrors;
        protected TextBox tbHFPartNumber;
        protected Button btDelete;
        protected TextBox tbDescription;
        protected TextBox tbSize;
        protected TextBox tbListPrice;
        protected DropDownList ddPriceCodes;
        protected TextBox tbWeight;
        protected TextBox tbCostPrice;
        protected TextBox tbPowerHP;
        protected TextBox tbFlowCFM;
        protected DropDownList ddCategories;
        protected TextBox tbStages;
        protected TextBox tbNotes;
        protected DropDownList ddFrames;
        protected DropDownList ddBrands;
        protected Button btSave;
        protected Button btCancel;
        protected PlaceHolder phEditForm;
        protected GridView gvData;
        protected Panel pnlData;
        protected HtmlInputHidden hdProductID;

        // Methods
        protected void btAdd_Click(object sender, EventArgs e)
        {
            this.InitializeForm();
            this.phEditForm.Visible = true;
        }

        protected void btCancel_Click(object sender, EventArgs e)
        {
            this.hdProductID.Value = "";
            this.tbHFPartNumber.Text = "";
            this.phEditForm.Visible = false;
        }

        protected void btDelete_Click(object sender, EventArgs e)
        {
            data data = new data();
            if (this.hdProductID.Value != "")
            {
                data.AssignProductNotes(int.Parse(this.hdProductID.Value), "");
                if (data.DeleteProduct(int.Parse(this.hdProductID.Value)))
                {
                    this.lblMsg.Text = "Successfully deleted product!";
                    this.hdProductID.Value = "";
                    this.tbHFPartNumber.Text = "";
                    this.phEditForm.Visible = false;
                }
                else
                {
                    this.lblMsg.CssClass = "errorMsg";
                    this.lblMsg.Text = "Unable to delete product!  It may be associated with other data such as frame configurations.";
                }
            }
        }

        protected void btFind_Click(object sender, EventArgs e)
        {
            if (this.tbPartNumber.Text.Trim() != "")
            {
                data data = new data();
                DataSet products = data.GetProducts(this.tbPartNumber.Text.Trim(), this.Session["BrandAccess"].ToString());
                if (products.Tables[0].Rows.Count == 0)
                {
                    this.lblMsg.CssClass = "errorMsg";
                    this.lblMsg.Text = "Cannot find a product with that part number!";
                }
                else
                {
                    this.InitializeForm();
                    this.phEditForm.Visible = true;
                    DataRow row = products.Tables[0].Rows[0];
                    this.hdProductID.Value = row["ProductID"].ToString();
                    this.tbHFPartNumber.Text = row["HFPartNumber"].ToString();
                    this.tbDescription.Text = row["Description"].ToString();
                    this.tbSize.Text = row["Size"].ToString();
                    if (!row.IsNull("CategoryID") && (this.ddCategories.Items.FindByValue(row["CategoryID"].ToString()) != null))
                    {
                        this.ddCategories.ClearSelection();
                        this.ddCategories.Items.FindByValue(row["CategoryID"].ToString()).Selected = true;
                    }
                    this.tbListPrice.Text = row["ListPrice"].ToString();
                    this.tbCostPrice.Text = row["CostPrice"].ToString();
                    if (this.ddPriceCodes.Items.FindByValue(row["PriceCode"].ToString()) != null)
                    {
                        this.ddPriceCodes.ClearSelection();
                        this.ddPriceCodes.Items.FindByValue(row["PriceCode"].ToString()).Selected = true;
                    }
                    if (!row.IsNull("Weight"))
                    {
                        this.tbWeight.Text = row["Weight"].ToString();
                    }
                    if (!row.IsNull("PowerHP"))
                    {
                        this.tbPowerHP.Text = row["PowerHP"].ToString();
                    }
                    if (!row.IsNull("FlowCFM"))
                    {
                        this.tbFlowCFM.Text = row["FlowCFM"].ToString();
                    }
                    if (!row.IsNull("Stages"))
                    {
                        this.tbStages.Text = row["Stages"].ToString();
                    }
                    if (!row.IsNull("FrameID") && (this.ddFrames.Items.FindByValue(row["FrameID"].ToString()) != null))
                    {
                        this.ddFrames.ClearSelection();
                        this.ddFrames.Items.FindByValue(row["FrameID"].ToString()).Selected = true;
                    }
                    if (!row.IsNull("BrandID") && (this.ddBrands.Items.FindByValue(row["BrandID"].ToString()) != null))
                    {
                        this.ddBrands.ClearSelection();
                        this.ddBrands.Items.FindByValue(row["BrandID"].ToString()).Selected = true;
                    }
                    products = data.GetProductNotes(int.Parse(row["ProductID"].ToString()));
                    if (products.Tables[0].Rows.Count > 0)
                    {
                        string str = "";
                        foreach (DataRow row2 in products.Tables[0].Rows)
                        {
                            str = str + row2["NoteID"] + "|";
                        }
                        if (str.Length > 0)
                        {
                            str = str.TrimEnd(new char[] { '|' });
                        }
                        this.tbNotes.Text = str;
                    }
                }
            }
        }

        protected void btSave_Click(object sender, EventArgs e)
        {
            string str = "";
            string hFPartNumber = "";
            string description = "";
            string size = "";
            double listPrice = 0.0;
            string priceCode = "";
            double weight = 0.0;
            double costPrice = 0.0;
            double powerHP = 0.0;
            double flowCFM = 0.0;
            int frameID = -1;
            int stages = -1;
            int brandID = -1;
            int categoryID = -1;
            string noteIDList = "";
            string str7 = "";
            data data = new data();
            hFPartNumber = this.tbHFPartNumber.Text;
            if (!Regex.IsMatch(this.tbHFPartNumber.Text, "^([a-zA-Z0-9-_])*$"))
            {
                str = str + "Invalid characters in part number " + hFPartNumber + "!  Part number can only contain letters, numbers, dashes, and underscores.<br />";
            }
            if (hFPartNumber.Trim() == "")
            {
                str = str + "The part number field cannot be empty!<br />";
            }
            description = this.tbDescription.Text;
            if (description.Trim() == "")
            {
                str = str + "The description field cannot be empty!<br />";
            }
            size = this.tbSize.Text;
            try
            {
                listPrice = double.Parse(this.tbListPrice.Text);
            }
            catch
            {
                str = str + "Invalid list price!<br />";
            }
            if (this.ddPriceCodes.SelectedIndex == 0)
            {
                str = str + "You must select a price code!<br />";
            }
            else
            {
                priceCode = this.ddPriceCodes.SelectedValue;
            }
            if (this.tbWeight.Text == "")
            {
                weight = -1.0;
            }
            else
            {
                try
                {
                    weight = double.Parse(this.tbWeight.Text);
                }
                catch
                {
                    str = str + "Invalid weight!<br />";
                }
            }
            try
            {
                costPrice = double.Parse(this.tbCostPrice.Text);
            }
            catch
            {
                str = str + "Invalid cost price!<br />";
            }
            if (this.tbPowerHP.Text == "")
            {
                powerHP = -1.0;
            }
            else
            {
                try
                {
                    powerHP = double.Parse(this.tbPowerHP.Text);
                }
                catch
                {
                    str = str + "Invalid number for powerhp!<br />";
                }
            }
            if (this.tbFlowCFM.Text == "")
            {
                flowCFM = -1.0;
            }
            else
            {
                try
                {
                    flowCFM = double.Parse(this.tbFlowCFM.Text);
                }
                catch
                {
                    str = str + "Invalid number for flowcfm!<br />";
                }
            }
            frameID = int.Parse(this.ddFrames.SelectedValue);
            if (this.tbStages.Text == "")
            {
                stages = -1;
            }
            else
            {
                try
                {
                    stages = int.Parse(this.tbStages.Text);
                }
                catch
                {
                    str = str + "Invalid number of stages!<br />";
                }
            }
            brandID = int.Parse(this.ddBrands.SelectedValue);
            categoryID = int.Parse(this.ddCategories.SelectedValue);
            if (this.tbNotes.Text == "")
            {
                noteIDList = "";
            }
            else
            {
                string[] strArray = this.tbNotes.Text.Split(new char[] { '|' });
                if (str7 == "")
                {
                    str7 = "|";
                    foreach (DataRow row in data.GetNotes(-1).Tables[0].Rows)
                    {
                        str7 = str7 + row["NoteID"].ToString() + "|";
                    }
                }
                bool flag = false;
                string[] strArray2 = strArray;
                int index = 0;
                while (true)
                {
                    if (index < strArray2.Length)
                    {
                        string s = strArray2[index];
                        try
                        {
                            if (str7.Contains("|" + int.Parse(s).ToString() + "|"))
                            {
                                index++;
                                continue;
                            }
                            flag = true;
                        }
                        catch
                        {
                            flag = true;
                        }
                    }
                    if (flag)
                    {
                        str = str + "Invalid note id!<br />";
                    }
                    else
                    {
                        noteIDList = this.tbNotes.Text;
                    }
                    break;
                }
            }
            if (str == "")
            {
                if (this.hdProductID.Value != "")
                {
                    if (!data.UpdateProduct(int.Parse(this.hdProductID.Value), hFPartNumber, description, size, listPrice, priceCode, weight, costPrice, powerHP, flowCFM, categoryID, stages, frameID, brandID))
                    {
                        str = str + "Error updating product!<br />";
                    }
                    else
                    {
                        this.lblMsg.Text = "Successfully updated product.";
                    }
                }
                else if (data.InsertProduct(hFPartNumber, description, size, listPrice, priceCode, weight, costPrice, powerHP, flowCFM, categoryID, stages, frameID, brandID) < 0)
                {
                    str = str + "Error inserting product!<br />";
                }
                else
                {
                    this.lblMsg.Text = "Successfully added product.";
                }
                if (str == "")
                {
                    int productID = data.GetProductID(hFPartNumber);
                    if (noteIDList != "")
                    {
                        data.AssignProductNotes(productID, noteIDList);
                    }
                    else
                    {
                        data.AssignProductNotes(productID, "");
                    }
                }
            }
            if (str != "")
            {
                this.lblErrors.Text = str;
                this.phEditForm.Visible = true;
            }
        }

        protected void btUploadProducts_Click(object sender, EventArgs e)
        {
            string str;
            CSVReader reader;
            DataTable table;
            int num;
            data data;
            string str9;
            if (this.fuProducts.PostedFile.FileName == string.Empty)
            {
                return;
            }
            else
            {
                str = "";
                string[] strArray = this.fuProducts.FileName.Split(new char[] { '.' });
                if (strArray[strArray.Length - 1].ToLower() != "csv")
                {
                    this.lblMsg.Text = "Data file must be a CSV file!";
                    this.lblMsg.Style["color"] = "red";
                    return;
                }
                else
                {
                    reader = new CSVReader(this.fuProducts.PostedFile.InputStream);
                    table = new DataTable();
                    string[] cSVLine = reader.GetCSVLine();
                    int index = 0;
                    while (true)
                    {
                        if (index >= cSVLine.Length)
                        {
                            num = -1;
                            data = new data();
                            str9 = "";
                            break;
                        }
                        string columnName = cSVLine[index];
                        table.Columns.Add(columnName);
                        index++;
                    }
                }
            }
            while (true)
            {
                string[] cSVLine = reader.GetCSVLine();
                if (cSVLine != null)
                {
                    num++;
                    table.Rows.Add(cSVLine);
                    string input = table.Rows[num]["HFPARTNUMBER"].ToString().Trim();
                    if (!Regex.IsMatch(input, "^([a-zA-Z0-9-_])*$"))
                    {
                        str = str + "Invalid characters in part number " + input + "!  Part number can only contain letters, numbers, dashes, and underscores.<br />";
                    }
                    if (input.Trim() != "")
                    {
                        string description = table.Rows[num]["DESCRIPTION"].ToString().Trim();
                        if (description.Trim() != "")
                        {
                            double num2;
                            string size = table.Rows[num]["SIZE"].ToString().Trim();
                            try
                            {
                                num2 = double.Parse(table.Rows[num]["LISTPRICE"].ToString().Trim());
                            }
                            catch
                            {
                                str = str + "Invalid list price for part number " + input + "!<br />";
                                continue;
                            }
                            string priceCode = table.Rows[num]["PRCODE"].ToString().Trim();
                            if (priceCode.Trim() != "")
                            {
                                double num3;
                                double num4;
                                double num5;
                                double num6;
                                int num7 = 0;
                                int num8 = 0;
                                int num9 = 0;
                                int num10 = 0;
                                string str8;
                                if (table.Rows[num]["WEIGHT"].ToString().Trim() == "")
                                {
                                    num3 = -1.0;
                                }
                                else
                                {
                                    try
                                    {
                                        num3 = double.Parse(table.Rows[num]["WEIGHT"].ToString().Trim());
                                    }
                                    catch
                                    {
                                        str = str + "Invalid weight for part number " + input + "!<br />";
                                        continue;
                                    }
                                }
                                try
                                {
                                    num4 = double.Parse(table.Rows[num]["COSTPRICE"].ToString().Trim());
                                }
                                catch
                                {
                                    str = str + "Invalid cost price for part number " + input + "!<br />";
                                    continue;
                                }
                                if (table.Rows[num]["POWERHP"].ToString().Trim() == "")
                                {
                                    num5 = -1.0;
                                }
                                else
                                {
                                    try
                                    {
                                        num5 = double.Parse(table.Rows[num]["POWERHP"].ToString().Trim());
                                    }
                                    catch
                                    {
                                        str = str + "Invalid powerhp for part number " + input + "!<br />";
                                        continue;
                                    }
                                }
                                if (table.Rows[num]["FLOWCFM"].ToString().Trim() == "")
                                {
                                    num6 = -1.0;
                                }
                                else
                                {
                                    try
                                    {
                                        num6 = double.Parse(table.Rows[num]["FLOWCFM"].ToString().Trim());
                                    }
                                    catch
                                    {
                                        str = str + "Invalid flowcfm for part number " + input + "!<br />";
                                        continue;
                                    }
                                }
                                if (table.Rows[num]["FRAME"].ToString().Trim() == "")
                                {
                                    num7 = -1;
                                }
                                else if (data.GetFrameIDByName(table.Rows[num]["FRAME"].ToString().Trim()) == -1)
                                {
                                    str = str + "Invalid frame for part number " + input + "!<br />";
                                    continue;
                                }
                                if (table.Rows[num]["STAGES"].ToString().Trim() == "")
                                {
                                    num8 = -1;
                                }
                                else
                                {
                                    try
                                    {
                                        num8 = int.Parse(table.Rows[num]["STAGES"].ToString().Trim());
                                    }
                                    catch
                                    {
                                        str = str + "Invalid number of stages for part number " + input + "!<br />";
                                        continue;
                                    }
                                }
                                if (table.Rows[num]["BRAND"].ToString().Trim() == "")
                                {
                                    num9 = -1;
                                }
                                else if (data.GetBrandIDByName(table.Rows[num]["BRAND"].ToString().Trim()) == -1)
                                {
                                    str = str + "Invalid brand for part number " + input + "!<br />";
                                    continue;
                                }
                                if (table.Rows[num]["CATEGORY"].ToString().Trim() == "")
                                {
                                    num10 = -1;
                                }
                                else if (data.GetCategoryID(table.Rows[num]["CATEGORY"].ToString().Trim()) == -1)
                                {
                                    str = str + "Invalid category for part number " + input + "!<br />";
                                    continue;
                                }
                                if (table.Rows[num]["NOTEIDS"].ToString().Trim() != "")
                                {
                                    string[] strArray4 = table.Rows[num]["NOTEIDS"].ToString().Trim().Split(new char[] { '|' });
                                    if (str9 == "")
                                    {
                                        str9 = "|";
                                        foreach (DataRow row in data.GetNotes(-1).Tables[0].Rows)
                                        {
                                            str9 = str9 + row["NoteID"].ToString() + "|";
                                        }
                                    }
                                    bool flag = false;
                                    string[] strArray6 = strArray4;
                                    int index = 0;
                                    while (true)
                                    {
                                        if (index < strArray6.Length)
                                        {
                                            string s = strArray6[index];
                                            try
                                            {
                                                if (str9.Contains("|" + int.Parse(s).ToString() + "|"))
                                                {
                                                    index++;
                                                    continue;
                                                }
                                                flag = true;
                                            }
                                            catch
                                            {
                                                flag = true;
                                            }
                                        }
                                        if (!flag)
                                        {
                                            str8 = table.Rows[num]["NOTEIDS"].ToString();
                                        }
                                        else
                                        {
                                            str = str + "Invalid note id for part number " + input + "!<br />";
                                            continue;
                                        }
                                        break;
                                    }
                                }
                                else
                                {
                                    str8 = "";
                                }
                                if (data.DoesPartNumberExist(input))
                                {
                                    if (!data.UpdateProduct(-1, input, description, size, num2, priceCode, num3, num4, num5, num6, num10, num8, num7, num9))
                                    {
                                        str = str + "Error updating part number " + input + "!<br />";
                                        continue;
                                    }
                                }
                                else if (data.InsertProduct(input, description, size, num2, priceCode, num3, num4, num5, num6, num10, num8, num7, num9) < 0)
                                {
                                    str = str + "Error inserting part number " + input + "!<br />";
                                    continue;
                                }
                                int productID = data.GetProductID(input);
                                if (str8 != "")
                                {
                                    data.AssignProductNotes(productID, str8);
                                }
                                else
                                {
                                    data.AssignProductNotes(productID, "");
                                }
                            }
                            else
                            {
                                str = str + "The price code field cannot be empty for part number " + input + "!<br />";
                            }
                        }
                        else
                        {
                            str = str + "The description field cannot be empty for part number " + input + "!<br />";
                        }
                    }
                    else
                    {
                        object[] objArray = new object[] { str, "The part number field cannot be empty as in row number ", num + 2, "!<br />" };
                        str = string.Concat(objArray);
                    }
                    continue;
                }
                else
                {
                    this.lblMsg.Text = "Product upload complete!";
                    if (str != "")
                    {
                        this.lblErrors.Text = str;
                        return;
                    }
                }
                break;
            }
        }

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.FindControl("lblNoteIDs") != null)
            {
                string str = "";
                foreach (DataRow row in new data().GetProductNotes(int.Parse(((Label)e.Row.FindControl("lblProductID")).Text)).Tables[0].Rows)
                {
                    str = str + row["NoteID"].ToString() + "|";
                }
                if (str != "")
                {
                    str = str.Remove(str.Length - 1);
                }
                ((Label)e.Row.FindControl("lblNoteIDs")).Text = str;
            }
        }

        protected void InitializeForm()
        {
            data data = new data();
            DataSet categories = data.GetCategories(-1);
            if (categories.Tables.Count != 0)
            {
                DataView defaultView = categories.Tables[0].DefaultView;
                defaultView.Sort = "Name ASC";
                this.ddCategories.DataSource = defaultView;
                this.ddCategories.DataBind();
            }
            this.ddCategories.Items.Insert(0, new ListItem("None Selected", "-1"));
            this.ddCategories.SelectedIndex = 0;
            categories = data.GetPriceCodes("", false);
            if (categories.Tables.Count != 0)
            {
                DataView defaultView = categories.Tables[0].DefaultView;
                defaultView.Sort = "PriceCode ASC";
                this.ddPriceCodes.DataSource = defaultView;
                this.ddPriceCodes.DataBind();
            }
            this.ddPriceCodes.Items.Insert(0, new ListItem("Select one...", "-1"));
            this.ddPriceCodes.SelectedIndex = 0;
            categories = data.GetFrames(-1, this.Session["BrandAccess"].ToString());
            if (categories.Tables.Count != 0)
            {
                categories.Tables[0].Columns.Add(new DataColumn("PaddedName", Type.GetType("System.String")));
                string s = "";
                foreach (DataRow row in categories.Tables[0].Rows)
                {
                    s = row["Name"].ToString();
                    try
                    {
                        int.Parse(s);
                        s = s.PadLeft(50, '0');
                    }
                    catch
                    {
                    }
                    row["PaddedName"] = s;
                }
                categories.AcceptChanges();
                DataView defaultView = categories.Tables[0].DefaultView;
                defaultView.Sort = "PaddedName";
                this.ddFrames.DataSource = defaultView;
                this.ddFrames.DataBind();
            }
            this.ddFrames.Items.Insert(0, new ListItem("None Selected", "-1"));
            this.ddFrames.SelectedIndex = 0;
            categories = data.GetBrands(-1);
            if (categories.Tables.Count != 0)
            {
                DataView defaultView = categories.Tables[0].DefaultView;
                defaultView.Sort = "Name ASC";
                this.ddBrands.DataSource = defaultView;
                this.ddBrands.DataBind();
            }
            this.ddBrands.Items.Insert(0, new ListItem("None Selected", "-1"));
            this.ddBrands.SelectedIndex = 0;
            this.hdProductID.Value = "";
            this.tbHFPartNumber.Text = "";
            this.tbDescription.Text = "";
            this.tbSize.Text = "";
            this.tbListPrice.Text = "";
            this.tbWeight.Text = "";
            this.tbCostPrice.Text = "";
            this.tbPowerHP.Text = "";
            this.tbFlowCFM.Text = "";
            this.tbStages.Text = "";
            this.tbNotes.Text = "";
        }

        protected void lbExport_Click(object sender, EventArgs e)
        {
            DataSet productDataSheet = new data().GetProductDataSheet();
            this.gvData.DataSource = productDataSheet;
            this.gvData.DataBind();
            base.Response.Clear();
            base.Response.ContentType = "application/ms-excel";
            base.Response.AddHeader("content-disposition", "attachment;filename=Products.xls");
            StringWriter writer = new StringWriter();
            HtmlTextWriter writer2 = new HtmlTextWriter(writer);
            this.pnlData.RenderControl(writer2);
            base.Response.Write(writer.ToString());
            base.Response.End();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            util.CheckSession();
            this.phEditForm.Visible = false;
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
        }

        // Properties
        protected DefaultProfile Profile =>
            ((DefaultProfile)this.Context.Profile);

        protected global_asax ApplicationInstance =>
            ((global_asax)this.Context.ApplicationInstance);
    }
}