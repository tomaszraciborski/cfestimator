﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace cfEstimator
{  
    public class Master : MasterPage
    {
        // Fields
        protected ContentPlaceHolder head;
        protected HtmlGenericControl profileLink;
        protected HtmlGenericControl adminLink;
        protected LinkButton lbLogout;
        protected HtmlGenericControl hdrMenuBar;
        protected PlaceHolder phFieldService;
        protected HtmlGenericControl topLeftMenu;
        protected TextBox tbPartNumber;
        protected Button btPartNumber;
        protected HtmlGenericControl topRightMenu;
        protected ContentPlaceHolder ContentPlaceHolder1;
        protected HiddenField hdTimeout;
        protected HtmlForm form1;
        protected HtmlGenericControl MasterPageBodyTag;

        // Methods
        protected void btPartNumber_Click(object sender, EventArgs e)
        {
            if (this.tbPartNumber.Text.Trim() != "")
            {
                base.Response.Redirect("estimate.aspx?pn=" + this.tbPartNumber.Text.Trim().ToLower(), true);
            }
        }

        protected void lbLogout_Click(object sender, EventArgs e)
        {
            base.Session.RemoveAll();
            if (bool.Parse(ConfigurationManager.AppSettings["CheckReferrer"]))
            {
                base.Session["AccessAllowed"] = true;
            }
            FormsAuthentication.SignOut();
            base.Response.Redirect("login.aspx");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (bool.Parse(ConfigurationManager.AppSettings["CheckReferrer"]) && ((base.Session["AccessAllowed"] == null) || !((bool)base.Session["AccessAllowed"])))
            {
                base.Response.Redirect("unauth.htm");
            }
            this.hdTimeout.Value = HttpContext.Current.Session.Timeout.ToString();
            string str = base.Request.Path.ToLower();
            if (str.Contains("login.aspx") || str.Contains("register.aspx"))
            {
                this.topLeftMenu.Visible = false;
                this.topRightMenu.Visible = false;
                this.hdrMenuBar.Visible = false;
            }
            else
            {
                this.MasterPageBodyTag.Attributes.Add("onload", "document.getElementById('" + this.tbPartNumber.ClientID + "').focus();");
                this.topLeftMenu.Visible = true;
                this.topRightMenu.Visible = true;
                this.hdrMenuBar.Visible = true;
                string script = (string.Concat(new object[] { "", "var intervalInMs = ", ((HttpContext.Current.Session.Timeout - 2) * 60) * 0x3e8, ";\n" }) + "setInterval(\"checkstate()\", intervalInMs);\n") + "function checkstate(){" + "sndReq(\"fn=k\");}\n";
                this.Page.ClientScript.RegisterClientScriptBlock(base.GetType(), "mymasterscript", script, true);
            }
            if ((base.Session["IsAdmin"] != null) && ((bool)base.Session["IsAdmin"]))
            {
                this.adminLink.Visible = true;
                this.profileLink.Visible = false;
            }
            else if (str.Contains("/adm"))
            {
                base.Response.Redirect("error.aspx?cd=unauth");
            }
            if (((base.Session["IsAdmin"] != null) && ((bool)base.Session["IsAdmin"])) || ((base.Session["ISGDEmployee"] != null) && ((bool)base.Session["ISGDEmployee"])))
            {
                this.phFieldService.Visible = true;
            }
            else
            {
                this.phFieldService.Visible = false;
            }
        }

        // Properties
        protected DefaultProfile Profile =>
            ((DefaultProfile)this.Context.Profile);

        protected global_asax ApplicationInstance =>
            ((global_asax)this.Context.ApplicationInstance);
    }
}