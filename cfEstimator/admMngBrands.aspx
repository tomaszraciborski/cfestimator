﻿﻿<%@ page language="C#" masterpagefile="~/cfestimator.master" autoeventwireup="true" CodeBehind="admMngBrands.aspx.cs" Inherits="cfEstimator.admMngBrands" title="Manage Brands" %>

<%@ Register src="admMenu.ascx" tagname="admMenu" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<%--<div id="headerNav">
<span>Manage:</span><a href="">Users</a><a href="" class="selected">Brands</a><a href="">Categories</a>
</div>
<h2>Manage: 
<a href="admMngUsers.aspx">Users</a> |
<a href="admMngBrands.aspx" class="selected">Brands</a> |
<a href="admMngCategories.aspx">Categories</a></h2>--%>
<uc1:admMenu ID="admMenu1" runat="server" TagID="hrefBrands" />
<div style="text-align: center">    
    <asp:Label runat="server" ID="lblMsg" CssClass="infoMsg" EnableViewState="false"></asp:Label></div>
<asp:PlaceHolder ID="phEditForm" runat="server" Visible="false">
<div class="dataFormBoxed">
<table>
<tr>
<td class="requiredField">BrandID:</td>
<td><asp:Label ID="lblBrandID" runat="server"></asp:Label></td>
</tr>
<tr>
<td class="requiredField">Name:</td>
<td><asp:TextBox ID="tbName" runat="server"></asp:TextBox></td>
</tr>
<tr>
<td colspan="2">
    <asp:Button ID="btSave" runat="server" Text="Save Changes" 
        onclick="btSave_Click" /> &nbsp;
    <asp:Button ID="btCancel" runat="server" Text="Cancel" 
        onclick="btCancel_Click" />
</td>
</tr>
</table>
</div>
</asp:PlaceHolder>
<div class="gridActions">
<strong>Actions:</strong>
<asp:LinkButton ID="lbAdd" runat="server" onclick="lbAdd_Click">Add new brand</asp:LinkButton>
</div>
<asp:GridView ID="gvData" runat="server" CssClass="gridTable" AllowPaging="True" AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="BrandID" OnRowDeleting="DeleteRow" OnSelectedIndexChanging="SelectRow" CellPadding="0" PageSize="20" OnPageIndexChanging="ChangePage" GridLines="None" BorderStyle="None" Width="100%">
    <Columns>
        <asp:CommandField ShowSelectButton="True" SelectImageUrl="~/img/edit.gif" SelectText="View" ButtonType="Image" ItemStyle-Width="16px" />
        <asp:TemplateField ShowHeader="false">
            <ItemStyle HorizontalAlign="Center" Width="16px" />
            <ItemTemplate>
                <asp:ImageButton ID="btnDel" runat="server" 
                    CausesValidation="False" CommandName="Delete"
                    ImageUrl="~/img/delete.gif" ToolTip="Delete" OnClientClick="return confirm('Are you sure you want to delete this brand?')" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="BrandID" HeaderText="BrandID" />
        <asp:BoundField DataField="Name" HeaderText="Brand Name" />
    </Columns>
    <HeaderStyle CssClass="gridHeader" />
    <RowStyle CssClass="gridRow" VerticalAlign="Top" />
    <AlternatingRowStyle CssClass="gridAltRow" VerticalAlign="Top" />
    <PagerStyle CssClass="gridPager" />
</asp:GridView>
</asp:Content>


