﻿﻿<%@ page language="C#" masterpagefile="~/cfestimator.master" autoeventwireup="true" CodeBehind="Package.aspx.cs" Inherits="cfEstimator.Package" title="Package Configuration" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script language="javascript" type="text/javascript">
var alt = false;
function ShowNote(id) {
  alt = true;
  sndReq("fn=n&id=" + id);
}

function OpenModelPopup(mytb)
{
	document.getElementById ('dd').style.display='none';
    document.getElementById ('ModalPopupDiv').style.visibility='visible';
    document.getElementById ('ModalPopupDiv').style.display='';
    document.getElementById ('ModalPopupDiv').style.top= Math.round ((document.documentElement.clientHeight/2)+ document.documentElement.scrollTop)-100 + 'px';
    document.getElementById ('ModalPopupDiv').style.left=Math.round ((document.documentElement.clientWidth/2)+ document.documentElement.scrollLeft)-150 + 'px';

    document.getElementById ('MaskedDiv').style.display='';
    document.getElementById ('MaskedDiv').style.visibility='visible';
    document.getElementById ('MaskedDiv').style.top='-70px';
    document.getElementById ('MaskedDiv').style.left='-10px';
    document.getElementById ('MaskedDiv').style.width=  document.documentElement.clientWidth + 'px';
    document.getElementById ('MaskedDiv').style.height= document.documentElement.clientHeight+ 'px';
    //document.getElementById (mytb).focus();
}
function CloseModalPopup()
{
	document.getElementById ('dd').style.display='';
    document.getElementById ('MaskedDiv').style.display='none';
    document.getElementById ('ModalPopupDiv').style.display='none';
}

//AJAX calls
//Include this method if you want to do something with the response from your AJAX call
function processResponse(resp)
{
  if (resp != "")
  {
    if (alt)
    {
      document.getElementById("ModalMsg").innerHTML = resp;
      OpenModelPopup();
    }
    else
    {
      setTotals(resp);
    }
  }
}

/*
function HideRow(gvID, rowID)
{
  var htable = document.getElementById(gvID);
  var currRow = htable.rows[rowID+1];
  //alert(currRow.nodeName);
  currRow.style.display = "none";
  if ((rowID+2) != htable.rows.length)
  {
    for (var i=(rowID+2);i<=(htable.rows.length-1);i++)
    {
      if (htable.rows[i].style.display != "none")
      {
        if (htable.rows[i].className == "gridRow")
          htable.rows[i].className = "gridAltRow";
        else
          htable.rows[i].className = "gridRow";
      }
    }
    //alert(currRow.className);
  }
  alert(htable.rows.length);
}
*/

function DeleteRow(gvID, r)
{
  //alert(r.parentNode.parentNode.nodeName);
  var htable = document.getElementById(gvID);
  var rIndex=r.parentNode.parentNode.rowIndex; //calculates the index of the row to delete
  htable.deleteRow(rIndex); //deletes the row
  if (rIndex < htable.rows.length)
  {
    for (var i=rIndex;i<=(htable.rows.length-1);i++)
    {
      if (htable.rows[i].className == "gridRow")
        htable.rows[i].className = "gridAltRow";
      else
        htable.rows[i].className = "gridRow";
    }
    //alert(currRow.className);
  }
  alt = false;
  sndReq("s=miniestimate&fn=d&r=" + (rIndex-1));
}

/*
var prevQty = -1;
function qtyChanged(r)
{
  if (isNaN(parseInt(r.value, 10)))
  {
    r.value = prevQty;
    alert("Invalid quantity entered!");
    return false;
  }
  var qty = parseInt(r.value, 10);
  if (qty != r.value)
    r.value = qty;
  var currRow = r.parentNode.parentNode;
  var rIndex=currRow.rowIndex; //calculates the index of the row affected
  var totalCellIndex = totalCol; //currRow.cells.length - 1;
  //var totalPrice = currRow.cells[totalCellIndex].innerHTML.substring(1).replace(",","");
  var refSellPrice = currRow.cells[refSellCol].innerHTML.substring(1).replace(",","");
  currRow.cells[wtCol].innerHTML = getRowWeight(currRow.cells[partNoCol].innerHTML,qty);
  currRow.cells[totalCellIndex].innerHTML = formatCurrency(parseFloat(refSellPrice * qty));
  //enabling the below alert will prevent links from firing, so would have to click them twice
  //alert(myinput.value);
  sndReq("s=miniestimate&fn=q&r=" + (rIndex-1) + "&n=" + r.value);
  //return true;
}

function getRowWeight(pnum, qty)
{
  var prodlist = new String(document.getElementById(hdProdList).value);
  if (prodlist.length > 1)
  {
    prodlist = prodlist.substring(1, prodlist.length - 1);
    var prods = prodlist.split('|');
    for (var i in prods)
    {
      if (prods[i].indexOf(pnum + "`") == 0)
      {
        proddetails = prods[i].split('`');
        if (proddetails[1] != 0)
        {
          return (formatNumber(proddetails[1] * qty));
        }
      }
    }
  }
  return "";
}

function setPrevQty(r)
{
  prevQty = r.value;
}
*/

function formatCurrency(num) {
  num = num.toString().replace(/\$|\,/g,'');
  if(isNaN(num))
    num = "0";
  sign = (num == (num = Math.abs(num)));
  num = Math.floor(num*100+0.50000000001);
  cents = num%100;
  num = Math.floor(num/100).toString();
  if(cents<10)
    cents = "0" + cents;
  for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
    num = num.substring(0,num.length-(4*i+3))+','+
  num.substring(num.length-(4*i+3));
  return (((sign)?'':'-') + '$' + num + '.' + cents);
}

function formatNumber(num) {
  num = parseFloat(num.toString().replace(",",""));

  if (num == 0)
    return "";
  var fNum = num.toFixed(2);
  var decPos=fNum.indexOf(".");
  if (decPos>-1)
  {
    first=fNum.substring(0,decPos);
    second=fNum.substring(decPos,fNum.length);
    while (second.charAt(second.length-1)=="0")
      second=second.substring(0,second.length-1);
    if (second.length>1)
      return first+second;
    else
      return first;
  }

  return fNum;
}

var prevQty = -1;
function setPrevQty(r)
{
  prevQty = r.value;
}

function qtyChanged(r)
{
  if (r.value != prevQty)
  {
    if (isNaN(parseInt(r.value, 10)) || (r.value < 1))
    {
      r.value = prevQty;
      alert("Invalid quantity entered!");
      return false;
    }
    
    var qty = parseInt(r.value, 10);
    if (qty != r.value)
      r.value = qty;
    var rIndex=r.parentNode.parentNode.rowIndex; //calculates the index of the row affected
    alt = false;
    sndReq("s=miniestimate&fn=q&r=" + (rIndex-1) + "&n=" + qty);
  }
}

function setTotals(sTotals)
{
  //alert(sTotals);
  var params = parseParams(sTotals, '|', true);

  if ((typeof(params["rownum"])!="undefined") && (params["rownum"] != null))
  {
    var htable = document.getElementById(gvEstimate);
    var currRow = htable.rows[parseInt(params["rownum"]) + 1];
    currRow.cells[wtCol].innerHTML = formatNumber(params["rowwt"]);
    currRow.cells[currRow.cells.length + totalCol].innerHTML = params["rowtotal"];
    //currRow.cells[currRow.cells.length + commAmtCol].innerHTML = params["rowcomm"];
  }
}
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h2>Package Configuration</h2>
<!--
<div class="pageHeader">
<div style="float: left; font-weight: bold; font-size: 14px;">Package Configuration</div>
<div style="float: right; font-size: 12px;"><a href="docs/1260.pdf" target="_blank">Associated Documentation</a></div>
</div>
-->
<div style="text-align: center"><asp:Label runat="server" ID="lblMsg" CssClass="infoMsg" EnableViewState="false"></asp:Label></div>
<div id="dd">
<div class="packageUpdate" id="divPkgFilters" runat="server">
Frame: 
<asp:DropDownList ID="ddFrame" runat="server" AutoPostBack="true"
    onselectedindexchanged="ddFrame_SelectedIndexChanged" DataTextField="Name" DataValueField="FrameConfigID">
<asp:ListItem Selected="True" Text="Select..." Value="-1" />
</asp:DropDownList>
&nbsp; &nbsp;
Stages: 
<asp:DropDownList ID="ddStages" runat="server" AutoPostBack="true"
    onselectedindexchanged="ddStages_SelectedIndexChanged" DataTextField="Stages" DataValueField="Stages" Enabled="false">
<asp:ListItem Selected="True" Text="Select..." Value="-1" />
</asp:DropDownList>
&nbsp; &nbsp;
# of Units: 
<asp:TextBox ID="tbUnits" runat="server" style="width: 20px" Enabled="false">1</asp:TextBox>
&nbsp; &nbsp;
Max HP: 
<asp:DropDownList ID="ddHP" runat="server" Enabled="false" DataTextField="Description" DataValueField="PowerHP">
<asp:ListItem Selected="True" Text="Choose below..." Value="-1" />
<asp:ListItem Text="100 HP (75 KW)" Value="100" />
</asp:DropDownList>
&nbsp; &nbsp;
Max Flow: 
<asp:DropDownList ID="ddFlow" runat="server" Enabled="false" DataTextField="Description" DataValueField="FlowCFM">
<asp:ListItem Selected="True" Text="Choose below..." Value="-1" />
<asp:ListItem Text="2750 CFM (1000 m3HR)" Value="2750" />
</asp:DropDownList>
<asp:Button ID="btGet" runat="server" Text="Get Default" onclick="btGet_Click" Enabled="false" /> 
<asp:Button ID="btFilter" runat="server" Text="Filter Items" onclick="btFilter_Click" Enabled="false" />
<asp:Button ID="btClear" runat="server" Text="Clear All" onclick="btClear_Click" />
</div>
<div style="font-size: 11px; background-color: #cccccc;">
<strong>Current filters applied:</strong>
<asp:Label ID="lblFilters" runat="server" Text="None"></asp:Label>
</div>
</div>
<table width="100%" cellpadding="0" cellspacing="0" style="margin: 3px 0 3px 0;">
<tr valign="top">
<td runat="server" id="tdAccessoryList">
<div class="accessoryList">
<table cellpadding="2" cellspacing="0" border="0" style="margin-right: 5px;">
<tr><th nowrap>Add An Item</th></tr>
<asp:Literal ID="litAccessoryList" runat="server"></asp:Literal>
</table>
</div>
</td>
<td width="100%">
<div id="productList" runat="server" style="border: solid 1px black; padding: 3px; height: 319px; overflow: auto;">
Begin by selecting a Frame above.  You may then build your package by:
<ol>
<li><strong>Starting with a default package</strong><br />
Fill out the options above click the "Go" button.  This will add the default
configuration for the package you've selected to the Mini-Estimate Sheet
below. You can remove items from the sheet if necessary and select from a list 
of categories on the left to add new items.<br /><br />
</li>

<li><strong>Starting from scratch</strong><br />
You can select from a list of categories on the left to add items to your 
Mini-Estimate Sheet.  After adding an item, you can adjust the quantity you 
need by changing the value of the "Qty" field associated with that item in your sheet.
</li>
</ol>
Once your mini-estimate is complete, simply click the “Add Mini-Estimate to Estimate Sheet” link to add all items to your master estimate sheet.
</div>
</td>
<td id="tdDiagram" runat="server"><img alt="diagram" width="341" height="327" src="img/blowerDiagram.gif" runat="server" id="diagram" style="margin-left: 5px;"/></td>
</tr>
</table>

<div class="dataForm">
<div class="gridActions">
<strong>Actions:</strong>
<asp:LinkButton ID="lbAddMini" runat="server" onclick="lbAddMini_Click">Add Mini-Estimate to Estimate Sheet</asp:LinkButton>
</div>
<asp:GridView ID="gvEstimate" runat="server" CssClass="gridTable" AllowPaging="False" AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="RowID" CellPadding="0" PageSize="5" GridLines="None" BorderStyle="None" Width="100%" name="gvEstimate">
        <Columns>
            <asp:TemplateField>
                <ItemStyle Width="16px" />
                <ItemTemplate>
                    <a href="#" onclick="DeleteRow('<%=gvEstimate.ClientID %>',this)"><img alt="Delete" src="img/delete.gif" border="0" width="16px" height="16px" /></a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Weight" HeaderText="Wt"/>
            <asp:BoundField DataField="PartNo" HeaderText="Part #"/>
            <asp:TemplateField HeaderText="Qty">
            <ItemTemplate>
                <asp:TextBox ID="tbQuantity" runat="server" CssClass="thinTexbox" style="width: 20px;" onfocus="setPrevQty(this)" onchange="qtyChanged(this)" Text='<%# DataBinder.Eval(Container, "DataItem.Quantity")%>' onkeypress="return submitenter(this,event,null,true)"></asp:TextBox>
            </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Description" HeaderText="Description"/>
            <asp:BoundField DataField="Cost" HeaderText="Cost" Visible="false" DataFormatString="{0:c}"/>
            <asp:BoundField DataField="RefSell" HeaderText="Ref Sell" DataFormatString="{0:c}"/>
            <asp:BoundField DataField="Discount" HeaderText="Disc" Visible="false"/>
            <asp:BoundField DataField="PriceCode" HeaderText="PC" Visible="false"/>
            <asp:BoundField DataField="Total" HeaderText="Discounted Total" Visible="true" DataFormatString="{0:c}"/>
            <asp:BoundField DataField="Commission" HeaderText="Comm (%)" Visible="false"/>
            <asp:BoundField DataField="CommissionAmount" HeaderText="Comm ($)" Visible="false"/>
        </Columns>
        <HeaderStyle CssClass="gridHeader" />
        <RowStyle CssClass="gridRow" VerticalAlign="Top" />
        <AlternatingRowStyle CssClass="gridAltRow" VerticalAlign="Top" />
        <PagerStyle CssClass="gridPager" />
</asp:GridView>
</div>
<asp:HiddenField ID="hdProdList" runat="server" value="|"/>

<div id="MaskedDiv" class="MaskedDiv">
</div>
<div id="ModalPopupDiv" class="ModalPopup">
  <div id="ModalMsg">My Message</div>
  <p style="text-align: center">
    <a href="javascript:CloseModalPopup();">Close</a>
  </p>
</div>
</asp:Content>

