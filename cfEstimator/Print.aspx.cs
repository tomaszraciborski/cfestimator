﻿using ExpertPdf.HtmlToPdf;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using static cfEstimator.App_Code;

namespace cfEstimator
{
    public partial class Print : Page, IRequiresSessionState
    {
        // Fields
        protected CheckBox cbShowCost;
        protected CheckBox cbShowPriceCode;
        protected CheckBox cbShowSummary;
        protected CheckBox cbCustomerView;
        protected CheckBox cbTotalsOnly;
        protected Button btSavePDF;
        protected Label lblQuoteNum;
        protected Label lblRep;
        protected HtmlGenericControl divApproval;
        protected Label lblQuoteFor;
        protected Label lblApproval;
        protected Label lblAddress;
        protected Label lblEstimateDate;
        protected Label lblApplication;
        protected Label lblInstallation;
        protected Label lblDiscount;
        protected HtmlGenericControl divDiscount;
        protected Label lblNonDiscSell;
        protected Label lblTotalSell;
        protected Label lblTotalWeight;
        protected Label lblCommPercent;
        protected Label lblTotalComm;
        protected Label lblTotalCost;
        protected Label lblBeforeMarginPercent;
        protected Label lblBeforeMargin;
        protected Label lblAfterMarginPercent;
        protected Label lblAfterMargin;
        protected HtmlTable tblTotals;
        protected GridView gvEstimate;
        protected Label lblEstimateNote;
        protected HtmlGenericControl divNotes;
        protected Panel pnlPDFOutput;
        protected HtmlForm form1;

        // Methods
        protected void BindGrid()
        {
            if (this.Session["estimate"] == null)
            {
                this.Session["estimate"] = util.GetEmptyTable();
            }
            DataTable table = ((DataTable)this.Session["estimate"]).Copy();
            double num = 0.0;
            double num2 = 0.0;
            int num3 = 0;
            double num4 = 0.0;
            double num5 = 0.0;
            double num6 = 0.0;
            double num7 = 0.0;
            double num8 = 0.0;
            double num9 = 0.0;
            double num10 = 0.0;
            int num11 = 0;
            double num12 = 0.0;
            double num13 = 0.0;
            double num14 = 0.0;
            foreach (DataRow row in table.Rows)
            {
                if (row["PartNo"].ToString() == "_ST")
                {
                    row["Total"] = num10;
                    row["Quantity"] = num11;
                    row["Weight"] = num12;
                    row["CommissionAmount"] = num13;
                    if (num10 > 0.0)
                    {
                        num14 = num13 / num10;
                    }
                    row["Commission"] = num14;
                    num10 = 0.0;
                    num11 = 0;
                    num12 = 0.0;
                    num13 = 0.0;
                    continue;
                }
                num += (double)row["Total"];
                num2 += (double)row["TotalNonDisc"];
                num3 += (int)row["Quantity"];
                if (!row.IsNull("SingleWeight"))
                {
                    num4 += (double)row["Weight"];
                    double num1 = (double)row["SingleWeight"];
                }
                num5 += (double)row["CommissionAmount"];
                num6 += ((double)row["Cost"]) * ((int)row["Quantity"]);
                num10 += (double)row["Total"];
                num11 += (int)row["Quantity"];
                if (!row.IsNull("Weight"))
                {
                    num12 += (double)row["Weight"];
                }
                num13 += (double)row["CommissionAmount"];
            }
            num7 = num - num6;
            num8 = num7 - num5;
            double num15 = 0.0;
            double num16 = 0.0;
            double num17 = 0.0;
            if (num != 0.0)
            {
                num15 = (num5 / num) * 100.0;
                num16 = (num7 / num) * 100.0;
                num17 = (num8 / num) * 100.0;
            }
            int count = table.Rows.Count;
            DataRow row2 = table.NewRow();
            row2["RowID"] = count;
            row2["PartNo"] = "_TTL";
            row2["Description"] = "Total";
            row2["Weight"] = num4;
            row2["Quantity"] = num3;
            row2["Total"] = num;
            row2["CommissionAmount"] = num5;
            if (num > 0.0)
            {
                num9 = num5 / num;
            }
            row2["Commission"] = num9;
            table.Rows.InsertAt(row2, count);
            this.lblTotalSell.Text = $"{num:C}";
            this.lblNonDiscSell.Text = $"{num2:C}";
            this.lblTotalWeight.Text = $"{num4:N}" + " lbs";
            this.lblTotalComm.Text = $"{num5:C}";
            this.lblCommPercent.Text = $"{num15:N}" + " %";
            this.lblTotalCost.Text = $"{num6:C}";
            this.lblBeforeMargin.Text = $"{num7:C}";
            this.lblAfterMargin.Text = $"{num8:C}";
            this.lblBeforeMarginPercent.Text = $"{num16:N}" + " %";
            this.lblAfterMarginPercent.Text = $"{num17:N}" + " %";
            string approverByRange = "";
            double discount = 0.0;
            if (table.Rows.Count > 0)
            {
                data data = new data();
                DataSet discounts = data.GetDiscounts(this.Session["DiscountCode"].ToString(), -1);
                if (discounts.Tables[0].Rows.Count > 0)
                {
                    discount = (double)discounts.Tables[0].Rows[0]["DefaultDiscount"];
                    approverByRange = data.GetApproverByRange(num2, discount);
                }
            }
            this.lblApproval.Text = approverByRange;
            this.lblDiscount.Text = $"{(discount * 100.0):N}" + " %";
            this.gvEstimate.DataSource = table;
            this.gvEstimate.DataBind();
            if (this.cbTotalsOnly.Checked)
            {
                int num20 = -1;
                int num21 = 0;
                while (true)
                {
                    if (num21 < this.gvEstimate.Columns.Count)
                    {
                        if (this.gvEstimate.Columns[num21].HeaderText != "Total")
                        {
                            num21++;
                            continue;
                        }
                        num20 = num21;
                    }
                    if (num20 != -1)
                    {
                        for (int i = 0; i < (this.gvEstimate.Rows.Count - 1); i++)
                        {
                            this.gvEstimate.Rows[i].Cells[num20].Text = "";
                        }
                    }
                    break;
                }
            }
        }

        protected void btSavePDF_Click(object sender, EventArgs e)
        {
            StringWriter writer = new StringWriter();
            HtmlTextWriter writer2 = new HtmlTextWriter(writer);
            this.pnlPDFOutput.RenderControl(writer2);
            string htmlString = writer.ToString();
            string urlBase = base.Request.Url.ToString().Replace("print.aspx", "");
            PdfConverter converter = new PdfConverter
            {
                LicenseKey = ConfigurationManager.AppSettings["PDFLicenseKey"].ToString(),
                PdfDocumentOptions = {
                PdfPageSize = PdfPageSize.A4,
                PdfCompressionLevel = PdfCompressionLevel.Normal,
                PdfPageOrientation = PDFPageOrientation.Portrait,
                BottomMargin = 10,
                TopMargin = 10,
                LeftMargin = 10,
                RightMargin = 10,
                ShowHeader = false,
                ShowFooter = false,
                GenerateSelectablePdf = true,
                FitWidth = true,
                EmbedFonts = false,
                LiveUrlsEnabled = true,
                JpegCompressionEnabled = true
            },
                PdfDocumentInfo = { AuthorName = "CF Estimator" }
            };
            byte[] buffer = null;
            buffer = (urlBase.Length <= 0) ? converter.GetPdfBytesFromHtmlString(htmlString) : converter.GetPdfBytesFromHtmlString(htmlString, urlBase);
            HttpResponse response = HttpContext.Current.Response;
            response.Clear();
            response.AddHeader("Content-Type", "binary/octet-stream");
            response.AddHeader("Content-Disposition", "attachment; filename=Estimate.pdf; size=" + buffer.Length.ToString());
            response.Flush();
            response.BinaryWrite(buffer);
            response.Flush();
            response.End();
        }

        protected void cbCustomerView_CheckedChanged(object sender, EventArgs e)
        {
            if (this.cbShowCost.Checked)
            {
                this.gvEstimate.Columns[4].Visible = !this.cbCustomerView.Checked;
            }
            if (this.cbShowPriceCode.Checked)
            {
                this.gvEstimate.Columns[5].Visible = !this.cbCustomerView.Checked;
            }
            if (this.cbShowSummary.Checked)
            {
                this.tblTotals.Visible = !this.cbCustomerView.Checked;
            }
            this.gvEstimate.Columns[6].Visible = !this.cbCustomerView.Checked;
            this.gvEstimate.Columns[8].Visible = !this.cbCustomerView.Checked;
            this.gvEstimate.Columns[10].Visible = !this.cbCustomerView.Checked;
            this.gvEstimate.Columns[11].Visible = !this.cbCustomerView.Checked;
            this.divApproval.Visible = !this.cbCustomerView.Checked;
            this.lblApproval.Visible = !this.cbCustomerView.Checked;
            this.divDiscount.Visible = !this.cbCustomerView.Checked;
            if (this.cbCustomerView.Checked)
            {
                this.cbTotalsOnly.Visible = true;
            }
            else
            {
                this.cbTotalsOnly.Visible = false;
                this.cbTotalsOnly.Checked = false;
            }
            this.BindGrid();
        }

        protected void cbShowCost_CheckedChanged(object sender, EventArgs e)
        {
            this.gvEstimate.Columns[4].Visible = this.cbShowCost.Checked;
            this.BindGrid();
        }

        protected void cbShowPriceCode_CheckedChanged(object sender, EventArgs e)
        {
            this.gvEstimate.Columns[5].Visible = this.cbShowPriceCode.Checked;
            this.BindGrid();
        }

        protected void cbShowSummary_CheckedChanged(object sender, EventArgs e)
        {
            this.tblTotals.Visible = this.cbShowSummary.Checked;
            this.BindGrid();
        }

        protected void cbTotalsOnly_CheckedChanged(object sender, EventArgs e)
        {
            this.BindGrid();
        }

        protected void gvEstimate_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.FindControl("lblPartNo") != null)
            {
                if (((Label)e.Row.FindControl("lblPartNo")).Text == "_FS")
                {
                    ((Label)e.Row.FindControl("lblPartNo")).Visible = false;
                }
                if ((((Label)e.Row.FindControl("lblPartNo")).Text == "_ST") || (((Label)e.Row.FindControl("lblPartNo")).Text == "_TTL"))
                {
                    ((HtmlGenericControl)e.Row.FindControl("divDesc")).Attributes["style"] = "text-align: center;";
                    e.Row.Font.Bold = true;
                    if (((Label)e.Row.FindControl("lblPartNo")).Text == "_TTL")
                    {
                        foreach (TableCell cell in e.Row.Cells)
                        {
                            cell.BorderColor = Color.Black;
                            cell.BorderWidth = new Unit(2);
                        }
                    }
                    ((Label)e.Row.FindControl("lblPartNo")).Text = "";
                }
            }
        }

        protected void LoadParams()
        {
            if (this.Session["EstimateParams"] != null)
            {
                Hashtable hashtable = (Hashtable)this.Session["EstimateParams"];
                this.lblQuoteNum.Text = HttpUtility.UrlDecode(hashtable["quotenum"].ToString());
                this.lblRep.Text = HttpUtility.UrlDecode(hashtable["rep"].ToString());
                this.lblQuoteFor.Text = HttpUtility.UrlDecode(hashtable["quotefor"].ToString());
                this.lblAddress.Text = HttpUtility.UrlDecode(hashtable["address"].ToString());
                this.lblApplication.Text = HttpUtility.UrlDecode(hashtable["application"].ToString());
                this.lblInstallation.Text = HttpUtility.UrlDecode(hashtable["installation"].ToString());
                this.lblEstimateDate.Text = HttpUtility.UrlDecode(hashtable["estimatedate"].ToString());
                if (hashtable["estimatenote"].ToString().Trim() != "")
                {
                    this.divNotes.Visible = true;
                    this.lblEstimateNote.Text = HttpUtility.UrlDecode(hashtable["estimatenote"].ToString()).Replace("\n", "<br />\n");
                }
                else
                {
                    this.divNotes.Visible = false;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            util.CheckSession();
            if (((this.Session["IsAdmin"] == null) || !((bool)this.Session["IsAdmin"])) && ((this.Session["ISGDEmployee"] == null) || !((bool)this.Session["ISGDEmployee"])))
            {
                this.Session["ShowCost"] = false;
                this.cbShowCost.Visible = false;
                this.Session["ShowPC"] = false;
                this.cbShowPriceCode.Visible = false;
                this.Session["ShowSummary"] = false;
                this.cbShowSummary.Visible = false;
            }
            if (this.Session["ShowCost"] == null)
            {
                this.Session["ShowCost"] = false;
            }
            if (this.Session["ShowPC"] == null)
            {
                this.Session["ShowPC"] = false;
            }
            if (this.Session["ShowSummary"] == null)
            {
                this.Session["ShowSummary"] = false;
            }
            if (!this.Page.IsPostBack)
            {
                if ((bool)this.Session["ShowCost"])
                {
                    this.cbShowCost.Checked = true;
                    this.gvEstimate.Columns[4].Visible = true;
                }
                if ((bool)this.Session["ShowPC"])
                {
                    this.cbShowPriceCode.Checked = true;
                    this.gvEstimate.Columns[5].Visible = true;
                }
                if ((bool)this.Session["ShowSummary"])
                {
                    this.cbShowSummary.Checked = true;
                    this.tblTotals.Visible = true;
                }
                this.LoadParams();
                if (this.Session["estimate"] == null)
                {
                    this.Session["estimate"] = util.GetEmptyTable();
                }
                this.BindGrid();
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
        }

        // Properties
        protected DefaultProfile Profile =>
            ((DefaultProfile)this.Context.Profile);

        protected global_asax ApplicationInstance =>
            ((global_asax)this.Context.ApplicationInstance);
    }
}