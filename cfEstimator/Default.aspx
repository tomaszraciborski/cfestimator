﻿<%@ page language="C#" masterpagefile="~/cfestimator.master" autoeventwireup="true" CodeBehind="Default.aspx.cs" Inherits="cfEstimator._Default" title="CF EstimatorWeb" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<H3>
Welcome to the new CF Estimator web application!
</H3>
<p>
This tool will allow you to select items from the Gardner Denver Price CF SBU Sheets and generate an estimate sheet.
You can begin building your estimate by entering a part number in the "Add Part Number" box above or
by selecting a price list from the "Quick Launch" menu.  Once in a price list, simply quick the price
of the item you'd like to add to your estimate.
</p>
</asp:Content>


