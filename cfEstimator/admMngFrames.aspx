﻿﻿<%@ page language="C#" masterpagefile="~/cfestimator.master" autoeventwireup="true" CodeBehind="admMngFrames.aspx.cs" Inherits="cfEstimator.admMngFrames" title="Manage Frames" %>
<%@ Register src="admMenu.ascx" tagname="admMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<uc1:admMenu ID="admMenu1" runat="server" TagID="hrefFrames" />
<div style="text-align: center">    
    <asp:Label runat="server" ID="lblMsg" CssClass="infoMsg" EnableViewState="false"></asp:Label></div>
<asp:PlaceHolder ID="phEditForm" runat="server" Visible="false">
<div class="dataFormBoxed">
<table>
<tr>
<td class="requiredField">Name:</td>
<td><asp:TextBox ID="tbName" runat="server" MaxLength="50"></asp:TextBox></td>
</tr>
<tr>
<td class="requiredField">Brand:</td>
<td><asp:DropDownList ID="ddBrands" runat="server" DataTextField="Name" DataValueField="BrandID"></asp:DropDownList></td>
</tr>
<tr>
<td class="requiredField">Filename:</td>
<td><asp:TextBox ID="tbFilename" runat="server" MaxLength="500"></asp:TextBox></td>
</tr>
<tr>
<td colspan="2">
    <asp:Button ID="btSave" runat="server" Text="Save Changes" 
        onclick="btSave_Click" /> &nbsp;
    <asp:Button ID="btCancel" runat="server" Text="Cancel" 
        onclick="btCancel_Click" />
</td>
</tr>
</table>
</div>
</asp:PlaceHolder>
<div class="gridActions">
<strong>Actions:</strong>
<asp:LinkButton ID="lbAdd" runat="server" onclick="lbAdd_Click">Add new frame</asp:LinkButton>
</div>
<asp:GridView ID="gvData" runat="server" CssClass="gridTable" AllowPaging="True" AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="FrameID" OnRowDeleting="DeleteRow" OnSelectedIndexChanging="SelectRow" CellPadding="0" PageSize="50" OnPageIndexChanging="ChangePage" GridLines="None" BorderStyle="None" Width="100%">
    <Columns>
        <asp:CommandField ShowSelectButton="True" SelectImageUrl="~/img/edit.gif" SelectText="View" ButtonType="Image" ItemStyle-Width="16px" />
        <asp:TemplateField ShowHeader="false">
            <ItemStyle HorizontalAlign="Center" Width="16px" />
            <ItemTemplate>
                <asp:ImageButton ID="btnDel" runat="server" 
                    CausesValidation="False" CommandName="Delete"
                    ImageUrl="~/img/delete.gif" ToolTip="Delete" OnClientClick="return confirm('Are you sure you want to delete this frame?')" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="FrameID" HeaderText="FrameID" Visible="false" />
        <asp:BoundField DataField="Name" HeaderText="Name" />
        <asp:BoundField DataField="BrandName" HeaderText="Brand" />
        <asp:BoundField DataField="Filename" HeaderText="Filename" />
    </Columns>
    <HeaderStyle CssClass="gridHeader" />
    <RowStyle CssClass="gridRow" VerticalAlign="Top" />
    <AlternatingRowStyle CssClass="gridAltRow" VerticalAlign="Top" />
    <PagerStyle CssClass="gridPager" />
</asp:GridView>
<input id="hdFrameID" type="hidden" runat="server"/>
</asp:Content>

