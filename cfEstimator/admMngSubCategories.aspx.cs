﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using static cfEstimator.App_Code;

namespace cfEstimator
{
    public class admMngSubCategories : Page, IRequiresSessionState
    {
        // Fields
        protected admMenu admMenu1;
        protected Label lblMsg;
        protected Label lblMasterCategory;
        protected Label lblCategoryID;
        protected TextBox tbName;
        protected TextBox tbDisplayName;
        protected TextBox tbMasterCategoryID;
        protected TextBox tbSortOrder;
        protected Button btSave;
        protected Button btCancel;
        protected PlaceHolder phEditForm;
        protected LinkButton lbAdd;
        protected GridView gvData;

        // Methods
        protected void BindGrid(int pgIndex)
        {
            DataView defaultView = new data().GetCategories(-1).Tables[0].DefaultView;
            defaultView.RowFilter = "MasterCategoryID = " + base.Request.QueryString["mcid"];
            defaultView.Sort = "Name";
            this.gvData.DataSource = defaultView;
            this.gvData.PageIndex = pgIndex;
            this.gvData.DataBind();
        }

        protected void btCancel_Click(object sender, EventArgs e)
        {
            this.tbName.Text = "";
            this.phEditForm.Visible = false;
        }

        protected void btSave_Click(object sender, EventArgs e)
        {
            int num;
            int num2;
            data data = new data();
            if (this.tbName.Text.Trim() == "")
            {
                goto TR_0000;
            }
            else if (this.tbDisplayName.Text.Trim() != "")
            {
                num = -1;
                if (this.tbMasterCategoryID.Text.Trim() == "")
                {
                    if (this.tbDisplayName.Text.Length > this.tbName.MaxLength)
                    {
                        this.lblMsg.CssClass = "errorMsg";
                        this.lblMsg.Text = "The Display Name of a Master Category must be " + this.tbName.MaxLength.ToString() + " characters or less!";
                        return;
                    }
                }
                else
                {
                    try
                    {
                        num = int.Parse(this.tbMasterCategoryID.Text.Trim());
                    }
                    catch
                    {
                        this.lblMsg.CssClass = "errorMsg";
                        this.lblMsg.Text = "Master CategoryID must be an integer!";
                        return;
                    }
                }
                num2 = -1;
                if (this.tbSortOrder.Text.Trim() == "")
                {
                    goto TR_000A;
                }
                else
                {
                    try
                    {
                        num2 = int.Parse(this.tbSortOrder.Text.Trim());
                        goto TR_000A;
                    }
                    catch
                    {
                        this.lblMsg.CssClass = "errorMsg";
                        this.lblMsg.Text = "Sort Order must be an integer!";
                    }
                }
            }
            else
            {
                goto TR_0000;
            }
            return;
            TR_0000:
            this.lblMsg.CssClass = "errorMsg";
            this.lblMsg.Text = "The Name and Display Name fields are required!";
            return;
            TR_000A:
            if (this.lblCategoryID.Text != "-")
            {
                if (!data.UpdateCategory(int.Parse(this.lblCategoryID.Text), this.tbDisplayName.Text, num, num2))
                {
                    this.lblMsg.CssClass = "errorMsg";
                    this.lblMsg.Text = "Unable to update category '" + this.tbName.Text + "'!";
                    return;
                }
                this.lblMsg.Text = "Successfully updated category '" + this.tbName.Text + "'!";
                this.phEditForm.Visible = false;
                this.BindGrid(0);
            }
            else if (data.InsertCategory(this.tbName.Text, this.tbDisplayName.Text, num, num2) <= 0)
            {
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "Unable to add category '" + this.tbName.Text + "'!";
            }
            else
            {
                this.lblMsg.Text = "Successfully added category '" + this.tbName.Text + "'!";
                this.phEditForm.Visible = false;
                this.BindGrid(0);
            }
        }

        protected void ChangePage(object sender, GridViewPageEventArgs e)
        {
            this.BindGrid(e.NewPageIndex);
        }

        protected void DeleteRow(object sender, GridViewDeleteEventArgs e)
        {
            if (new data().DeleteCategory((int)this.gvData.DataKeys[e.RowIndex].Value))
            {
                this.lblMsg.Text = "Successfully deleted category!";
                this.BindGrid(0);
            }
            else
            {
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "Unable to delete category!  It may be associated with other data.";
            }
        }

        protected void lbAdd_Click(object sender, EventArgs e)
        {
            this.lblCategoryID.Text = "-";
            this.tbName.Text = "";
            this.tbDisplayName.Text = "";
            this.tbName.Enabled = true;
            this.tbMasterCategoryID.Text = base.Request.QueryString["mcid"];
            this.tbSortOrder.Text = "";
            this.phEditForm.Visible = true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            util.CheckSession();
            if (!this.Page.IsPostBack)
            {
                DataSet categories = new data().GetCategories(int.Parse(base.Request.QueryString["mcid"]));
                this.lblMasterCategory.Text = categories.Tables[0].Rows[0]["DisplayName"].ToString();
                this.BindGrid(0);
            }
        }

        protected void SelectRow(object sender, GridViewSelectEventArgs e)
        {
            DataSet categories = new data().GetCategories((int)this.gvData.DataKeys[e.NewSelectedIndex].Value);
            if (categories.Tables[0].Rows.Count > 0)
            {
                this.lblCategoryID.Text = categories.Tables[0].Rows[0]["CategoryID"].ToString();
                this.tbName.Text = categories.Tables[0].Rows[0]["Name"].ToString();
                this.tbName.Enabled = false;
                this.tbDisplayName.Text = categories.Tables[0].Rows[0]["DisplayName"].ToString();
                this.tbMasterCategoryID.Text = categories.Tables[0].Rows[0]["MasterCategoryID"].ToString();
                this.tbSortOrder.Text = categories.Tables[0].Rows[0]["SortOrder"].ToString();
                this.phEditForm.Visible = true;
            }
        }

        // Properties
        protected DefaultProfile Profile =>
            ((DefaultProfile)this.Context.Profile);

        protected global_asax ApplicationInstance =>
            ((global_asax)this.Context.ApplicationInstance);
    }
}