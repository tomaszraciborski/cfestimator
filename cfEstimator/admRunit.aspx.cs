﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using static cfEstimator.App_Code;

namespace cfEstimator
{     
    public class admRunit : Page, IRequiresSessionState
    {
        // Fields
        protected DropDownList ddTables;
        protected Button Button1;
        protected GridView GridView1;

        // Methods
        protected void Button1_Click(object sender, EventArgs e)
        {
            DataSet set = this.runsql();
            this.GridView1.DataSource = set;
            this.GridView1.DataBind();
        }

        protected void ChangePage(object sender, GridViewPageEventArgs e)
        {
            DataSet set = this.runsql();
            this.GridView1.DataSource = set;
            this.GridView1.PageIndex = e.NewPageIndex;
            this.GridView1.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            DataSet set;
            if (this.Page.IsPostBack)
            {
                return;
            }
            else
            {
                SqlConnection connection = new SqlConnection();
                set = new DataSet();
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand selectCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "select * from sys.tables where name like 'tbl%' order by name"
                    };
                    connection.Open();
                    set = new DataSet();
                    new SqlDataAdapter(selectCommand).Fill(set);
                }
                catch (Exception)
                {
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
            }
            this.ddTables.DataSource = set;
            this.ddTables.DataBind();
            this.ddTables.Items.Insert(0, new ListItem("Select one...", "0"));
            this.ddTables.SelectedIndex = 0;
        }

        public DataSet runsql()
        {
            SqlConnection connection = new SqlConnection();
            DataSet dataSet = new DataSet();
            try
            {
                connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                SqlCommand selectCommand = new SqlCommand
                {
                    Connection = connection,
                    CommandText = "select * from " + this.ddTables.SelectedValue
                };
                connection.Open();
                dataSet = new DataSet();
                new SqlDataAdapter(selectCommand).Fill(dataSet);
            }
            catch (Exception exception1)
            {
                dataSet.Tables.Add();
                dataSet.Tables[0].Columns.Add("Error", "".GetType());
                object[] values = new object[] { exception1.Message };
                dataSet.Tables[0].Rows.Add(values);
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
            return dataSet;
        }

        // Properties
        protected DefaultProfile Profile =>
            ((DefaultProfile)this.Context.Profile);

        protected global_asax ApplicationInstance =>
            ((global_asax)this.Context.ApplicationInstance);
    }
}