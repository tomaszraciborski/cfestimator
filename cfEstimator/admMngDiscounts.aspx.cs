﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using static cfEstimator.App_Code;

namespace cfEstimator
{
   
    public class admMngDiscounts : Page, IRequiresSessionState
    {
        // Fields
        protected admMenu admMenu1;
        protected Label lblMsg;
        protected TextBox tbDiscountCode;
        protected TextBox tbDisplayName;
        protected TextBox tbDefaultDiscount;
        protected TextBox tbRank;
        protected Button btSave;
        protected Button btCancel;
        protected PlaceHolder phEditForm;
        protected LinkButton lbAdd;
        protected GridView gvData;
        protected HtmlInputHidden hdDiscountID;

        // Methods
        protected void BindGrid()
        {
            DataSet discounts = new data().GetDiscounts("", -1);
            this.gvData.DataSource = discounts;
            this.gvData.DataBind();
        }

        protected void btCancel_Click(object sender, EventArgs e)
        {
            this.tbDiscountCode.Text = "";
            this.phEditForm.Visible = false;
        }

        protected void btSave_Click(object sender, EventArgs e)
        {
            if (this.tbDiscountCode.Text.Trim() == "")
            {
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "The Discount Code field is required!";
            }
            else if (this.tbDisplayName.Text.Trim() == "")
            {
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "The Display Name field is required!";
            }
            else if (this.tbDefaultDiscount.Text.Trim() == "")
            {
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "The Default Discount field is required!";
            }
            else
            {
                try
                {
                    double.Parse(this.tbDefaultDiscount.Text);
                }
                catch
                {
                    this.lblMsg.CssClass = "errorMsg";
                    this.lblMsg.Text = "Invalid number in the Default Discount field!";
                    return;
                }
                if (this.tbRank.Text.Trim() != "")
                {
                    try
                    {
                        int.Parse(this.tbRank.Text);
                        data data = new data();
                        if (this.hdDiscountID.Value != "-")
                        {
                            if (!data.UpdateDiscount(int.Parse(this.hdDiscountID.Value), this.tbDiscountCode.Text, this.tbDisplayName.Text, double.Parse(this.tbDefaultDiscount.Text), int.Parse(this.tbRank.Text)))
                            {
                                this.lblMsg.CssClass = "errorMsg";
                                this.lblMsg.Text = "Unable to update discount '" + this.tbDiscountCode.Text + "'!";
                            }
                            else
                            {
                                this.lblMsg.Text = "Successfully updated discount '" + this.tbDiscountCode.Text + "'!";
                                this.phEditForm.Visible = false;
                                this.BindGrid();
                            }
                        }
                        else if (data.InsertDiscount(this.tbDiscountCode.Text, this.tbDisplayName.Text, double.Parse(this.tbDefaultDiscount.Text), int.Parse(this.tbRank.Text)) <= 0)
                        {
                            this.lblMsg.CssClass = "errorMsg";
                            this.lblMsg.Text = "Unable to add discount '" + this.tbDiscountCode.Text + "'!";
                        }
                        else
                        {
                            this.lblMsg.Text = "Successfully added discount '" + this.tbDiscountCode.Text + "'!";
                            this.phEditForm.Visible = false;
                            this.BindGrid();
                        }
                    }
                    catch
                    {
                        this.lblMsg.CssClass = "errorMsg";
                        this.lblMsg.Text = "Invalid integer in the Rank field!";
                    }
                }
                else
                {
                    this.lblMsg.CssClass = "errorMsg";
                    this.lblMsg.Text = "The Rank field is required!";
                }
            }
        }

        protected void DeleteRow(object sender, GridViewDeleteEventArgs e)
        {
            if (new data().DeleteDiscount((int)this.gvData.DataKeys[e.RowIndex].Value))
            {
                this.lblMsg.Text = "Successfully deleted discount!";
                this.BindGrid();
            }
            else
            {
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "Unable to delete discount!  It may be associated with other data.";
            }
        }

        protected void lbAdd_Click(object sender, EventArgs e)
        {
            this.hdDiscountID.Value = "-";
            this.tbDiscountCode.Text = "";
            this.tbDisplayName.Text = "";
            this.tbDefaultDiscount.Text = "";
            this.tbRank.Text = "";
            this.phEditForm.Visible = true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            util.CheckSession();
            if (!this.Page.IsPostBack)
            {
                this.BindGrid();
            }
        }

        protected void SelectRow(object sender, GridViewSelectEventArgs e)
        {
            data data = new data();
            DataSet discounts = data.GetDiscounts(data.GetDiscountCode((int)this.gvData.DataKeys[e.NewSelectedIndex].Value), -1);
            if (discounts.Tables[0].Rows.Count > 0)
            {
                this.hdDiscountID.Value = discounts.Tables[0].Rows[0]["DiscountID"].ToString();
                this.tbDiscountCode.Text = discounts.Tables[0].Rows[0]["DiscountCode"].ToString();
                this.tbDisplayName.Text = discounts.Tables[0].Rows[0]["DisplayName"].ToString();
                this.tbDefaultDiscount.Text = discounts.Tables[0].Rows[0]["DefaultDiscount"].ToString();
                this.tbRank.Text = discounts.Tables[0].Rows[0]["Rank"].ToString();
                this.phEditForm.Visible = true;
            }
        }

        // Properties
        protected DefaultProfile Profile =>
            ((DefaultProfile)this.Context.Profile);

        protected global_asax ApplicationInstance =>
            ((global_asax)this.Context.ApplicationInstance);
    }
}