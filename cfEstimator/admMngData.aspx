﻿<%@ page language="C#" masterpagefile="~/cfestimator.master" autoeventwireup="true" CodeBehind="admMngData.aspx.cs" Inherits="cfEstimator.admMngData" title="Manage Data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h2>Manage Data</h2>
<div style="text-align: center"><asp:Label runat="server" ID="lblMsg" CssClass="infoMsg" EnableViewState="false"></asp:Label></div>
<p>
    Upload product data (.csv file) <asp:FileUpload ID="fuProducts" runat="server" />  
    <asp:Button ID="btUploadProducts" runat="server" Text="Upload Products" 
        onclick="btUploadProducts_Click" />
</p>
<p>or select the data you'd like to manage below:</p>
<p>
<a href="admMngBrands.aspx">Manage Brands</a><br />
</p>
</asp:Content>
