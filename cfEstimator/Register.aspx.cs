﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using static cfEstimator.App_Code;

namespace cfEstimator
{
    public partial class Register : Page, IRequiresSessionState
    {
        // Fields
        protected Label lblMsg;
        protected ValidationSummary ValidationSummary1;
        protected TextBox tbUsername;
        protected RequiredFieldValidator RequiredFieldValidator1;
        protected RegularExpressionValidator RegularExpressionValidator1;
        protected CustomValidator CustomValidator4;
        protected TextBox tbPassword;
        protected CustomValidator CustomValidator1;
        protected TextBox tbPassword2;
        protected CustomValidator CustomValidator2;
        protected TextBox tbFirstName;
        protected RequiredFieldValidator RequiredFieldValidator2;
        protected TextBox tbLastName;
        protected RequiredFieldValidator RequiredFieldValidator3;
        protected TextBox tbEmailAddress;
        protected RequiredFieldValidator RequiredFieldValidator5;
        protected RegularExpressionValidator RegularExpressionValidator2;
        protected TextBox tbPhone;
        protected RequiredFieldValidator RequiredFieldValidator4;
        protected DropDownList ddCompany;
        protected CustomValidator CustomValidator3;
        protected TextBox tbAddress1;
        protected TextBox tbAddress2;
        protected TextBox tbCity;
        protected TextBox tbState;
        protected TextBox tbZip;
        protected Button btSave;
        protected HtmlGenericControl dataForm;

        // Methods
        protected void btSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                if (new data().InsertUser(this.tbUsername.Text, this.tbPassword.Text, false, false, false, -1, "", this.tbFirstName.Text, this.tbLastName.Text, this.tbEmailAddress.Text, int.Parse(this.ddCompany.SelectedValue), this.tbAddress1.Text, this.tbAddress2.Text, this.tbCity.Text, this.tbState.Text, this.tbZip.Text, this.tbPhone.Text) > 0)
                {
                    this.lblMsg.Text = "Thank you for registering!  Please make a note of your login credentials.<br />";
                    string str = "You will receive an email once your registration is approved.";
                    try
                    {
                        string subject = ConfigurationManager.AppSettings["RegistrationSubj"];
                        string[] strArray = new string[] { "A new user has registered for access to CF Estimator:\n\nUsername: ", this.tbUsername.Text, "\nFirst Name: ", this.tbFirstName.Text, "\nLast Name: ", this.tbLastName.Text, "\nEmail: ", this.tbEmailAddress.Text, "\nPhone: " };
                        strArray[9] = this.tbPhone.Text;
                        strArray[10] = "\nCompany: ";
                        strArray[11] = this.ddCompany.SelectedItem.Text;
                        strArray[12] = "\n\nPlease login to CF Estimator to approve or deny this registration.";
                        util.SendEmail(ConfigurationManager.AppSettings["AdminEmail"], ConfigurationManager.AppSettings["AdminEmail"], "", "", subject, string.Concat(strArray), false, ConfigurationManager.AppSettings["MailServer"]);
                    }
                    catch (Exception)
                    {
                        string[] strArray2 = new string[] { "<br />We have recorded your registration data, but unfortunately an error occurred when notifying the admistrator of your submission.<br />Please email the administrator at <a href=\"mailto:", ConfigurationManager.AppSettings["AdminEmail"], "\">", ConfigurationManager.AppSettings["AdminEmail"].ToString(), "</a> in order to complete your registration process.  Please include your username in the email.<br /><br />Thank you, and we apologize for the inconvenience." };
                        str = string.Concat(strArray2);
                    }
                    this.lblMsg.Text = this.lblMsg.Text + str;
                    this.dataForm.Disabled = true;
                }
                else
                {
                    this.lblMsg.CssClass = "errorMsg";
                    this.lblMsg.Text = "Registration failed!  Please <a href='mailto:" + ConfigurationManager.AppSettings["AdminEmail"] + "'>email the administrator</a> for further instructions.";
                }
            }
        }

        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {
            bool flag = true;
            if (this.tbPassword.Text.Trim() == "")
            {
                flag = false;
            }
            args.IsValid = flag;
        }

        protected void CustomValidator2_ServerValidate(object source, ServerValidateEventArgs args)
        {
            bool flag = true;
            if ((this.tbPassword.Text.Trim() != "") && (this.tbPassword.Text != this.tbPassword2.Text))
            {
                flag = false;
            }
            args.IsValid = flag;
        }

        protected void CustomValidator3_ServerValidate(object source, ServerValidateEventArgs args)
        {
            bool flag = true;
            if (this.ddCompany.SelectedIndex == 0)
            {
                flag = false;
            }
            args.IsValid = flag;
        }

        protected void CustomValidator4_ServerValidate(object source, ServerValidateEventArgs args)
        {
            bool flag = true;
            if ((this.tbUsername.Text.Trim() != "") && !new data().IsUsernameOK(this.tbUsername.Text))
            {
                flag = false;
            }
            args.IsValid = flag;
        }

        protected void LoadCompanies()
        {
            DataSet companies = new data().GetCompanies(-1);
            this.ddCompany.DataSource = companies;
            this.ddCompany.DataBind();
            this.ddCompany.Items.Insert(0, new ListItem("None Selected", "-1", true));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.IsPostBack)
            {
                this.LoadCompanies();
            }
        }

        // Properties
        protected DefaultProfile Profile =>
            ((DefaultProfile)this.Context.Profile);

        protected global_asax ApplicationInstance =>
            ((global_asax)this.Context.ApplicationInstance);
    }
}