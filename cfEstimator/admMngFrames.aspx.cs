﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using static cfEstimator.App_Code;

namespace cfEstimator
{
    public class admMngFrames : Page, IRequiresSessionState
    {
        // Fields
        protected admMenu admMenu1;
        protected Label lblMsg;
        protected TextBox tbName;
        protected DropDownList ddBrands;
        protected TextBox tbFilename;
        protected Button btSave;
        protected Button btCancel;
        protected PlaceHolder phEditForm;
        protected LinkButton lbAdd;
        protected GridView gvData;
        protected HtmlInputHidden hdFrameID;

        // Methods
        protected void BindGrid(int pgIndex)
        {
            DataSet frames = new data().GetFrames(-1, this.Session["BrandAccess"].ToString());
            frames.Tables[0].Columns.Add(new DataColumn("PaddedName", Type.GetType("System.String")));
            string s = "";
            foreach (DataRow row in frames.Tables[0].Rows)
            {
                s = row["Name"].ToString();
                try
                {
                    int.Parse(s);
                    s = s.PadLeft(50, '0');
                }
                catch
                {
                }
                row["PaddedName"] = s;
            }
            frames.AcceptChanges();
            DataView defaultView = frames.Tables[0].DefaultView;
            defaultView.Sort = "PaddedName";
            this.gvData.DataSource = defaultView;
            this.gvData.PageIndex = pgIndex;
            this.gvData.DataBind();
        }

        protected void btCancel_Click(object sender, EventArgs e)
        {
            this.tbName.Text = "";
            this.phEditForm.Visible = false;
        }

        protected void btSave_Click(object sender, EventArgs e)
        {
            data data = new data();
            if (this.tbName.Text.Trim() == "")
            {
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "The Name field is required!";
            }
            else if (this.hdFrameID.Value != "-")
            {
                if (!data.UpdateFrame(int.Parse(this.hdFrameID.Value), this.tbName.Text, this.tbFilename.Text, int.Parse(this.ddBrands.SelectedValue)))
                {
                    this.lblMsg.CssClass = "errorMsg";
                    this.lblMsg.Text = "Unable to update frame '" + this.tbName.Text + "'!";
                }
                else
                {
                    this.lblMsg.Text = "Successfully updated frame '" + this.tbName.Text + "'!";
                    this.phEditForm.Visible = false;
                    this.BindGrid(0);
                }
            }
            else if (data.InsertFrame(this.tbName.Text, this.tbFilename.Text, int.Parse(this.ddBrands.SelectedValue)) <= 0)
            {
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "Unable to add frame '" + this.tbName.Text + "'!";
            }
            else
            {
                this.lblMsg.Text = "Successfully added frame '" + this.tbName.Text + "'!";
                this.phEditForm.Visible = false;
                this.BindGrid(0);
            }
        }

        protected void ChangePage(object sender, GridViewPageEventArgs e)
        {
            this.BindGrid(e.NewPageIndex);
        }

        protected void DeleteRow(object sender, GridViewDeleteEventArgs e)
        {
            if (new data().DeleteFrame((int)this.gvData.DataKeys[e.RowIndex].Value))
            {
                this.lblMsg.Text = "Successfully deleted frame!";
                this.BindGrid(0);
            }
            else
            {
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "Unable to delete frame!  It may be associated with other data.";
            }
        }

        protected void lbAdd_Click(object sender, EventArgs e)
        {
            this.hdFrameID.Value = "-";
            this.tbName.Text = "";
            this.tbFilename.Text = "";
            this.ddBrands.ClearSelection();
            this.ddBrands.SelectedIndex = 0;
            this.phEditForm.Visible = true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            util.CheckSession();
            if (!this.Page.IsPostBack)
            {
                DataSet brands = new data().GetBrands(-1);
                this.ddBrands.DataSource = brands;
                this.ddBrands.DataBind();
                this.ddBrands.Items.Insert(0, new ListItem("None Selected", "-1"));
                this.ddBrands.SelectedIndex = 0;
                this.BindGrid(0);
            }
        }

        protected void SelectRow(object sender, GridViewSelectEventArgs e)
        {
            DataSet frames = new data().GetFrames((int)this.gvData.DataKeys[e.NewSelectedIndex].Value, this.Session["BrandAccess"].ToString());
            if (frames.Tables[0].Rows.Count > 0)
            {
                this.hdFrameID.Value = frames.Tables[0].Rows[0]["FrameID"].ToString();
                this.tbName.Text = frames.Tables[0].Rows[0]["Name"].ToString();
                this.tbFilename.Text = frames.Tables[0].Rows[0]["Filename"].ToString();
                this.ddBrands.ClearSelection();
                if (frames.Tables[0].Rows[0].IsNull("BrandID"))
                {
                    this.ddBrands.SelectedIndex = 0;
                }
                else
                {
                    this.ddBrands.SelectedValue = frames.Tables[0].Rows[0]["BrandID"].ToString();
                }
                this.phEditForm.Visible = true;
            }
        }

        // Properties
        protected DefaultProfile Profile =>
            ((DefaultProfile)this.Context.Profile);

        protected global_asax ApplicationInstance =>
            ((global_asax)this.Context.ApplicationInstance);
    }
}