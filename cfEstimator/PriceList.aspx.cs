﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using static cfEstimator.App_Code;

namespace cfEstimator
{
    public class PriceList : Page, IRequiresSessionState
    {
        // Methods
        public string GetPriceLink(string PartNumber)
        {
            data data = new data();
            try
            {
                object[] objArray = new object[] { "<a href=\"estimate.aspx?pn=", PartNumber, "\">$", Math.Round(data.GetPrice(PartNumber, this.Session["BrandAccess"].ToString()), 2), "</a>" };
                return string.Concat(objArray);
            }
            catch (Exception)
            {
                return "INVALID PART NUMBER";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            util.CheckSession();
        }

        // Properties
        protected DefaultProfile Profile =>
            ((DefaultProfile)this.Context.Profile);

        protected global_asax ApplicationInstance =>
            ((global_asax)this.Context.ApplicationInstance);
    }
}