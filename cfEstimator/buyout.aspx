﻿﻿<%@ page language="C#" masterpagefile="~/cfestimator.master" autoeventwireup="true"  CodeBehind="buyout.aspx.cs" Inherits="cfEstimator.buyout" title="Add Buy-Out Item" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h2>Add Buy-Out Item</h2>
<div style="text-align: center"><asp:Label runat="server" ID="lblMsg" CssClass="infoMsg" EnableViewState="false"></asp:Label></div>
<div class="dataForm">
<p>Bold fields below are required.</p>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
<table>
<tr>
<td class="requiredField">Description: </td>
<td><asp:TextBox ID="tbDescription" MaxLength="100" style="width: 300px;" 
     runat="server"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
        ControlToValidate="tbDescription" EnableClientScript="False" 
        ErrorMessage="Please enter a Description.">*</asp:RequiredFieldValidator></td>
</tr>
<tr id="trCost" runat="server">
<td class="requiredField">Cost: </td>
<td><asp:TextBox ID="tbCost" runat="server" style="width: 75px;"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
        ControlToValidate="tbCost" EnableClientScript="False" 
        ErrorMessage="Please enter a Cost.">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
            ID="RegularExpressionValidator1" runat="server" EnableClientScript="false" 
            ErrorMessage="Please enter a monetary value in the Cost field."
            ControlToValidate="tbCost" ValidationExpression="(-)*[0-9]+(\.[0-9][0-9])?">*</asp:RegularExpressionValidator></td>
</tr>
<tr id="trSellPrice" runat="server">
<td class="requiredField">Sell Price: </td>
<td><asp:TextBox ID="tbSellPrice" runat="server" style="width: 75px;"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
        ControlToValidate="tbSellPrice" EnableClientScript="False" 
        ErrorMessage="Please enter a Sell Price.">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
            ID="RegularExpressionValidator3" runat="server" EnableClientScript="false" 
            ErrorMessage="Please enter a monetary value in the Sell Price field."
            ControlToValidate="tbSellPrice" ValidationExpression="(-)*[0-9]+(\.[0-9][0-9])?">*</asp:RegularExpressionValidator></td>
</tr>
<tr>
<td>Weight: </td>
<td><asp:TextBox ID="tbWeight" runat="server" style="width: 75px;"></asp:TextBox> (lbs)</td>
</tr>
<tr>
<td class="requiredField">Quantity: </td>
<td><asp:TextBox ID="tbQty" runat="server" style="width: 30px;">1</asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
        ControlToValidate="tbQty" EnableClientScript="False" 
        ErrorMessage="Please enter a Quantity.">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
            ID="RegularExpressionValidator2" runat="server" EnableClientScript="false" 
            ErrorMessage="Please enter an integer in the Quantity field."
            ControlToValidate="tbQty" ValidationExpression="[0-9]+">*</asp:RegularExpressionValidator></td>
</tr>
<tr>
<td class="requiredField">Price Code: </td>
<td>
<asp:DropDownList ID="ddPriceCodes" runat="server" DataTextField="Description" 
        DataValueField="PriceCode" AutoPostBack="True" 
        onselectedindexchanged="ddPriceCodes_SelectedIndexChanged">
    </asp:DropDownList><asp:CustomValidator
        ID="CustomValidator1" runat="server" 
        ErrorMessage="Please select a Price Code." EnableClientScript="false"
        onservervalidate="CustomValidator1_ServerValidate">*</asp:CustomValidator>
</td>
</tr>
<asp:PlaceHolder ID="phCustom" runat="server" Visible="false">
<tr id="trMarkup" runat="server">
<td class="requiredField">Custom Mark-up Multiplier: </td>
<td><asp:TextBox ID="tbMarkup" runat="server">1</asp:TextBox><asp:CustomValidator
        ID="CustomValidator2" runat="server" 
        ErrorMessage="Please enter a number other than zero in the Custom Mark-up Multiplier field." EnableClientScript="false"
        onservervalidate="CustomValidator2_ServerValidate">*</asp:CustomValidator></td>
</tr>
<tr>
<td class="requiredField">Custom Commission (%): </td>
<td><asp:TextBox ID="tbCommission" runat="server">1</asp:TextBox> %<asp:CustomValidator
        ID="CustomValidator3" runat="server" 
        ErrorMessage="Please enter a number in the Custom Commission field." EnableClientScript="false"
        onservervalidate="CustomValidator3_ServerValidate">*</asp:CustomValidator></td>
</tr>
</asp:PlaceHolder>
<tr>
<td>&nbsp;</td>
<td>
<asp:Button ID="btAdd" runat="server" Text="Add to Estimate" 
        onclick="btAdd_Click" />
<asp:Button ID="btCancel" runat="server" Text="Cancel" CausesValidation="false" 
        onclick="btCancel_Click" />
</td>
</tr>
</table>
</div>
</asp:Content>


