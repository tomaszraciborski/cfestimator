﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using static cfEstimator.App_Code;

namespace cfEstimator
{
    public class admMenu : UserControl
    {
        // Fields
        protected HtmlAnchor hrefUsers;
        protected HtmlAnchor hrefProducts;
        protected HtmlAnchor hrefBrands;
        protected HtmlAnchor hrefCategories;
        protected HtmlAnchor hrefCompanies;
        protected HtmlAnchor hrefConfigs;
        protected HtmlAnchor hrefDiscounts;
        protected HtmlAnchor hrefFrames;
        protected HtmlAnchor hrefNotes;
        protected HtmlAnchor hrefPriceCodes;
        protected HtmlAnchor hrefErrors;
        private string _tagid = "hrefUsers";

        // Methods
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.IsPostBack)
            {
                ((HtmlAnchor)this.FindControl(this._tagid)).Attributes["class"] = "selected";
            }
        }

        // Properties
        protected DefaultProfile Profile =>
            ((DefaultProfile)this.Context.Profile);

        protected global_asax ApplicationInstance =>
            ((global_asax)this.Context.ApplicationInstance);

        public string TagID
        {
            get =>
                this._tagid;
            set =>
                this._tagid = value;
        }
    }
}