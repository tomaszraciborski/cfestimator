﻿<%@ page language="C#" masterpagefile="~/cfestimator.master" autoeventwireup="true" CodeBehind="error.aspx.cs" Inherits="cfEstimator.error" title="Error" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div id="errorContent">
<p style="color: red">
<asp:Literal runat="server" ID="litMsg"></asp:Literal>
</p>
<p>
We apologize for the inconvenience.<br />
Click
<asp:LinkButton ID="lbStartOver" runat="server" onclick="lbStartOver_Click">here</asp:LinkButton>
to start a new session.
</p>
</div>
</asp:Content>

