﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using static cfEstimator.App_Code;

namespace cfEstimator
{
    public class estimate : Page, IRequiresSessionState
    {
        // Fields
        protected Label lblMsg;
        protected TextBox tbQuoteNum;
        protected TextBox tbEstimateDate;
        protected Literal litDate;
        protected TextBox tbRep;
        protected ListBox lbDiscount;
        protected TextBox tbQuoteFor;
        protected TextBox tbAddress;
        protected TextBox tbApplication;
        protected TextBox tbInstallation;
        protected Label lblApproval;
        protected Label lblNonDiscSell;
        protected Label lblTotalSell;
        protected Label lblTotalWeight;
        protected Label lblCommPercent;
        protected Label lblTotalComm;
        protected LinkButton lbShowGDTotals;
        protected HtmlTableRow trShowGDTotals;
        protected HtmlTable tblTotals;
        protected LinkButton lbHideGDTotals;
        protected Label lblTotalCost;
        protected Label lblBeforeMarginPercent;
        protected Label lblBeforeMargin;
        protected Label lblAfterMarginPercent;
        protected Label lblAfterMargin;
        protected HtmlTable tblGDTotals;
        protected LinkButton lbClear;
        protected LinkButton lbSave;
        protected LinkButton lbPrint;
        protected LinkButton lbUpdatePrices;
        protected LinkButton lbCost;
        protected LinkButton lbPC;
        protected PlaceHolder phGDOnly;
        protected HtmlGenericControl gridActions;
        protected GridView gvEstimate;
        protected TextBox tbEstimateNote;
        protected HiddenField hdEstimateID;
        protected HiddenField hdHasST;
        protected TextBox tbEstimateName;
        protected Button btSaveEstimate;

        // Methods
        protected void AddMiniEstimateSheet()
        {
            if (this.Session["miniestimate"] != null)
            {
                DataTable table2 = (DataTable)this.Session["estimate"];
                foreach (DataRow row in ((DataTable)this.Session["miniestimate"]).Rows)
                {
                    table2.ImportRow(row);
                }
            }
        }

        protected void AddRow(string PartNum, int Qty)
        {
            DataTable table = (DataTable)this.Session["estimate"];
            DataSet set = new data().GetProductsWithDiscount(PartNum, this.Session["DiscountCode"].ToString(), this.Session["BrandAccess"].ToString());
            if (set.Tables[0].Rows.Count <= 0)
            {
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "Part number '" + Uri.EscapeUriString(PartNum) + "' is invalid.";
            }
            else
            {
                DataRow row = table.NewRow();
                DataRow row2 = set.Tables[0].Rows[0];
                row["RowID"] = table.Rows.Count;
                if (!row2.IsNull("Weight"))
                {
                    row["SingleWeight"] = double.Parse(row2["Weight"].ToString());
                    row["Weight"] = Qty * double.Parse(row2["Weight"].ToString());
                }
                row["PartNo"] = row2["HFPartNumber"];
                row["Quantity"] = Qty;
                row["Description"] = (row2["Size"].ToString().Trim() != "") ? (row2["Description"] + " - " + row2["Size"]) : row2["Description"];
                row["Cost"] = row2["CostPrice"];
                row["RefSell"] = row2["ListPrice"];
                row["UnitSell"] = double.Parse(row2["ListPrice"].ToString()) * double.Parse(row2["Multiplier"].ToString());
                row["Discount"] = row2["Multiplier"];
                row["PriceCode"] = row2["PriceCode"];
                row["Total"] = (Qty * double.Parse(row2["ListPrice"].ToString())) * double.Parse(row2["Multiplier"].ToString());
                row["TotalNonDisc"] = Qty * double.Parse(row2["ListPrice"].ToString());
                row["Commission"] = row2["Commission"];
                row["CommissionAmount"] = double.Parse(row2["Commission"].ToString()) * double.Parse(row["Total"].ToString());
                table.Rows.InsertAt(row, table.Rows.Count);
                this.Session["estimate"] = table;
            }
        }

        protected void AddRow(int InsertRowID, double Weight, string PartNum, int Qty, string Description, double Cost, double RefSell, double UnitSell, double Discount, string PriceCode, double Total, double TotalNonDisc, double Commission, double CommissionAmount)
        {
            DataTable table = (DataTable)this.Session["estimate"];
            DataRow row = table.NewRow();
            row["RowID"] = InsertRowID;
            row["Weight"] = Weight;
            row["PartNo"] = PartNum;
            row["Quantity"] = Qty;
            row["Description"] = Description;
            row["Cost"] = Cost;
            row["RefSell"] = RefSell;
            row["UnitSell"] = UnitSell;
            row["Discount"] = Discount;
            row["PriceCode"] = PriceCode;
            row["Total"] = Total;
            row["TotalNonDisc"] = TotalNonDisc;
            row["Commission"] = Commission;
            row["CommissionAmount"] = CommissionAmount;
            table.Rows.InsertAt(row, InsertRowID);
            this.Session["estimate"] = table;
        }

        protected void AddSubtotalRow(int InsertRowID, double Weight, string PartNum, int Qty, string Description, double Total, double Commission, double CommissionAmount)
        {
            DataTable table = (DataTable)this.Session["estimate"];
            DataRow row = table.NewRow();
            row["RowID"] = InsertRowID;
            row["Weight"] = Weight;
            row["PartNo"] = PartNum;
            row["Quantity"] = Qty;
            row["Description"] = Description;
            row["Total"] = Total;
            row["Commission"] = Commission;
            row["CommissionAmount"] = CommissionAmount;
            table.Rows.InsertAt(row, InsertRowID);
            this.Session["estimate"] = table;
        }

        protected void BindGrid()
        {
            if (this.Session["estimate"] == null)
            {
                this.Session["estimate"] = util.GetEmptyTable();
            }
            DataTable table = (DataTable)this.Session["estimate"];
            double num = 0.0;
            double num2 = 0.0;
            int num3 = 0;
            double num4 = 0.0;
            double num5 = 0.0;
            double num6 = 0.0;
            double num7 = 0.0;
            double num8 = 0.0;
            double num9 = 0.0;
            int num10 = 0;
            double num11 = 0.0;
            double num12 = 0.0;
            double num13 = 0.0;
            bool flag = false;
            foreach (DataRow row in table.Rows)
            {
                if (row["PartNo"].ToString() == "_ST")
                {
                    flag = true;
                    row["Total"] = num9;
                    row["Quantity"] = num10;
                    row["Weight"] = num11;
                    row["CommissionAmount"] = num12;
                    if (num9 > 0.0)
                    {
                        num13 = num12 / num9;
                    }
                    row["Commission"] = num13;
                    num9 = 0.0;
                    num10 = 0;
                    num11 = 0.0;
                    num12 = 0.0;
                    continue;
                }
                num += (double)row["Total"];
                num2 += (double)row["TotalNonDisc"];
                num3 += (int)row["Quantity"];
                if (!row.IsNull("SingleWeight"))
                {
                    num4 += (double)row["Weight"];
                    double num1 = (double)row["SingleWeight"];
                }
                num5 += (double)row["CommissionAmount"];
                num6 += ((double)row["Cost"]) * ((int)row["Quantity"]);
                num9 += (double)row["Total"];
                num10 += (int)row["Quantity"];
                if (!row.IsNull("Weight"))
                {
                    num11 += (double)row["Weight"];
                }
                num12 += (double)row["CommissionAmount"];
            }
            this.hdHasST.Value = !flag ? "false" : "true";
            num7 = num - num6;
            num8 = num7 - num5;
            double num14 = 0.0;
            double num15 = 0.0;
            double num16 = 0.0;
            if (num != 0.0)
            {
                num14 = (num5 / num) * 100.0;
                num15 = (num7 / num) * 100.0;
                num16 = (num8 / num) * 100.0;
            }
            this.lblTotalSell.Text = $"{num:C}";
            this.lblNonDiscSell.Text = $"{num2:C}";
            this.lblTotalWeight.Text = $"{num4:N}" + " lbs";
            this.lblTotalComm.Text = $"{num5:C}";
            this.lblCommPercent.Text = $"{num14:N}" + " %";
            this.lblTotalCost.Text = $"{num6:C}";
            this.lblBeforeMargin.Text = $"{num7:C}";
            this.lblAfterMargin.Text = $"{num8:C}";
            this.lblBeforeMarginPercent.Text = $"{num15:N}" + " %";
            this.lblAfterMarginPercent.Text = $"{num16:N}" + " %";
            string approverByRange = "";
            if (table.Rows.Count <= 0)
            {
                this.gridActions.Visible = false;
            }
            else
            {
                this.gridActions.Visible = true;
                data data = new data();
                DataSet discounts = data.GetDiscounts(this.Session["DiscountCode"].ToString(), -1);
                if (discounts.Tables[0].Rows.Count > 0)
                {
                    approverByRange = data.GetApproverByRange(num2, (double)discounts.Tables[0].Rows[0]["DefaultDiscount"]);
                }
            }
            this.lblApproval.Text = approverByRange;
            this.Session["estimate"] = table;
            this.gvEstimate.DataSource = table;
            this.gvEstimate.DataBind();
        }

        protected void btSaveEstimate_Click(object sender, EventArgs e)
        {
            string str;
            DataTable table = (DataTable)this.Session["estimate"];
            using (StringWriter writer = new StringWriter())
            {
                table.WriteXml(writer);
                str = writer.ToString();
            }
            Hashtable hashtable = (Hashtable)this.Session["EstimateParams"];
            int createdUserID = int.Parse(this.Session["UserID"].ToString());
            int num2 = new data().InsertEstimate(this.tbEstimateName.Text, createdUserID, createdUserID, str, this.Session["DiscountCode"].ToString(), hashtable["quotenum"].ToString(), hashtable["rep"].ToString(), hashtable["quotefor"].ToString(), hashtable["application"].ToString(), hashtable["address"].ToString(), hashtable["installation"].ToString(), hashtable["estimatedate"].ToString(), hashtable["estimatenote"].ToString());
            if (num2 <= 0)
            {
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "An error occurred while saving Estimate Sheet!";
            }
            else
            {
                this.lblMsg.Text = "Successfully saved Estimate Sheet!";
                this.Session["EstimateID"] = num2;
                this.hdEstimateID.Value = num2.ToString();
            }
            this.BindGrid();
        }

        protected void gv_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "ibUp_Click")
            {
                int num = int.Parse(e.CommandArgument.ToString());
                if (num != 0)
                {
                    int pos = num - 1;
                    DataTable table = (DataTable)this.Session["estimate"];
                    DataRow row = table.Rows[num];
                    DataRow row2 = table.NewRow();
                    row2.ItemArray = row.ItemArray;
                    row2["RowID"] = pos;
                    table.Rows.Remove(row);
                    table.Rows.InsertAt(row2, pos);
                    for (int i = 0; i < table.Rows.Count; i++)
                    {
                        table.Rows[i]["RowID"] = i;
                    }
                    this.Session["estimate"] = table;
                    this.lblMsg.Text = e.CommandArgument.ToString();
                    this.BindGrid();
                }
            }
            else if (e.CommandName == "ibDown_Click")
            {
                int num4 = int.Parse(e.CommandArgument.ToString());
                int pos = num4 + 1;
                DataTable table2 = (DataTable)this.Session["estimate"];
                if (num4 != (table2.Rows.Count - 1))
                {
                    DataRow row = table2.Rows[num4];
                    DataRow row4 = table2.NewRow();
                    row4.ItemArray = row.ItemArray;
                    row4["RowID"] = pos;
                    table2.Rows.Remove(row);
                    table2.Rows.InsertAt(row4, pos);
                    int num6 = 0;
                    while (true)
                    {
                        if (num6 >= table2.Rows.Count)
                        {
                            this.Session["estimate"] = table2;
                            this.lblMsg.Text = e.CommandArgument.ToString();
                            this.BindGrid();
                            break;
                        }
                        table2.Rows[num6]["RowID"] = num6;
                        num6++;
                    }
                }
            }
        }

        protected void gvEstimate_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.FindControl("lbPartNo") != null)
            {
                if (((Label)e.Row.FindControl("lbPartNo")).Text == "_FS")
                {
                    ((Label)e.Row.FindControl("lbPartNo")).Visible = false;
                    ((TextBox)e.Row.FindControl("tbQuantity")).Enabled = false;
                }
                if (((Label)e.Row.FindControl("lbPartNo")).Text == "_ST")
                {
                    ((ImageButton)e.Row.FindControl("ibSubtotal")).Visible = false;
                    ((ImageButton)e.Row.FindControl("ibUp")).Visible = false;
                    ((ImageButton)e.Row.FindControl("ibDown")).Visible = false;
                    ((Label)e.Row.FindControl("lbPartNo")).Text = "";
                    ((TextBox)e.Row.FindControl("tbQuantity")).Visible = false;
                    ((Label)e.Row.FindControl("lblQuantity")).Visible = true;
                    ((TextBox)e.Row.FindControl("tbDescription")).Visible = false;
                    ((HtmlGenericControl)e.Row.FindControl("divSubtotalDesc")).Visible = true;
                    e.Row.Font.Bold = true;
                    e.Row.BackColor = Color.Yellow;
                }
            }
        }

        protected void lbClear_Click(object sender, EventArgs e)
        {
            this.Session.Remove("ShowCost");
            this.Session.Remove("ShowPC");
            this.Session.Remove("estimate");
            this.Session.Remove("EstimateParams");
            this.Session.Remove("DiscountCode");
            this.Session.Remove("EstimateID");
            base.Response.Redirect("estimate.aspx");
        }

        protected void lbCost_Click(object sender, EventArgs e)
        {
            if ((this.Session["ShowCost"] == null) || !((bool)this.Session["ShowCost"]))
            {
                this.Session["ShowCost"] = true;
                this.lbCost.Text = "Hide Cost";
                this.gvEstimate.Columns[6].Visible = true;
            }
            else
            {
                this.Session["ShowCost"] = false;
                this.lbCost.Text = "Show Cost";
                this.gvEstimate.Columns[6].Visible = false;
            }
            this.BindGrid();
        }

        protected void lbDiscount_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.Session["DiscountCode"] = this.lbDiscount.SelectedValue;
            if (this.Session["estimate"] != null)
            {
                DataTable table = (DataTable)this.Session["estimate"];
                data data = new data();
                foreach (DataRow row in table.Rows)
                {
                    if (row["PartNo"].ToString() == "_ST")
                    {
                        continue;
                    }
                    if (!row["PriceCode"].ToString().Contains(" M"))
                    {
                        double num;
                        double num2;
                        if (row["PartNo"].ToString() == "_FS")
                        {
                            num = 1.0;
                            num2 = 0.0;
                        }
                        else
                        {
                            DataSet multiplierAndDiscount = data.GetMultiplierAndDiscount(row["PriceCode"].ToString(), this.Session["DiscountCode"].ToString());
                            num = (double)multiplierAndDiscount.Tables[0].Rows[0]["Multiplier"];
                            num2 = (double)multiplierAndDiscount.Tables[0].Rows[0]["Commission"];
                        }
                        row["UnitSell"] = double.Parse(row["RefSell"].ToString()) * num;
                        row["Discount"] = num;
                        row["Total"] = (int.Parse(row["Quantity"].ToString()) * double.Parse(row["RefSell"].ToString())) * num;
                        row["Commission"] = num2;
                        row["CommissionAmount"] = num2 * double.Parse(row["Total"].ToString());
                    }
                }
                this.Session["estimate"] = table;
            }
            this.BindGrid();
        }

        protected void lbHideGDTotals_Click(object sender, EventArgs e)
        {
            this.tblTotals.Visible = true;
            this.tblGDTotals.Visible = false;
            this.BindGrid();
        }

        protected void lbPC_Click(object sender, EventArgs e)
        {
            if ((this.Session["ShowPC"] == null) || !((bool)this.Session["ShowPC"]))
            {
                this.Session["ShowPC"] = true;
                this.lbPC.Text = "Hide Price Code";
                this.gvEstimate.Columns[7].Visible = true;
            }
            else
            {
                this.Session["ShowPC"] = false;
                this.lbPC.Text = "Show Price Code";
                this.gvEstimate.Columns[7].Visible = false;
            }
            this.BindGrid();
        }

        protected void lbPrint_Click(object sender, EventArgs e)
        {
            base.Response.Redirect("print.aspx");
        }

        protected void lbSave_Click(object sender, EventArgs e)
        {
            if (this.Session["EstimateID"] == null)
            {
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "An error occurred while saving Estimate Sheet!  Try choosing the \"Save as...\" option.";
            }
            else
            {
                string str;
                DataTable table = (DataTable)this.Session["estimate"];
                using (StringWriter writer = new StringWriter())
                {
                    table.WriteXml(writer);
                    str = writer.ToString();
                }
                Hashtable hashtable = (Hashtable)this.Session["EstimateParams"];
                int lastModifiedUserID = int.Parse(this.Session["UserID"].ToString());
                if (new data().UpdateEstimate(int.Parse(this.Session["EstimateID"].ToString()), lastModifiedUserID, str, this.Session["DiscountCode"].ToString(), hashtable["quotenum"].ToString(), hashtable["rep"].ToString(), hashtable["quotefor"].ToString(), hashtable["application"].ToString(), hashtable["address"].ToString(), hashtable["installation"].ToString(), hashtable["estimatedate"].ToString(), hashtable["estimatenote"].ToString()))
                {
                    this.lblMsg.Text = "Successfully saved Estimate Sheet!";
                }
                else
                {
                    this.lblMsg.CssClass = "errorMsg";
                    this.lblMsg.Text = "An error occurred while saving Estimate Sheet!";
                }
                this.BindGrid();
            }
        }

        protected void lbShowGDTotals_Click(object sender, EventArgs e)
        {
            this.tblGDTotals.Visible = true;
            this.tblTotals.Visible = false;
            this.BindGrid();
        }

        protected void lbUpdatePrices_Click(object sender, EventArgs e)
        {
            DataTable table = ((DataTable)this.Session["estimate"]).Copy();
            this.Session["estimate"] = util.GetEmptyTable();
            for (int i = 0; i < table.Rows.Count; i++)
            {
                if ((table.Rows[i]["PartNo"].ToString() != "") && (table.Rows[i]["PartNo"].ToString() != "_ST"))
                {
                    this.AddRow(table.Rows[i]["PartNo"].ToString(), (int)table.Rows[i]["Quantity"]);
                }
                else
                {
                    DataTable table2 = (DataTable)this.Session["estimate"];
                    table2.ImportRow(table.Rows[i]);
                    this.Session["estimate"] = table2;
                }
                if (((DataTable)this.Session["estimate"]).Rows.Count != (i + 1))
                {
                    break;
                }
            }
            if (((DataTable)this.Session["estimate"]).Rows.Count != table.Rows.Count)
            {
                this.Session["estimate"] = table;
                this.lblMsg.Text = this.lblMsg.Text + "<br />Unable to update prices!";
            }
            this.BindGrid();
        }

        protected void LoadDiscounts()
        {
            DataSet discounts = new data().GetDiscounts("", int.Parse(this.Session["MaxDiscountRank"].ToString()));
            this.lbDiscount.DataSource = discounts;
            this.lbDiscount.DataBind();
            this.lbDiscount.SelectedIndex = 0;
            if (this.Session["DiscountCode"] == null)
            {
                this.Session["DiscountCode"] = this.lbDiscount.SelectedValue;
            }
            else
            {
                foreach (ListItem item in this.lbDiscount.Items)
                {
                    if (item.Value == this.Session["DiscountCode"].ToString())
                    {
                        this.lbDiscount.ClearSelection();
                        item.Selected = true;
                        break;
                    }
                }
            }
        }

        protected void LoadParams()
        {
            if (this.Session["EstimateParams"] != null)
            {
                Hashtable hashtable = (Hashtable)this.Session["EstimateParams"];
                this.tbQuoteNum.Text = HttpUtility.UrlDecode(hashtable["quotenum"].ToString());
                this.tbRep.Text = HttpUtility.UrlDecode(hashtable["rep"].ToString());
                this.tbQuoteFor.Text = HttpUtility.UrlDecode(hashtable["quotefor"].ToString());
                this.tbAddress.Text = HttpUtility.UrlDecode(hashtable["address"].ToString());
                this.tbApplication.Text = HttpUtility.UrlDecode(hashtable["application"].ToString());
                this.tbInstallation.Text = HttpUtility.UrlDecode(hashtable["installation"].ToString());
                this.tbEstimateDate.Text = HttpUtility.UrlDecode(hashtable["estimatedate"].ToString());
                this.tbEstimateNote.Text = HttpUtility.UrlDecode(hashtable["estimatenote"].ToString());
            }
            else
            {
                this.tbRep.Text = new data().GetUserCompany(int.Parse(this.Session["UserID"].ToString()));
                this.tbEstimateDate.Text = DateTime.Now.ToShortDateString();
                Hashtable hashtable2 = new Hashtable {
                {
                    "quotenum",
                    ""
                },
                {
                    "rep",
                    this.tbRep.Text
                },
                {
                    "quotefor",
                    ""
                },
                {
                    "address",
                    ""
                },
                {
                    "application",
                    ""
                },
                {
                    "installation",
                    ""
                },
                {
                    "estimatedate",
                    this.tbEstimateDate.Text
                },
                {
                    "estimatenote",
                    ""
                }
            };
                this.Session["EstimateParams"] = hashtable2;
            }
        }

        protected void LoadSheet(int EstimateID)
        {
            DataSet estimate = new data().GetEstimate(EstimateID, int.Parse(this.Session["UserID"].ToString()));
            if (estimate.Tables[0].Rows.Count > 0)
            {
                DataRow row = estimate.Tables[0].Rows[0];
                DataTable emptyTable = util.GetEmptyTable();
                emptyTable.ReadXml(new StringReader(row["Products"].ToString()));
                this.Session["estimate"] = emptyTable;
                Hashtable hashtable = new Hashtable {
                {
                    "quotenum",
                    row["QuoteNumber"].ToString()
                },
                {
                    "rep",
                    row["Representative"].ToString()
                },
                {
                    "quotefor",
                    row["QuotationFor"].ToString()
                },
                {
                    "address",
                    row["Address"].ToString()
                },
                {
                    "application",
                    row["Application"].ToString()
                },
                {
                    "installation",
                    row["ForInstallationAt"].ToString()
                },
                {
                    "estimatedate",
                    ((DateTime) row["EstimateDate"]).ToShortDateString()
                },
                {
                    "estimatenote",
                    row["EstimateNote"].ToString()
                }
            };
                this.Session["EstimateParams"] = hashtable;
                this.Session["DiscountCode"] = row["DiscountCode"].ToString();
                this.hdEstimateID.Value = EstimateID.ToString();
                this.Session["EstimateID"] = EstimateID;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            util.CheckSession();
            if (((this.Session["IsAdmin"] != null) && ((bool)this.Session["IsAdmin"])) || ((this.Session["ISGDEmployee"] != null) && ((bool)this.Session["ISGDEmployee"])))
            {
                this.phGDOnly.Visible = true;
            }
            else
            {
                this.Session["ShowCost"] = false;
                this.Session["ShowPC"] = false;
            }
            if (this.Session["ShowCost"] == null)
            {
                this.Session["ShowCost"] = false;
            }
            if (this.Session["ShowPC"] == null)
            {
                this.Session["ShowPC"] = false;
            }
            string script = (((((((((((("" + "var gvEstimate = '" + this.gvEstimate.ClientID + "';\n") + "var lblApproval = '" + this.lblApproval.ClientID + "';\n") + "var lblTotalSell = '" + this.lblTotalSell.ClientID + "';\n") + "var lblNonDiscSell = '" + this.lblNonDiscSell.ClientID + "';\n") + "var lblApproval = '" + this.lblApproval.ClientID + "';\n") + "var lblTotalWeight = '" + this.lblTotalWeight.ClientID + "';\n") + "var lblCommPercent = '" + this.lblCommPercent.ClientID + "';\n") + "var lblTotalComm = '" + this.lblTotalComm.ClientID + "';\n") + "var hdEstimateID = '" + this.hdEstimateID.ClientID + "';\n") + "var hdHasST = '" + this.hdHasST.ClientID + "';\n") + "var wtCol = 2;\n") + "var partNoCol = 3;\n" + "var refSellCol = -5;\n") + "var totalCol = -3;\n" + "var commAmtCol = -1;\n";
            if (this.phGDOnly.Visible)
            {
                script = ((((script + "var lblTotalCost = '" + this.lblTotalCost.ClientID + "';\n") + "var lblBeforeMarginPercent = '" + this.lblBeforeMarginPercent.ClientID + "';\n") + "var lblBeforeMargin = '" + this.lblBeforeMargin.ClientID + "';\n") + "var lblAfterMarginPercent = '" + this.lblAfterMarginPercent.ClientID + "';\n") + "var lblAfterMargin = '" + this.lblAfterMargin.ClientID + "';\n";
                this.trShowGDTotals.Visible = true;
            }
            this.Page.ClientScript.RegisterClientScriptBlock(base.GetType(), "mycustomscript", script, true);
            if (!this.Page.IsPostBack)
            {
                if ((bool)this.Session["ShowCost"])
                {
                    this.lbCost.Text = "Hide Cost";
                    this.gvEstimate.Columns[6].Visible = true;
                }
                if ((bool)this.Session["ShowPC"])
                {
                    this.lbPC.Text = "Hide Price Code";
                    this.gvEstimate.Columns[7].Visible = true;
                }
                if (base.Request.QueryString["eid"] != null)
                {
                    this.LoadSheet(int.Parse(base.Request.QueryString["eid"]));
                }
                if (this.Session["EstimateID"] != null)
                {
                    this.hdEstimateID.Value = this.Session["EstimateID"].ToString();
                }
                this.LoadDiscounts();
                this.LoadParams();
                if (this.Session["estimate"] == null)
                {
                    this.Session["estimate"] = util.GetEmptyTable();
                }
                if (base.Request.QueryString["pn"] != null)
                {
                    this.AddRow(base.Request.QueryString["pn"], 1);
                }
                else if (base.Request.QueryString["addmini"] != null)
                {
                    this.AddMiniEstimateSheet();
                }
                this.BindGrid();
            }
        }

        protected void SetSubTotal(int lastrownum)
        {
            DataTable table = (DataTable)this.Session["estimate"];
            double total = 0.0;
            int qty = 0;
            double weight = 0.0;
            double commissionAmount = 0.0;
            double commission = 0.0;
            for (int i = 0; i <= lastrownum; i++)
            {
                DataRow row = table.Rows[i];
                if (row["PartNo"].ToString() == "_ST")
                {
                    total = 0.0;
                    qty = 0;
                    weight = 0.0;
                    commissionAmount = 0.0;
                }
                else
                {
                    total += (double)row["Total"];
                    qty += (int)row["Quantity"];
                    if (!row.IsNull("Weight"))
                    {
                        weight += (double)row["Weight"];
                    }
                    commissionAmount += (double)row["CommissionAmount"];
                }
            }
            if (total > 0.0)
            {
                commission = commissionAmount / total;
            }
            this.AddSubtotalRow(lastrownum + 1, weight, "_ST", qty, "Subtotal", total, commission, commissionAmount);
        }

        protected void SubtotalRow(object sender, GridViewSelectEventArgs e)
        {
            int pos = e.NewSelectedIndex + 1;
            DataTable table = (DataTable)this.Session["estimate"];
            DataRow row = table.NewRow();
            row["RowID"] = pos;
            row["PartNo"] = "_ST";
            row["Description"] = "Subtotal";
            table.Rows.InsertAt(row, pos);
            for (int i = 0; i < table.Rows.Count; i++)
            {
                table.Rows[i]["RowID"] = i;
            }
            this.Session["estimate"] = table;
            this.BindGrid();
        }

        // Properties
        protected DefaultProfile Profile =>
            ((DefaultProfile)this.Context.Profile);

        protected global_asax ApplicationInstance =>
            ((global_asax)this.Context.ApplicationInstance);
    }
}