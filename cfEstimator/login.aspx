﻿<%@ Page Language="C#" masterpagefile="~/cfestimator.master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="cfEstimator.Login" title="CF Estimator Login"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div id="loginContent">
<div>&nbsp;</div>
<div>Welcome to CF Estimator Web!</div>
<div>Please login below.</div>
<div>&nbsp;</div>
<table border="0" cellpadding="0" cellspacing="0" width="279" align="center">
<tr><td colspan="2"><img alt="" src="img/loginTop.gif" style="width: 279px; height: 12px" /></td></tr>
<tr class="loginRow"><td colspan="2">&nbsp;</td></tr>
<tr class="loginRow"><td align="right">Username: </td><td><asp:TextBox ID="tbUsername" runat="server" style="width: 150px; height: 15px;"></asp:TextBox></td></tr>
<tr class="loginRow"><td align="right">Password: </td><td><asp:TextBox ID="tbPassword" runat="server" 
                            TextMode="Password" style="width: 150px; height: 15px;"></asp:TextBox></td></tr>
<tr class="loginRow"><td colspan="2">&nbsp;&nbsp;<asp:ImageButton ID="ibLogin" 
        runat="server" ImageUrl="~/img/btnGo.gif" ImageAlign="AbsBottom" 
        onclick="ImageButton1_Click" /></td></tr>
<tr class="loginRow"><td colspan="2">
    <asp:CheckBox ID="cbRememberMe" runat="server" Text="Remember Login?" />
    </td></tr>
<tr><td colspan="2"><img alt="" src="img/loginBottom.gif" style="width: 279px; height: 12px" /></td></tr>
</table>
<div>&nbsp;<asp:Label ID="lblFailureText" runat="server" ForeColor="Red" EnableViewState="False"></asp:Label>
                </div>
<div>First time users <a href="register.aspx">register here</a>!</div>
<p><a href="mailto:<%= ConfigurationManager.AppSettings["AdminEmail"] %>">Email the administrator</a><br />if you've forgotten your login or password.</p>
</div>
</asp:Content>

