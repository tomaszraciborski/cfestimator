﻿﻿<%@ page language="C#" masterpagefile="~/cfestimator.master" autoeventwireup="true" CodeBehind="estimate.aspx.cs" Inherits="cfEstimator.estimate" title="Estimate Sheet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script language="javascript" type="text/javascript">

function checkOnClear()
{
  return confirm("Are you sure you want to clear this estimate sheet?  Any data not previously saved will be lost.");
}

function checkOnSave()
{
  if (document.getElementById(hdEstimateID).value == "")
  {
    document.getElementById('hrSaveAs').click();
    return false;
  }
}

function OpenModelPopup(mytb)
{
	document.getElementById ('dd').style.display='none';
    document.getElementById ('ModalPopupDiv').style.visibility='visible';
    document.getElementById ('ModalPopupDiv').style.display='';
    document.getElementById ('ModalPopupDiv').style.top= Math.round ((document.documentElement.clientHeight/2)+ document.documentElement.scrollTop)-100 + 'px';
    document.getElementById ('ModalPopupDiv').style.left=Math.round ((document.documentElement.clientWidth/2)+ document.documentElement.scrollLeft)-150 + 'px';

    document.getElementById ('MaskedDiv').style.display='';
    document.getElementById ('MaskedDiv').style.visibility='visible';
    document.getElementById ('MaskedDiv').style.top='-70px';
    document.getElementById ('MaskedDiv').style.left='-10px';
    document.getElementById ('MaskedDiv').style.width=  document.documentElement.clientWidth + 'px';
    document.getElementById ('MaskedDiv').style.height= document.documentElement.clientHeight+ 'px';
    document.getElementById (mytb).focus();
}
function CloseModalPopup()
{
	document.getElementById ('dd').style.display='';
    document.getElementById ('MaskedDiv').style.display='none';
    document.getElementById ('ModalPopupDiv').style.display='none';
}

function checkSize(myfield, maxsize)
{
    var myvalue = new String(myfield.value);
    if (myvalue.length >= maxsize)
    {
        myfield.value = myvalue.substr(0, maxsize);
        alert("You've exceeded the maximum number of characters allowed in this field!");
        //myfield.blur();
    }
    return true;
}
	
//Include this method if you want to do something with the response from your AJAX call
function processResponse(resp)
{
  if (resp != "")
  {
    setTotals(resp);
  }
}

function DeleteRow(gvID, r)
{
  //alert(r.parentNode.parentNode.nodeName);
  var htable = document.getElementById(gvID);
  var rIndex=r.parentNode.parentNode.rowIndex; //calculates the index of the row to delete
  htable.deleteRow(rIndex); //deletes the row
  //alert(rIndex + " " + htable.rows.length);
  if (rIndex < htable.rows.length)
  {
    for (var i=rIndex;i<=(htable.rows.length-1);i++)
    {
      if (htable.rows[i].className == "gridRow")
        htable.rows[i].className = "gridAltRow";
      else if (htable.rows[i].className != "gridFooter")
        htable.rows[i].className = "gridRow";
    }
    //alert(currRow.className);
  }
  sndReq("s=estimate&fn=d&r=" + (rIndex-1));
  document.location = "estimate.aspx";
}

var prevQty = -1;
function setPrevQty(r)
{
  prevQty = r.value;
  //alert(prevQty);
}

function qtyChanged(r)
{
  if (r.value != prevQty)
  {
    if (isNaN(parseInt(r.value, 10)) || (r.value < 1))
    {
      r.value = prevQty;
      alert("Invalid quantity entered!");
      return false;
    }
    
    var qty = parseInt(r.value, 10);
    if (qty != r.value)
      r.value = qty;
    var rIndex=r.parentNode.parentNode.rowIndex; //calculates the index of the row affected
    
    /* All that is commented out below is now handled by setTotals
    var currRow = r.parentNode.parentNode;
    var rIndex=currRow.rowIndex; //calculates the index of the row affected
    //var totalCellIndex = totalCol; //currRow.cells.length - 3;
    //var totalPrice = currRow.cells[totalCellIndex].innerHTML.substring(1).replace(",","");
    //var refSellPrice = currRow.cells[totalCellIndex-2].innerHTML.substring(1).replace(",","");
    var proddetails = getProdDetails(currRow.cells[partNoCol].innerHTML);
    if (proddetails != "")
    {
      var refSellPrice = proddetails[2];//currRow.cells[currRow.cells.length + refSellCol].innerHTML.substring(1).replace(",","");
      var snglwt = (proddetails[1] == 0)?"":proddetails[1];
      var discount = proddetails[3];
      var comm = proddetails[4];
      //alert (snglwt + " " + qty);
      currRow.cells[wtCol].innerHTML = formatNumber(snglwt * qty); //getRowWeight(currRow.cells[partNoCol].innerHTML,qty);
      currRow.cells[currRow.cells.length + totalCol].innerHTML = formatCurrency(parseFloat(refSellPrice * qty * discount));
      currRow.cells[currRow.cells.length + commAmtCol].innerHTML = formatCurrency(parseFloat(refSellPrice * qty * discount * comm));
      //enabling the below alert will prevent links from firing, so would have to click them twice
      //alert(myinput.value);
    }
    */
    sndReq("s=estimate&fn=q&r=" + (rIndex-1) + "&n=" + qty);
    if (document.getElementById(hdHasST).value == "true")
        document.location = "estimate.aspx";
    //return true;
  }
}

/*
function getProdDetails(pnum)
{
  var prodlist = new String(document.getElementById(hdProdList).value);
  if (prodlist.length > 1)
  {
    prodlist = prodlist.substring(1, prodlist.length - 1);
    var prods = prodlist.split('|');
    for (var i in prods)
    {
      if (prods[i].indexOf(pnum + "`") == 0)
      {
        proddetails = prods[i].split('`');
        return proddetails;
      }
    }
  }
  return "";
}
*/

/*
function getRowWeight(pnum, qty)
{
  var prodlist = new String(document.getElementById(hdProdList).value);
  if (prodlist.length > 1)
  {
    prodlist = prodlist.substring(1, prodlist.length - 1);
    var prods = prodlist.split('|');
    for (var i in prods)
    {
      if (prods[i].indexOf(pnum + "`") == 0)
      {
        proddetails = prods[i].split('`');
        if (proddetails[1] != 0)
        {
          return (proddetails[1] * qty);
        }
      }
    }
  }
  return "";
}
*/

/*
function formatCurrency(num) {
  num = num.toString().replace(/\$|\,/g,'');
  if(isNaN(num))
    num = "0";
  sign = (num == (num = Math.abs(num)));
  num = Math.floor(num*100+0.50000000001);
  cents = num%100;
  num = Math.floor(num/100).toString();
  if(cents<10)
    cents = "0" + cents;
  for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
    num = num.substring(0,num.length-(4*i+3))+','+
  num.substring(num.length-(4*i+3));
  return (((sign)?'':'-') + '$' + num + '.' + cents);
}
*/

function formatNumber(num) {
  num = parseFloat(num.toString().replace(",",""));
  //alert(num);
  if (num == 0)
    return "";
  var fNum = num.toFixed(2);
  var decPos=fNum.indexOf(".");
  if (decPos>-1)
  {
    first=fNum.substring(0,decPos);
    second=fNum.substring(decPos,fNum.length);
    while (second.charAt(second.length-1)=="0")
      second=second.substring(0,second.length-1);
    if (second.length>1)
      return first+second;
    else
      return first;
  }

  return fNum;
}

function setTotals(sTotals)
{
  //alert(sTotals);
  var params = parseParams(sTotals, '|', true);
  //var totals = sTotals.split('|');
  //document.getElementById(lblTotalQty).innerHTML = totals[0];
  if ((typeof(params["rownum"])!="undefined") && (params["rownum"] != null))
  {
    var htable = document.getElementById(gvEstimate);
    var currRow = htable.rows[parseInt(params["rownum"]) + 1];
    currRow.cells[wtCol].innerHTML = formatNumber(params["rowwt"]);
    currRow.cells[currRow.cells.length + totalCol].innerHTML = params["rowtotal"];
    currRow.cells[currRow.cells.length + commAmtCol].innerHTML = params["rowcomm"];
  }

  if (document.getElementById(lblTotalWeight))
    document.getElementById(lblTotalWeight).innerHTML = params["totalwt"];
  if (document.getElementById(lblTotalSell))
    document.getElementById(lblTotalSell).innerHTML = params["totalsell"];
  if (document.getElementById(lblNonDiscSell))
    document.getElementById(lblNonDiscSell).innerHTML = params["totalnondiscsell"];
  if (document.getElementById(lblTotalComm))
    document.getElementById(lblTotalComm).innerHTML = params["totalcomm"];
  if (document.getElementById(lblCommPercent))
    document.getElementById(lblCommPercent).innerHTML = params["commperc"];
  if (document.getElementById(lblApproval))
    document.getElementById(lblApproval).innerHTML = params["approver"];

  if (typeof lblTotalCost != "undefined")
  {
      if (document.getElementById(lblTotalCost))
        document.getElementById(lblTotalCost).innerHTML = params["totalcost"];
      if (document.getElementById(lblBeforeMargin))
        document.getElementById(lblBeforeMargin).innerHTML = params["beforemargin"];
      if (document.getElementById(lblAfterMargin))
        document.getElementById(lblAfterMargin).innerHTML = params["aftermargin"];
      if (document.getElementById(lblBeforeMarginPercent))
        document.getElementById(lblBeforeMarginPercent).innerHTML = params["beforemarginperc"];
      if (document.getElementById(lblAfterMarginPercent))
        document.getElementById(lblAfterMarginPercent).innerHTML = params["aftermarginperc"];
  }
}

function updateParam(tb, param, maxsize)
{
  sndReq("fn=u&p=" + param + "&v=" + escape(tb.value.substr(0, maxsize)));
}

var prevDesc = "";
function setPrevDesc(tb)
{
  prevDesc = tb.value;
}

function descChanged(tb)
{
  if (tb.value != prevDesc)
  {
    if ((tb.value == "") || (tb.value == " "))
    {
      tb.value = prevDesc;
      alert("Description cannot be blank!");
      return false;
    }

    var currRow = tb.parentNode.parentNode;
    var rIndex=currRow.rowIndex; //calculates the index of the row affected
    sndReq("s=estimate&fn=desc&r=" + (rIndex-1) + "&v=" + escape(tb.value));
  }
}

/*
function setTotals(mytable)
{
  var mylabel = document.getElementById(lblTotalQty);
  var htable = document.getElementById(gvEstimate);
  if (htable.rows.length > 1)
  {
      var totalqty = 0;
      for (var i=1;i<=(htable.rows.length-1);i++)
      {
        if (htable.rows[i].className != "gridFooter")
        {
          
        }
      }
  }
  alert(mylabel.innerHTML);
}
*/

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h2>Estimate Sheet</h2>
<div style="text-align: center"><asp:Label runat="server" ID="lblMsg" CssClass="infoMsg" EnableViewState="false"></asp:Label></div>
<div class="dataForm">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td align="left" valign="top">

<table class="estimateHeaderTable" cellspacing="0" cellpadding="0" border="0">
<tr>
    <th>Quote #:</th>
    <td><asp:TextBox ID="tbQuoteNum" runat="server" MaxLength="50" Width="370px" onchange="updateParam(this,'quotenum');" onkeypress="return submitenter(this,event,null,true)"></asp:TextBox></td>
    <th>Date:</th>
    <td>
        <asp:TextBox ID="tbEstimateDate" runat="server" MaxLength="10" Width="70px" onchange="updateParam(this,'estimatedate');" onkeypress="return submitenter(this,event,null,true)"></asp:TextBox>
        <asp:Literal ID="litDate" runat="server"></asp:Literal>
    </td>
</tr>
<tr>
    <th>Representative:</th>
    <td><asp:TextBox ID="tbRep" runat="server" MaxLength="50" Width="370px" onchange="updateParam(this,'rep');" onkeypress="return submitenter(this,event,null,true)"></asp:TextBox></td>
    <th>Discount:</th>
    <td rowspan="4">
        <div id="dd"><asp:ListBox ID="lbDiscount" runat="server" Rows="6" AutoPostBack="true" 
            onselectedindexchanged="lbDiscount_SelectedIndexChanged" DataTextField="DisplayName" DataValueField="DiscountCode" SelectionMode="Single">
            <asp:ListItem Selected="True" Value="DISC00">0%</asp:ListItem>
        </asp:ListBox></div>
    </td>
</tr>
<tr>
    <th>Quotation For:</th>
    <td>
        <asp:TextBox ID="tbQuoteFor" runat="server" MaxLength="50" Width="370px" onchange="updateParam(this,'quotefor');" onkeypress="return submitenter(this,event,null,true)"></asp:TextBox>
    </td>
    <td>&nbsp;</td>
</tr>
<tr>
    <th>Address:</th>
    <td>
        <asp:TextBox ID="tbAddress" runat="server" MaxLength="150" Width="370px" onchange="updateParam(this,'address');" onkeypress="return submitenter(this,event,null,true)"></asp:TextBox>
    </td>
    <td>&nbsp;</td>
</tr>
<tr>
    <th>Application:</th>
    <td>
        <asp:TextBox ID="tbApplication" runat="server" MaxLength="100" Width="370px" onchange="updateParam(this,'application');" onkeypress="return submitenter(this,event,null,true)"></asp:TextBox>
    </td>
    <td>&nbsp;</td>
</tr>
<tr>
    <th>For Installation At:</th>
    <td>
        <asp:TextBox ID="tbInstallation" runat="server" MaxLength="100" Width="370px" onchange="updateParam(this,'installation');" onkeypress="return submitenter(this,event,null,true)"></asp:TextBox>
    </td>
    <td>&nbsp;</td>
</tr>
</table>

</td>
<td align="right" valign="top">
<table id="tblTotals" runat="server" class="estimateSummaryTable" cellspacing="0" cellpadding="0" border="0" align="right">
<tr class="estimateSummaryTableHeader">
<td align="center" colspan="2"><img src="img/totalsHeader.gif" width="227" height="10" border="0" /></td>
</tr>
<tr>
    <th>Approval Required:</th>
    <td>
        <asp:Label runat="server" ID="lblApproval" CssClass="estimateSummary" EnableViewState="false" Text="Representative"></asp:Label>
    </td>
</tr>
<tr>
    <th>Total Non Disc Sell:</th>
    <td>
        <asp:Label runat="server" ID="lblNonDiscSell" CssClass="estimateSummary" EnableViewState="false" Text="0"></asp:Label>
    </td>
</tr>
<tr>
    <th>Total Disc Sell:</th>
    <td>
        <asp:Label runat="server" ID="lblTotalSell" CssClass="estimateSummary" EnableViewState="false" Text="0"></asp:Label>
    </td>
</tr>
<tr>
    <th>Total Weight:</th>
    <td>
        <asp:Label runat="server" ID="lblTotalWeight" CssClass="estimateSummary" EnableViewState="false" Text="0"></asp:Label>
    </td>
</tr>
<tr>
    <th>Commission %:</th>
    <td>
        <asp:Label runat="server" ID="lblCommPercent" CssClass="estimateSummary" EnableViewState="false" Text="0"></asp:Label>
    </td>
</tr>
<tr>
    <th>Total Commission:</th>
    <td>
        <asp:Label runat="server" ID="lblTotalComm" CssClass="estimateSummary" EnableViewState="false" Text="0"></asp:Label>
    </td>
</tr>
<tr class="estimateSummaryTableHeader" id="trShowGDTotals" runat="server" visible="false">
<td align="center" colspan="2"><asp:LinkButton ID="lbShowGDTotals" runat="server" 
        onclick="lbShowGDTotals_Click"><img src="img/gdTotalsBottom.gif" width="227" height="10" border="0" /></asp:LinkButton></td>
</tr>
</table>
<div style="clear: both"></div>
<table class="estimateSummaryTable" id="tblGDTotals" runat="server" visible="false" cellspacing="0" cellpadding="0" border="0" align="right">
<tr class="estimateSummaryTableHeader">
<td align="center" colspan="2"><img src="img/totalsHeader.gif" width="227" height="10" border="0" /></td>
</tr>
<tr class="estimateSummaryTableHeader">
<td align="center" colspan="2" style="border-top: solid 1px #E0ECFF;">
    <asp:LinkButton ID="lbHideGDTotals" runat="server" 
        onclick="lbHideGDTotals_Click"><img src="img/gdTotalsTop.gif" width="227" height="10" border="0" /></asp:LinkButton></td>
</tr>
<tr>
    <th>Total Cost:</th>
    <td>
        <asp:Label runat="server" ID="lblTotalCost" CssClass="estimateSummary" EnableViewState="false" Text="0"></asp:Label>
    </td>
</tr>
<tr style="background-color: #CCCCCC">
    <th colspan="2" align="center">Before Commission</th>
</tr>
<tr>
    <th>Margin %:</th>
    <td>
        <asp:Label runat="server" ID="lblBeforeMarginPercent" CssClass="estimateSummary" EnableViewState="false" Text="0"></asp:Label>
    </td>
</tr>
<tr>
    <th>Margin $:</th>
    <td>
        <asp:Label runat="server" ID="lblBeforeMargin" CssClass="estimateSummary" EnableViewState="false" Text="0"></asp:Label>
    </td>
</tr>
<tr style="background-color: #CCCCCC">
    <th colspan="2" align="center">After Commission</th>
</tr>
<tr>
    <th>Margin %:</th>
    <td>
        <asp:Label runat="server" ID="lblAfterMarginPercent" CssClass="estimateSummary" EnableViewState="false" Text="0"></asp:Label>
    </td>
</tr>
<tr>
    <th>Margin $:</th>
    <td>
        <asp:Label runat="server" ID="lblAfterMargin" CssClass="estimateSummary" EnableViewState="false" Text="0"></asp:Label>
    </td>
</tr>
</table>

</td>
</tr>
</table>
<div style="clear: both;"></div>
<div class="gridActions" runat="server" id="gridActions">
<strong>Actions:</strong>
<asp:LinkButton ID="lbClear" runat="server" OnClientClick="return checkOnClear();" onclick="lbClear_Click">Clear All</asp:LinkButton> | 
<asp:LinkButton ID="lbSave" runat="server" OnClientClick="return checkOnSave();" onclick="lbSave_Click">Save</asp:LinkButton> | 
<a href="javascript:OpenModelPopup('<%=tbEstimateName.ClientID %>');" id="hrSaveAs">Save As...</a> |
<asp:LinkButton ID="lbPrint" runat="server" OnClick="lbPrint_Click">Print PDF</asp:LinkButton> |
<asp:LinkButton ID="lbUpdatePrices" runat="server" OnClick="lbUpdatePrices_Click">Update Prices</asp:LinkButton>
<asp:PlaceHolder ID="phGDOnly" runat="server" Visible="false">
| <asp:LinkButton ID="lbCost" runat="server" OnClick="lbCost_Click">Show Cost</asp:LinkButton>
| <asp:LinkButton ID="lbPC" runat="server" OnClick="lbPC_Click">Show Price Code</asp:LinkButton>
</asp:PlaceHolder>
</div>
<asp:GridView ID="gvEstimate" runat="server" CssClass="gridTable" ShowFooter="false" AllowPaging="False" AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="RowID" CellPadding="0" PageSize="5" GridLines="None" BorderStyle="None" Width="100%" name="gvEstimate" OnSelectedIndexChanging="SubtotalRow" OnRowDataBound="gvEstimate_RowDataBound" OnRowCommand="gv_RowCommand">
        <Columns>
            <asp:TemplateField>
                <ItemStyle Width="16px" />
                <ItemTemplate>
                    <nobr>
                    <asp:ImageButton ID="ibUp" runat="server" CommandName="ibUp_Click" AlternateText="Move Up" ImageUrl="~/img/up.gif" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.RowID")%>'/>
                    <asp:ImageButton ID="ibDown" runat="server" CommandName="ibDown_Click" AlternateText="Move Down" ImageUrl="~/img/down.gif" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.RowID")%>'/>
                    <asp:ImageButton ID="ibSubtotal" runat="server" CommandName="Select" AlternateText="Subtotal" ImageUrl="~/img/subtotal.gif" />
                    </nobr>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                 <ItemStyle Width="16px" />
                <ItemTemplate>
                    <a href="#" onclick="DeleteRow('<%=gvEstimate.ClientID %>',this)"><img alt="Delete" src="img/delete.gif" border="0" width="16px" height="16px" /></a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Weight" HeaderText="Wt"/>
            <asp:TemplateField HeaderText="Part #">
            <ItemTemplate>
                <asp:Label ID="lbPartNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PartNo")%>'></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Qty">
            <ItemTemplate>
                <asp:TextBox ID="tbQuantity" runat="server" CssClass="thinTexbox" style="width: 20px;" onfocus="setPrevQty(this)" onchange="qtyChanged(this)" Text='<%# DataBinder.Eval(Container, "DataItem.Quantity")%>' onkeypress="return submitenter(this,event,null,true)"></asp:TextBox><asp:Label ID="lblQuantity" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container, "DataItem.Quantity")%>'></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Description">
            <ItemTemplate>
                <asp:TextBox ID="tbDescription" MaxLength="100" runat="server" CssClass="thinTexbox" style="width: 300px;" onfocus="setPrevDesc(this)" onchange="descChanged(this)" Text='<%# DataBinder.Eval(Container, "DataItem.Description")%>' onkeypress="return submitenter(this,event,null,true)"></asp:TextBox><div id="divSubtotalDesc" runat="server" visible="false" class="subtotalDesc">Subtotal</div>
            </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Cost" HeaderText="Cost" Visible="false" DataFormatString="{0:c}"/>
            <asp:BoundField DataField="PriceCode" HeaderText="PC" Visible="false"/>
            <asp:BoundField DataField="RefSell" HeaderText="Ref Sell" DataFormatString="{0:c}"/>
            <asp:BoundField DataField="UnitSell" HeaderText="Unit Sell" DataFormatString="{0:c}"/>
            <asp:BoundField DataField="Discount" HeaderText="Disc" Visible="false"/>
            <asp:BoundField DataField="Total" HeaderText="Total" Visible="true" DataFormatString="{0:c}"/>
            <asp:BoundField DataField="Commission" HeaderText="Comm (%)" DataFormatString="{0:p}"/>
            <asp:BoundField DataField="CommissionAmount" HeaderText="Comm ($)" DataFormatString="{0:c}"/>
        </Columns>
        <HeaderStyle CssClass="gridHeader" />
        <FooterStyle CssClass="gridFooter" />
        <RowStyle CssClass="gridRow" VerticalAlign="Top" />
        <AlternatingRowStyle CssClass="gridAltRow" VerticalAlign="Top" />
        <PagerStyle CssClass="gridPager" />
</asp:GridView>
</div>

<table class="estimateHeaderTable" cellspacing="0" cellpadding="0" border="0">
<tr>
    <th>Additional Notes (1200 character limit)</th>
</tr>
<tr>
    <td><asp:TextBox ID="tbEstimateNote" runat="server" TextMode="MultiLine" Rows="10" MaxLength="1200" 
    Width="900px" onkeypress="checkSize(this,1200);"
    onchange="updateParam(this,'estimatenote',1200);"></asp:TextBox></td>
</tr>
</table>

<asp:HiddenField ID="hdEstimateID" runat="server" value=""/>
<asp:HiddenField ID="hdHasST" runat="server" value="false"/>
    
<div id="MaskedDiv" class="MaskedDiv">
        </div>
        <div id="ModalPopupDiv" class="ModalPopup">
        <p>Enter a name for this Estimate Sheet</p>
            <p><asp:TextBox ID="tbEstimateName" runat="server" Width="250px" MaxLength="50" onkeypress="return submitenter(this,event,'btSaveEstimate')"></asp:TextBox></p>
            <p>
	<asp:Button ID="btSaveEstimate" runat="server" Text="Save" onclick="btSaveEstimate_Click" /> 
	<input type=button value="Cancel" onclick="javascript:CloseModalPopup();" />
	</p>
        </div>
</asp:Content>

