﻿﻿<%@ page language="C#" masterpagefile="~/cfestimator.master" autoeventwireup="true" CodeBehind="admMngNotes.aspx.cs" Inherits="cfEstimator.admMngNotes" title="Manage Notes" %>
<%@ Register src="admMenu.ascx" tagname="admMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<uc1:admMenu ID="admMenu1" runat="server" TagID="hrefNotes" />
<div style="text-align: center">    
    <asp:Label runat="server" ID="lblMsg" CssClass="infoMsg" EnableViewState="false"></asp:Label></div>
<asp:PlaceHolder ID="phEditForm" runat="server" Visible="false">
<div class="dataFormBoxed">
<table>
<tr>
<td class="requiredField">NoteID:</td>
<td><asp:TextBox ID="tbNoteID" runat="server" MaxLength="3"></asp:TextBox></td>
</tr>
<tr>
<td class="requiredField">Description:</td>
<td><asp:TextBox ID="tbDescription" runat="server" MaxLength="100"></asp:TextBox></td>
</tr>
<tr>
<td class="requiredField" valign="top">Text:</td>
<td><asp:TextBox ID="tbText" runat="server" MaxLength="1000" TextMode="MultiLine" Columns="75" Rows="5"></asp:TextBox></td>
</tr>
<tr>
<td colspan="2">
    <asp:Button ID="btSave" runat="server" Text="Save Changes" 
        onclick="btSave_Click" /> &nbsp;
    <asp:Button ID="btCancel" runat="server" Text="Cancel" 
        onclick="btCancel_Click" />
</td>
</tr>
</table>
</div>
</asp:PlaceHolder>
<div class="gridActions">
<strong>Actions:</strong>
<asp:LinkButton ID="lbAdd" runat="server" onclick="lbAdd_Click">Add new note</asp:LinkButton>
</div>
<asp:GridView ID="gvData" runat="server" CssClass="gridTable" AllowPaging="True" AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="NoteID" OnRowDeleting="DeleteRow" OnSelectedIndexChanging="SelectRow" CellPadding="0" PageSize="20" OnPageIndexChanging="ChangePage" GridLines="None" BorderStyle="None" Width="100%">
    <Columns>
        <asp:CommandField ShowSelectButton="True" SelectImageUrl="~/img/edit.gif" SelectText="View" ButtonType="Image" ItemStyle-Width="16px" />
        <asp:TemplateField ShowHeader="false">
            <ItemStyle HorizontalAlign="Center" Width="16px" />
            <ItemTemplate>
                <asp:ImageButton ID="btnDel" runat="server" 
                    CausesValidation="False" CommandName="Delete"
                    ImageUrl="~/img/delete.gif" ToolTip="Delete" OnClientClick="return confirm('Are you sure you want to delete this note?')" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="NoteID" HeaderText="NoteID"/>
        <asp:BoundField DataField="Description" HeaderText="Description" />
    </Columns>
    <HeaderStyle CssClass="gridHeader" />
    <RowStyle CssClass="gridRow" VerticalAlign="Top" />
    <AlternatingRowStyle CssClass="gridAltRow" VerticalAlign="Top" />
    <PagerStyle CssClass="gridPager" />
</asp:GridView>
</asp:Content>


