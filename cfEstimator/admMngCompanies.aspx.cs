﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using static cfEstimator.App_Code;

namespace cfEstimator
{    
    public class admMngCompanies : Page, IRequiresSessionState
    {
        // Fields
        protected admMenu admMenu1;
        protected Label lblMsg;
        protected Label lblCompanyID;
        protected TextBox tbCompanyName;
        protected Button btSave;
        protected Button btCancel;
        protected PlaceHolder phEditForm;
        protected LinkButton lbAdd;
        protected GridView gvData;

        // Methods
        protected void BindGrid(int pgIndex)
        {
            DataView defaultView = new data().GetCompanies(-1).Tables[0].DefaultView;
            defaultView.Sort = "CompanyName";
            this.gvData.DataSource = defaultView;
            this.gvData.PageIndex = pgIndex;
            this.gvData.DataBind();
        }

        protected void btCancel_Click(object sender, EventArgs e)
        {
            this.tbCompanyName.Text = "";
            this.phEditForm.Visible = false;
        }

        protected void btSave_Click(object sender, EventArgs e)
        {
            data data = new data();
            if (this.tbCompanyName.Text.Trim() == "")
            {
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "The Company Name field is required!";
            }
            else if (this.lblCompanyID.Text != "-")
            {
                if (!data.UpdateCompany(int.Parse(this.lblCompanyID.Text), this.tbCompanyName.Text))
                {
                    this.lblMsg.CssClass = "errorMsg";
                    this.lblMsg.Text = "Unable to update company '" + this.tbCompanyName.Text + "'!";
                }
                else
                {
                    this.lblMsg.Text = "Successfully updated company '" + this.tbCompanyName.Text + "'!";
                    this.phEditForm.Visible = false;
                    this.BindGrid(0);
                }
            }
            else if (data.InsertCompany(this.tbCompanyName.Text) <= 0)
            {
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "Unable to add company '" + this.tbCompanyName.Text + "'!";
            }
            else
            {
                this.lblMsg.Text = "Successfully added company '" + this.tbCompanyName.Text + "'!";
                this.phEditForm.Visible = false;
                this.BindGrid(0);
            }
        }

        protected void ChangePage(object sender, GridViewPageEventArgs e)
        {
            this.BindGrid(e.NewPageIndex);
        }

        protected void DeleteRow(object sender, GridViewDeleteEventArgs e)
        {
            if (new data().DeleteCompany((int)this.gvData.DataKeys[e.RowIndex].Value))
            {
                this.lblMsg.Text = "Successfully deleted company!";
                this.BindGrid(0);
            }
            else
            {
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "Unable to delete company!  It may be associated with other data.";
            }
        }

        protected void lbAdd_Click(object sender, EventArgs e)
        {
            this.lblCompanyID.Text = "-";
            this.tbCompanyName.Text = "";
            this.phEditForm.Visible = true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            util.CheckSession();
            if (!this.Page.IsPostBack)
            {
                this.BindGrid(0);
            }
        }

        protected void SelectRow(object sender, GridViewSelectEventArgs e)
        {
            DataSet companies = new data().GetCompanies((int)this.gvData.DataKeys[e.NewSelectedIndex].Value);
            if (companies.Tables[0].Rows.Count > 0)
            {
                this.lblCompanyID.Text = companies.Tables[0].Rows[0]["CompanyID"].ToString();
                this.tbCompanyName.Text = companies.Tables[0].Rows[0]["CompanyName"].ToString();
                this.phEditForm.Visible = true;
            }
        }

        // Properties
        protected DefaultProfile Profile =>
            ((DefaultProfile)this.Context.Profile);

        protected global_asax ApplicationInstance =>
            ((global_asax)this.Context.ApplicationInstance);
    }
}