﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using static cfEstimator.App_Code;

namespace cfEstimator
{
    public class aftermarket : Page, IRequiresSessionState
    {
        // Fields
        protected DropDownList ddFrame;
        protected Literal litDoc;
        protected Literal litProductList;

        // Methods
        protected void ddFrame_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddFrame.SelectedIndex > 0)
            {
                this.LoadProducts(ConfigurationManager.AppSettings["AftermarketCategory"], int.Parse(this.ddFrame.SelectedValue));
            }
        }

        protected void LoadFrames()
        {
            data data = new data();
            DataSet framesByCategory = data.GetFramesByCategory(data.GetCategoryID(ConfigurationManager.AppSettings["AftermarketCategory"]), this.Session["BrandAccess"].ToString());
            framesByCategory.Tables[0].Columns.Add(new DataColumn("PaddedName", Type.GetType("System.String")));
            string s = "";
            foreach (DataRow row in framesByCategory.Tables[0].Rows)
            {
                s = row["Name"].ToString();
                try
                {
                    int.Parse(s);
                    s = s.PadLeft(50, '0');
                }
                catch
                {
                }
                row["PaddedName"] = s;
            }
            framesByCategory.AcceptChanges();
            DataView defaultView = framesByCategory.Tables[0].DefaultView;
            defaultView.Sort = "PaddedName";
            this.ddFrame.DataSource = defaultView;
            this.ddFrame.DataBind();
            this.ddFrame.Items.Insert(0, new ListItem("Select...", "-1"));
        }

        protected void LoadProducts(string CategoryName, int FrameID)
        {
            data data = new data();
            int categoryID = data.GetCategoryID(CategoryName);
            if (categoryID > 0)
            {
                DataSet frameDetails = data.GetProductsByCategory(categoryID, this.Session["BrandAccess"].ToString(), FrameID, -1, -1.0, -1.0);
                if (frameDetails.Tables[0].Rows.Count > 0)
                {
                    string str = "<table class=\"productTable\" cellpadding=0 cellspacing=0>\n" + "<tr class=\"productTableHeader\"><th>Part #</th><th>Description</th><th>Ref Sell</th></tr>\n";
                    string str2 = "";
                    string str3 = "";
                    foreach (DataRow row in frameDetails.Tables[0].Rows)
                    {
                        if (row["DisplayName"].ToString() != str2)
                        {
                            object[] objArray = new object[] { str, "<tr><th colspan=\"3\">", row["DisplayName"], "</th></tr>\n" };
                            str = string.Concat(objArray);
                            str2 = row["DisplayName"].ToString();
                        }
                        if (!row.IsNull("Size") && (row["Size"].ToString().Trim() != ""))
                        {
                            str3 = " - " + row["Size"];
                        }
                        str = str + "<tr>\n";
                        if (row.IsNull("NoteID"))
                        {
                            object[] objArray2 = new object[] { str, "<td>", row["HFPartNumber"], "</td>\n" };
                            str = string.Concat(objArray2);
                        }
                        else
                        {
                            object[] objArray3 = new object[] { str, "<td>", row["HFPartNumber"], "<a style=\"text-decoration: none\" href=\"javascript:ShowNote('", row["ProductID"], "')\">*</a></td>\n" };
                            str = string.Concat(objArray3);
                        }
                        object[] objArray4 = new object[] { str, "<td>", row["Description"], str3, "</td>\n" };
                        str = string.Concat(new object[] { string.Concat(objArray4), "<td><a href=\"estimate.aspx?pn=", row["HFPartNumber"], "\">", $"{row["ListPrice"]:c}", "</a></td>\n" }) + "</tr>\n";
                        str3 = "";
                    }
                    str = str + "</table>\n";
                    this.litProductList.Text = str;
                    frameDetails = data.GetFrameDetails(FrameID, this.Session["BrandAccess"].ToString());
                    if (!frameDetails.Tables[0].Rows[0].IsNull("Filename"))
                    {
                        this.litDoc.Text = "<a href=\"" + frameDetails.Tables[0].Rows[0]["Filename"].ToString() + "\" target=\"_blank\">View Associated Documentation</a>";
                    }
                    else
                    {
                        this.litDoc.Text = "";
                    }
                }
                else
                {
                    this.litProductList.Text = "No products match that criteria";
                    this.litDoc.Text = "";
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            util.CheckSession();
            if (!this.Page.IsPostBack)
            {
                this.LoadFrames();
            }
        }

        // Properties
        protected DefaultProfile Profile =>
            ((DefaultProfile)this.Context.Profile);

        protected global_asax ApplicationInstance =>
            ((global_asax)this.Context.ApplicationInstance);
    }
}