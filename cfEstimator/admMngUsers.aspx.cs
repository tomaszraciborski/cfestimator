﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using static cfEstimator.App_Code;

namespace cfEstimator
{
    public class admMngUsers : Page, IRequiresSessionState
    {
        // Fields
        protected admMenu admMenu1;
        protected Label lblMsg;
        protected LinkButton lbExport;
        protected GridView gvUsers;
        protected GridView gvData;
        protected Panel pnlData;

        // Methods
        protected void BindGrid(int pgIndex)
        {
            DataView defaultView = new data().GetUsers(-1).Tables[0].DefaultView;
            defaultView.Sort = "Username";
            this.gvUsers.DataSource = defaultView;
            this.gvUsers.PageIndex = pgIndex;
            this.gvUsers.DataBind();
        }

        protected void ChangePage(object sender, GridViewPageEventArgs e)
        {
            this.BindGrid(e.NewPageIndex);
        }

        protected void DeleteRow(object sender, GridViewDeleteEventArgs e)
        {
        }

        protected void DeleteUser(int UserID, string Username)
        {
            if (new data().DeleteUser(UserID))
            {
                this.lblMsg.Text = "Successfully deleted user '" + Username + "'!";
            }
            else
            {
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "Unable to delete user '" + Username + "'!";
            }
        }

        protected void lbExport_Click(object sender, EventArgs e)
        {
            DataSet userDataSheet = new data().GetUserDataSheet();
            this.gvData.DataSource = userDataSheet;
            this.gvData.DataBind();
            base.Response.Clear();
            base.Response.ContentType = "application/ms-excel";
            base.Response.AddHeader("content-disposition", "attachment;filename=Users.xls");
            StringWriter writer = new StringWriter();
            HtmlTextWriter writer2 = new HtmlTextWriter(writer);
            this.pnlData.RenderControl(writer2);
            base.Response.Write(writer.ToString());
            base.Response.End();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            util.CheckSession();
            if (!this.Page.IsPostBack)
            {
                if ((base.Request.QueryString["fn"] != null) && (base.Request.QueryString["fn"] == "delete"))
                {
                    this.DeleteUser(int.Parse(base.Request.QueryString["id"]), base.Request.QueryString["username"]);
                }
                this.BindGrid(0);
            }
        }

        protected void SelectRow(object sender, GridViewSelectEventArgs e)
        {
            base.Response.Redirect("userProfile.aspx?fn=edit&id=" + this.gvUsers.DataKeys[e.NewSelectedIndex].Value);
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
        }

        // Properties
        protected DefaultProfile Profile =>
            ((DefaultProfile)this.Context.Profile);

        protected global_asax ApplicationInstance =>
            ((global_asax)this.Context.ApplicationInstance);
    }

}