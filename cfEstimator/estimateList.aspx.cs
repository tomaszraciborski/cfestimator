﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using static cfEstimator.App_Code;

namespace cfEstimator
{
    public class estimateList : Page, IRequiresSessionState
    {
        // Fields
        protected Label lblMsg;
        protected Literal litEstimateName;
        protected TextBox tbUsername;
        protected Button btAdd;
        protected Button btCancel;
        protected GridView gvUsers;
        protected PlaceHolder phEditForm;
        protected GridView gvData;
        protected HtmlInputHidden hdEstimateID;

        // Methods
        protected void BindGrid(int pgIndex)
        {
            DataSet estimatesByUser = new data().GetEstimatesByUser(int.Parse(this.Session["UserID"].ToString()));
            this.gvData.DataSource = estimatesByUser;
            this.gvData.PageIndex = pgIndex;
            this.gvData.DataBind();
        }

        protected void btAdd_Click(object sender, EventArgs e)
        {
            data data = new data();
            if (this.tbUsername.Text.Trim() == "")
            {
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "Invalid username!";
            }
            else
            {
                int userID = data.GetUserID(this.tbUsername.Text);
                if (userID == -1)
                {
                    this.lblMsg.CssClass = "errorMsg";
                    this.lblMsg.Text = "Invalid username!";
                }
                else if (!data.InsertEstimateUser(int.Parse(this.hdEstimateID.Value), userID))
                {
                    this.lblMsg.CssClass = "errorMsg";
                    this.lblMsg.Text = "Unable to grant user access to estimate!";
                }
                else
                {
                    this.lblMsg.Text = "Successfully granted user access to estimate.";
                    DataSet estimateUsers = data.GetEstimateUsers(int.Parse(this.hdEstimateID.Value));
                    this.gvUsers.DataSource = estimateUsers;
                    this.gvUsers.DataBind();
                    this.tbUsername.Text = "";
                }
            }
        }

        protected void btCancel_Click(object sender, EventArgs e)
        {
            this.phEditForm.Visible = false;
            this.tbUsername.Text = "";
        }

        protected void ChangePage(object sender, GridViewPageEventArgs e)
        {
            this.BindGrid(e.NewPageIndex);
        }

        protected void DeleteRow(object sender, GridViewDeleteEventArgs e)
        {
            if (new data().DeleteEstimate((int)this.gvData.DataKeys[e.RowIndex].Value))
            {
                this.lblMsg.Text = "Successfully deleted estimate!";
                this.BindGrid(0);
            }
            else
            {
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "Unable to delete estimate!  It may be associated with other data.";
            }
        }

        protected void DeleteUserRow(object sender, GridViewDeleteEventArgs e)
        {
            data data = new data();
            if (!data.DeleteEstimateUser((int)this.gvUsers.DataKeys[e.RowIndex].Value))
            {
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "Unable to delete user's access to estimate!";
            }
            else
            {
                this.lblMsg.Text = "Successfully deleted user's access to estimate.";
                DataSet estimateUsers = data.GetEstimateUsers((int)this.gvUsers.DataKeys[e.RowIndex].Values["EstimateID"]);
                this.gvUsers.DataSource = estimateUsers;
                this.gvUsers.DataBind();
            }
        }

        protected void EditRow(object sender, GridViewEditEventArgs e)
        {
            this.phEditForm.Visible = true;
            DataSet estimateUsers = new data().GetEstimateUsers((int)this.gvData.DataKeys[e.NewEditIndex].Value);
            this.gvUsers.DataSource = estimateUsers;
            this.gvUsers.DataBind();
            this.hdEstimateID.Value = this.gvData.DataKeys[e.NewEditIndex].Value.ToString();
            this.tbUsername.Text = "";
            DataRow row = estimateUsers.Tables[0].Rows[0];
            if (!row.IsNull("Name") && (row["Name"].ToString().Trim() != ""))
            {
                this.litEstimateName.Text = "for estimate: " + row["Name"].ToString();
            }
            else if (row.IsNull("QuoteNumber") || (row["QuoteNumber"].ToString().Trim() == ""))
            {
                this.litEstimateName.Text = "";
            }
            else
            {
                this.litEstimateName.Text = "for quote #: " + row["QuoteNumber"].ToString();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            util.CheckSession();
            if (!this.Page.IsPostBack)
            {
                this.BindGrid(0);
            }
        }

        protected void SelectRow(object sender, GridViewSelectEventArgs e)
        {
            base.Response.Redirect("estimate.aspx?eid=" + this.gvData.DataKeys[e.NewSelectedIndex].Value, true);
        }

        // Properties
        protected DefaultProfile Profile =>
            ((DefaultProfile)this.Context.Profile);

        protected global_asax ApplicationInstance =>
            ((global_asax)this.Context.ApplicationInstance);
    }
}