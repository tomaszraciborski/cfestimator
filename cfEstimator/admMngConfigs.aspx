﻿﻿<%@ page language="C#" masterpagefile="~/cfestimator.master" autoeventwireup="true" CodeBehind="admMngConfigs.aspx.cs" Inherits="cfEstimator.admMngConfigs" title="Manage Configurations" %>
<%@ Register src="admMenu.ascx" tagname="admMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<uc1:admMenu ID="admMenu1" runat="server" TagID="hrefConfigs" />
<div style="text-align: center">    
    <asp:Label runat="server" ID="lblMsg" CssClass="infoMsg" EnableViewState="false"></asp:Label></div>

<div style="font-size: 12px;">
Select a Frame: 
<asp:DropDownList ID="ddFrame" runat="server" DataTextField="Name" DataValueField="FrameID" style="font-size: 8pt; vertical-align: text-bottom;">
<asp:ListItem Selected="True" Text="Select..." Value="-1" />
</asp:DropDownList>
&nbsp; &nbsp; Select a Frame Config Type:
<asp:DropDownList ID="ddFrameConfigType" runat="server" style="font-size: 8pt; vertical-align: text-bottom;">
</asp:DropDownList>
&nbsp; &nbsp;
<asp:Button ID="btGetCategories" runat="server" Text="Show Categories" onclick="btGetCategories_Click" />
<asp:Button ID="btGetProducts" runat="server" Text="Show Products" onclick="btGetProducts_Click" />
</div>
<asp:PlaceHolder ID="phDeleteFrameConfig" runat="server" Visible="false">
<div style="font-size: 12px;">
<br />
There are no products or categories associated with this frame configuration.  Click 
<asp:LinkButton ID="lbDeleteFrameConfig" runat="server" 
    onclick="lbDeleteFrameConfig_Click" 
    OnClientClick="return confirm('Are you sure you want to delete this frame configuration?')" >here</asp:LinkButton> 
to delete it.
<br />
</div>
</asp:PlaceHolder>
<asp:PlaceHolder ID="phEditForm" runat="server" Visible="false">
<div class="dataFormBoxed">
<table>
<tr>
<td class="requiredField">Part Number:</td>
<td><asp:TextBox ID="tbHFPartNumber" runat="server" MaxLength="25"></asp:TextBox></td>
</tr>
<tr>
<td class="requiredField">Is Default?:</td>
<td><asp:CheckBox ID="cbIsDefault" runat="server" Checked="true" /></td>
</tr>
<tr>
<td class="requiredField">Default #:</td>
<td><asp:TextBox ID="tbDefaultNumber" runat="server" MaxLength="3" Style="width: 50px"></asp:TextBox></td>
</tr>
<tr>
<td colspan="2">
    <asp:Button ID="btSave" runat="server" Text="Save Changes" 
        onclick="btSave_Click" /> &nbsp;
    <asp:Button ID="btCancel" runat="server" Text="Cancel" 
        onclick="btCancel_Click" />
</td>
</tr>
</table>
</div>
</asp:PlaceHolder>

<asp:PlaceHolder ID="phProducts" runat="server" Visible="false">
<div class="gridActions">
<strong>Actions:</strong>
<asp:LinkButton ID="lbAdd" runat="server" onclick="lbAdd_Click">Add new product</asp:LinkButton>
</div>
<asp:GridView ID="gvData" runat="server" CssClass="gridTable" AllowPaging="False" 
AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="FrameConfigProductID" 
OnRowDeleting="DeleteRow" CellPadding="0" GridLines="None" BorderStyle="None" Width="100%" 
EmptyDataText="There are currently no default products defined for this frame configuration.  Click the 'Add new product' link above to begin.">
    <Columns>
        <asp:TemplateField ShowHeader="false">
            <ItemStyle HorizontalAlign="Center" Width="16px" />
            <ItemTemplate>
                <asp:ImageButton ID="btnDel" runat="server" 
                    CausesValidation="False" CommandName="Delete"
                    ImageUrl="~/img/delete.gif" ToolTip="Delete" OnClientClick="return confirm('Are you sure you want to remove this product from this frame configuration?')" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="FrameConfigProductID" HeaderText="FrameConfigProductID" Visible="false"/>
        <asp:BoundField DataField="HFPartNumber" HeaderText="Part Number" />
        <asp:BoundField DataField="Description" HeaderText="Description" />
        <asp:BoundField DataField="Size" HeaderText="Size" />
        <asp:BoundField DataField="IsDefault" HeaderText="Is Default?" />
        <asp:BoundField DataField="DefaultNumber" HeaderText="Default #" />
    </Columns>
    <HeaderStyle CssClass="gridHeader" />
    <RowStyle CssClass="gridRow" VerticalAlign="Top" />
    <AlternatingRowStyle CssClass="gridAltRow" VerticalAlign="Top" />
    <PagerStyle CssClass="gridPager" />
</asp:GridView>
</asp:PlaceHolder>

<asp:PlaceHolder ID="phEditForm2" runat="server" Visible="false">
<div class="dataFormBoxed">
<table>
<tr>
<td class="requiredField">Category Name:</td>
<td><asp:TextBox ID="tbCategoryName" runat="server" MaxLength="50"></asp:TextBox></td>
</tr>
<tr>
<td colspan="2">
    <asp:Button ID="btSave2" runat="server" Text="Save Changes" 
        onclick="btSave2_Click" /> &nbsp;
    <asp:Button ID="btCancel2" runat="server" Text="Cancel" 
        onclick="btCancel2_Click" />
</td>
</tr>
</table>
</div>
</asp:PlaceHolder>

<asp:PlaceHolder ID="phCategory" runat="server" Visible="false">
<div class="gridActions">
<strong>Actions:</strong>
<asp:LinkButton ID="lbAdd2" runat="server" onclick="lbAdd2_Click">Add new category</asp:LinkButton>
</div>
<asp:GridView ID="gvData2" runat="server" CssClass="gridTable" AllowPaging="False" 
AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="FrameConfigCategoryID" 
OnRowDeleting="DeleteRow2" CellPadding="0" GridLines="None" BorderStyle="None" Width="100%" 
EmptyDataText="There are currently no default categories associated with this frame configuration.  Click the 'Add new category' link above to begin.">
    <Columns>
        <asp:TemplateField ShowHeader="false">
            <ItemStyle HorizontalAlign="Center" Width="16px" />
            <ItemTemplate>
                <asp:ImageButton ID="ImageButton1" runat="server" 
                    CausesValidation="False" CommandName="Delete"
                    ImageUrl="~/img/delete.gif" ToolTip="Delete" OnClientClick="return confirm('Are you sure you want to remove this category from this frame configuration?')" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="FrameConfigCategoryID" HeaderText="FrameConfigCategoryID" Visible="false"/>
        <asp:BoundField DataField="CategoryID" HeaderText="CategoryID" Visible="false"/>
        <asp:BoundField DataField="Name" HeaderText="Name" />
        <asp:BoundField DataField="DisplayName" HeaderText="DisplayName" />
    </Columns>
    <HeaderStyle CssClass="gridHeader" />
    <RowStyle CssClass="gridRow" VerticalAlign="Top" />
    <AlternatingRowStyle CssClass="gridAltRow" VerticalAlign="Top" />
    <PagerStyle CssClass="gridPager" />
</asp:GridView>
</asp:PlaceHolder>
    <asp:HiddenField ID="hdFrameConfigID" runat="server" />
</asp:Content>

