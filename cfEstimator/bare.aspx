﻿<%@ page language="C#" masterpagefile="~/cfestimator.master" autoeventwireup="true" CodeBehind="bare.aspx.cs" Inherits="cfEstimator.bare" title="Bare Units" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script type="text/javascript">
function ShowNote(id) {
  GetNotes(id);
}

function OpenModelPopup(mytb)
{
	document.getElementById ('dd').style.display='none';
    document.getElementById ('ModalPopupDiv').style.visibility='visible';
    document.getElementById ('ModalPopupDiv').style.display='';
    document.getElementById ('ModalPopupDiv').style.top= Math.round ((document.documentElement.clientHeight/2)+ document.documentElement.scrollTop)-100 + 'px';
    document.getElementById ('ModalPopupDiv').style.left=Math.round ((document.documentElement.clientWidth/2)+ document.documentElement.scrollLeft)-150 + 'px';

    document.getElementById ('MaskedDiv').style.display='';
    document.getElementById ('MaskedDiv').style.visibility='visible';
    document.getElementById ('MaskedDiv').style.top='-70px';
    document.getElementById ('MaskedDiv').style.left='-10px';
    document.getElementById ('MaskedDiv').style.width=  document.documentElement.clientWidth + 'px';
    document.getElementById ('MaskedDiv').style.height= document.documentElement.clientHeight+ 'px';
    //document.getElementById (mytb).focus();
}
function CloseModalPopup()
{
	document.getElementById ('dd').style.display='';
    document.getElementById ('MaskedDiv').style.display='none';
    document.getElementById ('ModalPopupDiv').style.display='none';
}

//AJAX calls
//Include this method if you want to do something with the response from your AJAX call
function processResponse(resp)
{
  if (resp != "")
  {
      document.getElementById("ModalMsg").innerHTML = resp;
      OpenModelPopup();
  }
}

function GetNotes(id)
{
  sndReq("fn=n&id=" + id);
}
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h2>Bare Units</h2>
<div class="headerFilters">
<div id="dd">Select a Frame: 
<asp:DropDownList ID="ddFrame" runat="server" AutoPostBack="true"
    onselectedindexchanged="ddFrame_SelectedIndexChanged" DataTextField="Name" DataValueField="FrameID">
<asp:ListItem Selected="True" Text="Select..." Value="-1" />
</asp:DropDownList></div>
</div>
<div style="text-align: center"><asp:Label runat="server" ID="lblMsg" CssClass="infoMsg" EnableViewState="false"></asp:Label></div>
    <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="false" 
    CssClass="productTable" CellPadding="0" CellSpacing="0" 
    HeaderStyle-CssClass="productTableHeader" 
    EmptyDataText="No products match that criteria">
    <Columns>
    <asp:TemplateField HeaderText="Part #">
    <ItemTemplate>
        <%# DataBinder.Eval(Container.DataItem, "HFPartNumber") %><a style="text-decoration: none" href="javascript:ShowNote('<%# DataBinder.Eval(Container.DataItem, "ProductID") %>')"><%# DataBinder.Eval(Container.DataItem, "isnote") %></a>
    </ItemTemplate>
    </asp:TemplateField>
    <asp:BoundField DataField="Description" HeaderText="Description" />
    <asp:BoundField DataField="Stages" HeaderText="Stages" />
    <asp:BoundField DataField="Size" HeaderText="Size" />
    <asp:TemplateField>
        <HeaderTemplate>Ref Sell</HeaderTemplate>
        <ItemTemplate>
            <a href="estimate.aspx?pn=<%# DataBinder.Eval(Container.DataItem, "HFPartNumber") %>"><%# DataBinder.Eval(Container.DataItem, "ListPrice", "{0:c}")%></a>
        </ItemTemplate>
    </asp:TemplateField>
    </Columns>
    </asp:GridView>
    
<div id="MaskedDiv" class="MaskedDiv">
</div>
<div id="ModalPopupDiv" class="ModalPopup">
  <div id="ModalMsg">My Message</div>
  <p style="text-align: center">
    <a href="javascript:CloseModalPopup();">Close</a>
  </p>
</div>
</asp:Content>


