﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace cfEstimator
{
    public class error : Page, IRequiresSessionState
    {
        // Fields
        protected Literal litMsg;
        protected LinkButton lbStartOver;

        // Methods
        protected void lbStartOver_Click(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            base.Response.Redirect("login.aspx");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (base.Request.QueryString["cd"] == null)
            {
                this.litMsg.Text = "An error has occurred!";
            }
            else
            {
                string str;
                if (((str = base.Request.QueryString["cd"].ToLower()) == null) || (str != "unauth"))
                {
                    this.litMsg.Text = "An error has occurred!";
                }
                else
                {
                    this.litMsg.Text = "You are unauthorized to view the page you are trying to access!";
                }
            }
        }

        // Properties
        protected DefaultProfile Profile =>
            ((DefaultProfile)this.Context.Profile);

        protected global_asax ApplicationInstance =>
            ((global_asax)this.Context.ApplicationInstance);
    }
}