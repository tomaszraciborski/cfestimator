﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using static cfEstimator.App_Code;

namespace cfEstimator
{
    public class admMngData : Page, IRequiresSessionState
    {
        // Fields
        protected Label lblMsg;
        protected FileUpload fuProducts;
        protected Button btUploadProducts;

        // Methods
        protected void btUploadProducts_Click(object sender, EventArgs e)
        {
            if (this.fuProducts.PostedFile.FileName != string.Empty)
            {
                string[] strArray = this.fuProducts.FileName.Split(new char[] { '.' });
                if (strArray[strArray.Length - 1].ToLower() == "csv")
                {
                    string[] strArray3;
                    CSVReader reader = new CSVReader(this.fuProducts.PostedFile.InputStream);
                    DataTable table = new DataTable();
                    foreach (string str2 in reader.GetCSVLine())
                    {
                        table.Columns.Add(str2);
                    }
                    int num = 0;
                    while ((strArray3 = reader.GetCSVLine()) != null)
                    {
                        table.Rows.Add(strArray3);
                        table.Rows[num]["HFPARTNUMBER"].ToString();
                        table.Rows[num]["DESCRIPTION"].ToString();
                        table.Rows[num]["SIZE"].ToString();
                        table.Rows[num]["LISTPRICE"].ToString();
                        table.Rows[num]["PRCODE"].ToString();
                        table.Rows[num]["WEIGHT"].ToString();
                        table.Rows[num]["COSTPRICE"].ToString();
                        num++;
                        if (num > 8)
                        {
                            return;
                        }
                    }
                }
                else
                {
                    this.lblMsg.Text = "Data file must be a CSV file!";
                    this.lblMsg.Style["color"] = "red";
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        // Properties
        protected DefaultProfile Profile =>
            ((DefaultProfile)this.Context.Profile);

        protected global_asax ApplicationInstance =>
            ((global_asax)this.Context.ApplicationInstance);
    }
}