﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using static cfEstimator.App_Code;

namespace cfEstimator
{
    public class Login : Page, IRequiresSessionState
    {
        // Fields
        private const int MAX_LOGIN_TRIES = 5;
        protected TextBox tbUsername;
        protected TextBox tbPassword;
        protected ImageButton ibLogin;
        protected CheckBox cbRememberMe;
        protected Label lblFailureText;

        // Methods
        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            this.Session["loginAttempts"] = (this.Session["loginAttempts"] != null) ? (((int)this.Session["loginAttempts"]) + 1) : 1;
            if (((int)this.Session["loginAttempts"]) > 5)
            {
                this.lblFailureText.Text = "No more login attempts allowed!<br />You have been locked out of the system.";
                this.tbUsername.Enabled = false;
                this.tbPassword.Enabled = false;
                this.ibLogin.Enabled = false;
            }
            else if (new data().IsAuthenticated(this.tbUsername.Text, this.tbPassword.Text) > 0)
            {
                FormsAuthentication.RedirectFromLoginPage(this.tbUsername.Text, this.cbRememberMe.Checked);
            }
            else
            {
                this.lblFailureText.Text = "Invalid username and/or password.<br />Please try to login again.";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (!this.Page.IsPostBack)
                {
                    if (!util.TestDBConnection(ConfigurationManager.ConnectionStrings["connStr"].ToString()))
                    {
                        this.lblFailureText.Text = "Unable to connect to database!";
                    }
                    ((HtmlGenericControl)base.Master.FindControl("MasterPageBodyTag")).Attributes.Add("onload", "document.getElementById('" + this.tbUsername.ClientID + "').focus();");
                }
            }
            catch
            {

            }
        }

        // Properties
        protected DefaultProfile Profile =>
            ((DefaultProfile)this.Context.Profile);

        protected global_asax ApplicationInstance =>
            ((global_asax)this.Context.ApplicationInstance);
    }
}