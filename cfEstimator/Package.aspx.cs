﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using static cfEstimator.App_Code;

namespace cfEstimator
{
    public class Package : Page, IRequiresSessionState
    {
        // Fields
        protected Label lblMsg;
        protected DropDownList ddFrame;
        protected DropDownList ddStages;
        protected TextBox tbUnits;
        protected DropDownList ddHP;
        protected DropDownList ddFlow;
        protected Button btGet;
        protected Button btFilter;
        protected Button btClear;
        protected HtmlGenericControl divPkgFilters;
        protected Label lblFilters;
        protected Literal litAccessoryList;
        protected HtmlTableCell tdAccessoryList;
        protected HtmlGenericControl productList;
        protected HtmlImage diagram;
        protected HtmlTableCell tdDiagram;
        protected LinkButton lbAddMini;
        protected GridView gvEstimate;
        protected HiddenField hdProdList;

        // Methods
        protected void AddPackage(int FrameConfigID, int Stages, int NumItems, double PowerHP, double FlowCFM)
        {
            if (this.ddStages.Items.FindByValue(Stages.ToString()) != null)
            {
                this.ddStages.ClearSelection();
                this.ddStages.Items.FindByValue(Stages.ToString()).Selected = true;
            }
            if (this.ddHP.Items.FindByValue(int.Parse(PowerHP.ToString()).ToString()) != null)
            {
                this.ddHP.ClearSelection();
                this.ddHP.Items.FindByValue(int.Parse(PowerHP.ToString()).ToString()).Selected = true;
            }
            if (this.ddFlow.Items.FindByValue(int.Parse(FlowCFM.ToString()).ToString()) != null)
            {
                this.ddFlow.ClearSelection();
                this.ddFlow.Items.FindByValue(int.Parse(FlowCFM.ToString()).ToString()).Selected = true;
            }
            foreach (DataRow row in new data().GetDefaultPackage(FrameConfigID, Stages, this.Session["BrandAccess"].ToString(), PowerHP, FlowCFM).Tables[0].Rows)
            {
                if (row.IsNull("DefaultNumber"))
                {
                    this.AddRow(row["HFPartNumber"].ToString(), NumItems);
                    continue;
                }
                this.AddRow(row["HFPartNumber"].ToString(), int.Parse(row["DefaultNumber"].ToString()) * NumItems);
            }
        }

        protected void AddRow(string PartNum, int Qty)
        {
            DataTable table = (DataTable)this.Session["miniestimate"];
            DataRow row = table.NewRow();
            DataRow row2 = new data().GetProductsWithDiscount(PartNum, this.Session["DiscountCode"].ToString(), this.Session["BrandAccess"].ToString()).Tables[0].Rows[0];
            row["RowID"] = table.Rows.Count;
            if (!row2.IsNull("Weight"))
            {
                row["SingleWeight"] = double.Parse(row2["Weight"].ToString());
                row["Weight"] = Qty * double.Parse(row2["Weight"].ToString());
            }
            row["PartNo"] = row2["HFPartNumber"];
            row["Quantity"] = Qty;
            row["Description"] = (row2["Size"].ToString().Trim() != "") ? (row2["Description"] + " - " + row2["Size"]) : row2["Description"];
            row["Cost"] = row2["CostPrice"];
            row["RefSell"] = row2["ListPrice"];
            row["UnitSell"] = double.Parse(row2["ListPrice"].ToString()) * double.Parse(row2["Multiplier"].ToString());
            row["Discount"] = row2["Multiplier"];
            row["PriceCode"] = row2["PriceCode"];
            row["Total"] = (Qty * double.Parse(row2["ListPrice"].ToString())) * double.Parse(row2["Multiplier"].ToString());
            row["TotalNonDisc"] = Qty * double.Parse(row2["ListPrice"].ToString());
            row["Commission"] = row2["Commission"];
            row["CommissionAmount"] = double.Parse(row2["Commission"].ToString()) * double.Parse(row["Total"].ToString());
            table.Rows.InsertAt(row, table.Rows.Count);
            this.Session["miniestimate"] = table;
        }

        protected void BindGrid()
        {
            DataTable table = (DataTable)this.Session["miniestimate"];
            this.gvEstimate.DataSource = table;
            this.gvEstimate.DataBind();
            double num = 0.0;
            foreach (DataRow row in table.Rows)
            {
                num = 0.0;
                if (!row.IsNull("SingleWeight"))
                {
                    num = (double)row["SingleWeight"];
                }
                if (!this.hdProdList.Value.Contains("|" + row["PartNo"] + "`"))
                {
                    object[] objArray = new object[] { this.hdProdList.Value, row["PartNo"], "`", num, "`", row["RefSell"], "|" };
                    this.hdProdList.Value = string.Concat(objArray);
                }
            }
        }

        protected void btClear_Click(object sender, EventArgs e)
        {
            base.Response.Redirect("package.aspx?clear=0&type=" + base.Request.QueryString["type"] + "&sf=true");
        }

        protected void btFilter_Click(object sender, EventArgs e)
        {
            string[] strArray = new string[] { "package.aspx?fcid=", this.ddFrame.SelectedValue, "&st=", this.ddStages.SelectedValue, "&hp=", this.ddHP.SelectedValue, "&f=", this.ddFlow.SelectedValue, "&sf=", "", "", "" };
            strArray[9] = base.Request.QueryString["sf"];
            strArray[10] = "&type=";
            strArray[11] = base.Request.QueryString["type"];
            base.Response.Redirect(string.Concat(strArray));
        }

        protected void btGet_Click(object sender, EventArgs e)
        {
            string[] strArray = new string[] 
            {
                "package.aspx?fn=add&fcid=",
                this.ddFrame.SelectedValue,
                "&st=",
                this.ddStages.SelectedValue,
                "&num=",
                this.tbUnits.Text,
                "&hp=",
                this.ddHP.SelectedValue,
                "&f=",
                this.ddFlow.SelectedValue,
                "&sf=",
                base.Request.QueryString["sf"],
                "&type=",
                base.Request.QueryString["type"]
            };
            
            /*
            strArray[9] = this.ddFlow.SelectedValue;
            strArray[10] = "&sf=";
            strArray[11] = base.Request.QueryString["sf"];
            strArray[12] = "&type=";
            strArray[13] = base.Request.QueryString["type"];
            */
            base.Response.Redirect(string.Concat(strArray));
        }

        protected void ClearPage()
        {
            this.Session["miniestimate"] = null;
            this.Session["productList"] = null;
        }

        protected void ddFrame_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddFrame.SelectedIndex > 0)
            {
                this.SelectFrame(int.Parse(this.ddFrame.SelectedValue));
            }
            else
            {
                this.ddStages.Enabled = false;
                this.tbUnits.Enabled = false;
                this.ddHP.Enabled = false;
                this.ddFlow.Enabled = false;
                this.btGet.Enabled = false;
                this.btFilter.Enabled = false;
            }
        }

        protected void ddStages_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddStages.SelectedIndex <= 0)
            {
                this.tbUnits.Enabled = false;
                this.ddHP.Enabled = false;
                this.ddFlow.Enabled = false;
                this.btGet.Enabled = false;
                this.btFilter.Enabled = false;
            }
            else
            {
                this.LoadPowerHP();
                this.LoadFlow();
                this.tbUnits.Enabled = true;
                this.ddHP.Enabled = true;
                this.ddFlow.Enabled = true;
                this.btGet.Enabled = true;
                this.btFilter.Enabled = true;
            }
        }

        protected void lbAddMini_Click(object sender, EventArgs e)
        {
            DataTable table1 = (DataTable)this.Session["miniestimate"];
            base.Response.Redirect("estimate.aspx?addmini=true");
        }

        protected void LoadCategories(int FrameConfigID, int Stages, double PowerHP, double FlowCFM, bool ShowFilters)
        {
            this.litAccessoryList.Text = "";
            DataSet frameConfigCategories = new data().GetFrameConfigCategories(FrameConfigID);
            if (frameConfigCategories.Tables.Count > 0)
            {
                foreach (DataRow row in frameConfigCategories.Tables[0].Rows)
                {
                    object text = this.litAccessoryList.Text;
                    object[] objArray = new object[] 
                    {
                        text,
                        "<tr><td nowrap><a href=\"?c=",
                        row["CategoryID"],
                        "&type=",
                        base.Request.QueryString["type"],
                        "&fcid=",
                        FrameConfigID,
                        "&st=",
                        Stages,
                        "&hp=",
                        PowerHP,
                        "&f=",
                        FlowCFM,
                        "&sf=",
                        ShowFilters.ToString(),
                        "\"> > ",
                        row["DisplayName"],
                        "</a></td></tr>\n"
                    };
                    /*
                    objArray[9] = "&hp=";
                    objArray[10] = PowerHP;
                    objArray[11] = "&f=";
                    objArray[12] = FlowCFM;
                    objArray[13] = "&sf=";
                    objArray[14] = ShowFilters.ToString();
                    objArray[15] = "\"> > ";
                    objArray[0x10] = row["DisplayName"];
                    objArray[0x11] = "</a></td></tr>\n";
                    */
                    this.litAccessoryList.Text = string.Concat(objArray);
                }
                object[] objArray2 = new object[] 
                {
                    this.litAccessoryList.Text,
                    "<tr><td nowrap><a href=\"buyout.aspx?s=miniestimate&type=",
                    base.Request.QueryString["type"],
                    "&fcid=",
                    FrameConfigID,
                    "&st=",
                    Stages,
                    "&hp=",
                    PowerHP,
                    "&f=",
                    FlowCFM,
                    "&sf=",
                    ShowFilters.ToString(),
                    "\"> > Buy-Out Item</a></td></tr>\n"
                };
                /*
                objArray2[9] = "&f=";
                objArray2[10] = FlowCFM;
                objArray2[11] = "&sf=";
                objArray2[12] = ShowFilters.ToString();
                objArray2[13] = "\"> > Buy-Out Item</a></td></tr>\n";
                */
                this.litAccessoryList.Text = string.Concat(objArray2);
            }
        }

        protected void LoadFlow()
        {
            DataSet set = new data().GetFlowCFMByFrameConfigCategoryProducts(int.Parse(this.ddFrame.SelectedValue), int.Parse(this.ddStages.SelectedValue), this.Session["BrandAccess"].ToString());
            set.Tables[0].Columns.Add(new DataColumn("Description", Type.GetType("System.String")));
            string str = "";
            double a = 0.0;
            double num2 = 0.0;
            foreach (DataRow row in set.Tables[0].Rows)
            {
                a = (double)row["FlowCFM"];
                num2 = a * 1.699;
                str = Math.Round(a).ToString() + " CFM (" + Math.Round(num2).ToString() + " m3HR)";
                row["Description"] = str;
            }
            set.AcceptChanges();
            this.ddFlow.DataSource = set;
            this.ddFlow.DataBind();
            this.ddFlow.Items.Insert(0, new ListItem("Choose below...", "-1"));
        }

        protected void LoadFrames(string FrameConfigType)
        {
            DataSet framesByType = new data().GetFramesByType(FrameConfigType, this.Session["BrandAccess"].ToString());
            framesByType.Tables[0].Columns.Add(new DataColumn("PaddedName", Type.GetType("System.String")));
            string s = "";
            foreach (DataRow row in framesByType.Tables[0].Rows)
            {
                s = row["Name"].ToString();
                try
                {
                    int.Parse(s);
                    s = s.PadLeft(50, '0');
                }
                catch
                {
                }
                row["PaddedName"] = s;
            }
            framesByType.AcceptChanges();
            DataView defaultView = framesByType.Tables[0].DefaultView;
            defaultView.Sort = "PaddedName";
            this.ddFrame.DataSource = defaultView;
            this.ddFrame.DataBind();
            this.ddFrame.Items.Insert(0, new ListItem("Select...", "-1"));
        }

        protected void LoadPowerHP()
        {
            DataSet set = new data().GetPowerHPByFrameConfigCategoryProducts(int.Parse(this.ddFrame.SelectedValue), int.Parse(this.ddStages.SelectedValue), this.Session["BrandAccess"].ToString());
            set.Tables[0].Columns.Add(new DataColumn("Description", Type.GetType("System.String")));
            string str = "";
            double a = 0.0;
            double num2 = 0.0;
            foreach (DataRow row in set.Tables[0].Rows)
            {
                a = (double)row["PowerHP"];
                num2 = a * 0.746;
                str = Math.Round(a).ToString() + " HP (" + Math.Round(num2).ToString() + " KW)";
                row["Description"] = str;
            }
            set.AcceptChanges();
            this.ddHP.DataSource = set;
            this.ddHP.DataBind();
            this.ddHP.Items.Insert(0, new ListItem("Choose below...", "-1"));
        }

        protected void LoadProducts(int CategoryID, int Stages, double PowerHP, double FlowCFM, bool ShowFilters)
        {
            this.tdDiagram.Visible = false;
            data data = new data();
            int frameIDByFrameConfigID = data.GetFrameIDByFrameConfigID(int.Parse(base.Request.QueryString["fcid"]));
            string str = "<table class=\"productTable\" cellpadding=0 cellspacing=0>\n" + "<tr class=\"productTableHeader\"><th>Part #</th><th>Description</th><th>Ref Sell</th></tr>\n";
            string str2 = "";
            string str3 = "";
            foreach (DataRow row in data.GetProductsByCategory(CategoryID, this.Session["BrandAccess"].ToString(), frameIDByFrameConfigID, Stages, PowerHP, FlowCFM).Tables[0].Rows)
            {
                if (row["DisplayName"].ToString() != str2)
                {
                    object[] objArray = new object[] { str, "<tr><th colspan=\"3\">", row["DisplayName"], "</th></tr>\n" };
                    str = string.Concat(objArray);
                    str2 = row["DisplayName"].ToString();
                }
                if (!row.IsNull("Size") && (row["Size"].ToString().Trim() != ""))
                {
                    str3 = " - " + row["Size"];
                }
                str = str + "<tr>\n";
                if (row.IsNull("NoteID"))
                {
                    object[] objArray2 = new object[] { str, "<td>", row["HFPartNumber"], "</td>\n" };
                    str = string.Concat(objArray2);
                }
                else
                {
                    object[] objArray3 = new object[] { str, "<td>", row["HFPartNumber"], "<a style=\"text-decoration: none\" href=\"javascript:ShowNote('", row["ProductID"], "')\">*</a></td>\n" };
                    str = string.Concat(objArray3);
                }
                object[] objArray4 = new object[] { str, "<td>", row["Description"], str3, "</td>\n" };
                object[] objArray5 = new object[] {
                    string.Concat(objArray4),
                    "<td><a href=\"?pn=",
                    row["HFPartNumber"],
                    "&type=",
                    base.Request.QueryString["type"],
                    "&fcid=",
                    base.Request.QueryString["fcid"],
                    "&st=",
                    Stages,
                    "", "", "", "", "", "", "", "", "", "" };
                objArray5[9] = "&hp=";
                objArray5[10] = PowerHP;
                objArray5[11] = "&f=";
                objArray5[12] = FlowCFM;
                objArray5[13] = "&sf=";
                objArray5[14] = ShowFilters.ToString();
                objArray5[15] = "\">";
                objArray5[0x10] = $"{row["ListPrice"]:c}";
                objArray5[0x11] = "</a></td>\n";
                str = string.Concat(objArray5) + "</tr>\n";
                str3 = "";
            }
            str = str + "</table>\n";
            this.productList.InnerHtml = str;
            this.Session["productList"] = str;
        }

        protected void LoadStages(int FrameConfigID)
        {
            data data = new data();
            string[] columnNames = new string[] { "Stages" };
            DataTable table = data.GetFrameProducts(data.GetFrameIDByFrameConfigID(int.Parse(base.Request.QueryString["fcid"])), this.Session["BrandAccess"].ToString()).Tables[0].DefaultView.ToTable(true, columnNames);
            foreach (DataRow row in table.Rows)
            {
                if (row["Stages"].ToString() == "")
                {
                    row.Delete();
                    break;
                }
            }
            this.ddStages.DataSource = table;
            this.ddStages.DataBind();
            this.ddStages.Items.Insert(0, new ListItem("Select...", "-1"));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            util.CheckSession();
            string script = ((("" + "var gvEstimate = '" + this.gvEstimate.ClientID + "';\n") + "var hdProdList = '" + this.hdProdList.ClientID + "';\n") + "var wtCol = 1;\n" + "var partNoCol = 2;\n") + "var refSellCol = -2;\n" + "var totalCol = -1;\n";
            this.Page.ClientScript.RegisterClientScriptBlock(base.GetType(), "mycustomscript", script, true);
            if (!this.Page.IsPostBack)
            {
                if (this.Session["DiscountCode"] == null)
                {
                    DataSet discounts = new data().GetDiscounts("", (int)this.Session["MaxDiscountRank"]);
                    this.Session["DiscountCode"] = discounts.Tables[0].Rows[0]["DiscountCode"];
                }
                if (base.Request.QueryString["type"] != null)
                {
                    this.LoadFrames(base.Request.QueryString["type"]);
                }
                if (base.Request.QueryString["clear"] != null)
                {
                    this.tdAccessoryList.Visible = false;
                    this.ClearPage();
                }
                else
                {
                    if (this.Session["miniestimate"] == null)
                    {
                        this.Session["miniestimate"] = util.GetEmptyTable();
                    }
                    if (base.Request.QueryString["fcid"] == null)
                    {
                        this.tdAccessoryList.Visible = false;
                    }
                    else
                    {
                        this.SetFrame(int.Parse(base.Request.QueryString["fcid"]), int.Parse(base.Request.QueryString["st"]), double.Parse(base.Request.QueryString["hp"]), double.Parse(base.Request.QueryString["f"]), bool.Parse(base.Request.QueryString["sf"]));
                        if ((base.Request.QueryString["fn"] != null) && (base.Request.QueryString["fn"] == "add"))
                        {
                            this.Session["productList"] = null;
                            this.AddPackage(int.Parse(base.Request.QueryString["fcid"]), int.Parse(base.Request.QueryString["st"]), int.Parse(base.Request.QueryString["num"]), double.Parse(base.Request.QueryString["hp"]), double.Parse(base.Request.QueryString["f"]));
                        }
                        if (base.Request.QueryString["pn"] != null)
                        {
                            this.AddRow(base.Request.QueryString["pn"], 1);
                        }
                        if (base.Request.QueryString["c"] != null)
                        {
                            this.LoadProducts(int.Parse(base.Request.QueryString["c"]), int.Parse(base.Request.QueryString["st"]), double.Parse(base.Request.QueryString["hp"]), double.Parse(base.Request.QueryString["f"]), bool.Parse(base.Request.QueryString["sf"]));
                        }
                        else if (this.Session["productList"] != null)
                        {
                            this.tdDiagram.Visible = false;
                            this.productList.InnerHtml = this.Session["productList"].ToString();
                        }
                    }
                    this.BindGrid();
                }
                if (!bool.Parse(base.Request.QueryString["sf"]))
                {
                    this.divPkgFilters.Visible = false;
                    this.diagram.Src = "img/exhausterDiagram.gif";
                    if ((base.Request.QueryString["fcid"] == null) && (this.ddFrame.Items.Count > 1))
                    {
                        this.ddFrame.ClearSelection();
                        this.ddFrame.SelectedIndex = 1;
                        this.SelectFrame(int.Parse(this.ddFrame.SelectedValue));
                    }
                    this.tdAccessoryList.Visible = true;
                }
            }
        }

        protected void SelectFrame(int FrameConfigID)
        {
            string[] strArray = new string[] { "package.aspx?fcid=", FrameConfigID.ToString(), "&type=", base.Request.QueryString["type"], "&st=-1&hp=-1&f=-1&sf=", base.Request.QueryString["sf"] };
            base.Response.Redirect(string.Concat(strArray));
        }

        protected void SetFrame(int FrameConfigID, int Stages, double PowerHP, double FlowCFM, bool ShowFilters)
        {
            if (this.ddFrame.Items.FindByValue(FrameConfigID.ToString()) != null)
            {
                this.ddFrame.ClearSelection();
                this.ddFrame.Items.FindByValue(FrameConfigID.ToString()).Selected = true;
                this.lblFilters.Text = "Frame: " + this.ddFrame.SelectedItem.Text;
                if (Stages != -1)
                {
                    this.lblFilters.Text = this.lblFilters.Text + ", Stages: " + Stages.ToString();
                }
                if (PowerHP != -1.0)
                {
                    this.lblFilters.Text = this.lblFilters.Text + ", Max HP: " + PowerHP.ToString();
                }
                if (FlowCFM != -1.0)
                {
                    this.lblFilters.Text = this.lblFilters.Text + ", Max Flow: " + FlowCFM.ToString() + " CFM";
                }
            }
            this.LoadCategories(FrameConfigID, Stages, PowerHP, FlowCFM, ShowFilters);
            this.LoadStages(FrameConfigID);
            if (this.ddStages.Items.Count > 1)
            {
                this.ddStages.Enabled = true;
            }
        }

        // Properties
        protected DefaultProfile Profile =>
            ((DefaultProfile)this.Context.Profile);

        protected global_asax ApplicationInstance =>
            ((global_asax)this.Context.ApplicationInstance);
    }
}