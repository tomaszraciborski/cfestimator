﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using static cfEstimator.App_Code;

namespace cfEstimator
{   
    public class buyout : Page, IRequiresSessionState
    {
        // Fields
        protected Label lblMsg;
        protected ValidationSummary ValidationSummary1;
        protected TextBox tbDescription;
        protected RequiredFieldValidator RequiredFieldValidator2;
        protected TextBox tbCost;
        protected RequiredFieldValidator RequiredFieldValidator1;
        protected RegularExpressionValidator RegularExpressionValidator1;
        protected HtmlTableRow trCost;
        protected TextBox tbSellPrice;
        protected RequiredFieldValidator RequiredFieldValidator4;
        protected RegularExpressionValidator RegularExpressionValidator3;
        protected HtmlTableRow trSellPrice;
        protected TextBox tbWeight;
        protected TextBox tbQty;
        protected RequiredFieldValidator RequiredFieldValidator3;
        protected RegularExpressionValidator RegularExpressionValidator2;
        protected DropDownList ddPriceCodes;
        protected CustomValidator CustomValidator1;
        protected TextBox tbMarkup;
        protected CustomValidator CustomValidator2;
        protected HtmlTableRow trMarkup;
        protected TextBox tbCommission;
        protected CustomValidator CustomValidator3;
        protected PlaceHolder phCustom;
        protected Button btAdd;
        protected Button btCancel;

        // Methods
        protected void btAdd_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                string str = "";
                if (base.Request.QueryString["s"] != null)
                {
                    DataSet discounts;
                    double num;
                    double num2;
                    double num3;
                    str = base.Request.QueryString["s"];
                    if (this.Session[str] == null)
                    {
                        this.Session[str] = util.GetEmptyTable();
                    }
                    DataTable table = (DataTable)this.Session[str];
                    data data = new data();
                    if (this.Session["DiscountCode"] == null)
                    {
                        discounts = data.GetDiscounts("", (int)this.Session["MaxDiscountRank"]);
                        this.Session["DiscountCode"] = discounts.Tables[0].Rows[0]["DiscountCode"];
                    }
                    DataRow row = table.NewRow();
                    row["RowID"] = table.Rows.Count;
                    if (this.ddPriceCodes.SelectedValue == "_c")
                    {
                        num = 1.0;
                        num2 = double.Parse(this.tbCommission.Text) / 100.0;
                        num3 = double.Parse(this.tbMarkup.Text);
                        row["PriceCode"] = num3.ToString() + " M";
                    }
                    else
                    {
                        discounts = data.GetMultiplierAndDiscount(this.ddPriceCodes.SelectedValue, this.Session["DiscountCode"].ToString());
                        num = (double)discounts.Tables[0].Rows[0]["Multiplier"];
                        num2 = (double)discounts.Tables[0].Rows[0]["Commission"];
                        num3 = (double)discounts.Tables[0].Rows[0]["DefaultMarkup"];
                        row["PriceCode"] = this.ddPriceCodes.SelectedValue;
                    }
                    double num4 = 0.0;
                    double num5 = 0.0;
                    if (this.trSellPrice.Visible)
                    {
                        num5 = double.Parse(this.tbSellPrice.Text);
                        num4 = num5 / num3;
                    }
                    else
                    {
                        num4 = double.Parse(this.tbCost.Text);
                        num5 = num4 * num3;
                    }
                    int num6 = int.Parse(this.tbQty.Text);
                    if (this.tbWeight.Text != "")
                    {
                        row["SingleWeight"] = double.Parse(this.tbWeight.Text);
                        row["Weight"] = num6 * double.Parse(this.tbWeight.Text);
                    }
                    row["PartNo"] = "";
                    row["Quantity"] = num6;
                    row["Description"] = this.tbDescription.Text;
                    row["Cost"] = num4;
                    row["RefSell"] = num5;
                    row["UnitSell"] = num5 * num;
                    row["Discount"] = num;
                    row["Total"] = (num6 * num5) * num;
                    row["TotalNonDisc"] = num6 * num5;
                    row["Commission"] = num2;
                    row["CommissionAmount"] = num2 * double.Parse(row["Total"].ToString());
                    table.Rows.InsertAt(row, table.Rows.Count);
                    this.Session[str] = table;
                    if (str == "miniestimate")
                    {
                        base.Response.Redirect("package.aspx?" + base.Request.Url.Query.Substring(base.Request.Url.Query.IndexOf("type=")));
                    }
                    else
                    {
                        base.Response.Redirect("estimate.aspx");
                    }
                }
            }
        }

        protected void btCancel_Click(object sender, EventArgs e)
        {
            if (base.Request.QueryString["s"] != null)
            {
                if (base.Request.QueryString["s"] == "miniestimate")
                {
                    base.Response.Redirect("package.aspx?" + base.Request.Url.Query.Substring(base.Request.Url.Query.IndexOf("type=")));
                }
                else
                {
                    base.Response.Redirect("estimate.aspx");
                }
            }
        }

        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {
            bool flag = true;
            if (this.ddPriceCodes.SelectedIndex == 0)
            {
                flag = false;
            }
            args.IsValid = flag;
        }

        protected void CustomValidator2_ServerValidate(object source, ServerValidateEventArgs args)
        {
            bool flag = true;
            if (this.ddPriceCodes.SelectedValue == "_c")
            {
                if (this.tbMarkup.Text.Trim() == "")
                {
                    flag = false;
                }
                else
                {
                    try
                    {
                        if (double.Parse(this.tbMarkup.Text) == 0.0)
                        {
                            flag = false;
                        }
                    }
                    catch
                    {
                        flag = false;
                    }
                }
            }
            args.IsValid = flag;
        }

        protected void CustomValidator3_ServerValidate(object source, ServerValidateEventArgs args)
        {
            bool flag = true;
            if (this.ddPriceCodes.SelectedValue == "_c")
            {
                if (this.tbCommission.Text.Trim() == "")
                {
                    flag = false;
                }
                else
                {
                    try
                    {
                        double.Parse(this.tbCommission.Text);
                    }
                    catch
                    {
                        flag = false;
                    }
                }
            }
            args.IsValid = flag;
        }

        protected void ddPriceCodes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddPriceCodes.SelectedValue == "_c")
            {
                this.phCustom.Visible = true;
            }
            else
            {
                this.phCustom.Visible = false;
            }
        }

        protected void LoadPriceCodes()
        {
            DataSet priceCodes = new data().GetPriceCodes("", true);
            this.ddPriceCodes.DataSource = priceCodes;
            this.ddPriceCodes.DataBind();
            this.ddPriceCodes.Items.Insert(0, new ListItem("(Enter Custom Mark-up)", "_c"));
            this.ddPriceCodes.Items.Insert(0, new ListItem("Select one...", "0"));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            util.CheckSession();
            if (!this.Page.IsPostBack)
            {
                if (((this.Session["IsAdmin"] != null) && ((bool)this.Session["IsAdmin"])) || ((this.Session["ISGDEmployee"] != null) && ((bool)this.Session["ISGDEmployee"])))
                {
                    this.trSellPrice.Visible = false;
                    this.tbSellPrice.Text = "0";
                }
                else
                {
                    this.trCost.Visible = false;
                    this.tbCost.Text = "0";
                    this.trMarkup.Visible = false;
                }
                this.LoadPriceCodes();
            }
        }

        // Properties
        protected DefaultProfile Profile =>
            ((DefaultProfile)this.Context.Profile);

        protected global_asax ApplicationInstance =>
            ((global_asax)this.Context.ApplicationInstance);
    }
}