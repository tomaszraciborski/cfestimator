﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net.Mail;
using System.Web;
using System.Web.Security;


namespace cfEstimator
{
    public class App_Code : IHttpHandler
    {
        /// <summary>
        /// You will need to configure this handler in the Web.config file of your 
        /// web and register it with IIS before being able to use it. For more information
        /// see the following link: https://go.microsoft.com/?linkid=8101007
        /// </summary>
        #region IHttpHandler Members

        public bool IsReusable
        {
            // Return false in case your Managed Handler cannot be reused for another request.
            // Usually this would be false in case you have some state information preserved per request.
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            //write your handler implementation here.
        }

        #endregion

        public class CSVReader
        {
            // Fields
            private Stream objStream;
            private StreamReader objReader;

            // Methods
            public CSVReader(Stream filestream) : this(filestream, null)
            {
            }

            public CSVReader(Stream filestream, System.Text.Encoding enc)
            {
                this.objStream = filestream;
                if (filestream.CanRead)
                {
                    this.objReader = (enc != null) ? new StreamReader(filestream, enc) : new StreamReader(filestream);
                }
            }

            public string[] GetCSVLine()
            {
                string data = this.objReader.ReadLine();
                if (data == null)
                {
                    return null;
                }
                if (data.Length == 0)
                {
                    return new string[0];
                }
                ArrayList result = new ArrayList();
                this.ParseCSVData(result, data);
                return (string[])result.ToArray(typeof(string));
            }

            private int GetSingleQuote(string data, int SFrom)
            {
                int num = SFrom - 1;
                while (++num < data.Length)
                {
                    if (data[num] != '"')
                    {
                        continue;
                    }
                    if ((num >= (data.Length - 1)) || (data[num + 1] != '"'))
                    {
                        return num;
                    }
                    num++;
                }
                return -1;
            }

            private void ParseCSVData(ArrayList result, string data)
            {
                int startSeperatorPos = -1;
                while (startSeperatorPos < data.Length)
                {
                    result.Add(this.ParseCSVField(ref data, ref startSeperatorPos));
                }
            }

            private string ParseCSVField(ref string data, ref int StartSeperatorPos)
            {
                if (StartSeperatorPos == (data.Length - 1))
                {
                    StartSeperatorPos++;
                    return "";
                }
                int startIndex = StartSeperatorPos + 1;
                if (data[startIndex] != '"')
                {
                    int index = data.IndexOf(',', startIndex);
                    if (index == -1)
                    {
                        StartSeperatorPos = data.Length;
                        return data.Substring(startIndex);
                    }
                    StartSeperatorPos = index;
                    return data.Substring(startIndex, index - startIndex);
                }
                int singleQuote = this.GetSingleQuote(data, startIndex + 1);
                int num3 = 1;
                while (singleQuote == -1)
                {
                    data = data + "\n" + this.objReader.ReadLine();
                    singleQuote = this.GetSingleQuote(data, startIndex + 1);
                    num3++;
                    if (num3 > 20)
                    {
                        throw new Exception("lines overflow: " + data);
                    }
                }
                StartSeperatorPos = singleQuote + 1;
                return data.Substring(startIndex + 1, (singleQuote - startIndex) - 1).Replace("'", "''").Replace("\"\"", "\"");
            }
        }

        public class data
        {
            // Methods
            public bool AssignProductNotes(int ProductID, string NoteIDList)
            {
                SqlConnection connection = new SqlConnection();
                bool flag = false;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "DELETE FROM tblProductNote WHERE ProductID = @ProductID"
                    };
                    command.Parameters.Add("@ProductID", System.Data.SqlDbType.Int, 4).Value = ProductID;
                    connection.Open();
                    command.ExecuteNonQuery();
                    if (NoteIDList != "")
                    {
                        command.CommandText = "INSERT INTO tblProductNote (ProductID, NoteID) VALUES (@ProductID, @NoteID)";
                        foreach (string str in NoteIDList.Split(new char[] { '|' }))
                        {
                            command.Parameters.Clear();
                            command.Parameters.Add("@NoteID", SqlDbType.Int, 4).Value = int.Parse(str);
                            command.Parameters.Add("@ProductID", SqlDbType.Int, 4).Value = ProductID;
                            command.ExecuteNonQuery();
                        }
                    }
                    flag = true;
                }
                catch (Exception exception)
                {
                    this.InsertException(string.Concat(new object[] { "Error while assigning notes ", NoteIDList, " to product ", ProductID }), exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }

            public bool AssignUserBrands(int UserID, string BrandsList)
            {
                SqlConnection connection = new SqlConnection();
                bool flag = false;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "DELETE FROM tblBrandUser WHERE UserID = @UserID"
                    };
                    command.Parameters.Add("@UserID", SqlDbType.Int, 4).Value = UserID;
                    connection.Open();
                    command.ExecuteNonQuery();
                    if (BrandsList != "")
                    {
                        command.CommandText = "INSERT INTO tblBrandUser (BrandID, UserID) VALUES (@BrandID, @UserID)";
                        foreach (string str in BrandsList.Split(new char[] { '|' }))
                        {
                            command.Parameters.Clear();
                            command.Parameters.Add("@BrandID", SqlDbType.Int, 4).Value = int.Parse(str);
                            command.Parameters.Add("@UserID", SqlDbType.Int, 4).Value = UserID;
                            command.ExecuteNonQuery();
                        }
                    }
                    flag = true;
                }
                catch (Exception exception)
                {
                    this.InsertException(string.Concat(new object[] { "Error while assigning brands ", BrandsList, " to user ", UserID }), exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }

            public bool DeleteBrand(int BrandID)
            {
                SqlConnection connection = new SqlConnection();
                bool flag = false;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "DELETE FROM tblBrand WHERE BrandID = @BrandID"
                    };
                    command.Parameters.Add("@BrandID", SqlDbType.Int, 4).Value = BrandID;
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        flag = true;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while deleting brand " + BrandID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }

            public bool DeleteCategory(int CategoryID)
            {
                SqlConnection connection = new SqlConnection();
                bool flag = false;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "DELETE FROM tblCategory WHERE CategoryID = @CategoryID"
                    };
                    command.Parameters.Add("@CategoryID", SqlDbType.Int, 4).Value = CategoryID;
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        flag = true;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while deleting category " + CategoryID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }

            public bool DeleteCommissionMultiplier(int PriceCodeDiscountID)
            {
                SqlConnection connection = new SqlConnection();
                bool flag = false;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "DELETE FROM tblPriceCodeDiscount WHERE PriceCodeDiscountID = @PriceCodeDiscountID"
                    };
                    command.Parameters.Add("@PriceCodeDiscountID", SqlDbType.Int, 4).Value = PriceCodeDiscountID;
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        flag = true;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while deleting commission/multiplier with PriceCodeDiscountID " + PriceCodeDiscountID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }

            public bool DeleteCompany(int CompanyID)
            {
                SqlConnection connection = new SqlConnection();
                bool flag = false;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "DELETE FROM tblCompany WHERE CompanyID = @CompanyID"
                    };
                    command.Parameters.Add("@CompanyID", SqlDbType.Int, 4).Value = CompanyID;
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        flag = true;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while deleting company " + CompanyID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }

            public bool DeleteDiscount(int DiscountID)
            {
                SqlConnection connection = new SqlConnection();
                bool flag = false;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "DELETE FROM tblDiscount WHERE DiscountID = @DiscountID"
                    };
                    command.Parameters.Add("@DiscountID", SqlDbType.Int, 4).Value = DiscountID;
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        flag = true;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while deleting discount " + DiscountID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }

            public bool DeleteEstimate(int EstimateID)
            {
                SqlConnection connection = new SqlConnection();
                bool flag = false;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "DELETE FROM tblEstimateUser WHERE EstimateID = @EstimateID; DELETE FROM tblEstimate WHERE EstimateID = @EstimateID"
                    };
                    command.Parameters.Add("@EstimateID", SqlDbType.Int, 4).Value = EstimateID;
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        flag = true;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while deleting estimate " + EstimateID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }

            public bool DeleteEstimateUser(int EstimateUserID)
            {
                SqlConnection connection = new SqlConnection();
                bool flag = false;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "DELETE FROM tblEstimateUser WHERE EstimateUserID = @EstimateUserID"
                    };
                    command.Parameters.Add("@EstimateUserID", SqlDbType.Int, 4).Value = EstimateUserID;
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        flag = true;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while deleting user's estimate access for estimateuserid " + EstimateUserID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }

            public bool DeleteException(int ExceptionID)
            {
                SqlConnection connection = new SqlConnection();
                bool flag = false;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "DELETE FROM tblExceptions WHERE ExceptionID = @ExceptionID"
                    };
                    command.Parameters.Add("@ExceptionID", SqlDbType.Int, 4).Value = ExceptionID;
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        flag = true;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while deleting exception " + ExceptionID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }

            public bool DeleteFrame(int FrameID)
            {
                SqlConnection connection = new SqlConnection();
                bool flag = false;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "DELETE FROM tblFrame WHERE FrameID = @FrameID"
                    };
                    command.Parameters.Add("@FrameID", SqlDbType.Int, 4).Value = FrameID;
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        flag = true;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while deleting frame " + FrameID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }

            public bool DeleteFrameConfig(int FrameConfigID)
            {
                SqlConnection connection = new SqlConnection();
                bool flag = false;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "DELETE FROM tblFrameConfig WHERE FrameConfigID = @FrameConfigID"
                    };
                    command.Parameters.Add("@FrameConfigID", SqlDbType.Int, 4).Value = FrameConfigID;
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        flag = true;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while deleting FrameConfig " + FrameConfigID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }

            public bool DeleteFrameConfigCategory(int FrameConfigCategoryID)
            {
                SqlConnection connection = new SqlConnection();
                bool flag = false;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "DELETE FROM tblFrameConfigCategory WHERE FrameConfigCategoryID = @FrameConfigCategoryID"
                    };
                    command.Parameters.Add("@FrameConfigCategoryID", SqlDbType.Int, 4).Value = FrameConfigCategoryID;
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        flag = true;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while deleting FrameConfigCategory " + FrameConfigCategoryID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }

            public bool DeleteFrameConfigProduct(int FrameConfigProductID)
            {
                SqlConnection connection = new SqlConnection();
                bool flag = false;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "DELETE FROM tblFrameConfigProduct WHERE FrameConfigProductID = @FrameConfigProductID"
                    };
                    command.Parameters.Add("@FrameConfigProductID", SqlDbType.Int, 4).Value = FrameConfigProductID;
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        flag = true;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while deleting FrameConfigProduct " + FrameConfigProductID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }

            public bool DeleteNote(int NoteID)
            {
                SqlConnection connection = new SqlConnection();
                bool flag = false;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "DELETE FROM tblNote WHERE NoteID = @NoteID"
                    };
                    command.Parameters.Add("@NoteID", SqlDbType.Int, 4).Value = NoteID;
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        flag = true;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while deleting note " + NoteID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }

            public bool DeletePriceCode(int PriceCodeID)
            {
                SqlConnection connection = new SqlConnection();
                bool flag = false;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "DELETE FROM tblPriceCode WHERE PriceCodeID = @PriceCodeID"
                    };
                    command.Parameters.Add("@PriceCodeID", SqlDbType.Int, 4).Value = PriceCodeID;
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        flag = true;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while deleting price code " + PriceCodeID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }

            public bool DeleteProduct(int ProductID)
            {
                SqlConnection connection = new SqlConnection();
                bool flag = false;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "DELETE FROM tblProduct WHERE ProductID = @ProductID"
                    };
                    command.Parameters.Add("@ProductID", SqlDbType.Int, 4).Value = ProductID;
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        flag = true;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while deleting product " + ProductID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }

            public bool DeleteUser(int UserID)
            {
                SqlConnection connection = new SqlConnection();
                bool flag = false;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "DELETE FROM tblBrandUser WHERE UserID = @UserID"
                    };
                    command.Parameters.Add("@UserID", SqlDbType.Int, 4).Value = UserID;
                    connection.Open();
                    command.ExecuteNonQuery();
                    command.CommandText = "DELETE FROM tblUser WHERE UserID = @UserID";
                    if (command.ExecuteNonQuery() > 0)
                    {
                        flag = true;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while deleting user " + UserID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }

            public bool DoesPartNumberExist(string HFPartNumber)
            {
                SqlConnection connection = new SqlConnection();
                bool flag = false;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT ProductID FROM tblProduct WHERE (UPPER(HFPartNumber) = UPPER(@HFPartNumber))"
                    };
                    command.Parameters.Add("@HFPartNumber", SqlDbType.VarChar, 0x19).Value = HFPartNumber;
                    connection.Open();
                    if (command.ExecuteReader().HasRows)
                    {
                        flag = true;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while checking to see if Part Number '" + HFPartNumber + "' exists.", exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }

            public string GetApproverByRange(double NonDiscSell, double Discount)
            {
                if (Discount == 0.0)
                {
                    Discount = 0.0001;
                }
                SqlConnection connection = new SqlConnection();
                string str = "";
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT a.Title FROM tblApprover AS a INNER JOIN tblApproverRange AS b ON a.ApproverID = b.ApproverID WHERE (@NonDiscSell > b.MinNonDiscSell) AND (@NonDiscSell <= b.MaxNonDiscSell) AND (@Discount > b.MinDiscount) AND (@Discount <= b.MaxDiscount)"
                    };
                    command.Parameters.Add("@NonDiscSell", SqlDbType.Float).Value = NonDiscSell;
                    command.Parameters.Add("@Discount", SqlDbType.Float).Value = Discount;
                    connection.Open();
                    if (((string)command.ExecuteScalar()) == null)
                    {
                        command.CommandText = "SELECT Title FROM tblApprover WHERE IsDefault = 1";
                        str = (string)command.ExecuteScalar();
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException(string.Concat(new object[] { "Error while getting Approver for NonDiscSell: ", NonDiscSell, " and Discount: ", Discount }), exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return str;
            }

            public int GetBrandIDByName(string Name)
            {
                SqlConnection connection = new SqlConnection();
                int num = -1;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT BrandID FROM tblBrand where UPPER(Name) = UPPER(@Name)"
                    };
                    command.Parameters.Add("@Name", SqlDbType.VarChar, 50).Value = Name;
                    connection.Open();
                    num = (int)command.ExecuteScalar();
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting BrandID for Name: " + Name, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return num;
            }

            public DataSet GetBrands(int BrandID)
            {
                SqlConnection connection = new SqlConnection();
                DataSet dataSet = new DataSet();
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand selectCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT * FROM tblBrand"
                    };
                    if (BrandID > 0)
                    {
                        selectCommand.CommandText = selectCommand.CommandText + " WHERE (BrandID = @BrandID)";
                        selectCommand.Parameters.Add("@BrandID", SqlDbType.Int, 4).Value = BrandID;
                    }
                    connection.Open();
                    dataSet = new DataSet();
                    new SqlDataAdapter(selectCommand).Fill(dataSet);
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting brand(s) for BrandID " + BrandID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return dataSet;
            }

            public DataSet GetBrandsByUser(int UserID)
            {
                SqlConnection connection = new SqlConnection();
                DataSet dataSet = new DataSet();
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand selectCommand = new SqlCommand
                    {
                        Connection = connection
                    };
                    selectCommand.CommandText = selectCommand.CommandText + "SELECT a.* FROM tblBrand as a INNER JOIN tblBrandUser as b ON a.BrandID = b.BrandID WHERE (b.UserID = @UserID)";
                    selectCommand.Parameters.Add("@UserID", SqlDbType.Int, 4).Value = UserID;
                    connection.Open();
                    dataSet = new DataSet();
                    new SqlDataAdapter(selectCommand).Fill(dataSet);
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting brand(s) for user " + UserID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return dataSet;
            }

            public DataSet GetCategories(int CategoryID)
            {
                SqlConnection connection = new SqlConnection();
                DataSet dataSet = new DataSet();
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand selectCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT * FROM tblCategory"
                    };
                    if (CategoryID > 0)
                    {
                        selectCommand.CommandText = selectCommand.CommandText + " WHERE (CategoryID = @CategoryID)";
                        selectCommand.Parameters.Add("@CategoryID", SqlDbType.Int, 4).Value = CategoryID;
                    }
                    selectCommand.CommandText = selectCommand.CommandText + " ORDER BY ISNULL(SortOrder, 99999), DisplayName";
                    connection.Open();
                    dataSet = new DataSet();
                    new SqlDataAdapter(selectCommand).Fill(dataSet);
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting Categories for CategoryID: " + CategoryID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return dataSet;
            }

            public int GetCategoryID(string CategoryName)
            {
                SqlConnection connection = new SqlConnection();
                int num = -1;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT CategoryID FROM tblCategory WHERE UPPER(Name) = UPPER(@Name)"
                    };
                    command.Parameters.Add("@Name", SqlDbType.VarChar, 30).Value = CategoryName;
                    connection.Open();
                    num = (int)command.ExecuteScalar();
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting CategoryID for CategoryName: " + CategoryName, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return num;
            }

            public DataSet GetCommissionsMultipliers(int PriceCodeDiscountID, int DiscountID)
            {
                SqlConnection connection = new SqlConnection();
                DataSet dataSet = new DataSet();
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand selectCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT a.PriceCodeDiscountID, a.PriceCodeID, a.DiscountID, a.Multiplier, a.Commission, b.DiscountCode, b.DisplayName AS DiscountDisplayName, b.Rank, c.PriceCode, c.DisplayName AS PCDisplayName FROM tblPriceCodeDiscount AS a INNER JOIN tblDiscount AS b ON a.DiscountID = b.DiscountID INNER JOIN tblPriceCode AS c ON a.PriceCodeID = c.PriceCodeID "
                    };
                    if (PriceCodeDiscountID != -1)
                    {
                        selectCommand.CommandText = selectCommand.CommandText + "WHERE a.PriceCodeDiscountID = @PriceCodeDiscountID ";
                        selectCommand.Parameters.Add("@PriceCodeDiscountID", SqlDbType.Int, 4).Value = PriceCodeDiscountID;
                    }
                    else if (DiscountID != -1)
                    {
                        selectCommand.CommandText = selectCommand.CommandText + "WHERE a.DiscountID = @DiscountID ";
                        selectCommand.Parameters.Add("@DiscountID", SqlDbType.Int, 4).Value = DiscountID;
                    }
                    selectCommand.CommandText = selectCommand.CommandText + "ORDER BY b.Rank, b.DiscountCode, c.PriceCode";
                    connection.Open();
                    dataSet = new DataSet();
                    new SqlDataAdapter(selectCommand).Fill(dataSet);
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting multipliers and commissions for DiscountID: " + DiscountID.ToString(), exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return dataSet;
            }

            public DataSet GetCompanies(int CompanyID)
            {
                SqlConnection connection = new SqlConnection();
                DataSet dataSet = new DataSet();
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand selectCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT CompanyID, CompanyName FROM tblCompany"
                    };
                    if (CompanyID > 0)
                    {
                        selectCommand.CommandText = selectCommand.CommandText + " where CompanyID = @CompanyID";
                        selectCommand.Parameters.Add("@CompanyID", SqlDbType.Int, 4).Value = CompanyID;
                    }
                    connection.Open();
                    dataSet = new DataSet();
                    new SqlDataAdapter(selectCommand).Fill(dataSet);
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting company(s) " + CompanyID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return dataSet;
            }

            public DataSet GetDefaultPackage(int FrameConfigID, int Stages, string BrandAccess, double PowerHP, double FlowCFM)
            {
                SqlConnection connection = new SqlConnection();
                DataSet dataSet = new DataSet();
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand selectCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT b.FrameConfigID, a.ProductID, a.HFPartNumber, a.Description, a.Size, a.ListPrice, a.CostPrice, a.CategoryID, a.PowerHP, a.FlowCFM, b.IsDefault, b.DefaultNumber FROM tblFrameConfigProduct AS b RIGHT OUTER JOIN tblProduct AS a ON b.ProductID = a.ProductID WHERE (b.IsDefault = 1) AND (b.FrameConfigID = @FrameConfigID) AND (a.FrameID = (SELECT FrameID FROM tblFrameConfig WHERE FrameConfigID = @FrameConfigID) OR a.FrameID IS NULL) AND ((a.Stages IS NULL) OR (a.Stages = @Stages)) AND (a.BrandID IN (" + BrandAccess + ") OR a.BrandID IS NULL)"
                    };
                    if (PowerHP != -1.0)
                    {
                        object[] objArray = new object[] { selectCommand.CommandText, " AND ((a.PowerHP IS NULL) OR ((a.PowerHP >= ", PowerHP, ") AND (a.PowerHP <= ", PowerHP * double.Parse(ConfigurationManager.AppSettings["HPMultiplier"]), "))) " };
                        selectCommand.CommandText = string.Concat(objArray);
                    }
                    if (FlowCFM != -1.0)
                    {
                        object[] objArray2 = new object[] { selectCommand.CommandText, " AND ((a.FlowCFM IS NULL) OR ((a.FlowCFM >= ", FlowCFM, ") AND (a.FlowCFM <= ", FlowCFM * double.Parse(ConfigurationManager.AppSettings["FlowMultiplier"]), "))) " };
                        selectCommand.CommandText = string.Concat(objArray2);
                    }
                    selectCommand.Parameters.Add("@FrameConfigID", SqlDbType.Int, 4).Value = FrameConfigID;
                    selectCommand.Parameters.Add("@Stages", SqlDbType.Int, 4).Value = Stages;
                    connection.Open();
                    dataSet = new DataSet();
                    new SqlDataAdapter(selectCommand).Fill(dataSet);
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting default package for FrameConfigID: " + FrameConfigID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return dataSet;
            }

            public string GetDiscountCode(int DiscountID)
            {
                SqlConnection connection = new SqlConnection();
                string str = "";
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT DiscountCode FROM tblDiscount WHERE DiscountID = @DiscountID"
                    };
                    command.Parameters.Add("@DiscountID", SqlDbType.Int, 4).Value = DiscountID;
                    connection.Open();
                    str = (string)command.ExecuteScalar();
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting discount code for DiscountID: " + DiscountID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return str;
            }

            public DataSet GetDiscounts(string DiscountCode, int MaxDiscountRank)
            {
                SqlConnection connection = new SqlConnection();
                DataSet dataSet = new DataSet();
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand selectCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT DiscountID, DiscountCode, DisplayName, DefaultDiscount, Rank FROM tblDiscount "
                    };
                    if (DiscountCode != "")
                    {
                        selectCommand.CommandText = selectCommand.CommandText + "WHERE DiscountCode = @DiscountCode ";
                        selectCommand.Parameters.Add("@DiscountCode", SqlDbType.VarChar, 15).Value = DiscountCode;
                    }
                    else if (MaxDiscountRank >= 0)
                    {
                        selectCommand.CommandText = selectCommand.CommandText + "WHERE Rank <= @MaxDiscountRank ";
                        selectCommand.Parameters.Add("@MaxDiscountRank", SqlDbType.Int, 4).Value = MaxDiscountRank;
                    }
                    selectCommand.CommandText = selectCommand.CommandText + "ORDER BY Rank, DiscountCode";
                    connection.Open();
                    dataSet = new DataSet();
                    new SqlDataAdapter(selectCommand).Fill(dataSet);
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting Discounts for DiscountCode: " + DiscountCode, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return dataSet;
            }

            public DataSet GetEstimate(int EstimateID, int UserID)
            {
                SqlConnection connection = new SqlConnection();
                DataSet dataSet = new DataSet();
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand selectCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT a.*, b.* FROM tblEstimate a INNER JOIN tblEstimateUser b ON a.EstimateID = b.EstimateID WHERE b.EstimateID = @EstimateID AND b.UserID = @UserID"
                    };
                    selectCommand.Parameters.Add("@EstimateID", SqlDbType.Int, 4).Value = EstimateID;
                    selectCommand.Parameters.Add("@UserID", SqlDbType.Int, 4).Value = UserID;
                    connection.Open();
                    dataSet = new DataSet();
                    new SqlDataAdapter(selectCommand).Fill(dataSet);
                }
                catch (Exception exception)
                {
                    this.InsertException(string.Concat(new object[] { "Error while getting Estimate with EstimateID ", EstimateID, " for UserID: ", UserID }), exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return dataSet;
            }

            public DataSet GetEstimatesByUser(int UserID)
            {
                SqlConnection connection = new SqlConnection();
                DataSet dataSet = new DataSet();
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand selectCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT a.EstimateID, a.CreatedDate, a.LastModifiedDate, a.Name, a.CreatedUserID, a.LastModifiedUserID, a.QuoteNumber, a.EstimateDate, a.EstimateNote, CreatedUsername = (SELECT Username From tblUser WHERE UserID = a.CreatedUserID), ModifiedUsername = (SELECT Username From tblUser WHERE UserID = a.LastModifiedUserID) FROM tblEstimate a INNER JOIN tblEstimateUser b ON a.EstimateID = b.EstimateID INNER JOIN tblUser c ON b.UserID = c.UserID WHERE b.UserID = @UserID ORDER BY a.LastModifiedDate DESC"
                    };
                    selectCommand.Parameters.Add("@UserID", SqlDbType.Int, 4).Value = UserID;
                    connection.Open();
                    dataSet = new DataSet();
                    new SqlDataAdapter(selectCommand).Fill(dataSet);
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting Estimates for UserID: " + UserID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return dataSet;
            }

            public DataSet GetEstimateUsers(int EstimateID)
            {
                SqlConnection connection = new SqlConnection();
                DataSet dataSet = new DataSet();
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand selectCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT a.UserID, a.EstimateID, a.EstimateUserID, b.Username, c.Name, c.QuoteNumber FROM tblEstimateUser AS a INNER JOIN tblUser AS b ON a.UserID = b.UserID INNER JOIN tblEstimate as c ON a.EstimateID = c.EstimateID WHERE (a.EstimateID = @EstimateID)"
                    };
                    selectCommand.Parameters.Add("@EstimateID", SqlDbType.Int, 4).Value = EstimateID;
                    connection.Open();
                    dataSet = new DataSet();
                    new SqlDataAdapter(selectCommand).Fill(dataSet);
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting users with access to estimate: " + EstimateID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return dataSet;
            }

            public DataSet GetExceptions(int ExceptionID)
            {
                SqlConnection connection = new SqlConnection();
                DataSet dataSet = new DataSet();
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand selectCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT a.ExceptionID, a.ExceptionDate, a.Message, a.MessageBody, b.Username FROM tblExceptions a INNER JOIN tblUser b ON a.UserID = b.UserID"
                    };
                    if (ExceptionID > 0)
                    {
                        selectCommand.CommandText = selectCommand.CommandText + " WHERE a.ExceptionID = @ExceptionID";
                        selectCommand.Parameters.Add("@ExceptionID", SqlDbType.Int, 4).Value = ExceptionID;
                    }
                    connection.Open();
                    dataSet = new DataSet();
                    new SqlDataAdapter(selectCommand).Fill(dataSet);
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting exception(s) " + ExceptionID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return dataSet;
            }

            public DataSet GetFlowCFMByFrameConfigCategoryProducts(int FrameConfigID, int Stages, string BrandAccess)
            {
                SqlConnection connection = new SqlConnection();
                DataSet dataSet = new DataSet();
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand selectCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT DISTINCT FlowCFM FROM tblProduct WHERE CategoryID IN ( SELECT CategoryID FROM tblCategory WHERE (   CategoryID IN (SELECT CategoryID FROM tblFrameConfigCategory WHERE FrameConfigID = @FrameConfigID)   OR   MasterCategoryID IN (SELECT CategoryID FROM tblFrameConfigCategory WHERE FrameConfigID = @FrameConfigID) ) ) AND (FrameID = (SELECT FrameID FROM tblFrameConfig WHERE FrameConfigID = @FrameConfigID) OR FrameID IS NULL) AND ((Stages IS NULL) OR (Stages = @Stages)) AND (BrandID IN (" + BrandAccess + ") OR BrandID IS NULL) AND FlowCFM IS NOT NULL ORDER BY FlowCFM "
                    };
                    selectCommand.Parameters.Add("@FrameConfigID", SqlDbType.Int, 4).Value = FrameConfigID;
                    selectCommand.Parameters.Add("@Stages", SqlDbType.Int, 4).Value = Stages;
                    connection.Open();
                    dataSet = new DataSet();
                    new SqlDataAdapter(selectCommand).Fill(dataSet);
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting FlowCFM for products in categories associated with FrameConfigID: " + FrameConfigID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return dataSet;
            }

            public DataSet GetFrameConfigCategories(int FrameConfigID)
            {
                SqlConnection connection = new SqlConnection();
                DataSet dataSet = new DataSet();
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand selectCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT a.CategoryID, a.Name, a.DisplayName, a.MasterCategoryID, a.SortOrder, b.FrameConfigCategoryID, b.FrameConfigID FROM tblCategory a INNER JOIN tblFrameConfigCategory b ON a.CategoryID = b.CategoryID WHERE (b.FrameConfigID = @FrameConfigID) ORDER BY ISNULL(a.SortOrder, 99999), a.DisplayName"
                    };
                    selectCommand.Parameters.Add("@FrameConfigID", SqlDbType.Int, 4).Value = FrameConfigID;
                    connection.Open();
                    dataSet = new DataSet();
                    new SqlDataAdapter(selectCommand).Fill(dataSet);
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting categories for FrameConfigID: " + FrameConfigID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return dataSet;
            }

            public DataSet GetFrameConfigCategoryProducts(int FrameConfigID, int Stages, string BrandAccess, double PowerHP, double FlowCFM)
            {
                SqlConnection connection = new SqlConnection();
                DataSet dataSet = new DataSet();
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand selectCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT * FROM tblProduct WHERE CategoryID IN ( SELECT CategoryID FROM tblCategory WHERE (   CategoryID IN (SELECT CategoryID FROM tblFrameConfigCategory WHERE FrameConfigID = @FrameConfigID)   OR   MasterCategoryID IN (SELECT CategoryID FROM tblFrameConfigCategory WHERE FrameConfigID = @FrameConfigID) ) ) AND (FrameID = (SELECT FrameID FROM tblFrameConfig WHERE FrameConfigID = @FrameConfigID) OR FrameID IS NULL) AND ((Stages IS NULL) OR (Stages = @Stages)) AND (BrandID IN (" + BrandAccess + ") OR BrandID IS NULL) "
                    };
                    if (PowerHP != -1.0)
                    {
                        object[] objArray = new object[] { selectCommand.CommandText, " AND ((PowerHP IS NULL) OR ((PowerHP >= ", PowerHP, ") AND (PowerHP <= ", PowerHP * double.Parse(ConfigurationManager.AppSettings["HPMultiplier"]), "))) " };
                        selectCommand.CommandText = string.Concat(objArray);
                    }
                    if (FlowCFM != -1.0)
                    {
                        object[] objArray2 = new object[] { selectCommand.CommandText, " AND ((FlowCFM IS NULL) OR ((FlowCFM >= ", FlowCFM, ") AND (FlowCFM <= ", FlowCFM * double.Parse(ConfigurationManager.AppSettings["FlowMultiplier"]), "))) " };
                        selectCommand.CommandText = string.Concat(objArray2);
                    }
                    selectCommand.Parameters.Add("@FrameConfigID", SqlDbType.Int, 4).Value = FrameConfigID;
                    selectCommand.Parameters.Add("@Stages", SqlDbType.Int, 4).Value = Stages;
                    connection.Open();
                    dataSet = new DataSet();
                    new SqlDataAdapter(selectCommand).Fill(dataSet);
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting products in categories associated with FrameConfigID: " + FrameConfigID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return dataSet;
            }

            public int GetFrameConfigID(int FrameID, string FrameConfigType)
            {
                SqlConnection connection = new SqlConnection();
                int num = -1;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT FrameConfigID FROM tblFrameConfig WHERE (Type = @FrameConfigType) AND (FrameID = @FrameID)"
                    };
                    command.Parameters.Add("@FrameConfigType", SqlDbType.VarChar, 0x19).Value = FrameConfigType;
                    command.Parameters.Add("@FrameID", SqlDbType.Int, 4).Value = FrameID;
                    connection.Open();
                    num = (int)command.ExecuteScalar();
                }
                catch (Exception exception)
                {
                    this.InsertException(string.Concat(new object[] { "Error while getting FrameConfigID for FrameID ", FrameID, " of type ", FrameConfigType }), exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return num;
            }

            public DataSet GetFrameConfigProducts(int FrameConfigID, string BrandAccess)
            {
                SqlConnection connection = new SqlConnection();
                DataSet dataSet = new DataSet();
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand selectCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT a.*, b.HFPartNumber, b.Description, b.Size FROM tblFrameConfigProduct a INNER JOIN tblProduct b ON a.ProductID = b.ProductID WHERE (a.FrameConfigID = @FrameConfigID) AND (BrandID IN (" + BrandAccess + ") OR BrandID IS NULL)"
                    };
                    selectCommand.Parameters.Add("@FrameConfigID", SqlDbType.Int, 4).Value = FrameConfigID;
                    connection.Open();
                    dataSet = new DataSet();
                    new SqlDataAdapter(selectCommand).Fill(dataSet);
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting Products associated with for FrameConfigID: " + FrameConfigID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return dataSet;
            }

            public DataSet GetFrameDetails(int FrameID, string BrandAccess)
            {
                SqlConnection connection = new SqlConnection();
                DataSet dataSet = new DataSet();
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand selectCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT FrameID, BrandID, Filename FROM tblFrame WHERE (FrameID = @FrameID) AND (BrandID IN (" + BrandAccess + ") OR BrandID IS NULL)"
                    };
                    selectCommand.Parameters.Add("@FrameID", SqlDbType.Int, 4).Value = FrameID;
                    connection.Open();
                    dataSet = new DataSet();
                    new SqlDataAdapter(selectCommand).Fill(dataSet);
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting details for FrameID: " + FrameID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return dataSet;
            }

            public int GetFrameIDByFrameConfigID(int FrameConfigID)
            {
                SqlConnection connection = new SqlConnection();
                int num = -1;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT FrameID FROM tblFrameConfig where FrameConfigID = @FrameConfigID"
                    };
                    command.Parameters.Add("@FrameConfigID", SqlDbType.Int, 4).Value = FrameConfigID;
                    connection.Open();
                    num = (int)command.ExecuteScalar();
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting FrameID for: " + FrameConfigID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return num;
            }

            public int GetFrameIDByName(string Name)
            {
                SqlConnection connection = new SqlConnection();
                int num = -1;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT FrameID FROM tblFrame where UPPER(Name) = UPPER(@Name)"
                    };
                    command.Parameters.Add("@Name", SqlDbType.VarChar, 50).Value = Name;
                    connection.Open();
                    num = (int)command.ExecuteScalar();
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting FrameID for Name: " + Name, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return num;
            }

            public DataSet GetFrameProducts(int FrameID, string BrandAccess)
            {
                SqlConnection connection = new SqlConnection();
                DataSet dataSet = new DataSet();
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand selectCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT FrameID, ProductID, HFPartNumber, Description, Size, ListPrice, PriceCode, Weight, CostPrice, PowerHP, FlowCFM, CategoryID, Stages FROM tblProduct WHERE (FrameID = @FrameID) AND (BrandID IN (" + BrandAccess + ") OR BrandID IS NULL) ORDER BY Stages ASC, HFPartNumber ASC"
                    };
                    selectCommand.Parameters.Add("@FrameID", SqlDbType.Int, 4).Value = FrameID;
                    connection.Open();
                    dataSet = new DataSet();
                    new SqlDataAdapter(selectCommand).Fill(dataSet);
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting products for FrameID: " + FrameID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return dataSet;
            }

            public DataSet GetFrames(int FrameID, string BrandAccess)
            {
                SqlConnection connection = new SqlConnection();
                DataSet dataSet = new DataSet();
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand selectCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT a.*, b.Name as BrandName FROM tblFrame a LEFT OUTER JOIN tblBrand b ON a.BrandID = b.BrandID WHERE (a.BrandID IN (" + BrandAccess + ") OR a.BrandID IS NULL) "
                    };
                    if (FrameID > 0)
                    {
                        selectCommand.CommandText = selectCommand.CommandText + " AND (FrameID = @FrameID) ";
                        selectCommand.Parameters.Add("@FrameID", SqlDbType.Int, 4).Value = FrameID;
                    }
                    connection.Open();
                    dataSet = new DataSet();
                    new SqlDataAdapter(selectCommand).Fill(dataSet);
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting Frames for FrameID: " + FrameID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return dataSet;
            }

            public DataSet GetFramesByCategory(int CategoryID, string BrandAccess)
            {
                SqlConnection connection = new SqlConnection();
                DataSet dataSet = new DataSet();
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand selectCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT FrameID, Name FROM tblFrame WHERE FrameID IN (   SELECT DISTINCT FrameID FROM tblProduct WHERE CategoryID IN (     SELECT CategoryID FROM tblCategory     WHERE CategoryID = @CategoryID OR MasterCategoryID = @CategoryID   )   AND (BrandID IN (" + BrandAccess + ") OR BrandID IS NULL)   AND FrameID IS NOT NULL )"
                    };
                    selectCommand.Parameters.Add("@CategoryID", SqlDbType.Int, 4).Value = CategoryID;
                    connection.Open();
                    dataSet = new DataSet();
                    new SqlDataAdapter(selectCommand).Fill(dataSet);
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting Frames for Category ID: " + CategoryID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return dataSet;
            }

            public DataSet GetFramesByType(string FrameConfigType, string BrandAccess)
            {
                SqlConnection connection = new SqlConnection();
                DataSet dataSet = new DataSet();
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand selectCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT a.FrameID, a.Name, b.Type, b.FrameConfigID FROM tblFrame a INNER JOIN tblFrameConfig b ON a.FrameID = b.FrameID WHERE (b.Type = @FrameConfigType) AND (a.BrandID IN (" + BrandAccess + ") OR a.BrandID IS NULL) "
                    };
                    selectCommand.CommandText = selectCommand.CommandText + "ORDER BY a.Name ASC";
                    selectCommand.Parameters.Add("@FrameConfigType", SqlDbType.VarChar, 50).Value = FrameConfigType;
                    connection.Open();
                    dataSet = new DataSet();
                    new SqlDataAdapter(selectCommand).Fill(dataSet);
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting frames for Frame Config Type: " + FrameConfigType, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return dataSet;
            }

            public DataSet GetMultiplierAndDiscount(string PriceCode, string DiscountCode)
            {
                SqlConnection connection = new SqlConnection();
                DataSet dataSet = new DataSet();
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand selectCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT b.Multiplier, b.Commission, a.DiscountCode, c.PriceCode, c.DefaultMarkup FROM tblDiscount AS a INNER JOIN tblPriceCodeDiscount AS b ON a.DiscountID = b.DiscountID INNER JOIN tblPriceCode AS c ON b.PriceCodeID = c.PriceCodeID WHERE a.DiscountCode = @DiscountCode AND c.PriceCode = @PriceCode"
                    };
                    selectCommand.Parameters.Add("@DiscountCode", SqlDbType.VarChar, 15).Value = DiscountCode;
                    selectCommand.Parameters.Add("@PriceCode", SqlDbType.VarChar, 15).Value = PriceCode;
                    connection.Open();
                    dataSet = new DataSet();
                    new SqlDataAdapter(selectCommand).Fill(dataSet);
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting Multiplier and Discount for " + PriceCode + " and " + DiscountCode, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return dataSet;
            }

            public int GetNewNoteID()
            {
                SqlConnection connection = new SqlConnection();
                DataSet set1 = new DataSet();
                int result = 0;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT MAX(NoteID) AS NoteID FROM tblNote"
                    };
                    connection.Open();
                    int.TryParse(command.ExecuteScalar().ToString(), out result);
                    result++;
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting Notes for NoteID: " + result, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return result;
            }

            public DataSet GetNotes(int NoteID)
            {
                SqlConnection connection = new SqlConnection();
                DataSet dataSet = new DataSet();
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand selectCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT * FROM tblNote"
                    };
                    if (NoteID > 0)
                    {
                        selectCommand.CommandText = selectCommand.CommandText + " WHERE (NoteID = @NoteID)";
                        selectCommand.Parameters.Add("@NoteID", SqlDbType.Int, 4).Value = NoteID;
                    }
                    connection.Open();
                    dataSet = new DataSet();
                    new SqlDataAdapter(selectCommand).Fill(dataSet);
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting Notes for NoteID: " + NoteID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return dataSet;
            }

            public DataSet GetPowerHPByFrameConfigCategoryProducts(int FrameConfigID, int Stages, string BrandAccess)
            {
                SqlConnection connection = new SqlConnection();
                DataSet dataSet = new DataSet();
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand selectCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT DISTINCT PowerHP FROM tblProduct WHERE CategoryID IN ( SELECT CategoryID FROM tblCategory WHERE (   CategoryID IN (SELECT CategoryID FROM tblFrameConfigCategory WHERE FrameConfigID = @FrameConfigID)   OR   MasterCategoryID IN (SELECT CategoryID FROM tblFrameConfigCategory WHERE FrameConfigID = @FrameConfigID) ) ) AND (FrameID = (SELECT FrameID FROM tblFrameConfig WHERE FrameConfigID = @FrameConfigID) OR FrameID IS NULL) AND ((Stages IS NULL) OR (Stages = @Stages)) AND (BrandID IN (" + BrandAccess + ") OR BrandID IS NULL) AND PowerHP IS NOT NULL ORDER BY PowerHP "
                    };
                    selectCommand.Parameters.Add("@FrameConfigID", SqlDbType.Int, 4).Value = FrameConfigID;
                    selectCommand.Parameters.Add("@Stages", SqlDbType.Int, 4).Value = Stages;
                    connection.Open();
                    dataSet = new DataSet();
                    new SqlDataAdapter(selectCommand).Fill(dataSet);
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting PowerHP for products in categories associated with FrameConfigID: " + FrameConfigID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return dataSet;
            }

            public double GetPrice(string PartNumber, string BrandAccess)
            {
                SqlConnection connection = new SqlConnection();
                double num = 0.0;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT ListPrice FROM tblProduct WHERE HFPartNumber = @HFPartNumber AND (BrandID IN (" + BrandAccess + ") OR BrandID IS NULL)"
                    };
                    command.Parameters.Add("@HFPartNumber", SqlDbType.VarChar, 0x19).Value = PartNumber;
                    connection.Open();
                    num = (double)command.ExecuteScalar();
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting price for part number " + PartNumber, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                    throw exception;
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return num;
            }

            public string GetPriceCode(int PriceCodeID)
            {
                SqlConnection connection = new SqlConnection();
                string str = "";
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT PriceCode FROM tblPriceCode WHERE PriceCodeID = @PriceCodeID"
                    };
                    command.Parameters.Add("@PriceCodeID", SqlDbType.Int, 4).Value = PriceCodeID;
                    connection.Open();
                    str = (string)command.ExecuteScalar();
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting price code for PriceCodeID: " + PriceCodeID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return str;
            }

            public DataSet GetPriceCodes(string PriceCode, bool BuyoutOnly)
            {
                SqlConnection connection = new SqlConnection();
                DataSet dataSet = new DataSet();
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand selectCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT * FROM tblPriceCode "
                    };
                    if (PriceCode != "")
                    {
                        selectCommand.CommandText = selectCommand.CommandText + "WHERE PriceCode = @PriceCode ";
                        selectCommand.Parameters.Add("@PriceCode", SqlDbType.VarChar, 15).Value = PriceCode;
                    }
                    else if (BuyoutOnly)
                    {
                        selectCommand.CommandText = selectCommand.CommandText + "WHERE ShowOnBuyout = @ShowOnBuyout ";
                        selectCommand.Parameters.Add("@ShowOnBuyout", SqlDbType.Bit).Value = true;
                    }
                    connection.Open();
                    dataSet = new DataSet();
                    new SqlDataAdapter(selectCommand).Fill(dataSet);
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting price codes for PriceCode: " + PriceCode, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return dataSet;
            }

            public DataSet GetProductDataSheet()
            {
                SqlConnection connection = new SqlConnection();
                DataSet dataSet = new DataSet();
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand selectCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT a.PRODUCTID, a.HFPARTNUMBER, a.DESCRIPTION, a.SIZE, a.LISTPRICE, a.PRICECODE AS PRCODE, a.WEIGHT, a.COSTPRICE, a.POWERHP, a.FLOWCFM, c.Name AS FRAME, a.STAGES, b.Name AS BRAND, d.Name AS CATEGORY FROM tblProduct a INNER JOIN tblBrand b ON a.BrandID = b.BrandID INNER JOIN tblFrame c ON a.FrameID = c.FrameID INNER JOIN tblCategory d ON a.CategoryID = d.CategoryID"
                    };
                    connection.Open();
                    dataSet = new DataSet();
                    new SqlDataAdapter(selectCommand).Fill(dataSet);
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting products for Excel export.", exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return dataSet;
            }

            public int GetProductID(string PartNumber)
            {
                SqlConnection connection = new SqlConnection();
                int num = -1;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT ProductID FROM tblProduct where UPPER(HFPartNumber) = UPPER(@PartNumber)"
                    };
                    command.Parameters.Add("@PartNumber", SqlDbType.VarChar, 0x19).Value = PartNumber;
                    connection.Open();
                    num = (int)command.ExecuteScalar();
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting ProductID for Part Number: " + PartNumber, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return num;
            }

            public DataSet GetProductNotes(int ProductID)
            {
                SqlConnection connection = new SqlConnection();
                DataSet dataSet = new DataSet();
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand selectCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT a.ProductNoteID, a.ProductID, b.NoteID, b.Text, b.Description FROM tblProductNote AS a INNER JOIN tblNote AS b ON a.NoteID = b.NoteID WHERE (a.ProductID = @ProductID) ORDER BY a.NoteID"
                    };
                    selectCommand.Parameters.Add("@ProductID", SqlDbType.Int, 4).Value = ProductID;
                    connection.Open();
                    dataSet = new DataSet();
                    new SqlDataAdapter(selectCommand).Fill(dataSet);
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting Notes for ProductID: " + ProductID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return dataSet;
            }

            public DataSet GetProducts(string PartNumber, string BrandAccess)
            {
                SqlConnection connection = new SqlConnection();
                DataSet dataSet = new DataSet();
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand selectCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT * FROM tblProduct WHERE (BrandID IN (" + BrandAccess + ") OR BrandID IS NULL)"
                    };
                    if (PartNumber != "")
                    {
                        selectCommand.CommandText = selectCommand.CommandText + " AND HFPartNumber = @HFPartNumber";
                        selectCommand.Parameters.Add("@HFPartNumber", SqlDbType.VarChar, 0x19).Value = PartNumber;
                    }
                    connection.Open();
                    dataSet = new DataSet();
                    new SqlDataAdapter(selectCommand).Fill(dataSet);
                }
                catch (Exception exception)
                {
                    if (PartNumber != "")
                    {
                        this.InsertException("Error while getting products associated with part number " + PartNumber, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                    }
                    else
                    {
                        this.InsertException("Error while getting products", exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                    }
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return dataSet;
            }

            public DataSet GetProductsByCategory(int CategoryID, string BrandAccess, int FrameID, int Stages, double PowerHP, double FlowCFM)
            {
                SqlConnection connection = new SqlConnection();
                DataSet dataSet = new DataSet();
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand selectCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT b.DisplayName, b.SortOrder, a.ProductID, a.HFPartNumber, a.Description, a.Size, a.ListPrice, a.PriceCode, a.Weight, a.CostPrice, a.PowerHP, a.FlowCFM, a.CategoryID, a.Stages, a.FrameID, a.BrandID, MAX(c.NoteID) as NoteID FROM tblCategory AS b INNER JOIN tblProduct AS a ON b.CategoryID = a.CategoryID LEFT OUTER JOIN tblProductNote as c ON a.ProductID = c.ProductID WHERE (a.BrandID IN (" + BrandAccess + ") OR a.BrandID IS NULL) AND ((b.MasterCategoryID = @CategoryID) OR (b.CategoryID = @CategoryID)) "
                    };
                    if (FrameID > 0)
                    {
                        selectCommand.CommandText = selectCommand.CommandText + " AND (a.FrameID = @FrameID OR a.FrameID IS NULL) ";
                        selectCommand.Parameters.Add("@FrameID", SqlDbType.Int, 4).Value = FrameID;
                    }
                    if (Stages > 0)
                    {
                        object[] objArray = new object[] { selectCommand.CommandText, " AND (a.Stages = ", Stages, " OR a.Stages IS NULL) " };
                        selectCommand.CommandText = string.Concat(objArray);
                        selectCommand.Parameters.Add("@Stages", SqlDbType.Int, 4).Value = Stages;
                    }
                    if (PowerHP != -1.0)
                    {
                        object[] objArray2 = new object[] { selectCommand.CommandText, " AND ((a.PowerHP IS NULL) OR ((a.PowerHP >= ", PowerHP, ") AND (a.PowerHP <= ", PowerHP * double.Parse(ConfigurationManager.AppSettings["HPMultiplier"]), "))) " };
                        selectCommand.CommandText = string.Concat(objArray2);
                    }
                    if (FlowCFM != -1.0)
                    {
                        object[] objArray3 = new object[] { selectCommand.CommandText, " AND ((a.FlowCFM IS NULL) OR ((a.FlowCFM >= ", FlowCFM, ") AND (a.FlowCFM <= ", FlowCFM * double.Parse(ConfigurationManager.AppSettings["FlowMultiplier"]), "))) " };
                        selectCommand.CommandText = string.Concat(objArray3);
                    }
                    selectCommand.CommandText = selectCommand.CommandText + " GROUP BY b.DisplayName, b.SortOrder,  a.ProductID, a.HFPartNumber, a.Description, a.Size, a.ListPrice, a.PriceCode, a.Weight,  a.CostPrice, a.PowerHP, a.FlowCFM, a.CategoryID, a.Stages, a.FrameID, a.BrandID ";
                    selectCommand.CommandText = selectCommand.CommandText + " ORDER BY ISNULL(b.SortOrder, 99999), b.DisplayName, a.Description";
                    selectCommand.Parameters.Add("@CategoryID", SqlDbType.Int, 4).Value = CategoryID;
                    connection.Open();
                    dataSet = new DataSet();
                    new SqlDataAdapter(selectCommand).Fill(dataSet);
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting products of CategoryID: " + CategoryID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return dataSet;
            }

            public DataSet GetProductsWithDiscount(string PartNumber, string DiscountCode, string BrandAccess)
            {
                SqlConnection connection = new SqlConnection();
                DataSet dataSet = new DataSet();
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand selectCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT d.*, b.Multiplier, b.Commission FROM tblDiscount a INNER JOIN tblPriceCodeDiscount b ON a.DiscountID = b.DiscountID INNER JOIN tblPriceCode c ON b.PriceCodeID = c.PriceCodeID INNER JOIN tblProduct d ON c.PriceCode = d.PriceCode WHERE a.DiscountCode = @DiscountCode AND (d.BrandID IN (" + BrandAccess + ") OR d.BrandID IS NULL)"
                    };
                    selectCommand.Parameters.Add("@DiscountCode", SqlDbType.VarChar, 15).Value = DiscountCode;
                    if (PartNumber != "")
                    {
                        selectCommand.CommandText = selectCommand.CommandText + " AND HFPartNumber = @HFPartNumber";
                        selectCommand.Parameters.Add("@HFPartNumber", SqlDbType.VarChar, 0x19).Value = PartNumber;
                    }
                    connection.Open();
                    dataSet = new DataSet();
                    new SqlDataAdapter(selectCommand).Fill(dataSet);
                }
                catch (Exception exception)
                {
                    if (PartNumber != "")
                    {
                        this.InsertException("Error while getting products with discount associated with part number " + PartNumber, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                    }
                    else
                    {
                        this.InsertException("Error while getting products with discount", exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                    }
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return dataSet;
            }

            public string GetUserCompany(int UserID)
            {
                SqlConnection connection = new SqlConnection();
                string str = "";
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT a.CompanyName FROM tblCompany AS a INNER JOIN tblUser AS b ON a.CompanyID = b.CompanyID where b.UserID = @UserID"
                    };
                    command.Parameters.Add("@UserID", SqlDbType.Int, 4).Value = UserID;
                    connection.Open();
                    str = (string)command.ExecuteScalar();
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting Company Name for userid: " + UserID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return str;
            }

            public DataSet GetUserDataSheet()
            {
                SqlConnection connection = new SqlConnection();
                DataSet dataSet = new DataSet();
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand selectCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT a.Username, a.LastLoginDate, a.IsActive, a.IsAdmin, a.IsGDEmployee, a.Note, a.FirstName, a.LastName, b.CompanyName, a.EmailAddress, a.Address1, a.Address2, a.City, a.State, a.Zip, a.Phone, a.MaxDiscountRank FROM tblUser a INNER JOIN tblCompany b ON a.CompanyID = b.CompanyID;"
                    };
                    connection.Open();
                    dataSet = new DataSet();
                    new SqlDataAdapter(selectCommand).Fill(dataSet);
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting users for Excel export.", exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return dataSet;
            }

            public int GetUserID(string Username)
            {
                SqlConnection connection = new SqlConnection();
                int num = -1;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT UserID FROM tblUser where Username = @Username"
                    };
                    command.Parameters.Add("@Username", SqlDbType.VarChar, 0x19).Value = Username;
                    connection.Open();
                    num = (int)command.ExecuteScalar();
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting UserID for: " + Username, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return num;
            }

            public DataSet GetUsers(int UserID)
            {
                SqlConnection connection = new SqlConnection();
                DataSet dataSet = new DataSet();
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand selectCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT * FROM tblUser"
                    };
                    if (UserID > 0)
                    {
                        selectCommand.CommandText = selectCommand.CommandText + " where UserID = @UserID";
                        selectCommand.Parameters.Add("@UserID", SqlDbType.Int, 4).Value = UserID;
                    }
                    connection.Open();
                    dataSet = new DataSet();
                    new SqlDataAdapter(selectCommand).Fill(dataSet);
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while getting user(s) " + UserID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return dataSet;
            }

            public int InsertBrand(string Name)
            {
                SqlConnection connection = new SqlConnection();
                int num = -1;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "INSERT INTO tblBrand (Name) VALUES (@Name); SELECT @BrandID = SCOPE_IDENTITY()"
                    };
                    command.Parameters.Add("@Name", SqlDbType.VarChar, 50).Value = Name;
                    SqlParameter parameter = new SqlParameter("@BrandID", SqlDbType.Int, 4)
                    {
                        Direction = ParameterDirection.Output
                    };
                    command.Parameters.Add(parameter);
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        num = (int)parameter.Value;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error adding brand " + Name, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return num;
            }

            public int InsertCategory(string Name, string DisplayName, int MasterCategoryID, int SortOrder)
            {
                SqlConnection connection = new SqlConnection();
                int num = -1;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "INSERT INTO tblCategory (Name, DisplayName, MasterCategoryID, SortOrder) VALUES (@Name, @DisplayName, @MasterCategoryID, @SortOrder); SELECT @CategoryID = SCOPE_IDENTITY()"
                    };
                    command.Parameters.Add("@Name", SqlDbType.VarChar, 30).Value = Name;
                    command.Parameters.Add("@DisplayName", SqlDbType.VarChar, 150).Value = DisplayName;
                    if (MasterCategoryID != -1)
                    {
                        command.Parameters.Add("@MasterCategoryID", SqlDbType.Int, 4).Value = MasterCategoryID;
                    }
                    else
                    {
                        command.Parameters.Add("@MasterCategoryID", SqlDbType.Int, 4).SqlValue = DBNull.Value;
                    }
                    if (SortOrder != -1)
                    {
                        command.Parameters.Add("@SortOrder", SqlDbType.Int, 4).Value = SortOrder;
                    }
                    else
                    {
                        command.Parameters.Add("@SortOrder", SqlDbType.Int, 4).SqlValue = DBNull.Value;
                    }
                    SqlParameter parameter = new SqlParameter("@CategoryID", SqlDbType.Int, 4)
                    {
                        Direction = ParameterDirection.Output
                    };
                    command.Parameters.Add(parameter);
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        num = (int)parameter.Value;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error adding category " + Name, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return num;
            }

            public int InsertCommissionMultiplier(int PriceCodeID, int DiscountID, double Commission, double Multiplier)
            {
                SqlConnection connection = new SqlConnection();
                int num = -1;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "INSERT INTO tblPriceCodeDiscount (PriceCodeID, DiscountID, Commission, Multiplier) VALUES (@PriceCodeID, @DiscountID, @Commission, @Multiplier); SELECT @PriceCodeDiscountID = SCOPE_IDENTITY()"
                    };
                    command.Parameters.Add("@PriceCodeID", SqlDbType.Int, 4).Value = PriceCodeID;
                    command.Parameters.Add("@DiscountID", SqlDbType.Int, 4).Value = DiscountID;
                    command.Parameters.Add("@Commission", SqlDbType.Float).Value = Commission;
                    command.Parameters.Add("@Multiplier", SqlDbType.Float).Value = Multiplier;
                    SqlParameter parameter = new SqlParameter("@PriceCodeDiscountID", SqlDbType.Int, 4)
                    {
                        Direction = ParameterDirection.Output
                    };
                    command.Parameters.Add(parameter);
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        num = (int)parameter.Value;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException(string.Concat(new object[] { "Error adding commission and multiplier for PriceCodeID ", PriceCodeID, " and DiscountID ", DiscountID }), exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return num;
            }

            public int InsertCompany(string CompanyName)
            {
                SqlConnection connection = new SqlConnection();
                int num = -1;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "INSERT INTO tblCompany (CompanyName) VALUES (@CompanyName); SELECT @CompanyID = SCOPE_IDENTITY()"
                    };
                    command.Parameters.Add("@CompanyName", SqlDbType.VarChar, 50).Value = CompanyName;
                    SqlParameter parameter = new SqlParameter("@CompanyID", SqlDbType.Int, 4)
                    {
                        Direction = ParameterDirection.Output
                    };
                    command.Parameters.Add(parameter);
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        num = (int)parameter.Value;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error adding company " + CompanyName, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return num;
            }

            public int InsertDiscount(string DiscountCode, string DisplayName, double DefaultDiscount, int Rank)
            {
                SqlConnection connection = new SqlConnection();
                int num = -1;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "INSERT INTO tblDiscount (DiscountCode, DisplayName, DefaultDiscount, Rank) VALUES (@DiscountCode, @DisplayName, @DefaultDiscount, @Rank); SELECT @DiscountID = SCOPE_IDENTITY()"
                    };
                    command.Parameters.Add("@DiscountCode", SqlDbType.VarChar, 15).Value = DiscountCode;
                    command.Parameters.Add("@DisplayName", SqlDbType.VarChar, 50).Value = DisplayName;
                    command.Parameters.Add("@DefaultDiscount", SqlDbType.Float).Value = DefaultDiscount;
                    command.Parameters.Add("@Rank", SqlDbType.Int).Value = Rank;
                    SqlParameter parameter = new SqlParameter("@DiscountID", SqlDbType.Int, 4)
                    {
                        Direction = ParameterDirection.Output
                    };
                    command.Parameters.Add(parameter);
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        num = (int)parameter.Value;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error adding discount code " + DiscountCode, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return num;
            }

            public int InsertEstimate(string Name, int CreatedUserID, int LastModifiedUserID, string Products, string DiscountCode, string QuoteNumber, string Representative, string QuotationFor, string Application, string Address, string ForInstallationAt, string EstimateDate, string EstimateNote)
            {
                SqlConnection connection = new SqlConnection();
                int num = -1;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "INSERT INTO tblEstimate (CreatedDate, LastModifiedDate, Name, CreatedUserID, LastModifiedUserID, Products, DiscountCode, QuoteNumber, Representative, QuotationFor, Application, Address, ForInstallationAt, EstimateDate, EstimateNote) VALUES (getDate(), getDate(), @Name, @CreatedUserID, @LastModifiedUserID, @Products, @DiscountCode, @QuoteNumber, @Representative, @QuotationFor, @Application, @Address, @ForInstallationAt, @EstimateDate, @EstimateNote); SELECT @EstimateID = SCOPE_IDENTITY(); INSERT INTO tblEstimateUser (EstimateID, UserID) VALUES (@EstimateID, @CreatedUserID)"
                    };
                    command.Parameters.Add("@Name", SqlDbType.VarChar, 50).Value = Name;
                    command.Parameters.Add("@CreatedUserID", SqlDbType.Int, 4).Value = CreatedUserID;
                    command.Parameters.Add("@LastModifiedUserID", SqlDbType.Int, 4).Value = LastModifiedUserID;
                    command.Parameters.Add("@Products", SqlDbType.Text).Value = (Products == "") ? ((object)DBNull.Value) : ((object)Products);
                    if (DiscountCode != "")
                    {
                        command.Parameters.Add("@DiscountCode", SqlDbType.VarChar, 15).Value = DiscountCode;
                    }
                    else
                    {
                        command.Parameters.Add("@DiscountCode", SqlDbType.VarChar, 15).SqlValue = DBNull.Value;
                    }
                    if (QuoteNumber != "")
                    {
                        command.Parameters.Add("@QuoteNumber", SqlDbType.VarChar, 50).Value = QuoteNumber;
                    }
                    else
                    {
                        command.Parameters.Add("@QuoteNumber", SqlDbType.VarChar, 50).SqlValue = DBNull.Value;
                    }
                    if (Representative != "")
                    {
                        command.Parameters.Add("@Representative", SqlDbType.VarChar, 50).Value = Representative;
                    }
                    else
                    {
                        command.Parameters.Add("@Representative", SqlDbType.VarChar, 50).SqlValue = DBNull.Value;
                    }
                    if (QuotationFor != "")
                    {
                        command.Parameters.Add("@QuotationFor", SqlDbType.VarChar, 50).Value = QuotationFor;
                    }
                    else
                    {
                        command.Parameters.Add("@QuotationFor", SqlDbType.VarChar, 50).SqlValue = DBNull.Value;
                    }
                    if (Application != "")
                    {
                        command.Parameters.Add("@Application", SqlDbType.VarChar, 100).Value = Application;
                    }
                    else
                    {
                        command.Parameters.Add("@Application", SqlDbType.VarChar, 100).SqlValue = DBNull.Value;
                    }
                    if (Address != "")
                    {
                        command.Parameters.Add("@Address", SqlDbType.VarChar, 150).Value = Address;
                    }
                    else
                    {
                        command.Parameters.Add("@Address", SqlDbType.VarChar, 150).SqlValue = DBNull.Value;
                    }
                    if (ForInstallationAt != "")
                    {
                        command.Parameters.Add("@ForInstallationAt", SqlDbType.VarChar, 100).Value = ForInstallationAt;
                    }
                    else
                    {
                        command.Parameters.Add("@ForInstallationAt", SqlDbType.VarChar, 100).SqlValue = DBNull.Value;
                    }
                    if (EstimateDate != "")
                    {
                        command.Parameters.Add("@EstimateDate", SqlDbType.DateTime).Value = DateTime.Parse(EstimateDate);
                    }
                    else
                    {
                        command.Parameters.Add("@EstimateDate", SqlDbType.DateTime).SqlValue = DBNull.Value;
                    }
                    if (EstimateNote != "")
                    {
                        command.Parameters.Add("@EstimateNote", SqlDbType.VarChar, 0x4b0).Value = EstimateNote;
                    }
                    else
                    {
                        command.Parameters.Add("@EstimateNote", SqlDbType.VarChar, 0x4b0).SqlValue = DBNull.Value;
                    }
                    SqlParameter parameter = new SqlParameter("@EstimateID", SqlDbType.Int, 4)
                    {
                        Direction = ParameterDirection.Output
                    };
                    command.Parameters.Add(parameter);
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        num = (int)parameter.Value;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error adding estimate " + Name, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return num;
            }

            public bool InsertEstimateUser(int EstimateID, int UserID)
            {
                SqlConnection connection = new SqlConnection();
                bool flag = false;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "INSERT INTO tblEstimateUser (EstimateID, UserID) VALUES (@EstimateID, @UserID)"
                    };
                    command.Parameters.Add("@EstimateID", SqlDbType.Int, 4).Value = EstimateID;
                    command.Parameters.Add("@UserID", SqlDbType.Int, 4).Value = UserID;
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        flag = true;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException(string.Concat(new object[] { "Error granting user ", UserID, " access to estimate ", EstimateID }), exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }

            public bool InsertException(string Message, string MessageBody, int UserID, string Username)
            {
                SqlConnection connection = new SqlConnection();
                bool flag = false;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "INSERT INTO tblExceptions (ExceptionDate, Message, MessageBody, UserID) VALUES (GETDATE(), @Message, @MessageBody, 1)"
                    };
                    command.Parameters.Add("@Message", SqlDbType.VarChar, 250).Value = Message;
                    command.Parameters.Add("@MessageBody", SqlDbType.VarChar, 0x5dc).Value = MessageBody;
                    command.Parameters.Add("@UserID", SqlDbType.Int, 4).Value = UserID;
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        flag = true;
                    }
                }
                catch (Exception)
                {
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }

            public int InsertFrame(string Name, string Filename, int BrandID)
            {
                SqlConnection connection = new SqlConnection();
                int num = -1;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "INSERT INTO tblFrame (Name, Filename, BrandID) VALUES (@Name, @Filename, @BrandID); SELECT @FrameID = SCOPE_IDENTITY()"
                    };
                    command.Parameters.Add("@Name", SqlDbType.VarChar, 50).Value = Name;
                    command.Parameters.Add("@Filename", SqlDbType.VarChar, 500).Value = (Filename == "") ? ((object)DBNull.Value) : ((object)Filename);
                    if (BrandID != -1)
                    {
                        command.Parameters.Add("@BrandID", SqlDbType.Int, 4).Value = BrandID;
                    }
                    else
                    {
                        command.Parameters.Add("@BrandID", SqlDbType.Int, 4).SqlValue = DBNull.Value;
                    }
                    SqlParameter parameter = new SqlParameter("@FrameID", SqlDbType.Int, 4)
                    {
                        Direction = ParameterDirection.Output
                    };
                    command.Parameters.Add(parameter);
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        num = (int)parameter.Value;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error adding frame " + Name, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return num;
            }

            public int InsertFrameConfig(string FrameConfigType, int FrameID)
            {
                SqlConnection connection = new SqlConnection();
                int num = -1;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "INSERT INTO tblFrameConfig (Type, FrameID) VALUES (@FrameConfigType, @FrameID); SELECT @FrameConfigID = SCOPE_IDENTITY()"
                    };
                    command.Parameters.Add("@FrameConfigType", SqlDbType.VarChar, 0x19).Value = FrameConfigType;
                    command.Parameters.Add("@FrameID", SqlDbType.Int, 4).SqlValue = FrameID;
                    SqlParameter parameter = new SqlParameter("@FrameConfigID", SqlDbType.Int, 4)
                    {
                        Direction = ParameterDirection.Output
                    };
                    command.Parameters.Add(parameter);
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        num = (int)parameter.Value;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException(string.Concat(new object[] { "Error adding frame config for frame ", FrameID, " of type ", FrameConfigType }), exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return num;
            }

            public int InsertFrameConfigCategory(int FrameConfigID, int CategoryID)
            {
                SqlConnection connection = new SqlConnection();
                int num = -1;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "INSERT INTO tblFrameConfigCategory (FrameConfigID, CategoryID) VALUES (@FrameConfigID, @CategoryID); SELECT @FrameConfigCategoryID = SCOPE_IDENTITY()"
                    };
                    command.Parameters.Add("@FrameConfigID", SqlDbType.Int, 4).Value = FrameConfigID;
                    command.Parameters.Add("@CategoryID", SqlDbType.Int, 4).Value = CategoryID;
                    SqlParameter parameter = new SqlParameter("@FrameConfigCategoryID", SqlDbType.Int, 4)
                    {
                        Direction = ParameterDirection.Output
                    };
                    command.Parameters.Add(parameter);
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        num = (int)parameter.Value;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException(string.Concat(new object[] { "Error adding category ", CategoryID, " to FrameConfigID: ", FrameConfigID }), exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return num;
            }

            public int InsertFrameConfigProduct(int FrameConfigID, int ProductID, bool IsDefault, int DefaultNumber)
            {
                SqlConnection connection = new SqlConnection();
                int num = -1;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "INSERT INTO tblFrameConfigProduct (FrameConfigID, ProductID, IsDefault, DefaultNumber) VALUES (@FrameConfigID, @ProductID, @IsDefault, @DefaultNumber); SELECT @FrameConfigProductID = SCOPE_IDENTITY()"
                    };
                    command.Parameters.Add("@FrameConfigID", SqlDbType.Int, 4).Value = FrameConfigID;
                    command.Parameters.Add("@ProductID", SqlDbType.Int, 4).Value = ProductID;
                    command.Parameters.Add("@IsDefault", SqlDbType.Bit).Value = IsDefault;
                    command.Parameters.Add("@DefaultNumber", SqlDbType.Int, 4).Value = DefaultNumber;
                    SqlParameter parameter = new SqlParameter("@FrameConfigProductID", SqlDbType.Int, 4)
                    {
                        Direction = ParameterDirection.Output
                    };
                    command.Parameters.Add(parameter);
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        num = (int)parameter.Value;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException(string.Concat(new object[] { "Error adding product ", ProductID, " to FrameConfigID: ", FrameConfigID }), exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return num;
            }

            public bool InsertNote(int NoteID, string Description, string Text)
            {
                SqlConnection connection = new SqlConnection();
                bool flag = false;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "INSERT INTO tblNote (NoteID, Description, Text) VALUES (@NoteID, @Description, @Text)"
                    };
                    command.Parameters.Add("@NoteID", SqlDbType.Int, 4).Value = NoteID;
                    command.Parameters.Add("@Description", SqlDbType.VarChar, 100).Value = Description;
                    command.Parameters.Add("@Text", SqlDbType.VarChar, 0x3e8).Value = Text;
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        flag = true;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error adding note " + Description, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }

            public int InsertPriceCode(string PriceCode, string DisplayName, string Description, bool ShowOnBuyout, double DefaultMarkup)
            {
                SqlConnection connection = new SqlConnection();
                int num = -1;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "INSERT INTO tblPriceCode (PriceCode, DisplayName, Description, ShowOnBuyout, DefaultMarkup) VALUES (@PriceCode, @DisplayName, @Description, @ShowOnBuyout, @DefaultMarkup); SELECT @PriceCodeID = SCOPE_IDENTITY()"
                    };
                    command.Parameters.Add("@PriceCode", SqlDbType.VarChar, 15).Value = PriceCode;
                    command.Parameters.Add("@DisplayName", SqlDbType.VarChar, 50).Value = DisplayName;
                    command.Parameters.Add("@Description", SqlDbType.VarChar, 100).Value = Description;
                    command.Parameters.Add("@ShowOnBuyout", SqlDbType.Bit).Value = ShowOnBuyout;
                    if (DefaultMarkup != -1.0)
                    {
                        command.Parameters.Add("@DefaultMarkup", SqlDbType.Float).Value = DefaultMarkup;
                    }
                    else
                    {
                        command.Parameters.Add("@DefaultMarkup", SqlDbType.Float).SqlValue = DBNull.Value;
                    }
                    SqlParameter parameter = new SqlParameter("@PriceCodeID", SqlDbType.Int, 4)
                    {
                        Direction = ParameterDirection.Output
                    };
                    command.Parameters.Add(parameter);
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        num = (int)parameter.Value;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error adding price code " + PriceCode, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return num;
            }

            public int InsertProduct(string HFPartNumber, string Description, string Size, double ListPrice, string PriceCode, double Weight, double CostPrice, double PowerHP, double FlowCFM, int CategoryID, int Stages, int FrameID, int BrandID)
            {
                SqlConnection connection = new SqlConnection();
                int num = -1;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "INSERT INTO tblProduct (HFPartNumber, Description, Size, ListPrice, PriceCode, Weight, CostPrice, PowerHP, FlowCFM, CategoryID, Stages, FrameID, BrandID) VALUES (@HFPartNumber, @Description, @Size, @ListPrice, @PriceCode, @Weight, @CostPrice, @PowerHP, @FlowCFM, @CategoryID, @Stages, @FrameID, @BrandID); SELECT @ProductID = SCOPE_IDENTITY()"
                    };
                    command.Parameters.Add("@HFPartNumber", SqlDbType.VarChar, 0x19).Value = HFPartNumber;
                    command.Parameters.Add("@Description", SqlDbType.VarChar, 100).Value = Description;
                    command.Parameters.Add("@Size", SqlDbType.VarChar, 50).Value = Size;
                    command.Parameters.Add("@ListPrice", SqlDbType.Float).Value = ListPrice;
                    command.Parameters.Add("@PriceCode", SqlDbType.VarChar, 15).Value = PriceCode;
                    command.Parameters.Add("@Weight", SqlDbType.Float).Value = (Weight != -1.0) ? ((object)Weight) : ((object)DBNull.Value);
                    command.Parameters.Add("@CostPrice", SqlDbType.Float).Value = CostPrice;
                    command.Parameters.Add("@PowerHP", SqlDbType.Float).Value = (PowerHP != -1.0) ? ((object)PowerHP) : ((object)DBNull.Value);
                    command.Parameters.Add("@FlowCFM", SqlDbType.Float).Value = (FlowCFM != -1.0) ? ((object)FlowCFM) : ((object)DBNull.Value);
                    command.Parameters.Add("@CategoryID", SqlDbType.Int).Value = (CategoryID != -1) ? ((object)CategoryID) : ((object)DBNull.Value);
                    command.Parameters.Add("@Stages", SqlDbType.Int).Value = (Stages != -1) ? ((object)Stages) : ((object)DBNull.Value);
                    command.Parameters.Add("@FrameID", SqlDbType.Int).Value = (FrameID != -1) ? ((object)FrameID) : ((object)DBNull.Value);
                    command.Parameters.Add("@BrandID", SqlDbType.Int).Value = (BrandID != -1) ? ((object)BrandID) : ((object)DBNull.Value);
                    SqlParameter parameter = new SqlParameter("@ProductID", SqlDbType.Int, 4)
                    {
                        Direction = ParameterDirection.Output
                    };
                    command.Parameters.Add(parameter);
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        num = (int)parameter.Value;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error adding product " + HFPartNumber, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return num;
            }

            public int InsertUser(string Username, string Password, bool IsActive, bool IsAdmin, bool IsGDEmployee, int MaxDiscountRank, string Note, string FirstName, string LastName, string EmailAddress, int CompanyID, string Address1, string Address2, string City, string State, string Zip, string Phone)
            {
                SqlConnection connection = new SqlConnection();
                int num = -1;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "INSERT INTO tblUser (Username, Password, IsActive, IsAdmin, IsGDEmployee, MaxDiscountRank, Note, FirstName, LastName, EmailAddress, CompanyID, Address1, Address2, City, State, Zip, Phone) VALUES (@Username, @Password, @IsActive, @IsAdmin, @IsGDEmployee, @MaxDiscountRank, @Note, @FirstName, @LastName, @EmailAddress, @CompanyID, @Address1, @Address2, @City, @State, @Zip, @Phone); SELECT @UserID = SCOPE_IDENTITY()"
                    };
                    command.Parameters.Add("@Username", SqlDbType.VarChar, 0x19).Value = Username;
                    command.Parameters.Add("@Password", SqlDbType.VarChar, 100).Value = util.EncryptString(Password, Username);
                    command.Parameters.Add("@IsActive", SqlDbType.Bit).Value = IsActive;
                    command.Parameters.Add("@IsAdmin", SqlDbType.Bit).Value = IsAdmin;
                    command.Parameters.Add("@IsGDEmployee", SqlDbType.Bit).Value = IsGDEmployee;
                    command.Parameters.Add("@MaxDiscountRank", SqlDbType.Int, 4).Value = (MaxDiscountRank < 0) ? ((object)DBNull.Value) : ((object)MaxDiscountRank);
                    command.Parameters.Add("@Note", SqlDbType.VarChar, 250).Value = Note;
                    command.Parameters.Add("@FirstName", SqlDbType.VarChar, 50).Value = FirstName;
                    command.Parameters.Add("@LastName", SqlDbType.VarChar, 50).Value = LastName;
                    if (CompanyID != -1)
                    {
                        command.Parameters.Add("@CompanyID", SqlDbType.Int, 4).Value = CompanyID;
                    }
                    else
                    {
                        command.Parameters.Add("@CompanyID", SqlDbType.Int, 4).SqlValue = DBNull.Value;
                    }
                    command.Parameters.Add("@EmailAddress", SqlDbType.VarChar, 100).Value = EmailAddress;
                    command.Parameters.Add("@Address1", SqlDbType.VarChar, 50).Value = Address1;
                    command.Parameters.Add("@Address2", SqlDbType.VarChar, 50).Value = Address2;
                    command.Parameters.Add("@City", SqlDbType.VarChar, 30).Value = City;
                    command.Parameters.Add("@State", SqlDbType.VarChar, 15).Value = State;
                    command.Parameters.Add("@Zip", SqlDbType.VarChar, 15).Value = Zip;
                    command.Parameters.Add("@Phone", SqlDbType.VarChar, 15).Value = Phone;
                    SqlParameter parameter = new SqlParameter("@UserID", SqlDbType.Int, 4)
                    {
                        Direction = ParameterDirection.Output
                    };
                    command.Parameters.Add(parameter);
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        num = (int)parameter.Value;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error adding user " + Username, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return num;
            }

            public bool IsAdmin(string Username)
            {
                SqlConnection connection = new SqlConnection();
                bool flag = false;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT UserID FROM tblUser where (Username = @Username) AND (IsAdmin = 1)"
                    };
                    command.Parameters.Add("@Username", SqlDbType.VarChar, 0x19).Value = Username;
                    connection.Open();
                    if (command.ExecuteReader().HasRows)
                    {
                        flag = true;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while checking if user '" + Username + "' is an admin.", exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }

            public int IsAuthenticated(string Username, string Password)
            {
                SqlConnection connection = new SqlConnection();
                int num = -1;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT UserID FROM tblUser WHERE (Username = @Username) AND (Password = @Password) AND (IsActive = 1)"
                    };
                    command.Parameters.Add("@Username", SqlDbType.VarChar, 0x19).Value = Username;
                    command.Parameters.Add("@Password", SqlDbType.VarChar, 100).Value = util.EncryptString(Password, Username);
                    connection.Open();
                    num = (int)command.ExecuteScalar();
                    command.CommandText = "UPDATE tblUser SET LastLoginDate = GETDATE() WHERE (UserID = " + num + ")";
                    command.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while authenticating user: " + Username, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return num;
            }

            public bool IsGDEmployee(string Username)
            {
                SqlConnection connection = new SqlConnection();
                bool flag = false;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT UserID FROM tblUser where (Username = @Username) AND ((IsAdmin = 1) OR (ISGDEmployee = 1))"
                    };
                    command.Parameters.Add("@Username", SqlDbType.VarChar, 0x19).Value = Username;
                    connection.Open();
                    if (command.ExecuteReader().HasRows)
                    {
                        flag = true;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while checking if user '" + Username + "' is a GD employee.", exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }

            public bool IsUsernameOK(string Username)
            {
                SqlConnection connection = new SqlConnection();
                bool flag = false;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT UserID FROM tblUser where (Username = @Username)"
                    };
                    command.Parameters.Add("@Username", SqlDbType.VarChar, 0x19).Value = Username;
                    connection.Open();
                    if (!command.ExecuteReader().HasRows)
                    {
                        flag = true;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error while checking to see if username '" + Username + "' exists.", exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }

            public bool UpdateBrand(int BrandID, string Name)
            {
                SqlConnection connection = new SqlConnection();
                bool flag = false;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "UPDATE tblBrand SET Name=@Name WHERE BrandID = @BrandID"
                    };
                    command.Parameters.Add("@Name", SqlDbType.VarChar, 50).Value = Name;
                    command.Parameters.Add("@BrandID", SqlDbType.Int, 4).Value = BrandID;
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        flag = true;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error updating BrandID " + BrandID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }

            public bool UpdateCategory(int CategoryID, string DisplayName, int MasterCategoryID, int SortOrder)
            {
                SqlConnection connection = new SqlConnection();
                bool flag = false;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "UPDATE tblCategory SET DisplayName=@DisplayName, MasterCategoryID=@MasterCategoryID, SortOrder=@SortOrder WHERE CategoryID = @CategoryID"
                    };
                    command.Parameters.Add("@DisplayName", SqlDbType.VarChar, 150).Value = DisplayName;
                    if (MasterCategoryID != -1)
                    {
                        command.Parameters.Add("@MasterCategoryID", SqlDbType.Int, 4).Value = MasterCategoryID;
                    }
                    else
                    {
                        command.Parameters.Add("@MasterCategoryID", SqlDbType.Int, 4).SqlValue = DBNull.Value;
                    }
                    if (SortOrder != -1)
                    {
                        command.Parameters.Add("@SortOrder", SqlDbType.Int, 4).Value = SortOrder;
                    }
                    else
                    {
                        command.Parameters.Add("@SortOrder", SqlDbType.Int, 4).SqlValue = DBNull.Value;
                    }
                    command.Parameters.Add("@CategoryID", SqlDbType.Int, 4).Value = CategoryID;
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        flag = true;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error updating CategoryID " + CategoryID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }

            public bool UpdateCommissionMultiplier(int PriceCodeDiscountID, int PriceCodeID, int DiscountID, double Commission, double Multiplier)
            {
                SqlConnection connection = new SqlConnection();
                bool flag = false;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "UPDATE tblPriceCodeDiscount SET PriceCodeID=@PriceCodeID, DiscountID=@DiscountID, Commission=@Commission, Multiplier=@Multiplier WHERE PriceCodeDiscountID = @PriceCodeDiscountID"
                    };
                    command.Parameters.Add("@PriceCodeDiscountID", SqlDbType.Int, 4).Value = PriceCodeDiscountID;
                    command.Parameters.Add("@PriceCodeID", SqlDbType.Int, 4).Value = PriceCodeID;
                    command.Parameters.Add("@DiscountID", SqlDbType.Int, 4).Value = DiscountID;
                    command.Parameters.Add("@Commission", SqlDbType.Float).Value = Commission;
                    command.Parameters.Add("@Multiplier", SqlDbType.Float).Value = Multiplier;
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        flag = true;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error updating commission/multiplier with PriceCodeDiscountID " + PriceCodeDiscountID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }

            public bool UpdateCompany(int CompanyID, string CompanyName)
            {
                SqlConnection connection = new SqlConnection();
                bool flag = false;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "UPDATE tblCompany SET CompanyName=@CompanyName WHERE CompanyID = @CompanyID"
                    };
                    command.Parameters.Add("@CompanyName", SqlDbType.VarChar, 50).Value = CompanyName;
                    command.Parameters.Add("@CompanyID", SqlDbType.Int, 4).Value = CompanyID;
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        flag = true;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error updating CompanyID " + CompanyID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }

            public bool UpdateDiscount(int DiscountID, string DiscountCode, string DisplayName, double DefaultDiscount, int Rank)
            {
                SqlConnection connection = new SqlConnection();
                bool flag = false;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "UPDATE tblDiscount SET DiscountCode=@DiscountCode, DisplayName=@DisplayName, DefaultDiscount=@DefaultDiscount, Rank=@Rank WHERE DiscountID = @DiscountID"
                    };
                    command.Parameters.Add("@DiscountID", SqlDbType.Int, 4).Value = DiscountID;
                    command.Parameters.Add("@DiscountCode", SqlDbType.VarChar, 15).Value = DiscountCode;
                    command.Parameters.Add("@DisplayName", SqlDbType.VarChar, 50).Value = DisplayName;
                    command.Parameters.Add("@DefaultDiscount", SqlDbType.Float).Value = DefaultDiscount;
                    command.Parameters.Add("@Rank", SqlDbType.Int).Value = Rank;
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        flag = true;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error updating DiscountID " + DiscountID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }

            public bool UpdateEstimate(int EstimateID, int LastModifiedUserID, string Products, string DiscountCode, string QuoteNumber, string Representative, string QuotationFor, string Application, string Address, string ForInstallationAt, string EstimateDate, string EstimateNote)
            {
                SqlConnection connection = new SqlConnection();
                bool flag = false;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "UPDATE tblEstimate SET LastModifiedDate = getDate(), LastModifiedUserID = @LastModifiedUserID, Products = @Products, DiscountCode = @DiscountCode, Representative = @Representative, QuoteNumber = @QuoteNumber, QuotationFor = @QuotationFor, Address = @Address, Application = @Application, ForInstallationAt = @ForInstallationAt, EstimateDate = @EstimateDate, EstimateNote = @EstimateNote WHERE EstimateID = @EstimateID"
                    };
                    command.Parameters.Add("@EstimateID", SqlDbType.Int, 4).Value = EstimateID;
                    command.Parameters.Add("@LastModifiedUserID", SqlDbType.Int, 4).Value = LastModifiedUserID;
                    command.Parameters.Add("@Products", SqlDbType.Text).Value = (Products == "") ? ((object)DBNull.Value) : ((object)Products);
                    if (DiscountCode != "")
                    {
                        command.Parameters.Add("@DiscountCode", SqlDbType.VarChar, 15).Value = DiscountCode;
                    }
                    else
                    {
                        command.Parameters.Add("@DiscountCode", SqlDbType.VarChar, 15).SqlValue = DBNull.Value;
                    }
                    if (QuoteNumber != "")
                    {
                        command.Parameters.Add("@QuoteNumber", SqlDbType.VarChar, 50).Value = QuoteNumber;
                    }
                    else
                    {
                        command.Parameters.Add("@QuoteNumber", SqlDbType.VarChar, 50).SqlValue = DBNull.Value;
                    }
                    if (Representative != "")
                    {
                        command.Parameters.Add("@Representative", SqlDbType.VarChar, 50).Value = Representative;
                    }
                    else
                    {
                        command.Parameters.Add("@Representative", SqlDbType.VarChar, 50).SqlValue = DBNull.Value;
                    }
                    if (QuotationFor != "")
                    {
                        command.Parameters.Add("@QuotationFor", SqlDbType.VarChar, 50).Value = QuotationFor;
                    }
                    else
                    {
                        command.Parameters.Add("@QuotationFor", SqlDbType.VarChar, 50).SqlValue = DBNull.Value;
                    }
                    if (Application != "")
                    {
                        command.Parameters.Add("@Application", SqlDbType.VarChar, 100).Value = Application;
                    }
                    else
                    {
                        command.Parameters.Add("@Application", SqlDbType.VarChar, 100).SqlValue = DBNull.Value;
                    }
                    if (Address != "")
                    {
                        command.Parameters.Add("@Address", SqlDbType.VarChar, 150).Value = Address;
                    }
                    else
                    {
                        command.Parameters.Add("@Address", SqlDbType.VarChar, 150).SqlValue = DBNull.Value;
                    }
                    if (ForInstallationAt != "")
                    {
                        command.Parameters.Add("@ForInstallationAt", SqlDbType.VarChar, 100).Value = ForInstallationAt;
                    }
                    else
                    {
                        command.Parameters.Add("@ForInstallationAt", SqlDbType.VarChar, 100).SqlValue = DBNull.Value;
                    }
                    if (EstimateDate != "")
                    {
                        command.Parameters.Add("@EstimateDate", SqlDbType.DateTime).Value = DateTime.Parse(EstimateDate);
                    }
                    else
                    {
                        command.Parameters.Add("@EstimateDate", SqlDbType.DateTime).SqlValue = DBNull.Value;
                    }
                    if (EstimateNote != "")
                    {
                        command.Parameters.Add("@EstimateNote", SqlDbType.VarChar, 0x4b0).Value = EstimateNote;
                    }
                    else
                    {
                        command.Parameters.Add("@EstimateNote", SqlDbType.VarChar, 0x4b0).SqlValue = DBNull.Value;
                    }
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        flag = true;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error updating EstimateID " + EstimateID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }

            public bool UpdateFrame(int FrameID, string Name, string Filename, int BrandID)
            {
                SqlConnection connection = new SqlConnection();
                bool flag = false;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "UPDATE tblFrame SET Name=@Name, Filename=@Filename, BrandID=@BrandID WHERE FrameID = @FrameID"
                    };
                    command.Parameters.Add("@Name", SqlDbType.VarChar, 50).Value = Name;
                    command.Parameters.Add("@Filename", SqlDbType.VarChar, 500).Value = (Filename == "") ? ((object)DBNull.Value) : ((object)Filename);
                    if (BrandID != -1)
                    {
                        command.Parameters.Add("@BrandID", SqlDbType.Int, 4).Value = BrandID;
                    }
                    else
                    {
                        command.Parameters.Add("@BrandID", SqlDbType.Int, 4).SqlValue = DBNull.Value;
                    }
                    command.Parameters.Add("@FrameID", SqlDbType.Int, 4).Value = FrameID;
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        flag = true;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error updating FrameID " + FrameID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }

            public bool UpdateNote(int NoteID, string Description, string Text)
            {
                SqlConnection connection = new SqlConnection();
                bool flag = false;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "UPDATE tblNote SET Description=@Description, Text=@Text WHERE NoteID = @NoteID"
                    };
                    command.Parameters.Add("@NoteID", SqlDbType.Int, 4).Value = NoteID;
                    command.Parameters.Add("@Description", SqlDbType.VarChar, 100).Value = Description;
                    command.Parameters.Add("@Text", SqlDbType.VarChar, 0x3e8).Value = Text;
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        flag = true;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error updating NoteID " + NoteID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }

            public bool UpdatePriceCode(int PriceCodeID, string PriceCode, string DisplayName, string Description, bool ShowOnBuyout, double DefaultMarkup)
            {
                SqlConnection connection = new SqlConnection();
                bool flag = false;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "UPDATE tblPriceCode SET PriceCode=@PriceCode, DisplayName=@DisplayName, Description=@Description, ShowOnBuyout=@ShowOnBuyout, DefaultMarkup=@DefaultMarkup WHERE PriceCodeID = @PriceCodeID"
                    };
                    command.Parameters.Add("@PriceCodeID", SqlDbType.Int, 4).Value = PriceCodeID;
                    command.Parameters.Add("@PriceCode", SqlDbType.VarChar, 15).Value = PriceCode;
                    command.Parameters.Add("@DisplayName", SqlDbType.VarChar, 50).Value = DisplayName;
                    command.Parameters.Add("@Description", SqlDbType.VarChar, 100).Value = Description;
                    command.Parameters.Add("@ShowOnBuyout", SqlDbType.Bit).Value = ShowOnBuyout;
                    if (DefaultMarkup != -1.0)
                    {
                        command.Parameters.Add("@DefaultMarkup", SqlDbType.Float).Value = DefaultMarkup;
                    }
                    else
                    {
                        command.Parameters.Add("@DefaultMarkup", SqlDbType.Float).SqlValue = DBNull.Value;
                    }
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        flag = true;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error updating PriceCodeID " + PriceCodeID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }

            public bool UpdateProduct(int ProductID, string HFPartNumber, string Description, string Size, double ListPrice, string PriceCode, double Weight, double CostPrice, double PowerHP, double FlowCFM, int CategoryID, int Stages, int FrameID, int BrandID)
            {
                SqlConnection connection = new SqlConnection();
                bool flag = false;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "UPDATE tblProduct SET "
                    };
                    if (ProductID != -1)
                    {
                        command.CommandText = command.CommandText + "HFPartNumber=@HFPartNumber, ";
                    }
                    command.CommandText = command.CommandText + "Description=@Description, Size=@Size, ListPrice=@ListPrice, PriceCode=@PriceCode, Weight=@Weight, CostPrice=@CostPrice, PowerHP=@PowerHP, FlowCFM=@FlowCFM, CategoryID=@CategoryID, Stages=@Stages, FrameID=@FrameID, BrandID=@BrandID ";
                    command.CommandText = (ProductID == -1) ? (command.CommandText + "WHERE UPPER(HFPartNumber) = UPPER(@HFPartNumber)") : (command.CommandText + "WHERE ProductID = @ProductID");
                    if (ProductID != -1)
                    {
                        command.Parameters.Add("@ProductID", SqlDbType.Int).Value = ProductID;
                    }
                    command.Parameters.Add("@HFPartNumber", SqlDbType.VarChar, 0x19).Value = HFPartNumber;
                    command.Parameters.Add("@Description", SqlDbType.VarChar, 100).Value = Description;
                    command.Parameters.Add("@Size", SqlDbType.VarChar, 50).Value = Size;
                    command.Parameters.Add("@ListPrice", SqlDbType.Float).Value = ListPrice;
                    command.Parameters.Add("@PriceCode", SqlDbType.VarChar, 15).Value = PriceCode;
                    command.Parameters.Add("@Weight", SqlDbType.Float).Value = (Weight != -1.0) ? ((object)Weight) : ((object)DBNull.Value);
                    command.Parameters.Add("@CostPrice", SqlDbType.Float).Value = CostPrice;
                    command.Parameters.Add("@PowerHP", SqlDbType.Float).Value = (PowerHP != -1.0) ? ((object)PowerHP) : ((object)DBNull.Value);
                    command.Parameters.Add("@FlowCFM", SqlDbType.Float).Value = (FlowCFM != -1.0) ? ((object)FlowCFM) : ((object)DBNull.Value);
                    command.Parameters.Add("@CategoryID", SqlDbType.Int).Value = (CategoryID != -1) ? ((object)CategoryID) : ((object)DBNull.Value);
                    command.Parameters.Add("@Stages", SqlDbType.Int).Value = (Stages != -1) ? ((object)Stages) : ((object)DBNull.Value);
                    command.Parameters.Add("@FrameID", SqlDbType.Int).Value = (FrameID != -1) ? ((object)FrameID) : ((object)DBNull.Value);
                    command.Parameters.Add("@BrandID", SqlDbType.Int).Value = (BrandID != -1) ? ((object)BrandID) : ((object)DBNull.Value);
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        flag = true;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error updating Product with Part Number " + HFPartNumber, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }

            public bool UpdateUser(int UserID, string Username, string Password, bool IsActive, bool IsAdmin, bool IsGDEmployee, int MaxDiscountRank, string Note, string FirstName, string LastName, string EmailAddress, int CompanyID, string Address1, string Address2, string City, string State, string Zip, string Phone)
            {
                SqlConnection connection = new SqlConnection();
                bool flag = false;
                try
                {
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ToString();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection
                    };
                    string str = "";
                    if (Password != "")
                    {
                        str = "Password=@Password, ";
                    }
                    command.Parameters.Add("@UserID", SqlDbType.Int, 4).Value = UserID;
                    command.CommandText = "UPDATE tblUser SET " + str + "IsActive=@IsActive, IsAdmin=@IsAdmin, IsGDEmployee=@IsGDEmployee, MaxDiscountRank=@MaxDiscountRank, Note=@Note, FirstName=@FirstName, LastName=@LastName, EmailAddress=@EmailAddress, CompanyID=@CompanyID, Address1=@Address1, Address2=@Address2, City=@City, State=@State, Zip=@Zip, Phone=@Phone WHERE UserID = @UserID";
                    if (Password != "")
                    {
                        command.Parameters.Add("@Password", SqlDbType.VarChar, 100).Value = util.EncryptString(Password, Username);
                    }
                    command.Parameters.Add("@IsActive", SqlDbType.Bit).Value = IsActive;
                    command.Parameters.Add("@IsAdmin", SqlDbType.Bit).Value = IsAdmin;
                    command.Parameters.Add("@IsGDEmployee", SqlDbType.Bit).Value = IsGDEmployee;
                    command.Parameters.Add("@MaxDiscountRank", SqlDbType.Int, 4).Value = (MaxDiscountRank < 0) ? ((object)DBNull.Value) : ((object)MaxDiscountRank);
                    command.Parameters.Add("@Note", SqlDbType.VarChar, 250).Value = Note;
                    command.Parameters.Add("@FirstName", SqlDbType.VarChar, 50).Value = FirstName;
                    command.Parameters.Add("@LastName", SqlDbType.VarChar, 50).Value = LastName;
                    if (CompanyID != -1)
                    {
                        command.Parameters.Add("@CompanyID", SqlDbType.Int, 4).Value = CompanyID;
                    }
                    else
                    {
                        command.Parameters.Add("@CompanyID", SqlDbType.Int, 4).SqlValue = DBNull.Value;
                    }
                    command.Parameters.Add("@EmailAddress", SqlDbType.VarChar, 100).Value = EmailAddress;
                    command.Parameters.Add("@Address1", SqlDbType.VarChar, 50).Value = Address1;
                    command.Parameters.Add("@Address2", SqlDbType.VarChar, 50).Value = Address2;
                    command.Parameters.Add("@City", SqlDbType.VarChar, 30).Value = City;
                    command.Parameters.Add("@State", SqlDbType.VarChar, 15).Value = State;
                    command.Parameters.Add("@Zip", SqlDbType.VarChar, 15).Value = Zip;
                    command.Parameters.Add("@Phone", SqlDbType.VarChar, 15).Value = Phone;
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        flag = true;
                    }
                }
                catch (Exception exception)
                {
                    this.InsertException("Error updating user " + UserID, exception.Message + "<br/>\n" + exception.StackTrace, -1, "");
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }
        }

        public class util
        {
            // Methods
            public static void CheckSession()
            {
                string str = HttpContext.Current.Request.Path.ToLower();
                if ((!str.Contains("login.aspx") && (!str.Contains("register.aspx") && (!str.Contains("error.aspx") && !str.Contains("default.aspx")))) && (HttpContext.Current.Session["UserID"] == null))
                {
                    HttpContext.Current.Response.Redirect("default.aspx", true);
                }
            }

            public static string EncryptString(string inputString, string salt) =>
                FormsAuthentication.HashPasswordForStoringInConfigFile(inputString + salt, "MD5");

            public static DataTable GetEmptyTable()
            {
                DataTable table = new DataTable("tblEstimate");
                table.Columns.Add("RowID", 0.GetType());
                table.Columns.Add("SingleWeight", 0.0.GetType());
                table.Columns.Add("Weight", 0.0.GetType());
                table.Columns.Add("PartNo", "".GetType());
                table.Columns.Add("Quantity", 0.GetType());
                table.Columns.Add("Description", "".GetType());
                table.Columns.Add("Cost", 0.0.GetType());
                table.Columns.Add("RefSell", 0.0.GetType());
                table.Columns.Add("UnitSell", 0.0.GetType());
                table.Columns.Add("Discount", 0.0.GetType());
                table.Columns.Add("PriceCode", "".GetType());
                table.Columns.Add("Total", 0.0.GetType());
                table.Columns.Add("TotalNonDisc", 0.0.GetType());
                table.Columns.Add("Commission", 0.0.GetType());
                table.Columns.Add("CommissionAmount", 0.0.GetType());
                return table;
            }

            public static void SendEmail(string from, string to, string cc, string bcc, string subject, string body, bool isBodyHtml, string mailserv)
            {
                SmtpClient client = new SmtpClient(mailserv);
                MailMessage message = new MailMessage();
                if (string.IsNullOrEmpty(from))
                {
                    throw new ArgumentNullException("The from argument cannot be null");
                }
                if (string.IsNullOrEmpty(to))
                {
                    throw new ArgumentNullException("The to argument cannot be null");
                }
                if (string.IsNullOrEmpty(mailserv))
                {
                    throw new ArgumentNullException("The argument mailserv cannot be null");
                }
                char ch = ';';
                message.From = new MailAddress(from);
                if (to.Contains(","))
                {
                    ch = ',';
                }
                to = to.Trim(new char[] { ch });
                foreach (string str in to.Split(new char[] { ch }))
                {
                    message.To.Add(new MailAddress(str));
                }
                if (!string.IsNullOrEmpty(cc))
                {
                    ch = !cc.Contains(",") ? ';' : ',';
                    cc = cc.Trim(new char[] { ch });
                    foreach (string str2 in cc.Split(new char[] { ch }))
                    {
                        message.CC.Add(new MailAddress(str2));
                    }
                }
                if (!string.IsNullOrEmpty(bcc))
                {
                    ch = !bcc.Contains(",") ? ';' : ',';
                    bcc = bcc.Trim(new char[] { ch });
                    foreach (string str3 in bcc.Split(new char[] { ch }))
                    {
                        message.Bcc.Add(new MailAddress(str3));
                    }
                }
                message.IsBodyHtml = isBodyHtml;
                message.Subject = subject;
                message.Body = body;
                client.Send(message);
            }

            public static bool TestDBConnection(string connStr)
            {
                bool flag;
                SqlConnection connection = new SqlConnection();
                try
                {
                    connection.ConnectionString = connStr;
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "SELECT top 1 UserID FROM tblUser"
                    };
                    connection.Open();
                    command.ExecuteScalar();
                    flag = true;
                }
                catch (Exception)
                {
                    flag = false;
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
                return flag;
            }
        }
    }
}
