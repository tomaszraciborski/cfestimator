﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using static cfEstimator.App_Code;

namespace cfEstimator
{      
    public class userProfile : Page, IRequiresSessionState
    {
        // Fields
        protected Label lblMsg;
        protected ValidationSummary ValidationSummary1;
        protected TextBox tbUsername;
        protected RequiredFieldValidator RequiredFieldValidator1;
        protected RegularExpressionValidator RegularExpressionValidator1;
        protected CustomValidator CustomValidator4;
        protected HtmlTableCell tdPassword;
        protected TextBox tbPassword;
        protected CustomValidator CustomValidator1;
        protected HtmlTableCell tdPassword2;
        protected TextBox tbPassword2;
        protected CustomValidator CustomValidator2;
        protected CheckBox cbIsActive;
        protected CheckBox cbIsAdmin;
        protected CheckBox cbIsGDEmployee;
        protected DropDownList ddDiscounts;
        protected PlaceHolder phAdmin;
        protected CheckBoxList cbBrands;
        protected HtmlTableRow trBrand;
        protected TextBox tbFirstName;
        protected RequiredFieldValidator RequiredFieldValidator2;
        protected TextBox tbLastName;
        protected RequiredFieldValidator RequiredFieldValidator3;
        protected TextBox tbEmailAddress;
        protected RequiredFieldValidator RequiredFieldValidator5;
        protected TextBox tbPhone;
        protected RequiredFieldValidator RequiredFieldValidator4;
        protected DropDownList ddCompany;
        protected CustomValidator CustomValidator3;
        protected TextBox tbAddress1;
        protected TextBox tbAddress2;
        protected TextBox tbCity;
        protected TextBox tbState;
        protected TextBox tbZip;
        protected TextBox tbNote;
        protected HtmlTableRow trNote;
        protected Button btSave;
        protected Button btCancel;

        // Methods
        protected void btCancel_Click(object sender, EventArgs e)
        {
            base.Response.Redirect("admMngUsers.aspx");
        }

        protected void btSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                string brandsList = "";
                int num = 0;
                while (true)
                {
                    if (num >= this.cbBrands.Items.Count)
                    {
                        if (brandsList != "")
                        {
                            brandsList = brandsList.Remove(brandsList.Length - 1);
                        }
                        data data = new data();
                        if (this.ViewState["UserID"] != null)
                        {
                            if (!data.UpdateUser(int.Parse(this.ViewState["UserID"].ToString()), this.tbUsername.Text, this.tbPassword.Text.Trim(), this.cbIsActive.Checked, this.cbIsAdmin.Checked, this.cbIsGDEmployee.Checked, int.Parse(this.ddDiscounts.SelectedValue), this.tbNote.Text, this.tbFirstName.Text, this.tbLastName.Text, this.tbEmailAddress.Text, int.Parse(this.ddCompany.SelectedValue), this.tbAddress1.Text, this.tbAddress2.Text, this.tbCity.Text, this.tbState.Text, this.tbZip.Text, this.tbPhone.Text))
                            {
                                this.lblMsg.CssClass = "errorMsg";
                                this.lblMsg.Text = "Unable to update user information!";
                                break;
                            }
                            data.AssignUserBrands(int.Parse(this.ViewState["UserID"].ToString()), brandsList);
                            this.lblMsg.Text = "Successfully updated user information!";
                            return;
                        }
                        int userID = data.InsertUser(this.tbUsername.Text, this.tbPassword.Text.Trim(), this.cbIsActive.Checked, this.cbIsAdmin.Checked, this.cbIsGDEmployee.Checked, int.Parse(this.ddDiscounts.SelectedValue), this.tbNote.Text, this.tbFirstName.Text, this.tbLastName.Text, this.tbEmailAddress.Text, int.Parse(this.ddCompany.SelectedValue), this.tbAddress1.Text, this.tbAddress2.Text, this.tbCity.Text, this.tbState.Text, this.tbZip.Text, this.tbPhone.Text);
                        if (userID > 0)
                        {
                            data.AssignUserBrands(userID, brandsList);
                            base.Response.Redirect("userProfile.aspx?fn=added&id=" + userID);
                            return;
                        }
                        this.lblMsg.CssClass = "errorMsg";
                        this.lblMsg.Text = "Unable to add new user!";
                        return;
                    }
                    if (this.cbBrands.Items[num].Selected)
                    {
                        brandsList = brandsList + this.cbBrands.Items[num].Value + "|";
                    }
                    num++;
                }
            }
        }

        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {
            bool flag = true;
            if ((this.ViewState["UserID"] == null) && (this.tbPassword.Text.Trim() == ""))
            {
                flag = false;
            }
            args.IsValid = flag;
        }

        protected void CustomValidator2_ServerValidate(object source, ServerValidateEventArgs args)
        {
            bool flag = true;
            if ((this.tbPassword.Text.Trim() != "") && (this.tbPassword.Text != this.tbPassword2.Text))
            {
                flag = false;
            }
            args.IsValid = flag;
        }

        protected void CustomValidator3_ServerValidate(object source, ServerValidateEventArgs args)
        {
            bool flag = true;
            if (this.ddCompany.SelectedIndex == 0)
            {
                flag = false;
            }
            args.IsValid = flag;
        }

        protected void CustomValidator4_ServerValidate(object source, ServerValidateEventArgs args)
        {
            bool flag = true;
            if (((this.ViewState["UserID"] == null) && (this.tbUsername.Text.Trim() != "")) && !new data().IsUsernameOK(this.tbUsername.Text))
            {
                flag = false;
            }
            args.IsValid = flag;
        }

        protected void LoadInitial()
        {
            data data = new data();
            DataSet companies = data.GetCompanies(-1);
            this.ddCompany.DataSource = companies;
            this.ddCompany.DataBind();
            this.ddCompany.Items.Insert(0, new ListItem("None Selected", "-1", true));
            companies.Clear();
            companies = data.GetBrands(-1);
            this.cbBrands.DataSource = companies;
            this.cbBrands.DataBind();
            companies.Clear();
            companies = data.GetDiscounts("", -1);
            this.ddDiscounts.DataSource = companies;
            this.ddDiscounts.DataBind();
            this.ddDiscounts.Items.Insert(0, new ListItem("User can view all discounts", "-1"));
            this.ddDiscounts.SelectedIndex = 0;
        }

        protected void LoadUser(int UserID)
        {
            data data = new data();
            DataRow row = data.GetUsers(UserID).Tables[0].Rows[0];
            this.tbUsername.Text = row["Username"].ToString();
            if (row["IsActive"].ToString() != "")
            {
                this.cbIsActive.Checked = bool.Parse(row["IsActive"].ToString());
            }
            if (row["IsAdmin"].ToString() != "")
            {
                this.cbIsAdmin.Checked = bool.Parse(row["IsAdmin"].ToString());
            }
            if (row["IsGDEmployee"].ToString() != "")
            {
                this.cbIsGDEmployee.Checked = bool.Parse(row["IsGDEmployee"].ToString());
            }
            if (!row.IsNull("MaxDiscountRank"))
            {
                foreach (ListItem item in this.ddDiscounts.Items)
                {
                    if (item.Value == row["MaxDiscountRank"].ToString())
                    {
                        this.ddDiscounts.ClearSelection();
                        item.Selected = true;
                        break;
                    }
                }
            }
            this.tbFirstName.Text = row["FirstName"].ToString();
            this.tbLastName.Text = row["LastName"].ToString();
            this.tbEmailAddress.Text = row["EmailAddress"].ToString();
            this.tbPhone.Text = row["Phone"].ToString();
            this.ddCompany.SelectedValue = row["CompanyID"].ToString();
            this.tbAddress1.Text = row["Address1"].ToString();
            this.tbAddress2.Text = row["Address2"].ToString();
            this.tbCity.Text = row["City"].ToString();
            this.tbState.Text = row["State"].ToString();
            this.tbZip.Text = row["Zip"].ToString();
            this.tbNote.Text = row["Note"].ToString();
            foreach (DataRow row2 in data.GetBrandsByUser(UserID).Tables[0].Rows)
            {
                for (int i = 0; i < this.cbBrands.Items.Count; i++)
                {
                    if (this.cbBrands.Items[i].Value == row2["BrandID"].ToString())
                    {
                        this.cbBrands.Items[i].Selected = true;
                    }
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            util.CheckSession();
            if ((this.Session["IsAdmin"] == null) || !((bool)this.Session["IsAdmin"]))
            {
                this.phAdmin.Visible = false;
                this.trBrand.Visible = false;
                this.trNote.Visible = false;
                this.ViewState["UserID"] = this.Session["UserID"];
            }
            if (!this.Page.IsPostBack)
            {
                if ((base.Request.QueryString["fn"] != null) && (base.Request.QueryString["fn"].ToLower() == "added"))
                {
                    this.lblMsg.Text = "User has been successfully added!";
                }
                this.LoadInitial();
                if (this.Session["IsAdmin"] == null)
                {
                    goto TR_0000;
                }
                else if ((bool)this.Session["IsAdmin"])
                {
                    if (base.Request.QueryString["id"] != null)
                    {
                        this.tbUsername.Enabled = false;
                        this.ViewState["UserID"] = base.Request.QueryString["id"];
                        this.LoadUser(int.Parse(this.ViewState["UserID"].ToString()));
                        return;
                    }
                    this.tdPassword.Style.Value = "font-weight: bold;";
                    this.tdPassword2.Style.Value = "font-weight: bold;";
                }
                else
                {
                    goto TR_0000;
                }
            }
            return;
            TR_0000:
            this.phAdmin.Visible = false;
            this.trBrand.Visible = false;
            this.trNote.Visible = false;
            this.tbUsername.Enabled = false;
            this.btCancel.Visible = false;
            this.ViewState["UserID"] = this.Session["UserID"];
            this.LoadUser(int.Parse(this.ViewState["UserID"].ToString()));
        }

        // Properties
        protected DefaultProfile Profile =>
            ((DefaultProfile)this.Context.Profile);

        protected global_asax ApplicationInstance =>
            ((global_asax)this.Context.ApplicationInstance);
    }
}