﻿﻿<%@ page language="C#" masterpagefile="~/cfestimator.master" autoeventwireup="true"  CodeBehind="admMngPriceCodes.aspx.cs" Inherits="cfEstimator.admMngPriceCodes" title="Manage Price Codes" %>
<%@ Register src="admMenu.ascx" tagname="admMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<uc1:admMenu ID="admMenu1" runat="server" TagID="hrefPriceCodes" />
<div style="text-align: center">    
    <asp:Label runat="server" ID="lblMsg" CssClass="infoMsg" EnableViewState="false"></asp:Label></div>
<asp:PlaceHolder ID="phEditForm" runat="server" Visible="false">
<div class="dataFormBoxed">
<table>
<tr>
<td class="requiredField">Price Code:</td>
<td><asp:TextBox ID="tbPriceCode" runat="server" MaxLength="15"></asp:TextBox></td>
</tr>
<tr>
<td class="requiredField">Display Name:</td>
<td><asp:TextBox ID="tbDisplayName" runat="server" MaxLength="50"></asp:TextBox></td>
</tr>
<tr>
<td class="requiredField">Description:</td>
<td><asp:TextBox ID="tbDescription" runat="server" MaxLength="100" Width="600"></asp:TextBox></td>
</tr>
<tr>
<td class="requiredField">Show On Buyout:</td>
<td><asp:CheckBox ID="cbShowOnBuyout" runat="server" /></td>
</tr>
<tr>
<td>Default Markup:</td>
<td><asp:TextBox ID="tbDefaultMarkup" runat="server" MaxLength="25"></asp:TextBox></td>
</tr>
<tr>
<td colspan="2">
    <asp:Button ID="btSave" runat="server" Text="Save Changes" 
        onclick="btSave_Click" /> &nbsp;
    <asp:Button ID="btCancel" runat="server" Text="Cancel" 
        onclick="btCancel_Click" />
</td>
</tr>
</table>
</div>
</asp:PlaceHolder>
<div class="gridActions">
<strong>Actions:</strong>
<asp:LinkButton ID="lbAdd" runat="server" onclick="lbAdd_Click">Add new price code</asp:LinkButton>
</div>
<asp:GridView ID="gvData" runat="server" CssClass="gridTable" AllowSorting="False" 
    AutoGenerateColumns="False" DataKeyNames="PriceCodeID" OnRowDeleting="DeleteRow" 
    OnSelectedIndexChanging="SelectRow" CellPadding="0" GridLines="None" 
    BorderStyle="None" Width="100%">
    <Columns>
        <asp:CommandField ShowSelectButton="True" SelectImageUrl="~/img/edit.gif" SelectText="View" ButtonType="Image" ItemStyle-Width="16px" />
        <asp:TemplateField ShowHeader="false">
            <ItemStyle HorizontalAlign="Center" Width="16px" />
            <ItemTemplate>
                <asp:ImageButton ID="btnDel" runat="server" 
                    CausesValidation="False" CommandName="Delete"
                    ImageUrl="~/img/delete.gif" ToolTip="Delete" OnClientClick="return confirm('Are you sure you want to delete this price code?')" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="PriceCodeID" HeaderText="PriceCodeID" visible="false"/>
        <asp:BoundField DataField="PriceCode" HeaderText="Price Code" />
        <asp:BoundField DataField="DisplayName" HeaderText="Display Name" />
        <asp:BoundField DataField="Description" HeaderText="Description" />
        <asp:BoundField DataField="ShowOnBuyout" HeaderText="Show On Buyout?" />
        <asp:BoundField DataField="DefaultMarkup" HeaderText="Default Markup" />
    </Columns>
    <HeaderStyle CssClass="gridHeader" />
    <RowStyle CssClass="gridRow" VerticalAlign="Top" />
    <AlternatingRowStyle CssClass="gridAltRow" VerticalAlign="Top" />
</asp:GridView>
<input id="hdPriceCodeID" type="hidden" runat="server"/>
</asp:Content>


