﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using static cfEstimator.App_Code;

namespace cfEstimator
{
    public class admMngBrands : Page, IRequiresSessionState
    {
        // Fields
        protected admMenu admMenu1;
        protected Label lblMsg;
        protected Label lblBrandID;
        protected TextBox tbName;
        protected Button btSave;
        protected Button btCancel;
        protected PlaceHolder phEditForm;
        protected LinkButton lbAdd;
        protected GridView gvData;

        // Methods
        protected void BindGrid(int pgIndex)
        {
            DataView defaultView = new data().GetBrands(-1).Tables[0].DefaultView;
            defaultView.Sort = "Name";
            this.gvData.DataSource = defaultView;
            this.gvData.PageIndex = pgIndex;
            this.gvData.DataBind();
        }

        protected void btCancel_Click(object sender, EventArgs e)
        {
            this.tbName.Text = "";
            this.phEditForm.Visible = false;
        }

        protected void btSave_Click(object sender, EventArgs e)
        {
            data data = new data();
            if (this.tbName.Text.Trim() == "")
            {
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "The Name field is required!";
            }
            else if (this.lblBrandID.Text != "-")
            {
                if (!data.UpdateBrand(int.Parse(this.lblBrandID.Text), this.tbName.Text))
                {
                    this.lblMsg.CssClass = "errorMsg";
                    this.lblMsg.Text = "Unable to update brand '" + this.tbName.Text + "'!";
                }
                else
                {
                    this.lblMsg.Text = "Successfully updated brand '" + this.tbName.Text + "'!";
                    this.phEditForm.Visible = false;
                    this.BindGrid(0);
                }
            }
            else if (data.InsertBrand(this.tbName.Text) <= 0)
            {
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "Unable to add brand '" + this.tbName.Text + "'!";
            }
            else
            {
                this.lblMsg.Text = "Successfully added brand '" + this.tbName.Text + "'!";
                this.phEditForm.Visible = false;
                this.BindGrid(0);
            }
        }

        protected void ChangePage(object sender, GridViewPageEventArgs e)
        {
            this.BindGrid(e.NewPageIndex);
        }

        protected void DeleteRow(object sender, GridViewDeleteEventArgs e)
        {
            if (new data().DeleteBrand((int)this.gvData.DataKeys[e.RowIndex].Value))
            {
                this.lblMsg.Text = "Successfully deleted brand!";
                this.BindGrid(0);
            }
            else
            {
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "Unable to delete brand!  It may be associated with other data.";
            }
        }

        protected void lbAdd_Click(object sender, EventArgs e)
        {
            this.lblBrandID.Text = "-";
            this.tbName.Text = "";
            this.phEditForm.Visible = true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            util.CheckSession();
            if (!this.Page.IsPostBack)
            {
                this.BindGrid(0);
            }
        }

        protected void SelectRow(object sender, GridViewSelectEventArgs e)
        {
            DataSet brands = new data().GetBrands((int)this.gvData.DataKeys[e.NewSelectedIndex].Value);
            if (brands.Tables[0].Rows.Count > 0)
            {
                this.lblBrandID.Text = brands.Tables[0].Rows[0]["BrandID"].ToString();
                this.tbName.Text = brands.Tables[0].Rows[0]["Name"].ToString();
                this.phEditForm.Visible = true;
            }
        }

        // Properties
        protected DefaultProfile Profile =>
            ((DefaultProfile)this.Context.Profile);

        protected global_asax ApplicationInstance =>
            ((global_asax)this.Context.ApplicationInstance);
    }
}