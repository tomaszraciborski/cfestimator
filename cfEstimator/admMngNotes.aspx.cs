﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using static cfEstimator.App_Code;

namespace cfEstimator
{
    public class admMngNotes : Page, IRequiresSessionState
    {
        // Fields
        protected admMenu admMenu1;
        protected Label lblMsg;
        protected TextBox tbNoteID;
        protected TextBox tbDescription;
        protected TextBox tbText;
        protected Button btSave;
        protected Button btCancel;
        protected PlaceHolder phEditForm;
        protected LinkButton lbAdd;
        protected GridView gvData;

        // Methods
        protected void BindGrid(int pgIndex)
        {
            DataView defaultView = new data().GetNotes(-1).Tables[0].DefaultView;
            defaultView.Sort = "NoteID";
            this.gvData.DataSource = defaultView;
            this.gvData.PageIndex = pgIndex;
            this.gvData.DataBind();
        }

        protected void btCancel_Click(object sender, EventArgs e)
        {
            this.tbNoteID.Text = "";
            this.tbDescription.Text = "";
            this.tbText.Text = "";
            this.phEditForm.Visible = false;
        }

        protected void btSave_Click(object sender, EventArgs e)
        {
            data data = new data();
            if ((this.tbDescription.Text.Trim() == "") || (this.tbText.Text.Trim() == ""))
            {
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "The Description and Text fields are required!";
            }
            else
            {
                int result = 0;
                if (!int.TryParse(this.tbNoteID.Text, out result))
                {
                    this.lblMsg.CssClass = "errorMsg";
                    this.lblMsg.Text = "The NoteID field is required and must be an integer!";
                }
                else if (this.tbNoteID.Enabled)
                {
                    if (!data.InsertNote(result, this.tbDescription.Text, this.tbText.Text))
                    {
                        this.lblMsg.CssClass = "errorMsg";
                        this.lblMsg.Text = "Unable to add note '" + this.tbNoteID.Text + "'!";
                    }
                    else
                    {
                        this.lblMsg.Text = "Successfully added note '" + this.tbNoteID.Text + "'!";
                        this.phEditForm.Visible = false;
                        this.BindGrid(0);
                    }
                }
                else if (!data.UpdateNote(result, this.tbDescription.Text, this.tbText.Text))
                {
                    this.lblMsg.CssClass = "errorMsg";
                    this.lblMsg.Text = "Unable to update note '" + this.tbNoteID.Text + "'!";
                }
                else
                {
                    this.lblMsg.Text = "Successfully updated note '" + this.tbNoteID.Text + "'!";
                    this.phEditForm.Visible = false;
                    this.BindGrid(0);
                }
            }
        }

        protected void ChangePage(object sender, GridViewPageEventArgs e)
        {
            this.BindGrid(e.NewPageIndex);
        }

        protected void DeleteRow(object sender, GridViewDeleteEventArgs e)
        {
            if (new data().DeleteNote((int)this.gvData.DataKeys[e.RowIndex].Value))
            {
                this.lblMsg.Text = "Successfully deleted note!";
                this.BindGrid(0);
            }
            else
            {
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "Unable to delete note!  It may be associated with other data.";
            }
        }

        protected void lbAdd_Click(object sender, EventArgs e)
        {
            data data = new data();
            this.tbNoteID.Text = data.GetNewNoteID().ToString();
            this.tbDescription.Text = "";
            this.tbText.Text = "";
            this.tbNoteID.Enabled = true;
            this.phEditForm.Visible = true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            util.CheckSession();
            if (!this.Page.IsPostBack)
            {
                this.BindGrid(0);
            }
        }

        protected void SelectRow(object sender, GridViewSelectEventArgs e)
        {
            DataSet notes = new data().GetNotes((int)this.gvData.DataKeys[e.NewSelectedIndex].Value);
            if (notes.Tables[0].Rows.Count > 0)
            {
                this.tbNoteID.Text = notes.Tables[0].Rows[0]["NoteID"].ToString();
                this.tbDescription.Text = notes.Tables[0].Rows[0]["Description"].ToString();
                this.tbNoteID.Enabled = false;
                this.tbText.Text = notes.Tables[0].Rows[0]["Text"].ToString();
                this.phEditForm.Visible = true;
            }
        }

        // Properties
        protected DefaultProfile Profile =>
            ((DefaultProfile)this.Context.Profile);

        protected global_asax ApplicationInstance =>
            ((global_asax)this.Context.ApplicationInstance);
    }
}