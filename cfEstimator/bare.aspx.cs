﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using static cfEstimator.App_Code;

namespace cfEstimator
{
    public class bare : Page, IRequiresSessionState
    {
        // Fields
        protected DropDownList ddFrame;
        protected Label lblMsg;
        protected GridView gvData;

        // Methods
        protected void ddFrame_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddFrame.SelectedIndex > 0)
            {
                this.LoadProducts(ConfigurationManager.AppSettings["BareCategory"], int.Parse(this.ddFrame.SelectedValue));
            }
        }

        protected void LoadFrames()
        {
            data data = new data();
            DataSet framesByCategory = data.GetFramesByCategory(data.GetCategoryID(ConfigurationManager.AppSettings["BareCategory"]), this.Session["BrandAccess"].ToString());
            framesByCategory.Tables[0].Columns.Add(new DataColumn("PaddedName", Type.GetType("System.String")));
            string s = "";
            foreach (DataRow row in framesByCategory.Tables[0].Rows)
            {
                s = row["Name"].ToString();
                try
                {
                    int.Parse(s);
                    s = s.PadLeft(50, '0');
                }
                catch
                {
                }
                row["PaddedName"] = s;
            }
            framesByCategory.AcceptChanges();
            DataView defaultView = framesByCategory.Tables[0].DefaultView;
            defaultView.Sort = "PaddedName";
            this.ddFrame.DataSource = defaultView;
            this.ddFrame.DataBind();
            this.ddFrame.Items.Insert(0, new ListItem("Select...", "-1"));
        }

        protected void LoadProducts(string CategoryName, int FrameID)
        {
            data data = new data();
            int categoryID = data.GetCategoryID(CategoryName);
            if (categoryID > 0)
            {
                DataSet set = data.GetProductsByCategory(categoryID, this.Session["BrandAccess"].ToString(), FrameID, -1, -1.0, -1.0);
                set.Tables[0].Columns.Add(new DataColumn("isnote", Type.GetType("System.String")));
                foreach (DataRow row in set.Tables[0].Rows)
                {
                    row["isnote"] = !row.IsNull("NoteID") ? "*" : "";
                }
                set.AcceptChanges();
                this.gvData.DataSource = set;
                this.gvData.DataBind();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            util.CheckSession();
            if (!this.Page.IsPostBack)
            {
                this.LoadFrames();
            }
        }

        // Properties
        protected DefaultProfile Profile =>
            ((DefaultProfile)this.Context.Profile);

        protected global_asax ApplicationInstance =>
            ((global_asax)this.Context.ApplicationInstance);
    }
}