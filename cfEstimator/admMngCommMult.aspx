﻿﻿<%@ page language="C#" masterpagefile="~/cfestimator.master" autoeventwireup="true" CodeBehind="admMngCommMult.aspx.cs" Inherits="cfEstimator.admMngCommMult" title="Manage Discounts" %>
<%@ Register src="admMenu.ascx" tagname="admMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<uc1:admMenu ID="admMenu1" runat="server" TagID="hrefDiscounts" />
<div style="text-align: center">    
    <asp:Label runat="server" ID="lblMsg" CssClass="infoMsg" EnableViewState="false"></asp:Label></div>
<div>
<table width="100%">
<tr>
<td><strong>Commission/Multipliers for Discount: <asp:Label ID="lblDiscount" runat="server" Text=" "></asp:Label></strong></td>
<td align="right"><a href="admMngDiscounts.aspx"><< Back to Discounts</a></td>
</tr>
</table>
</div>
<asp:PlaceHolder ID="phEditForm" runat="server" Visible="false">
<div class="dataFormBoxed">
<table>
<tr>
<td class="requiredField">PriceCodeDiscountID:</td>
<td><asp:Label ID="lblPriceCodeDiscountID" runat="server"></asp:Label></td>
</tr>
<tr>
<td class="requiredField">Price Code:</td>
<td><asp:DropDownList ID="ddPCs" runat="server" DataTextField="DisplayName" DataValueField="PriceCodeID"></asp:DropDownList></td>
</tr>
<tr>
<td class="requiredField">Commission:</td>
<td><asp:TextBox ID="tbCommission" runat="server"></asp:TextBox></td>
</tr>
<tr>
<td class="requiredField">Multiplier:</td>
<td><asp:TextBox ID="tbMultiplier" runat="server"></asp:TextBox></td>
</tr>
<tr>
<td colspan="2">
    <asp:Button ID="btSave" runat="server" Text="Save Changes" 
        onclick="btSave_Click" /> &nbsp;
    <asp:Button ID="btCancel" runat="server" Text="Cancel" 
        onclick="btCancel_Click" />
</td>
</tr>
</table>
</div>
</asp:PlaceHolder>
<div class="gridActions">
<strong>Actions:</strong>
<asp:LinkButton ID="lbAdd" runat="server" onclick="lbAdd_Click">Add new commission/multiplier</asp:LinkButton>
</div>
<asp:GridView ID="gvData" runat="server" CssClass="gridTable" AllowPaging="False" AllowSorting="False" 
    AutoGenerateColumns="False" DataKeyNames="PriceCodeDiscountID" OnRowDeleting="DeleteRow" 
    OnSelectedIndexChanging="SelectRow" CellPadding="0"
    GridLines="None" BorderStyle="None" Width="100%">
    <Columns>
        <asp:CommandField ShowSelectButton="True" SelectImageUrl="~/img/edit.gif" SelectText="View" ButtonType="Image" ItemStyle-Width="16px" />
        <asp:TemplateField ShowHeader="false">
            <ItemStyle HorizontalAlign="Center" Width="16px" />
            <ItemTemplate>
                <asp:ImageButton ID="btnDel" runat="server" 
                    CausesValidation="False" CommandName="Delete"
                    ImageUrl="~/img/delete.gif" ToolTip="Delete" OnClientClick="return confirm('Are you sure you want to delete this commission/multiplier combination?')" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="PriceCodeDiscountID" Visible="false" />
        <asp:BoundField DataField="PriceCodeID" Visible="false" />
        <asp:BoundField DataField="DiscountID" Visible="false" />
        <asp:BoundField DataField="PCDisplayName" HeaderText="Price Code" />
        <asp:BoundField DataField="Commission" HeaderText="Commission" />
        <asp:BoundField DataField="Multiplier" HeaderText="Multiplier" />
    </Columns>
    <HeaderStyle CssClass="gridHeader" />
    <RowStyle CssClass="gridRow" VerticalAlign="Top" />
    <AlternatingRowStyle CssClass="gridAltRow" VerticalAlign="Top" />
</asp:GridView>
<asp:HiddenField ID="hdDiscountID" runat="server" Value="-1" />
</asp:Content>



