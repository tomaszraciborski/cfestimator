﻿﻿<%@ control language="C#" autoeventwireup="true" CodeBehind="admMenu.ascx.cs" Inherits="cfEstimator.admMenu" %>
<h2>Manage: 
<a href="admMngUsers.aspx" id="hrefUsers" runat="server">Users</a> |
<a href="admMngProducts.aspx"  id="hrefProducts" runat="server">Products</a> |
<a href="admMngBrands.aspx"  id="hrefBrands" runat="server">Brands</a> |
<a href="admMngCategories.aspx" id="hrefCategories" runat="server">Categories</a> |
<a href="admMngCompanies.aspx" id="hrefCompanies" runat="server">Companies</a> |
<a href="admMngConfigs.aspx" id="hrefConfigs" runat="server">Configurations</a> |
<a href="admMngDiscounts.aspx" id="hrefDiscounts" runat="server">Discounts</a> |
<a href="admMngFrames.aspx" id="hrefFrames" runat="server">Frames</a> |
<a href="admMngNotes.aspx" id="hrefNotes" runat="server">Notes</a> |
<a href="admMngPriceCodes.aspx" id="hrefPriceCodes" runat="server">PCs</a> |
<a href="admErrors.aspx" id="hrefErrors" runat="server">Errors</a></h2>
