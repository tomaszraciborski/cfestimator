﻿﻿<%@ page language="C#" masterpagefile="~/cfestimator.master" autoeventwireup="true"  CodeBehind="fieldService.aspx.cs" Inherits="cfEstimator.fieldService" title="Field Service" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script type="text/javascript">
var prevQty = -1;
function setPrevQty(mytb)
{
  prevQty = mytb.value;
}

function checkInt(mytb, isDomestic)
{
  if (mytb.value != prevQty)
  {
    var myval = mytb.value;
    if (myval == "")
      myval = 0;

    if (isNaN(parseInt(myval, 10)) || (myval < 0))
    {
      mytb.value = prevQty;
      alert("Invalid quantity entered!");
      return false;
    }
      
    myval = parseInt(myval, 10);
    if (myval != mytb.value)
      mytb.value = myval;
    if (isDomestic)
      setDomesticTotals();
    else
      setInternationalTotals();
  }
}

function checkCurrency(mytb, isDomestic)
{
  if (mytb.value != prevQty)
  {
    var myval = mytb.value;
    if (myval == "")
      myval = 0;
 
    if (isNaN(parseFloat(myval)) || (myval < 0))
    {
      mytb.value = prevQty;
      alert("Invalid quantity entered!");
      return false;
    }
    if (isDomestic)
      setDomesticTotals();
    else
      setInternationalTotals();
  }
}

function checkOnsiteDays(mytb)
{
  if (mytb.value != prevQty)
  {
      var myval = mytb.value;
      if (myval == "")
        myval = 0;

      if (isNaN(parseFloat(myval)) || (myval < 0))
      {
        mytb.value = prevQty;
        alert("Invalid quantity entered!");
        return false;
      }
  }
  setDomesticTotals();
  setInternationalTotals();
}

function multValues(mytb, Source, divDest)
{
  if (mytb.value != prevQty)
  {
      var myval = mytb.value;
      if (myval == "")
        myval = 0;

      if (isNaN(parseInt(myval, 10)) || (myval < 0))
      {
        mytb.value = prevQty;
        alert("Invalid quantity entered!");
        return false;
      }
      
      myval = parseInt(myval, 10);
      if (myval != mytb.value)
        mytb.value = myval;
        
      var valSource = document.getElementById(Source).innerHTML;
      if (valSource == "")
        valSource = 0;
      var oldVal = document.getElementById(divDest).innerHTML;
      var newVal = valSource * myval;
      var diffVal = newVal - oldVal;
      document.getElementById(divDest).innerHTML = valSource * myval;
      var currTotal = document.getElementById("onsiteHrs").innerHTML;
      var newTotal = formatNumber(parseFloat(currTotal) + parseFloat(diffVal));
      if (newTotal == "")
        newTotal = 0;
      document.getElementById("onsiteHrs").innerHTML = newTotal;
      var totalDays = Math.ceil(newTotal / 10);
      document.getElementById("onsiteDays").innerHTML = totalDays;
      document.getElementById("tbNumDays").value = totalDays;
      setDomesticTotals();
      setInternationalTotals();
  }
}

function setDomesticTotals()
{
  var totalDays = document.getElementById("tbNumDays").value;
  var dailyRate = convertToNumber(document.getElementById("divDomDailyRate").innerHTML);
  var dailyRateTotal = parseFloat(totalDays) * parseFloat(dailyRate);
  var numtrips = parseInt(document.getElementById("tbDomNumTrips").value, 10);
  var numtripsTotal = numtrips * convertToNumber(document.getElementById("divDomCostPerTrip").innerHTML);
  var airfare = convertToNumber(document.getElementById("tbDomAirfare").value);
  var airfareTotal = airfare * numtrips;
  var thirdpartyTotal = 1.3 * parseFloat(document.getElementById("tbDom3rdParty").value);
  var miscTotal = parseFloat(document.getElementById("tbDomMisc").value);
  document.getElementById("divDomDailyRateTotal").innerHTML = formatCurrency(dailyRateTotal);
  document.getElementById("divDomNumTrips").innerHTML = formatCurrency(numtripsTotal);
  document.getElementById("divDomAirfare").innerHTML = formatCurrency(airfareTotal);
  document.getElementById("divDom3rdParty").innerHTML = formatCurrency(thirdpartyTotal);
  document.getElementById("divDomMisc").innerHTML = formatCurrency(miscTotal);
  var domTotal =
  convertToNumber(document.getElementById("divDomDailyRateTotal").innerHTML) +
  convertToNumber(document.getElementById("divDomNumTrips").innerHTML) +
  convertToNumber(document.getElementById("divDomAirfare").innerHTML) +
  convertToNumber(document.getElementById("divDom3rdParty").innerHTML) +
  convertToNumber(document.getElementById("divDomMisc").innerHTML);
  document.getElementById("divDomTotal").innerHTML = formatCurrency(domTotal);
  document.getElementById(hdDomDays).value = totalDays;
  document.getElementById(hdDomTotal).value = domTotal;
}

function setInternationalTotals()
{
  var totalDays = document.getElementById("tbNumDays").value;
  var dailyRate = convertToNumber(document.getElementById("divIntDailyRate").innerHTML);
  var dailyRateTotal = parseFloat(totalDays) * parseFloat(dailyRate);
  var numtrips = parseInt(document.getElementById("tbIntNumTrips").value, 10);
  var numtripsTotal = numtrips * convertToNumber(document.getElementById("divIntCostPerTrip").innerHTML);
  var airfare = convertToNumber(document.getElementById("tbIntAirfare").value);
  var airfareTotal = airfare * numtrips;
  var thirdpartyTotal = 1.3 * parseFloat(document.getElementById("tbInt3rdParty").value);
  var miscTotal = parseFloat(document.getElementById("tbIntMisc").value);
  document.getElementById("divIntDailyRateTotal").innerHTML = formatCurrency(dailyRateTotal);
  document.getElementById("divIntNumTrips").innerHTML = formatCurrency(numtripsTotal);
  document.getElementById("divIntAirfare").innerHTML = formatCurrency(airfareTotal);
  document.getElementById("divInt3rdParty").innerHTML = formatCurrency(thirdpartyTotal);
  document.getElementById("divIntMisc").innerHTML = formatCurrency(miscTotal);
  var intTotal =
  convertToNumber(document.getElementById("divIntDailyRateTotal").innerHTML) +
  convertToNumber(document.getElementById("divIntNumTrips").innerHTML) +
  convertToNumber(document.getElementById("divIntAirfare").innerHTML) +
  convertToNumber(document.getElementById("divInt3rdParty").innerHTML) +
  convertToNumber(document.getElementById("divIntMisc").innerHTML);
  document.getElementById("divIntTotal").innerHTML = formatCurrency(intTotal);
  document.getElementById(hdIntDays).value = totalDays;
  document.getElementById(hdIntTotal).value = intTotal;
}

function convertToNumber(currency)
{
  var num = currency.toString().replace(/\$|\,/g,'');
  return parseFloat(num);
}

function formatCurrency(num) {
  num = num.toString().replace(/\$|\,/g,'');
  if(isNaN(num))
    num = "0";
  sign = (num == (num = Math.abs(num)));
  num = Math.floor(num*100+0.50000000001);
  cents = num%100;
  num = Math.floor(num/100).toString();
  if(cents<10)
    cents = "0" + cents;
  for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
    num = num.substring(0,num.length-(4*i+3))+','+
  num.substring(num.length-(4*i+3));
  return (((sign)?'':'-') + '$' + num + '.' + cents);
}
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h2>Field Service</h2>
<strong>Start-Up (Domestic)</strong>

<table border="0" cellpadding="0" cellspacing="0">
<tr valign="top"><td>

<table class="fieldServiceTable" 
border="0" cellpadding="0" cellspacing="0"
style="border-style:None;border-collapse:collapse;">
<tr class="headerRow">
    <td class="alignLeft"><strong>Blower/Motor</strong></td>
    <td>Hours/unit<sup>1</sup></td>
    <td># of Units</td>
    <td>On-Site Hours</td>
</tr>
<tr>
    <td class="alignLeft"> &nbsp; 6" Inlet & Smaller</td>
    <td><div id="divHrsPerUnit1">4</div></td>
    <td><input id="tbUnits1" type="text" class="thinTexbox" style="width: 45px" 
    onfocus="setPrevQty(this)" onchange="multValues(this, 'divHrsPerUnit1', 'divHrs1')" 
    onkeypress="return submitenter(this,event,null,true)"/></td>
    <td><div id="divHrs1">0</div></td>
</tr>
<tr>
    <td class="alignLeft"> &nbsp; 8" to 10" Inlet</td>
    <td><div id="divHrsPerUnit2">6</div></td>
    <td><input id="tbUnits2" type="text" class="thinTexbox" style="width: 45px" 
    onfocus="setPrevQty(this)" onchange="multValues(this, 'divHrsPerUnit2', 'divHrs2')"
    onkeypress="return submitenter(this,event,null,true)"/></td>
    <td><div id="divHrs2">0</div></td>
</tr>
<tr>
    <td class="alignLeft"> &nbsp; 12" to 14" Inlet</td>
    <td><div id="divHrsPerUnit3">8</div></td>
    <td><input id="tbUnits3" type="text" class="thinTexbox" style="width: 45px" 
    onfocus="setPrevQty(this)" onchange="multValues(this, 'divHrsPerUnit3', 'divHrs3')"
    onkeypress="return submitenter(this,event,null,true)"/></td>
    <td><div id="divHrs3">0</div></td>
</tr>
<tr>
    <td class="alignLeft"> &nbsp; 16" & Larger</td>
    <td><div id="divHrsPerUnit4">10</div></td>
    <td><input id="tbUnits4" type="text" class="thinTexbox" style="width: 45px" 
    onfocus="setPrevQty(this)" onchange="multValues(this, 'divHrsPerUnit4', 'divHrs4')"
    onkeypress="return submitenter(this,event,null,true)"/></td>
    <td><div id="divHrs4">0</div></td>
</tr>
<tr class="headerRow">
    <td class="alignLeft"><strong>Blower/Motor/Controls</strong><sup>2</sup></td>
    <td>Hours/unit<sup>1</sup></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>
<tr>
    <td class="alignLeft"> &nbsp; 6" Inlet & Smaller</td>
    <td><div id="divHrsPerUnit5">4</div></td>
    <td><input id="tbUnits5" type="text" class="thinTexbox" style="width: 45px" 
    onfocus="setPrevQty(this)" onchange="multValues(this, 'divHrsPerUnit5', 'divHrs5')"
    onkeypress="return submitenter(this,event,null,true)"/></td>
    <td><div id="divHrs5">0</div></td>
</tr>
<tr>
    <td class="alignLeft"> &nbsp; 8" to 10" Inlet</td>
    <td><div id="divHrsPerUnit6">6</div></td>
    <td><input id="tbUnits6" type="text" class="thinTexbox" style="width: 45px" 
    onfocus="setPrevQty(this)" onchange="multValues(this, 'divHrsPerUnit6', 'divHrs6')"
    onkeypress="return submitenter(this,event,null,true)"/></td>
    <td><div id="divHrs6">0</div></td>
</tr>
<tr>
    <td class="alignLeft"> &nbsp; 12" to 14" Inlet</td>
    <td><div id="divHrsPerUnit7">8</div></td>
    <td><input id="tbUnits7" type="text" class="thinTexbox" style="width: 45px" 
    onfocus="setPrevQty(this)" onchange="multValues(this, 'divHrsPerUnit7', 'divHrs7')"
    onkeypress="return submitenter(this,event,null,true)"/></td>
    <td><div id="divHrs7">0</div></td>
</tr>
<tr>
    <td class="alignLeft"> &nbsp; 16" & Larger</td>
    <td><div id="divHrsPerUnit8">10</div></td>
    <td><input id="tbUnits8" type="text" class="thinTexbox" style="width: 45px" 
    onfocus="setPrevQty(this)" onchange="multValues(this, 'divHrsPerUnit8', 'divHrs8')"
    onkeypress="return submitenter(this,event,null,true)"/></td>
    <td><div id="divHrs8">0</div></td>
</tr>
<tr class="headerRow">
    <td class="alignLeft"><strong>Extra Controls</strong><sup>3</sup> (ADD b/m $)</td>
    <td>Hours/panel<sup>4</sup></td>
    <td># of Panels<sup>5</sup></td>
    <td>On-Site Hours</td>
</tr>
<tr>
    <td class="alignLeft"> &nbsp; Valve Conrol/Comm</td>
    <td><div id="divHrsPerUnit9">8</div></td>
    <td><input id="tbUnits9" type="text" class="thinTexbox" style="width: 45px" 
    onfocus="setPrevQty(this)" onchange="multValues(this, 'divHrsPerUnit9', 'divHrs9')"
    onkeypress="return submitenter(this,event,null,true)"/></td>
    <td><div id="divHrs9">0</div></td>
</tr>
<tr>
    <td class="alignLeft"> &nbsp; MDOCS (DO System)</td>
    <td><div id="divHrsPerUnit10">80</div></td>
    <td><input id="tbUnits10" type="text" class="thinTexbox" style="width: 45px" 
    onfocus="setPrevQty(this)" onchange="multValues(this, 'divHrsPerUnit10', 'divHrs10')"
    onkeypress="return submitenter(this,event,null,true)"/></td>
    <td><div id="divHrs10">0</div></td>
</tr>
<tr>
    <td colspan="2"></td>
    <td><strong>ON-SITE HOURS</strong></td>
    <td><div id="onsiteHrs">0</div></td>
</tr>
<tr>
    <td colspan="2" style="text-align: right">(Rounded up to complete 10-hr day)</td>
    <td><strong>ON-SITE DAYS</strong></td>
    <td><div id="onsiteDays">0</div></td>
</tr>
<tr>
    <td colspan="3" style="text-align: right">(OR, input days required)</td>
    <td><input id="tbNumDays" type="text" class="thinTexbox" style="width: 45px" 
    onfocus="setPrevQty(this)" onchange="checkOnsiteDays(this)"
    onkeypress="return submitenter(this,event,null,true)"/></td>
</tr>
</table>

</td><td style="width: 20px;">&nbsp;</td><td>

<br />
<table class="fieldServiceTable" 
border="0" cellpadding="0" cellspacing="0"
style="border-style:None;border-collapse:collapse;">
<tr class="headerRow">
    <td colspan="4"><strong>Domestic</strong></td>
</tr>
<tr>
    <td class="alignLeft" colspan="2">Daily Rate</td>
    <td><div id="divDomDailyRate">$1,300</div></td>
    <td><div id="divDomDailyRateTotal">$0.00</div></td>
</tr>
<tr>
    <td class="alignLeft">#/Trips<sup>8</sup></td>
    <td><input id="tbDomNumTrips" type="text" class="thinTexbox" style="width: 75px" 
    onfocus="setPrevQty(this)" onchange="checkInt(this, true)"
    onkeypress="return submitenter(this,event,null,true)"/></td>
    <td><div id="divDomCostPerTrip">$900</div></td>
    <td><div id="divDomNumTrips">$0.00</div></td>
</tr>
<tr>
    <td class="alignLeft" colspan="2">Airfare<sup>6</sup></td>
    <td>$<input id="tbDomAirfare" type="text" class="thinTexbox" style="width: 75px" 
    value="750" onfocus="setPrevQty(this)" onchange="checkCurrency(this, true)"
    onkeypress="return submitenter(this,event,null,true)"/></td>
    <td><div id="divDomAirfare">$0.00</div></td>
</tr>
<tr>
    <td class="alignLeft" colspan="2">3rd Party<sup>7</sup></td>
    <td>$<input id="tbDom3rdParty" type="text" class="thinTexbox" style="width: 75px" 
    onfocus="setPrevQty(this)" onchange="checkCurrency(this, true)"
    onkeypress="return submitenter(this,event,null,true)"/></td>
    <td><div id="divDom3rdParty">$0.00</div></td>
</tr>
<tr>
    <td class="alignLeft" colspan="2">Misc</td>
    <td>$<input id="tbDomMisc" type="text" class="thinTexbox" style="width: 75px" 
    onfocus="setPrevQty(this)" onchange="checkCurrency(this, true)"
    onkeypress="return submitenter(this,event,null,true)"/></td>
    <td><div id="divDomMisc">$0.00</div></td>
</tr>
<tr>
    <td colspan="2"><asp:Button ID="btDomAdd" runat="server" Text="Add to Estimate" 
            CssClass="thinTexbox" onclick="btDomAdd_Click" /></td>
    <td class="alignRight"><strong>Total<sup>9</sup></strong></td>
    <td><div id="divDomTotal">$0.00</div></td>
</tr>
</table>
<br />
<table class="fieldServiceTable" 
border="0" cellpadding="0" cellspacing="0"
style="border-style:None;border-collapse:collapse;">
<tr class="headerRow">
    <td colspan="4"><strong>International</strong></td>
</tr>
<tr>
    <td class="alignLeft" colspan="2">Daily Rate</td>
    <td><div id="divIntDailyRate">$1,860</div></td>
    <td><div id="divIntDailyRateTotal">$0.00</div></td>
</tr>
<tr>
    <td class="alignLeft">#/Trips<sup>8</sup></td>
    <td><input id="tbIntNumTrips" type="text" class="thinTexbox" style="width: 75px"
    onfocus="setPrevQty(this)" onchange="checkInt(this, false)"
    onkeypress="return submitenter(this,event,null,true)"/></td>
    <td><div id="divIntCostPerTrip">$1,080</div></td>
    <td><div id="divIntNumTrips">$0.00</div></td>
</tr>
<tr>
    <td class="alignLeft" colspan="2">Airfare<sup>6</sup></td>
    <td>$<input id="tbIntAirfare" type="text" class="thinTexbox" style="width: 75px" value="5000" 
    onfocus="setPrevQty(this)" onchange="checkCurrency(this, false)"
    onkeypress="return submitenter(this,event,null,true)"/></td>
    <td><div id="divIntAirfare">$0.00</div></td>
</tr>
<tr>
    <td class="alignLeft" colspan="2">3rd Party<sup>7</sup></td>
    <td>$<input id="tbInt3rdParty" type="text" class="thinTexbox" style="width: 75px" 
    onfocus="setPrevQty(this)" onchange="checkCurrency(this, false)"
    onkeypress="return submitenter(this,event,null,true)"/></td>
    <td><div id="divInt3rdParty">$0.00</div></td>
</tr>
<tr>
    <td class="alignLeft" colspan="2">Misc</td>
    <td>$<input id="tbIntMisc" type="text" class="thinTexbox" style="width: 75px" 
    onfocus="setPrevQty(this)" onchange="checkCurrency(this, false)"
    onkeypress="return submitenter(this,event,null,true)"/></td>
    <td><div id="divIntMisc">$0.00</div></td>
</tr>
<tr>
    <td colspan="2"><asp:Button ID="btIntAdd" runat="server" Text="Add to Estimate" 
            CssClass="thinTexbox" onclick="btIntAdd_Click" /></td>
    <td class="alignRight"><strong>Total<sup>9</sup></strong></td>
    <td><div id="divIntTotal">$0.00</div></td>
</tr>
</table>

</td></tr></table>
<br />
<div id="notes">
<strong>Notes</strong>
<ol>
<li>These are on-site hours.  Does not include travel time.  That is added separately.</li>
<li>"Controls": Includes basic control: surge protection, vibration, overload.  If there is any inlet valve control or communication between our panel and DCS, use the EXTRA CONTROLS pricing.</li>
<li>"Extra Controls": This will be performed by panel supplier.  To be used for any inlet valve control, PLC communciation with DCS, MDOCS.  When in doubt, consult Julie Keller.<br /> This does not include blower start-up.  Still add for blower start-up using the "Blower/Motor" pricing.</li>
<li>On average, a start-up for panel under "Extra Controls" requires 8-hours on-site per panel.</li>
<li>A MDOCS system requires (2) trips for 1 week each.  Enter 1 under the # of panels.</li>
<li>This is an average of either mileage or airfare.  This can be adjusted accordingly.</li>
<li>The above rates cover blowers and panels.  "3rd Party" will cover anything else as necessary such as motors.  The mark-up is 1.3 times.</li>
<li>The above charges include training UNLESS training is specifically called out as a separate trip.  If so, add an additional trip under #/Trips.</li>
<li>All start-up pays zero (0%) commission.</li>
</ol>
</div>
<asp:HiddenField ID="hdDomDays" runat="server" Value="0" />
<asp:HiddenField ID="hdDomTotal" runat="server" Value="0" />
<asp:HiddenField ID="hdIntDays" runat="server" Value="0" />
<asp:HiddenField ID="hdIntTotal" runat="server" Value="0" />
</asp:Content>

