﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using static cfEstimator.App_Code;

namespace cfEstimator
{
    public class admErrors : Page, IRequiresSessionState
    {
        // Fields
        protected admMenu admMenu1;
        protected Label lblMsg;
        protected GridView gvErrors;

        // Methods
        protected void BindGrid(int pgIndex)
        {
            DataView defaultView = new data().GetExceptions(-1).Tables[0].DefaultView;
            defaultView.Sort = "ExceptionDate DESC";
            this.gvErrors.DataSource = defaultView;
            this.gvErrors.PageIndex = pgIndex;
            this.gvErrors.DataBind();
        }

        protected void ChangePage(object sender, GridViewPageEventArgs e)
        {
            this.BindGrid(e.NewPageIndex);
        }

        protected void DeleteException(int ExceptionID)
        {
            if (new data().DeleteException(ExceptionID))
            {
                this.lblMsg.Text = "Successfully deleted exception!";
            }
            else
            {
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "Unable to delete exception!";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            util.CheckSession();
            if (!this.Page.IsPostBack)
            {
                if ((base.Request.QueryString["fn"] != null) && (base.Request.QueryString["fn"] == "delete"))
                {
                    this.DeleteException(int.Parse(base.Request.QueryString["id"]));
                }
                this.BindGrid(0);
            }
        }

        // Properties
        protected DefaultProfile Profile =>
            ((DefaultProfile)this.Context.Profile);

        protected global_asax ApplicationInstance =>
            ((global_asax)this.Context.ApplicationInstance);
    }
}