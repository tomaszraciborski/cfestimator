﻿﻿<%@ page language="C#" masterpagefile="~/cfestimator.master" autoeventwireup="true" CodeBehind="admMngDiscounts.aspx.cs" Inherits="cfEstimator.admMngDiscounts" title="Manage Discounts" %>
<%@ Register src="admMenu.ascx" tagname="admMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<uc1:admMenu ID="admMenu1" runat="server" TagID="hrefDiscounts" />
<div style="text-align: center">    
    <asp:Label runat="server" ID="lblMsg" CssClass="infoMsg" EnableViewState="false"></asp:Label></div>
<asp:PlaceHolder ID="phEditForm" runat="server" Visible="false">
<div class="dataFormBoxed">
<table>
<tr>
<td class="requiredField">Discount Code:</td>
<td><asp:TextBox ID="tbDiscountCode" runat="server" MaxLength="15"></asp:TextBox></td>
</tr>
<tr>
<td class="requiredField">Display Name:</td>
<td><asp:TextBox ID="tbDisplayName" runat="server" MaxLength="50"></asp:TextBox></td>
</tr>
<tr>
<td class="requiredField">Default Discount:</td>
<td><asp:TextBox ID="tbDefaultDiscount" runat="server" MaxLength="25"></asp:TextBox></td>
</tr>
<tr>
<td class="requiredField">Rank:</td>
<td><asp:TextBox ID="tbRank" runat="server" MaxLength="25"></asp:TextBox></td>
</tr>
<tr>
<td colspan="2">
    <asp:Button ID="btSave" runat="server" Text="Save Changes" 
        onclick="btSave_Click" /> &nbsp;
    <asp:Button ID="btCancel" runat="server" Text="Cancel" 
        onclick="btCancel_Click" />
</td>
</tr>
</table>
</div>
</asp:PlaceHolder>
<div class="gridActions">
<strong>Actions:</strong>
<asp:LinkButton ID="lbAdd" runat="server" onclick="lbAdd_Click">Add new discount code</asp:LinkButton>
</div>
<asp:GridView ID="gvData" runat="server" CssClass="gridTable" AllowSorting="False" 
    AutoGenerateColumns="False" DataKeyNames="DiscountID" OnRowDeleting="DeleteRow" 
    OnSelectedIndexChanging="SelectRow" CellPadding="0" GridLines="None" 
    BorderStyle="None" Width="100%">
    <Columns>
        <asp:CommandField ShowSelectButton="True" SelectImageUrl="~/img/edit.gif" SelectText="View" ButtonType="Image" ItemStyle-Width="16px" />
        <asp:TemplateField ShowHeader="false">
            <ItemStyle HorizontalAlign="Center" Width="16px" />
            <ItemTemplate>
                <asp:ImageButton ID="btnDel" runat="server" 
                    CausesValidation="False" CommandName="Delete"
                    ImageUrl="~/img/delete.gif" ToolTip="Delete" OnClientClick="return confirm('Are you sure you want to delete this discount code?')" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField ShowHeader="false">
            <ItemStyle HorizontalAlign="Center" Width="100px" />
            <ItemTemplate>
            <a href="admMngCommMult.aspx?id=<%# DataBinder.Eval(Container, "DataItem.DiscountID")%>">Commission/Multipliers</a>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="DiscountID" HeaderText="DiscountID" Visible="false" />
        <asp:BoundField DataField="DiscountCode" HeaderText="Discount Code" />
        <asp:BoundField DataField="DisplayName" HeaderText="Display Name" />
        <asp:BoundField DataField="DefaultDiscount" HeaderText="Default Discount" />
        <asp:BoundField DataField="Rank" HeaderText="Rank" />
    </Columns>
    <HeaderStyle CssClass="gridHeader" />
    <RowStyle CssClass="gridRow" VerticalAlign="Top" />
    <AlternatingRowStyle CssClass="gridAltRow" VerticalAlign="Top" />
</asp:GridView>
<input id="hdDiscountID" type="hidden" runat="server"/>
</asp:Content>

