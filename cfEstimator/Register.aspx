﻿﻿<%@ page language="C#" masterpagefile="~/cfestimator.master" autoeventwireup="true" CodeBehind="Register.aspx.cs" Inherits="cfEstimator.Register" title="Registration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h2>Registration</h2>
<div style="text-align: center"><asp:Label runat="server" ID="lblMsg" CssClass="infoMsg" EnableViewState="false"></asp:Label></div>
<div class="dataForm" id="dataForm" runat="server">
<p>Bold fields below are required.</p>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
<table>
<tr>
<td class="requiredField">Username:</td>
<td><asp:TextBox ID="tbUsername" runat="server" MaxLength="25"></asp:TextBox><asp:RequiredFieldValidator
        ID="RequiredFieldValidator1" EnableClientScript="false" ControlToValidate="tbUsername" runat="server" 
        ErrorMessage="Please enter a Username.">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
            ID="RegularExpressionValidator1" runat="server" ControlToValidate="tbUsername" 
            ErrorMessage="Invalid characters in username!  Username must contain only numbers and letters." EnableClientScript="False" ValidationExpression="[a-zA-Z0-9]+">*</asp:RegularExpressionValidator><asp:CustomValidator
        ID="CustomValidator4" runat="server" ControlToValidate="tbUsername"
        ErrorMessage="Username already exists!  Please choose a different username." 
        onservervalidate="CustomValidator4_ServerValidate">*</asp:CustomValidator></td>
</tr>
<tr>
<td class="requiredField">Password:</td>
<td><asp:TextBox ID="tbPassword" runat="server" TextMode="Password" MaxLength="25"></asp:TextBox><asp:CustomValidator
        ID="CustomValidator1" runat="server" 
        ErrorMessage="Please enter a Password." EnableClientScript="false"
        onservervalidate="CustomValidator1_ServerValidate">*</asp:CustomValidator></td>
</tr>
<tr>
<td class="requiredField">Re-enter Password:</td>
<td><asp:TextBox ID="tbPassword2" runat="server" TextMode="Password" MaxLength="25"></asp:TextBox><asp:CustomValidator
        ID="CustomValidator2" runat="server" 
        ErrorMessage="Passwords must match." EnableClientScript="false"
        onservervalidate="CustomValidator2_ServerValidate">*</asp:CustomValidator></td>
</tr>
<tr>
<td class="requiredField">First Name:</td>
<td><asp:TextBox ID="tbFirstName" runat="server" MaxLength="50"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
        ControlToValidate="tbFirstName" EnableClientScript="False" 
        ErrorMessage="Please enter a First Name.">*</asp:RequiredFieldValidator>
                                </td>
</tr>
<tr>
<td class="requiredField">Last Name:</td>
<td><asp:TextBox ID="tbLastName" runat="server" MaxLength="50"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
        ControlToValidate="tbLastName" EnableClientScript="False" 
        ErrorMessage="Please enter a Last Name.">*</asp:RequiredFieldValidator>
                                </td>
</tr>
<tr>
<td class="requiredField">Email Address:</td>
<td><asp:TextBox ID="tbEmailAddress" runat="server" MaxLength="100" Width="330px"></asp:TextBox>
<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
        ControlToValidate="tbEmailAddress" EnableClientScript="False" 
        ErrorMessage="Please enter an Email Address.">*</asp:RequiredFieldValidator>
 <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
        ControlToValidate="tbEmailAddress" EnableClientScript="false"
        ErrorMessage="Invalid email address." ValidationExpression=".+@.+">*</asp:RegularExpressionValidator></td>
</tr>
<tr>
<td class="requiredField">Phone Number:</td>
<td><asp:TextBox ID="tbPhone" runat="server" MaxLength="15"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
        ControlToValidate="tbPhone" EnableClientScript="False" 
        ErrorMessage="Please enter a Phone Number.">*</asp:RequiredFieldValidator>
                                </td>
</tr>
<tr>
<td class="requiredField">Company Name:</td>
<td>
    <asp:DropDownList ID="ddCompany" runat="server" DataTextField="CompanyName" DataValueField="CompanyID" />
    <asp:CustomValidator ID="CustomValidator3" runat="server" 
        ControlToValidate="ddCompany" 
        onservervalidate="CustomValidator3_ServerValidate" ErrorMessage="Please select a Company Name.">*</asp:CustomValidator>
</td>
</tr>
<tr>
<td>Address:</td>
<td><asp:TextBox ID="tbAddress1" runat="server" MaxLength="50" Width="330px"></asp:TextBox></td>
</tr>
<tr>
<td>&nbsp;</td>
<td><asp:TextBox ID="tbAddress2" runat="server" MaxLength="50" Width="330px"></asp:TextBox></td>
</tr>
<tr>
<td>City:</td>
<td><asp:TextBox ID="tbCity" runat="server" MaxLength="50" Width="330px"></asp:TextBox></td>
</tr>
<tr>
<td>State/Province:</td>
<td><asp:TextBox ID="tbState" runat="server" MaxLength="15"></asp:TextBox></td>
</tr>
<tr>
<td>Zip:</td>
<td><asp:TextBox ID="tbZip" runat="server" MaxLength="15"></asp:TextBox></td>
</tr>
<tr>
<td>&nbsp;</td>
<td>
<asp:Button ID="btSave" runat="server" Text="Register" onclick="btSave_Click" />
</td>
</tr>
</table>
</div>
</asp:Content>

