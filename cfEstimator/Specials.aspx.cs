﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Profile;
using System.Web.Security;
using System.Web.SessionState;
using System.Data;
using System.Configuration;
using static cfEstimator.App_Code;

namespace cfEstimator
{
    public class Specials : Page, IRequiresSessionState
    {
        // Fields
        protected Literal litProductList;

        // Methods
        protected void LoadProducts(string CategoryName)
        {
            data data = new data();
            int categoryID = data.GetCategoryID(CategoryName);
            if (categoryID > 0)
            {
                DataSet set = data.GetProductsByCategory(categoryID, this.Session["BrandAccess"].ToString(), -1, -1, -1.0, -1.0);
                if (set.Tables[0].Rows.Count > 0)
                {
                    string str = "<table class=\"productTable\" cellpadding=0 cellspacing=0>\n" + "<tr class=\"productTableHeader\"><th>Part #</th><th>Description</th><th>Ref Sell</th></tr>\n";
                    string str2 = "";
                    string str3 = "";
                    foreach (DataRow row in set.Tables[0].Rows)
                    {
                        if (row["DisplayName"].ToString() != str2)
                        {
                            object[] objArray = new object[] { str, "<tr><th colspan=\"3\">", row["DisplayName"], "</th></tr>\n" };
                            str = string.Concat(objArray);
                            str2 = row["DisplayName"].ToString();
                        }
                        if (!row.IsNull("Size") && (row["Size"].ToString().Trim() != ""))
                        {
                            str3 = " - " + row["Size"];
                        }
                        str = str + "<tr>\n";
                        if (row.IsNull("NoteID"))
                        {
                            object[] objArray2 = new object[] { str, "<td>", row["HFPartNumber"], "</td>\n" };
                            str = string.Concat(objArray2);
                        }
                        else
                        {
                            object[] objArray3 = new object[] { str, "<td>", row["HFPartNumber"], "<a style=\"text-decoration: none\" href=\"javascript:ShowNote('", row["ProductID"], "')\">*</a></td>\n" };
                            str = string.Concat(objArray3);
                        }
                        object[] objArray4 = new object[] { str, "<td>", row["Description"], str3, "</td>\n" };
                        str = string.Concat(new object[] { string.Concat(objArray4), "<td><a href=\"estimate.aspx?pn=", row["HFPartNumber"], "\">", $"{row["ListPrice"]:c}", "</a></td>\n" }) + "</tr>\n";
                        str3 = "";
                    }
                    str = str + "</table>\n";
                    this.litProductList.Text = str;
                }
                else
                {
                    this.litProductList.Text = "No products match that criteria";
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            util.CheckSession();
            if (!this.Page.IsPostBack)
            {
                this.LoadProducts(ConfigurationManager.AppSettings["SpecialsCategory"]);
            }
        }

        // Properties
        protected DefaultProfile Profile =>
            ((DefaultProfile)this.Context.Profile);

        protected global_asax ApplicationInstance =>
            ((global_asax)this.Context.ApplicationInstance);
    }
}
