﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using static cfEstimator.App_Code;

namespace cfEstimator
{    
    public class admMngPriceCodes : Page, IRequiresSessionState
    {
        // Fields
        protected admMenu admMenu1;
        protected Label lblMsg;
        protected TextBox tbPriceCode;
        protected TextBox tbDisplayName;
        protected TextBox tbDescription;
        protected CheckBox cbShowOnBuyout;
        protected TextBox tbDefaultMarkup;
        protected Button btSave;
        protected Button btCancel;
        protected PlaceHolder phEditForm;
        protected LinkButton lbAdd;
        protected GridView gvData;
        protected HtmlInputHidden hdPriceCodeID;

        // Methods
        protected void BindGrid()
        {
            DataSet priceCodes = new data().GetPriceCodes("", false);
            this.gvData.DataSource = priceCodes;
            this.gvData.DataBind();
        }

        protected void btCancel_Click(object sender, EventArgs e)
        {
            this.tbPriceCode.Text = "";
            this.phEditForm.Visible = false;
        }

        protected void btSave_Click(object sender, EventArgs e)
        {
            double defaultMarkup = -1.0;
            if (this.tbPriceCode.Text.Trim() == "")
            {
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "The Price Code field is required!";
            }
            else if (this.tbDisplayName.Text.Trim() == "")
            {
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "The Display Name field is required!";
            }
            else if (this.tbDescription.Text.Trim() == "")
            {
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "The Description field is required!";
            }
            else
            {
                if (this.tbDefaultMarkup.Text.Trim() == "")
                {
                    if (this.cbShowOnBuyout.Checked)
                    {
                        this.lblMsg.CssClass = "errorMsg";
                        this.lblMsg.Text = "You must enter a Default Markup if \"Show On Buyout\" is checked!";
                        return;
                    }
                }
                else
                {
                    try
                    {
                        defaultMarkup = double.Parse(this.tbDefaultMarkup.Text);
                    }
                    catch
                    {
                        this.lblMsg.CssClass = "errorMsg";
                        this.lblMsg.Text = "Invalid number in the Default Markup field!";
                        return;
                    }
                }
                data data = new data();
                if (this.hdPriceCodeID.Value != "-")
                {
                    if (!data.UpdatePriceCode(int.Parse(this.hdPriceCodeID.Value), this.tbPriceCode.Text, this.tbDisplayName.Text, this.tbDescription.Text, this.cbShowOnBuyout.Checked, defaultMarkup))
                    {
                        this.lblMsg.CssClass = "errorMsg";
                        this.lblMsg.Text = "Unable to update price code '" + this.tbPriceCode.Text + "'!";
                    }
                    else
                    {
                        this.lblMsg.Text = "Successfully updated price code '" + this.tbPriceCode.Text + "'!";
                        this.phEditForm.Visible = false;
                        this.BindGrid();
                    }
                }
                else if (data.InsertPriceCode(this.tbPriceCode.Text, this.tbDisplayName.Text, this.tbDescription.Text, this.cbShowOnBuyout.Checked, defaultMarkup) <= 0)
                {
                    this.lblMsg.CssClass = "errorMsg";
                    this.lblMsg.Text = "Unable to add price code '" + this.tbPriceCode.Text + "'!";
                }
                else
                {
                    this.lblMsg.Text = "Successfully added price code '" + this.tbPriceCode.Text + "'!";
                    this.phEditForm.Visible = false;
                    this.BindGrid();
                }
            }
        }

        protected void DeleteRow(object sender, GridViewDeleteEventArgs e)
        {
            if (new data().DeletePriceCode((int)this.gvData.DataKeys[e.RowIndex].Value))
            {
                this.lblMsg.Text = "Successfully deleted price code!";
                this.BindGrid();
            }
            else
            {
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "Unable to delete price code!  It may be associated with other data.";
            }
        }

        protected void lbAdd_Click(object sender, EventArgs e)
        {
            this.hdPriceCodeID.Value = "-";
            this.tbPriceCode.Text = "";
            this.tbDisplayName.Text = "";
            this.tbDescription.Text = "";
            this.cbShowOnBuyout.Checked = true;
            this.tbDefaultMarkup.Text = "";
            this.phEditForm.Visible = true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            util.CheckSession();
            if (!this.Page.IsPostBack)
            {
                this.BindGrid();
            }
        }

        protected void SelectRow(object sender, GridViewSelectEventArgs e)
        {
            data data = new data();
            DataSet priceCodes = data.GetPriceCodes(data.GetPriceCode((int)this.gvData.DataKeys[e.NewSelectedIndex].Value), false);
            if (priceCodes.Tables[0].Rows.Count > 0)
            {
                this.hdPriceCodeID.Value = priceCodes.Tables[0].Rows[0]["PriceCodeID"].ToString();
                this.tbPriceCode.Text = priceCodes.Tables[0].Rows[0]["PriceCode"].ToString();
                this.tbDisplayName.Text = priceCodes.Tables[0].Rows[0]["DisplayName"].ToString();
                this.tbDescription.Text = priceCodes.Tables[0].Rows[0]["Description"].ToString();
                this.cbShowOnBuyout.Checked = (bool)priceCodes.Tables[0].Rows[0]["ShowOnBuyout"];
                this.tbDefaultMarkup.Text = priceCodes.Tables[0].Rows[0]["DefaultMarkup"].ToString();
                this.phEditForm.Visible = true;
            }
        }

        // Properties
        protected DefaultProfile Profile =>
            ((DefaultProfile)this.Context.Profile);

        protected global_asax ApplicationInstance =>
            ((global_asax)this.Context.ApplicationInstance);
    }
}