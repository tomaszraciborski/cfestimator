﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using static cfEstimator.App_Code;

namespace cfEstimator
{
    public class admMngCommMult : Page, IRequiresSessionState
    {
        // Fields
        protected admMenu admMenu1;
        protected Label lblMsg;
        protected Label lblDiscount;
        protected Label lblPriceCodeDiscountID;
        protected DropDownList ddPCs;
        protected TextBox tbCommission;
        protected TextBox tbMultiplier;
        protected Button btSave;
        protected Button btCancel;
        protected PlaceHolder phEditForm;
        protected LinkButton lbAdd;
        protected GridView gvData;
        protected HiddenField hdDiscountID;

        // Methods
        protected void BindGrid()
        {
            DataSet commissionsMultipliers = new data().GetCommissionsMultipliers(-1, int.Parse(this.hdDiscountID.Value));
            this.gvData.DataSource = commissionsMultipliers;
            this.gvData.DataBind();
        }

        protected void btCancel_Click(object sender, EventArgs e)
        {
            this.lblPriceCodeDiscountID.Text = "";
            this.ddPCs.ClearSelection();
            this.ddPCs.SelectedIndex = 0;
            this.tbCommission.Text = "0";
            this.tbMultiplier.Text = "0";
            this.phEditForm.Visible = false;
        }

        protected void btSave_Click(object sender, EventArgs e)
        {
            data data = new data();
            if (this.tbCommission.Text.Trim() == "")
            {
                goto TR_0000;
            }
            else if (this.tbMultiplier.Text.Trim() == "")
            {
                goto TR_0000;
            }
            else if (this.ddPCs.SelectedIndex != 0)
            {
                double commission = 0.0;
                double multiplier = 0.0;
                try
                {
                    commission = double.Parse(this.tbCommission.Text);
                    multiplier = double.Parse(this.tbMultiplier.Text);
                }
                catch
                {
                    this.lblMsg.CssClass = "errorMsg";
                    this.lblMsg.Text = "Commission and Multiplier must be numeric values!";
                    return;
                }
                int discountID = -1;
                try
                {
                    discountID = int.Parse(this.hdDiscountID.Value);
                }
                catch
                {
                    this.lblMsg.CssClass = "errorMsg";
                    this.lblMsg.Text = "Invalid DiscountID!";
                    return;
                }
                int priceCodeID = int.Parse(this.ddPCs.SelectedValue);
                if (this.lblPriceCodeDiscountID.Text == "-")
                {
                    if (data.InsertCommissionMultiplier(priceCodeID, discountID, commission, multiplier) <= 0)
                    {
                        this.lblMsg.CssClass = "errorMsg";
                        this.lblMsg.Text = "Unable to add commission and multiplier!";
                        return;
                    }
                    this.lblMsg.Text = "Successfully added commission and multiplier!";
                    this.phEditForm.Visible = false;
                    this.BindGrid();
                    return;
                }
                if (data.UpdateCommissionMultiplier(int.Parse(this.lblPriceCodeDiscountID.Text), priceCodeID, discountID, commission, multiplier))
                {
                    this.lblMsg.Text = "Successfully updated commission and multiplier!";
                    this.phEditForm.Visible = false;
                    this.BindGrid();
                    return;
                }
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "Unable to update commission and multiplier!";
            }
            else
            {
                goto TR_0000;
            }
            return;
            TR_0000:
            this.lblMsg.CssClass = "errorMsg";
            this.lblMsg.Text = "The Price Code, Commission, and Multiplier fields are required!";
        }

        protected void DeleteRow(object sender, GridViewDeleteEventArgs e)
        {
            if (new data().DeleteCommissionMultiplier((int)this.gvData.DataKeys[e.RowIndex].Value))
            {
                this.lblMsg.Text = "Successfully deleted commission/multiplier!";
                this.BindGrid();
            }
            else
            {
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "Unable to delete commission/multiplier!  It may be associated with other data.";
            }
        }

        protected void lbAdd_Click(object sender, EventArgs e)
        {
            this.lblPriceCodeDiscountID.Text = "-";
            this.ddPCs.ClearSelection();
            this.ddPCs.SelectedIndex = 0;
            this.tbCommission.Text = "0";
            this.tbMultiplier.Text = "0";
            this.phEditForm.Visible = true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            util.CheckSession();
            if (!this.Page.IsPostBack)
            {
                try
                {
                    int.Parse(base.Request.QueryString["id"]);
                    this.hdDiscountID.Value = base.Request.QueryString["id"];
                }
                catch
                {
                    this.lblMsg.CssClass = "errorMsg";
                    this.lblMsg.Text = "Invalid DiscountID passed in to this page!";
                    return;
                }
                data data = new data();
                DataSet discounts = data.GetDiscounts(data.GetDiscountCode(int.Parse(this.hdDiscountID.Value)), -1);
                this.lblDiscount.Text = discounts.Tables[0].Rows[0]["DisplayName"].ToString();
                discounts = data.GetPriceCodes("", false);
                this.ddPCs.DataSource = discounts;
                this.ddPCs.DataBind();
                this.ddPCs.Items.Insert(0, new ListItem("Select one...", "0"));
                this.ddPCs.SelectedIndex = 0;
                this.BindGrid();
            }
        }

        protected void SelectRow(object sender, GridViewSelectEventArgs e)
        {
            DataSet commissionsMultipliers = new data().GetCommissionsMultipliers((int)this.gvData.DataKeys[e.NewSelectedIndex].Value, -1);
            if (commissionsMultipliers.Tables[0].Rows.Count > 0)
            {
                this.lblPriceCodeDiscountID.Text = ((int)this.gvData.DataKeys[e.NewSelectedIndex].Value).ToString();
                this.ddPCs.ClearSelection();
                this.ddPCs.Items.FindByValue(commissionsMultipliers.Tables[0].Rows[0]["PriceCodeID"].ToString()).Selected = true;
                this.tbCommission.Text = commissionsMultipliers.Tables[0].Rows[0]["Commission"].ToString();
                this.tbMultiplier.Text = commissionsMultipliers.Tables[0].Rows[0]["Multiplier"].ToString();
                this.phEditForm.Visible = true;
            }
        }

        // Properties
        protected DefaultProfile Profile =>
            ((DefaultProfile)this.Context.Profile);

        protected global_asax ApplicationInstance =>
            ((global_asax)this.Context.ApplicationInstance);
    }
}