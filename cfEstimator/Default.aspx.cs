﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using static cfEstimator.App_Code;

namespace cfEstimator
{
    public class _Default : Page, IRequiresSessionState
    {
        // Methods
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.IsPostBack && (this.Session["UserID"] == null))
            {
                data data = new data();
                this.Session["UserID"] = data.GetUserID(base.User.Identity.Name);
                DataSet users = data.GetUsers(int.Parse(this.Session["UserID"].ToString()));
                this.Session["IsAdmin"] = (bool)users.Tables[0].Rows[0]["IsAdmin"];
                this.Session["ISGDEmployee"] = !((bool)this.Session["IsAdmin"]) ? ((bool)users.Tables[0].Rows[0]["ISGDEmployee"]) : true;
                this.Session["MaxDiscountRank"] = !users.Tables[0].Rows[0].IsNull("MaxDiscountRank") ? ((int)users.Tables[0].Rows[0]["MaxDiscountRank"]) : -1;
                users.Clear();
                if (((bool)this.Session["IsAdmin"]) || ((bool)this.Session["ISGDEmployee"]))
                {
                    users = data.GetBrands(-1);
                }
                else
                {
                    users = data.GetBrandsByUser(int.Parse(this.Session["UserID"].ToString()));
                }
                string str = "";
                if (users.Tables[0].Rows.Count <= 0)
                {
                    str = "-1";
                }
                else
                {
                    foreach (DataRow row in users.Tables[0].Rows)
                    {
                        str = str + row["BrandID"].ToString() + ",";
                    }
                    str = str.Remove(str.Length - 1);
                }
                this.Session["BrandAccess"] = str;
            }
        }

        // Properties
        protected DefaultProfile Profile =>
            ((DefaultProfile)this.Context.Profile);

        protected global_asax ApplicationInstance =>
            ((global_asax)this.Context.ApplicationInstance);
    }

}