﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using static cfEstimator.App_Code;

namespace cfEstimator
{
    
    public class admMngConfigs : Page, IRequiresSessionState
    {
        // Fields
        protected admMenu admMenu1;
        protected Label lblMsg;
        protected DropDownList ddFrame;
        protected DropDownList ddFrameConfigType;
        protected Button btGetCategories;
        protected Button btGetProducts;
        protected LinkButton lbDeleteFrameConfig;
        protected PlaceHolder phDeleteFrameConfig;
        protected TextBox tbHFPartNumber;
        protected CheckBox cbIsDefault;
        protected TextBox tbDefaultNumber;
        protected Button btSave;
        protected Button btCancel;
        protected PlaceHolder phEditForm;
        protected LinkButton lbAdd;
        protected GridView gvData;
        protected PlaceHolder phProducts;
        protected TextBox tbCategoryName;
        protected Button btSave2;
        protected Button btCancel2;
        protected PlaceHolder phEditForm2;
        protected LinkButton lbAdd2;
        protected GridView gvData2;
        protected PlaceHolder phCategory;
        protected HiddenField hdFrameConfigID;

        // Methods
        protected void BindCategoryGrid()
        {
            this.phCategory.Visible = true;
            data data = new data();
            DataSet frameConfigCategories = data.GetFrameConfigCategories(int.Parse(this.hdFrameConfigID.Value));
            this.gvData2.DataSource = frameConfigCategories;
            this.gvData2.DataBind();
            this.phDeleteFrameConfig.Visible = false;
            if (((frameConfigCategories.Tables[0].Rows.Count == 0) && (this.hdFrameConfigID.Value != "-1")) && (data.GetFrameConfigProducts(int.Parse(this.hdFrameConfigID.Value), this.Session["BrandAccess"].ToString()).Tables[0].Rows.Count == 0))
            {
                this.phDeleteFrameConfig.Visible = true;
            }
        }

        protected void BindProductsGrid()
        {
            this.phProducts.Visible = true;
            data data = new data();
            DataSet frameConfigProducts = data.GetFrameConfigProducts(int.Parse(this.hdFrameConfigID.Value), this.Session["BrandAccess"].ToString());
            this.gvData.DataSource = frameConfigProducts;
            this.gvData.DataBind();
            this.phDeleteFrameConfig.Visible = false;
            if (((frameConfigProducts.Tables[0].Rows.Count == 0) && (this.hdFrameConfigID.Value != "-1")) && (data.GetFrameConfigCategories(int.Parse(this.hdFrameConfigID.Value)).Tables[0].Rows.Count == 0))
            {
                this.phDeleteFrameConfig.Visible = true;
            }
        }

        protected void btCancel_Click(object sender, EventArgs e)
        {
            this.phEditForm.Visible = false;
        }

        protected void btCancel2_Click(object sender, EventArgs e)
        {
            this.phEditForm2.Visible = false;
        }

        protected void btGetCategories_Click(object sender, EventArgs e)
        {
            this.phProducts.Visible = false;
            this.phEditForm.Visible = false;
            if (this.SetFrameConfigID())
            {
                this.BindCategoryGrid();
            }
        }

        protected void btGetProducts_Click(object sender, EventArgs e)
        {
            this.phCategory.Visible = false;
            this.phEditForm2.Visible = false;
            if (this.SetFrameConfigID())
            {
                this.BindProductsGrid();
            }
        }

        protected void btSave_Click(object sender, EventArgs e)
        {
            data data = new data();
            int defaultNumber = 1;
            try
            {
                defaultNumber = int.Parse(this.tbDefaultNumber.Text);
            }
            catch
            {
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "Please enter an integer in the Default Number field!";
                return;
            }
            if (this.tbHFPartNumber.Text.Trim() != "")
            {
                DataSet products = data.GetProducts(this.tbHFPartNumber.Text, this.Session["BrandAccess"].ToString());
                if ((products.Tables.Count == 0) || (products.Tables[0].Rows.Count == 0))
                {
                    this.lblMsg.CssClass = "errorMsg";
                    this.lblMsg.Text = "Invalid Part Number!";
                }
                else
                {
                    int num3;
                    int productID = (int)products.Tables[0].Rows[0]["ProductID"];
                    if (int.Parse(this.hdFrameConfigID.Value) >= 0)
                    {
                        num3 = int.Parse(this.hdFrameConfigID.Value);
                    }
                    else
                    {
                        num3 = data.InsertFrameConfig(this.ddFrameConfigType.SelectedValue, int.Parse(this.ddFrame.SelectedValue));
                        this.hdFrameConfigID.Value = num3.ToString();
                    }
                    if (data.InsertFrameConfigProduct(num3, productID, this.cbIsDefault.Checked, defaultNumber) <= 0)
                    {
                        this.lblMsg.CssClass = "errorMsg";
                        this.lblMsg.Text = "Unable to add part number '" + this.tbHFPartNumber.Text + "'!";
                    }
                    else
                    {
                        this.lblMsg.Text = "Successfully added part number '" + this.tbHFPartNumber.Text + "'!";
                        this.phEditForm.Visible = false;
                        this.BindProductsGrid();
                    }
                }
            }
            else
            {
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "Please enter a Part Number!";
            }
        }

        protected void btSave2_Click(object sender, EventArgs e)
        {
            data data = new data();
            if (this.tbCategoryName.Text.Trim() == "")
            {
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "Please enter a Category Name!";
            }
            else
            {
                int categoryID = data.GetCategoryID(this.tbCategoryName.Text);
                if (categoryID < 0)
                {
                    this.lblMsg.CssClass = "errorMsg";
                    this.lblMsg.Text = "Invalid Category Name!";
                }
                else
                {
                    int num2;
                    if (int.Parse(this.hdFrameConfigID.Value) >= 0)
                    {
                        num2 = int.Parse(this.hdFrameConfigID.Value);
                    }
                    else
                    {
                        num2 = data.InsertFrameConfig(this.ddFrameConfigType.SelectedValue, int.Parse(this.ddFrame.SelectedValue));
                        this.hdFrameConfigID.Value = num2.ToString();
                    }
                    if (data.InsertFrameConfigCategory(num2, categoryID) <= 0)
                    {
                        this.lblMsg.CssClass = "errorMsg";
                        this.lblMsg.Text = "Unable to add category '" + this.tbCategoryName.Text + "'!";
                    }
                    else
                    {
                        this.lblMsg.Text = "Successfully added category '" + this.tbCategoryName.Text + "'!";
                        this.phEditForm2.Visible = false;
                        this.BindCategoryGrid();
                    }
                }
            }
        }

        protected void DeleteRow(object sender, GridViewDeleteEventArgs e)
        {
            if (new data().DeleteFrameConfigProduct((int)this.gvData.DataKeys[e.RowIndex].Value))
            {
                this.lblMsg.Text = "Successfully deleted product from frame config!";
                this.BindProductsGrid();
            }
            else
            {
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "Unable to delete product from the frame config!";
            }
        }

        protected void DeleteRow2(object sender, GridViewDeleteEventArgs e)
        {
            if (new data().DeleteFrameConfigCategory((int)this.gvData2.DataKeys[e.RowIndex].Value))
            {
                this.lblMsg.Text = "Successfully deleted category from frame config!";
                this.BindCategoryGrid();
            }
            else
            {
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "Unable to delete category from the frame config!";
            }
        }

        protected void lbAdd_Click(object sender, EventArgs e)
        {
            this.tbHFPartNumber.Text = "";
            this.tbDefaultNumber.Text = "1";
            this.phEditForm.Visible = true;
        }

        protected void lbAdd2_Click(object sender, EventArgs e)
        {
            this.tbCategoryName.Text = "";
            this.phEditForm2.Visible = true;
        }

        protected void lbDeleteFrameConfig_Click(object sender, EventArgs e)
        {
            if (new data().DeleteFrameConfig(int.Parse(this.hdFrameConfigID.Value)))
            {
                base.Response.Redirect("admMngConfigs.aspx");
            }
            else
            {
                this.lblMsg.CssClass = "errorMsg";
                this.lblMsg.Text = "Unable to delete frame configuration!";
            }
        }

        protected void LoadFrameConfigTypes()
        {
            this.ddFrameConfigType.Items.Insert(0, new ListItem("Select...", "-1"));
            foreach (string str in ConfigurationManager.AppSettings["FrameConfigType"].Split(new char[] { ',' }))
            {
                this.ddFrameConfigType.Items.Insert(this.ddFrameConfigType.Items.Count, new ListItem(str, str));
            }
            this.ddFrameConfigType.SelectedIndex = 0;
        }

        protected void LoadFrames()
        {
            DataSet frames = new data().GetFrames(-1, this.Session["BrandAccess"].ToString());
            frames.Tables[0].Columns.Add(new DataColumn("PaddedName", Type.GetType("System.String")));
            string s = "";
            foreach (DataRow row in frames.Tables[0].Rows)
            {
                s = row["Name"].ToString();
                try
                {
                    int.Parse(s);
                    s = s.PadLeft(50, '0');
                }
                catch
                {
                }
                row["PaddedName"] = s;
            }
            frames.AcceptChanges();
            DataView defaultView = frames.Tables[0].DefaultView;
            defaultView.Sort = "PaddedName";
            this.ddFrame.DataSource = defaultView;
            this.ddFrame.DataBind();
            this.ddFrame.Items.Insert(0, new ListItem("Select...", "-1"));
            this.ddFrame.SelectedIndex = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            util.CheckSession();
            if (!this.Page.IsPostBack)
            {
                this.LoadFrames();
                this.LoadFrameConfigTypes();
            }
        }

        protected bool SetFrameConfigID()
        {
            if (this.ddFrame.SelectedIndex == 0)
            {
                this.lblMsg.Text = "Please select a Frame.";
                return false;
            }
            if (this.ddFrameConfigType.SelectedIndex == 0)
            {
                this.lblMsg.Text = "Please select a Frame Config Type.";
                return false;
            }
            this.hdFrameConfigID.Value = new data().GetFrameConfigID(int.Parse(this.ddFrame.SelectedValue), this.ddFrameConfigType.SelectedValue).ToString();
            return true;
        }

        // Properties
        protected DefaultProfile Profile =>
            ((DefaultProfile)this.Context.Profile);

        protected global_asax ApplicationInstance =>
            ((global_asax)this.Context.ApplicationInstance);
    }
}