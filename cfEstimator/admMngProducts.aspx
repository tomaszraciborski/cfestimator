﻿<%@ page language="C#" masterpagefile="~/cfestimator.master" autoeventwireup="true" CodeBehind="admMngProducts.aspx.cs" Inherits="cfEstimator.admMngProducts" title="Manage Products" %>
<%@ Register src="admMenu.ascx" tagname="admMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<uc1:admMenu ID="admMenu1" runat="server" TagID="hrefProducts" />
<div style="text-align: center">    
    <asp:Label runat="server" ID="lblMsg" CssClass="infoMsg" EnableViewState="false"></asp:Label></div>
<div class="dataFormBoxed">
    Upload product data (.csv file) <asp:FileUpload ID="fuProducts" runat="server" />  
    <asp:Button ID="btUploadProducts" runat="server" Text="Upload" 
        onclick="btUploadProducts_Click" />
        or 
    <asp:Button ID="btAdd"
        runat="server" Text="Add Single Product" onclick="btAdd_Click" />
        <div style="float: right;">
        <asp:LinkButton ID="lbExport" runat="server" onclick="lbExport_Click"><img 
                src="img/button_excel.gif" border="0" width="24" height="24" 
                alt="Export Products to Excel" name="exportexcel" id="exportexcel" /></asp:LinkButton>
        </div>
        <br /><br />
        To edit an existing product, enter the Part Number: 
    <asp:TextBox ID="tbPartNumber" runat="server" onkeypress="return submitenter(this,event,'btFind')"></asp:TextBox> 
    <asp:Button ID="btFind"
        runat="server" Text="Find Product" onclick="btFind_Click" />
</div>
<p>
    <asp:Label ID="lblErrors" style="color: Red;" runat="server" Text="" EnableViewState="false"></asp:Label>
</p>
<asp:PlaceHolder ID="phEditForm" runat="server" Visible="false">
<div class="packageUpdate">
Bold fields below are required.
<table>
<tr>
<td class="requiredField">HFPartNumber:</td>
<td><asp:TextBox ID="tbHFPartNumber" runat="server" MaxLength="25" onkeypress="return submitenter(this,event,'',true)"></asp:TextBox>&nbsp;<asp:Button
        ID="btDelete" runat="server" Text="Delete This Product" onclick="btDelete_Click" OnClientClick="return confirm('Are you sure you want to delete this product?');"/></td>
</tr>
<tr>
<td class="requiredField">Description:</td>
<td><asp:TextBox ID="tbDescription" runat="server" MaxLength="100" Width="500" onkeypress="return submitenter(this,event,'',true)"></asp:TextBox></td>
</tr>
<tr>
<td>Size:</td>
<td><asp:TextBox ID="tbSize" runat="server" MaxLength="50" Width="200" onkeypress="return submitenter(this,event,'',true)"></asp:TextBox></td>
</tr>
<tr>
<td class="requiredField">List Price:</td>
<td><asp:TextBox ID="tbListPrice" runat="server" onkeypress="return submitenter(this,event,'',true)"></asp:TextBox></td>
</tr>
<tr>
<td class="requiredField">Price Code:</td>
<td><asp:DropDownList ID="ddPriceCodes" runat="server" DataTextField="PriceCode" DataValueField="PriceCode" onkeypress="return submitenter(this,event,'',true)"></asp:DropDownList></td>
</tr>
<tr>
<td>Weight:</td>
<td><asp:TextBox ID="tbWeight" runat="server" onkeypress="return submitenter(this,event,'',true)"></asp:TextBox></td>
</tr>
<tr>
<td class="requiredField">Cost Price:</td>
<td><asp:TextBox ID="tbCostPrice" runat="server" onkeypress="return submitenter(this,event,'',true)"></asp:TextBox></td>
</tr>
<tr>
<td>Power HP:</td>
<td><asp:TextBox ID="tbPowerHP" runat="server" onkeypress="return submitenter(this,event,'',true)"></asp:TextBox></td>
</tr>
<tr>
<td>Flow CFM:</td>
<td><asp:TextBox ID="tbFlowCFM" runat="server" onkeypress="return submitenter(this,event,'',true)"></asp:TextBox></td>
</tr>
<tr>
<td>Category:</td>
<td><asp:DropDownList ID="ddCategories" runat="server" DataTextField="Name" DataValueField="CategoryID" onkeypress="return submitenter(this,event,'',true)"></asp:DropDownList></td>
</tr>
<tr>
<td>Stages:</td>
<td><asp:TextBox ID="tbStages" runat="server" onkeypress="return submitenter(this,event,'',true)"></asp:TextBox></td>
</tr>
<tr>
<td>Notes:</td>
<td><asp:TextBox ID="tbNotes" runat="server" onkeypress="return submitenter(this,event,'',true)"></asp:TextBox> pipe-delimited list of Note IDs, e.g. 2|4|7</td>
</tr>
<tr>
<td>Frame:</td>
<td><asp:DropDownList ID="ddFrames" runat="server" DataTextField="Name" DataValueField="FrameID" onkeypress="return submitenter(this,event,'',true)"></asp:DropDownList></td>
</tr>
<tr>
<td>Brand:</td>
<td><asp:DropDownList ID="ddBrands" runat="server" DataTextField="Name" DataValueField="BrandID" onkeypress="return submitenter(this,event,'',true)"></asp:DropDownList></td>
</tr>
<tr>
<td colspan="2">
    <asp:Button ID="btSave" runat="server" Text="Save Changes" 
        onclick="btSave_Click" /> &nbsp;
    <asp:Button ID="btCancel" runat="server" Text="Cancel" 
        onclick="btCancel_Click" />
</td>
</tr>
</table>
</div>
</asp:PlaceHolder>
<asp:Panel ID="pnlData" runat="server" Visible="true">
    <asp:GridView ID="gvData" runat="server" EnableViewState="false" 
    AutoGenerateColumns="False" OnRowDataBound="gvData_RowDataBound">
    <Columns>
        <asp:TemplateField HeaderText="PRODUCTID" Visible="false">
            <ItemTemplate>
                <asp:Label ID="lblProductID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProductID")%>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="HFPARTNUMBER" HeaderText="HFPARTNUMBER"/>
        <asp:BoundField DataField="DESCRIPTION" HeaderText="DESCRIPTION"/>
        <asp:BoundField DataField="SIZE" HeaderText="SIZE"/>
        <asp:BoundField DataField="LISTPRICE" HeaderText="LISTPRICE"/>
        <asp:BoundField DataField="PRCODE" HeaderText="PRCODE"/>
        <asp:BoundField DataField="WEIGHT" HeaderText="WEIGHT"/>
        <asp:BoundField DataField="COSTPRICE" HeaderText="COSTPRICE"/>
        <asp:BoundField DataField="POWERHP" HeaderText="POWERHP"/>
        <asp:BoundField DataField="FLOWCFM" HeaderText="FLOWCFM"/>
        <asp:BoundField DataField="FRAME" HeaderText="FRAME"/>
        <asp:BoundField DataField="STAGES" HeaderText="STAGES"/>
        <asp:BoundField DataField="BRAND" HeaderText="BRAND"/>
        <asp:BoundField DataField="CATEGORY" HeaderText="CATEGORY"/>
        <asp:TemplateField HeaderText="NOTEIDS">
        <ItemTemplate>
            <asp:Label ID="lblNoteIDs" runat="server" Text=''></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
    </Columns>
    </asp:GridView>
</asp:Panel>
<input id="hdProductID" type="hidden" runat="server"/>
</asp:Content>

