﻿﻿<%@ page language="C#" masterpagefile="~/cfestimator.master" autoeventwireup="true" CodeBehind="PriceList.aspx.cs" Inherits="cfEstimator.PriceList" title="Price List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script language="javascript" type="text/javascript">
		function showNote(divID) {
			//document.getElementById("divSort").style.visibility = "visible";
			var style2 = document.getElementById(divID).style;
			style2.display = style2.display? "":"block";
			style2.top = myy-70;
			style2.left = myx-450;
		}
		
		function hideNote(divID) {
			var style2 = document.getElementById(divID).style;
			style2.display = style2.display? "":"none";
		}
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h2>Price List: Blower Modifications - Specials/Coatings</h2>
<table cellspacing="0" cellpadding="0" border="0" style="width:100%;border-collapse:collapse;" class="basicTable">
<tr valign="middle" class="basicTableHeader">
<th>Modification Description</th>
<th>Turbotron</th>
<th>260/725<br />310/400/732<br />510/550/42</th>
<th>810/850/741<br />870/742/751<br />1250/1260</th>
<th>752<br />1270/1400<br />761</th>
<th>1600<br />1870/2000<br />2400</th>
</tr>
<tr>
<th align="left">Base/Guard Mods</th>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td align="left">Jack Screws; Horizontal; Per Base</td>
<td>-</td>
<td><%=GetPriceLink("JSH250")%></td>
<td><%=GetPriceLink("JSH810")%></td>
<td><%=GetPriceLink("JSH1270")%></td>
<td><%=GetPriceLink("JSHBIG")%></td>
</tr>
<tr>
<td align="left">Base Leveling Screws; Vertical; Per Base</td>
<td><%=GetPriceLink("BLSV")%></td>
<td><%=GetPriceLink("BLSV")%></td>
<td><%=GetPriceLink("BLSV")%></td>
<td><%=GetPriceLink("BLSV")%></td>
<td><%=GetPriceLink("BLSV")%></td>
</tr>
<tr>
<td align="left">Machined Pads; Carbon Steel; Per Base<sup><a href="javascript:;" onmouseover="showNote('note1');" onmouseout="hideNote('note1');">1</a></sup></td>
<td><%=GetPriceLink("MPCST")%></td>
<td><%=GetPriceLink("MPCS810")%></td>
<td><%=GetPriceLink("MPCS1270")%></td>
<td><%=GetPriceLink("MPCS1270")%></td>
<td><a href="javascript:;" onmouseover="showNote('note1');" onmouseout="hideNote('note1');">Included</a></td>
</tr>
</table>

<div id="note1" class="note" style="display: none;">
Machined pads are a standard on the 1600, 1870, 2000 and 2400.  No price add required.  
This includes pags at the blower and moter feet.  Jacking screws on the pad are NOT 
included as a standard.  Add if necessary.</div>

<script language="JavaScript">
			<!--

			var firefox=document.getElementById&&!document.all;
			var myx=0;
			var myy=0;

			document.onmousemove=mouseMove;

			//--------------- mouse events code ----------------------
			function mouseMove(e){
				var str;
				if (firefox) {
					myx=e.clientX;
					myy=e.clientY;
					//str="x="+e.clientX+", y="+e.clientY;
				} else {
					myx=event.clientX;
					myy=event.clientY;
					//str="x="+event.clientX+", y="+event.clientY;
				}
				//document.title=str;
			}


			//-->
			</script>
</asp:Content>

