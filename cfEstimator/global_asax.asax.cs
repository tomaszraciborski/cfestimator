﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Profile;
using System.Web.Security;
using System.Web.SessionState;
using static cfEstimator.App_Code;

namespace cfEstimator
{
    [CompilerGlobalScope]
    public class global_asax : HttpApplication
    {
        // Fields
        private static bool __initialized;

        // Methods
        [DebuggerNonUserCode]
        public global_asax()
        {
            if (!__initialized)
            {
                __initialized = true;
            }
        }

        private void Application_End(object sender, EventArgs e)
        {
        }

        private void Application_Error(object sender, EventArgs e)
        {
            new data().InsertException("Trapped unhandled error!", base.Server.GetLastError().InnerException + "\n" + base.Server.GetLastError().StackTrace, -1, "");
            base.Response.Redirect("error.aspx");
        }

        private void Application_Start(object sender, EventArgs e)
        {
        }

        private void Session_End(object sender, EventArgs e)
        {
        }

        private void Session_Start(object sender, EventArgs e)
        {
        }

        // Properties
        protected DefaultProfile Profile =>
            ((DefaultProfile)base.Context.Profile);
    }
}