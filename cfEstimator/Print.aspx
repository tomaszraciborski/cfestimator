﻿﻿<%@ page language="C#" autoeventwireup="true" CodeBehind="Print.aspx.cs" Inherits="cfEstimator.Print" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customer Estimate</title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="font-family: Arial, Verdana, Helvetica, sans-serif; font-size: 10pt; color: #000; border-bottom: solid 1px black; padding-bottom: 5px;">
    <table width="100%">
    <tr>
    <td align="left">
        <asp:CheckBox ID="cbShowCost" runat="server" Text="Show Cost"  Checked="false" 
            AutoPostBack="true" oncheckedchanged="cbShowCost_CheckedChanged" /> &nbsp;
        <asp:CheckBox ID="cbShowPriceCode" runat="server" Text="Show Price Code" Checked="false"
            AutoPostBack="true" oncheckedchanged="cbShowPriceCode_CheckedChanged" /> &nbsp;
        <asp:CheckBox ID="cbShowSummary" runat="server" Text="Show Summary" Checked="false"
            AutoPostBack="true" oncheckedchanged="cbShowSummary_CheckedChanged" /> &nbsp;
        <asp:CheckBox ID="cbCustomerView" runat="server" Text="Customer View" Checked="false"
            AutoPostBack="true" oncheckedchanged="cbCustomerView_CheckedChanged" /> &nbsp;
        <asp:CheckBox ID="cbTotalsOnly" runat="server" Text="Totals Only" Checked="false"
            AutoPostBack="true" oncheckedchanged="cbTotalsOnly_CheckedChanged" Visible="false"/> &nbsp;
        <asp:Button ID="btSavePDF" runat="server" Text="Save As PDF" 
            style="font-size: 8pt;" onclick="btSavePDF_Click" />
    </td>
    <td align="right">
        <a href="estimate.aspx" style="color: #0033ff"><< Back to Estimate</a>
    </td>
    </tr>
    </table>
    </div>
    <asp:Panel ID="pnlPDFOutput" runat="server">
    <div style="font-family: Arial, Verdana, Helvetica, sans-serif; font-size: 10pt; color: #000;">
    <table width="100%" style="margin: 5px;">
    <tr>
    <td align="left" valign="top"><strong>QUOTE #:</strong> <asp:Label ID="lblQuoteNum" runat="server" Text=""></asp:Label></td>
    <td align="right" valign="top"><img src="img/gdCFLogo.gif" width="235" height="29" border="0" /></td>
    </tr>
    <tr>
    <td align="left" valign="top"><strong>REPRESENTATIVE:</strong> <asp:Label ID="lblRep" runat="server" Text=""></asp:Label></td>
    <td align="right" valign="top"><div id="divApproval" runat="server"><strong>QUOTE APPROVAL REQUIRED BY:</strong></div></td>
    </tr>
    <tr>
    <td align="left" valign="top"><strong>QUOTATION FOR:</strong> <asp:Label ID="lblQuoteFor" runat="server" Text=""></asp:Label></td>
    <td align="right" valign="top" style="color: #0033ff; font-weight: bold;"><asp:Label ID="lblApproval" runat="server" Text=""></asp:Label></td>
    </tr>
    <tr>
    <td align="left" valign="top"><strong>ADDRESS:</strong> <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label></td>
    <td align="right" valign="top"><strong>DATE:</strong> <asp:Label ID="lblEstimateDate" runat="server" Text=""></asp:Label></td>
    </tr>
    <tr>
    <td align="left" valign="top" colspan="2"><strong>APPLICATION:</strong> <asp:Label ID="lblApplication" runat="server" Text=""></asp:Label></td>
    </tr>
    <tr>
    <td align="left" valign="top"><strong>FOR INSTALLATION AT:</strong> <asp:Label ID="lblInstallation" runat="server" Text=""></asp:Label></td>
    <td align="right" valign="top"><div id="divDiscount" runat="server"><strong>DISCOUNT:</strong> <asp:Label ID="lblDiscount" runat="server" Text=""></asp:Label></div></td>
    </tr>
    </table>
    
    <table id="tblTotals" runat="server" cellspacing="0" cellpadding="2" border="1" width="450" rules="all" visible="false"
    style="border-style:Solid;width:100%;border-collapse:collapse;border: solid 2px black; margin-bottom: 10px;">
    <tr>
    <th align="center" colspan="2" style="background-color: #00ffff;">Summary</th>
    </tr>
    <tr align="left">
        <th>Total Non Disc Sell:</th>
        <td>
            <asp:Label runat="server" ID="lblNonDiscSell" Text="0"></asp:Label>
        </td>
    </tr>
    <tr align="left">
        <th>Total Disc Sell:</th>
        <td>
            <asp:Label runat="server" ID="lblTotalSell" Text="0"></asp:Label>
        </td>
    </tr>
    <tr align="left">
        <th>Total Weight:</th>
        <td>
            <asp:Label runat="server" ID="lblTotalWeight" Text="0"></asp:Label>
        </td>
    </tr>
    <tr align="left">
        <th>Commission %:</th>
        <td>
            <asp:Label runat="server" ID="lblCommPercent" Text="0"></asp:Label>
        </td>
    </tr>
    <tr align="left">
        <th>Total Commission:</th>
        <td>
            <asp:Label runat="server" ID="lblTotalComm" Text="0"></asp:Label>
        </td>
    </tr>
    <tr align="left">
        <th>Total Cost:</th>
        <td>
            <asp:Label runat="server" ID="lblTotalCost" Text="0"></asp:Label>
        </td>
    </tr>
    <tr style="background-color: #CCCCCC">
        <th colspan="2" align="center">Before Commission</th>
    </tr>
    <tr align="left">
        <th>Margin %:</th>
        <td>
            <asp:Label runat="server" ID="lblBeforeMarginPercent" Text="0"></asp:Label>
        </td>
    </tr>
    <tr align="left">
        <th>Margin $:</th>
        <td>
            <asp:Label runat="server" ID="lblBeforeMargin" Text="0"></asp:Label>
        </td>
    </tr>
    <tr style="background-color: #CCCCCC">
        <th colspan="2" align="center">After Commission</th>
    </tr>
    <tr align="left">
        <th>Margin %:</th>
        <td>
            <asp:Label runat="server" ID="lblAfterMarginPercent" Text="0"></asp:Label>
        </td>
    </tr>
    <tr align="left">
        <th>Margin $:</th>
        <td>
            <asp:Label runat="server" ID="lblAfterMargin" Text="0"></asp:Label>
        </td>
    </tr>
    </table>
        
    <asp:GridView ID="gvEstimate" runat="server" ShowFooter="false" AllowPaging="False" AllowSorting="False" 
    AutoGenerateColumns="False" DataKeyNames="RowID" CellPadding="2" GridLines="Both" BorderStyle="Solid" 
    Width="100%" name="gvEstimate" OnRowDataBound="gvEstimate_RowDataBound" style="border: solid 2px black;">
        <Columns>
            <asp:BoundField DataField="Weight" HeaderText="Wt"/>
            <asp:TemplateField HeaderText="Part #">
            <ItemTemplate>
                <asp:Label ID="lblPartNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PartNo")%>'></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Qty">
            <ItemTemplate>
                <asp:Label ID="lblQuantity" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Quantity")%>'></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Description">
            <ItemTemplate>
                <div id="divDesc" runat="server" class=""><%# DataBinder.Eval(Container, "DataItem.Description")%></div>
            </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Cost" HeaderText="Cost" Visible="false" DataFormatString="{0:c}"/>
            <asp:BoundField DataField="PriceCode" HeaderText="PC" Visible="false"/>
            <asp:BoundField DataField="RefSell" HeaderText="Ref Sell" DataFormatString="{0:c}"/>
            <asp:BoundField DataField="UnitSell" HeaderText="Unit Sell" DataFormatString="{0:c}"/>
            <asp:BoundField DataField="Discount" HeaderText="Disc" Visible="true"/>
            <asp:BoundField DataField="Total" HeaderText="Total" Visible="true" DataFormatString="{0:c}"/>
            <asp:BoundField DataField="Commission" HeaderText="Comm (%)" DataFormatString="{0:p}"/>
            <asp:BoundField DataField="CommissionAmount" HeaderText="Comm ($)" DataFormatString="{0:c}"/>
        </Columns>
        <HeaderStyle CssClass="gridHeader" />
        <FooterStyle CssClass="gridFooter" />
        <RowStyle CssClass="gridRow" VerticalAlign="Top" />
        <AlternatingRowStyle CssClass="gridAltRow" VerticalAlign="Top" />
        <PagerStyle CssClass="gridPager" />
    </asp:GridView>
    <div ID="divNotes" runat="server">
    <br /><strong>Additional Notes</strong><br />
    <asp:Label ID="lblEstimateNote" runat="server" Text=""></asp:Label>
    </div>

    </div>
    </asp:Panel>
    </form>
</body>
</html>

