﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using static cfEstimator.App_Code;

namespace cfEstimator
{
    public class main : Page, IRequiresSessionState
    {
        // Fields
        protected HtmlForm form1;

        // Methods
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!bool.Parse(ConfigurationManager.AppSettings["CheckReferrer"]))
            {
                base.Response.Redirect("default.aspx");
            }
            else if (base.Request.UrlReferrer == null)
            {
                base.Response.Redirect("unauth.htm?referrer=null");
            }
            else
            {
                string stringToEscape = base.Request.UrlReferrer.ToString().ToLower();
                bool flag = false;
                string[] strArray2 = ConfigurationManager.AppSettings["ReferrerURLs"].Split(new char[] { ',' });
                int index = 0;
                while (true)
                {
                    if (index < strArray2.Length)
                    {
                        string str2 = strArray2[index];
                        if (!stringToEscape.StartsWith(str2.ToLower()))
                        {
                            index++;
                            continue;
                        }
                        flag = true;
                    }
                    if (!flag)
                    {
                        base.Response.Redirect("unauth.htm?referrer=" + Uri.EscapeUriString(stringToEscape));
                        return;
                    }
                    this.Session["AccessAllowed"] = true;
                    base.Response.Redirect("default.aspx");
                    return;
                }
            }
        }

        // Properties
        protected DefaultProfile Profile =>
            ((DefaultProfile)this.Context.Profile);

        protected global_asax ApplicationInstance =>
            ((global_asax)this.Context.ApplicationInstance);
    }
}