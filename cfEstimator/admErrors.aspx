﻿
﻿<%@ page language="C#" masterpagefile="~/cfestimator.master" autoeventwireup="true" CodeBehind="admErrors.aspx.cs" Inherits="cfEstimator.admErrors" title="Manage Errors" %>
<%@ Register src="admMenu.ascx" tagname="admMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<uc1:admMenu ID="admMenu1" runat="server" TagID="hrefErrors" />
<div style="text-align: center"><asp:Label runat="server" ID="lblMsg" CssClass="infoMsg" EnableViewState="false"></asp:Label></div>
<asp:GridView ID="gvErrors" runat="server" CssClass="gridTable" AllowPaging="True" AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="ExceptionID" CellPadding="0" PageSize="10" OnPageIndexChanging="ChangePage" GridLines="None" BorderStyle="None" Width="100%" PagerSettings-Position="TopAndBottom">
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <a href="admErrors.aspx?fn=delete&id=<%# DataBinder.Eval(Container.DataItem, "ExceptionID").ToString() %>" onclick="javascript:return confirm('Are you sure you want to delete this exception?');"><img alt="Delete" src="img/delete.gif" border="0" width="16px" height="16px" /></a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ExceptionID" HeaderText="ExceptionID" InsertVisible="False"
                ReadOnly="True" SortExpression="ExceptionID" Visible="False" />
            <asp:BoundField DataField="ExceptionDate" HeaderText="Date" SortExpression="ExceptionDate" />
            <asp:BoundField DataField="Username" HeaderText="Username" SortExpression="Username" />
            <asp:BoundField DataField="Message" HeaderText="Message" SortExpression="Message" />
            <asp:BoundField DataField="MessageBody" HeaderText="Details" SortExpression="MessageBody" />
        </Columns>
        <HeaderStyle CssClass="gridHeader" />
        <RowStyle CssClass="gridRow" VerticalAlign="Top" />
        <AlternatingRowStyle CssClass="gridAltRow" VerticalAlign="Top" />
        <PagerStyle CssClass="gridPager" />
</asp:GridView>
</asp:Content>





