﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using static cfEstimator.App_Code;


namespace cfEstimator
{
    public class fieldService : Page, IRequiresSessionState
    {
        // Fields
        protected Button btDomAdd;
        protected Button btIntAdd;
        protected HiddenField hdDomDays;
        protected HiddenField hdDomTotal;
        protected HiddenField hdIntDays;
        protected HiddenField hdIntTotal;

        // Methods
        protected void AddRow(string Description, double Cost)
        {
            if (this.Session["estimate"] == null)
            {
                this.Session["estimate"] = util.GetEmptyTable();
            }
            DataTable table = (DataTable)this.Session["estimate"];
            DataRow row = table.NewRow();
            row["RowID"] = table.Rows.Count;
            row["PartNo"] = "_FS";
            row["Quantity"] = 1;
            row["Description"] = Description;
            row["Cost"] = Cost;
            row["RefSell"] = Cost;
            row["UnitSell"] = Cost;
            row["Discount"] = 1;
            row["Total"] = Cost;
            row["TotalNonDisc"] = Cost;
            row["Commission"] = 0;
            row["CommissionAmount"] = 0;
            table.Rows.InsertAt(row, table.Rows.Count);
            this.Session["estimate"] = table;
        }

        protected void btDomAdd_Click(object sender, EventArgs e)
        {
            this.AddRow("Domestic Field Service, " + this.hdDomDays.Value + " Days", double.Parse(this.hdDomTotal.Value));
            base.Response.Redirect("estimate.aspx");
        }

        protected void btIntAdd_Click(object sender, EventArgs e)
        {
            this.AddRow("International Field Service, " + this.hdIntDays.Value + " Days", double.Parse(this.hdIntTotal.Value));
            base.Response.Redirect("estimate.aspx");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            util.CheckSession();
            if (((this.Session["IsAdmin"] == null) || !((bool)this.Session["IsAdmin"])) && ((this.Session["ISGDEmployee"] == null) || !((bool)this.Session["ISGDEmployee"])))
            {
                base.Response.Redirect("error.aspx?cd=unauth");
            }
            string script = ((("" + "var hdDomDays = '" + this.hdDomDays.ClientID + "';\n") + "var hdDomTotal = '" + this.hdDomTotal.ClientID + "';\n") + "var hdIntDays = '" + this.hdIntDays.ClientID + "';\n") + "var hdIntTotal = '" + this.hdIntTotal.ClientID + "';\n";
            this.Page.ClientScript.RegisterClientScriptBlock(base.GetType(), "mycustomscript", script, true);
        }

        // Properties
        protected DefaultProfile Profile =>
            ((DefaultProfile)this.Context.Profile);

        protected global_asax ApplicationInstance =>
            ((global_asax)this.Context.ApplicationInstance);
    }
}