﻿﻿<%@ page language="C#" masterpagefile="~/cfestimator.master" autoeventwireup="true" CodeBehind="estimateList.aspx.cs" Inherits="cfEstimator.estimateList" title="Estimate List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h2>Estimate List</h2>
<div style="text-align: center"><asp:Label runat="server" ID="lblMsg" CssClass="infoMsg" EnableViewState="false"></asp:Label></div>
<asp:PlaceHolder ID="phEditForm" runat="server" Visible="false">
<div class="dataFormBoxed">
<strong>Access summary
    <asp:Literal ID="litEstimateName" runat="server"></asp:Literal></strong>
    <br /><br />
Grant access to username: 
    <asp:TextBox ID="tbUsername" runat="server"></asp:TextBox> &nbsp;
    <asp:Button ID="btAdd" runat="server" Text="Grant Access" 
        onclick="btAdd_Click" />&nbsp;
    <asp:Button ID="btCancel" runat="server" onclick="btCancel_Click" 
        Text="Cancel" />
    <br /><br />

<div style="text-decoration:underline; padding-bottom: 5px;">Current users with access:</div>

<asp:GridView ID="gvUsers" runat="server" AutoGenerateColumns="False" 
DataKeyNames="EstimateUserID,EstimateID" OnRowDeleting="DeleteUserRow" 
CellPadding="1" GridLines="None" BorderStyle="None" ShowHeader="false">
<Columns>
<asp:BoundField DataField="EstimateUserID" HeaderText="EstimateUserID" Visible="false" />
<asp:BoundField DataField="EstimateID" HeaderText="EstimateID" Visible="false" />
<asp:BoundField DataField="UserID" HeaderText="UserID" Visible="false" />
<asp:TemplateField ShowHeader="false">
    <ItemStyle HorizontalAlign="Center" Width="16px" />
    <ItemTemplate>
        <asp:ImageButton ID="btnDelUser" runat="server" 
            CausesValidation="False" CommandName="Delete"
            ImageUrl="~/img/delete.gif" ToolTip="Remove user access" OnClientClick="return confirm('Are you sure you want to remove this user\'s access to this estimate?')" />
    </ItemTemplate>
</asp:TemplateField>
<asp:BoundField DataField="Username" HeaderText="Username" />
</Columns>
</asp:GridView>

</div>
</asp:PlaceHolder>
<asp:GridView ID="gvData" runat="server" CssClass="gridTable" AllowPaging="True" AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="EstimateID" OnRowDeleting="DeleteRow" OnSelectedIndexChanging="SelectRow" OnRowEditing="EditRow" CellPadding="0" PageSize="20" OnPageIndexChanging="ChangePage" GridLines="None" BorderStyle="None" Width="100%">
    <Columns>
        <asp:CommandField ShowSelectButton="True" SelectImageUrl="~/img/edit.gif" SelectText="View" ButtonType="Image" ItemStyle-Width="16px" />
        <asp:TemplateField ShowHeader="false">
            <ItemStyle HorizontalAlign="Center" Width="16px" />
            <ItemTemplate>
                <asp:ImageButton ID="btnDel" runat="server" 
                    CausesValidation="False" CommandName="Delete"
                    ImageUrl="~/img/delete.gif" ToolTip="Delete" OnClientClick="return confirm('Are you sure you want to delete this estimate?')" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField ShowHeader="false">
            <ItemStyle HorizontalAlign="Center" Width="16px" />
            <ItemTemplate>
                <asp:ImageButton ID="btnAccess" runat="server" 
                    CausesValidation="False" CommandName="Edit"
                    ImageUrl="~/img/access.gif" ToolTip="Grant Access" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="EstimateID" HeaderText="EstimateID" Visible="false" />
        <asp:BoundField DataField="EstimateDate" HeaderText="Estimate Date" DataFormatString="{0:d}" />
        <asp:BoundField DataField="Name" HeaderText="Estimate Name" />
        <asp:BoundField DataField="QuoteNumber" HeaderText="Quote #" />
        <asp:BoundField DataField="LastModifiedDate" HeaderText="Modified Date" />
        <asp:BoundField DataField="ModifiedUsername" HeaderText="Modified By" />
        <asp:BoundField DataField="CreatedDate" HeaderText="Created Date" />
        <asp:BoundField DataField="CreatedUsername" HeaderText="Created By" />
    </Columns>
    <HeaderStyle CssClass="gridHeader" />
    <RowStyle CssClass="gridRow" VerticalAlign="Top" />
    <AlternatingRowStyle CssClass="gridAltRow" VerticalAlign="Top" />
    <PagerStyle CssClass="gridPager" />
</asp:GridView>
<input id="hdEstimateID" type="hidden" runat="server"/>
</asp:Content>

