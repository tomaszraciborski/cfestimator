﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using static cfEstimator.App_Code;

namespace cfEstimator
{   
    public class ajaxresponse : Page, IRequiresSessionState
    {
        // Fields
        protected HtmlForm form1;

        // Methods
        protected void DeleteRows(string sheetName, int rownum)
        {
            DataTable table = (DataTable)this.Session[sheetName];
            table.Rows[rownum].Delete();
            this.Session[sheetName] = table;
        }

        protected string GetNotes(int ProductID)
        {
            string str = "=" + "<ul>\n";
            foreach (DataRow row in new data().GetProductNotes(ProductID).Tables[0].Rows)
            {
                str = str + "<li>" + row["Text"].ToString() + "</li>\n";
            }
            return (str + "</ul>\n");
        }

        protected string GetTotals(string sheetName, int rownum)
        {
            DataTable table = (DataTable)this.Session[sheetName];
            double num = 0.0;
            double nonDiscSell = 0.0;
            int num3 = 0;
            double num4 = 0.0;
            double num5 = 0.0;
            double num6 = 0.0;
            double num7 = 0.0;
            double num8 = 0.0;
            foreach (DataRow row in table.Rows)
            {
                if (row["PartNo"].ToString() != "_ST")
                {
                    num += (double)row["Total"];
                    nonDiscSell += (double)row["TotalNonDisc"];
                    num3 += (int)row["Quantity"];
                    if (!row.IsNull("Weight"))
                    {
                        num4 += (double)row["Weight"];
                    }
                    num5 += (double)row["CommissionAmount"];
                    num6 += ((double)row["Cost"]) * ((int)row["Quantity"]);
                }
            }
            num7 = num - num6;
            num8 = num7 - num5;
            string approverByRange = "";
            if (table.Rows.Count > 0)
            {
                data data = new data();
                DataSet discounts = data.GetDiscounts(this.Session["DiscountCode"].ToString(), -1);
                if (discounts.Tables[0].Rows.Count > 0)
                {
                    approverByRange = data.GetApproverByRange(nonDiscSell, (double)discounts.Tables[0].Rows[0]["DefaultDiscount"]);
                }
            }
            double num10 = 0.0;
            double num11 = 0.0;
            double num12 = 0.0;
            if (num != 0.0)
            {
                num10 = (num5 / num) * 100.0;
                num11 = (num7 / num) * 100.0;
                num12 = (num8 / num) * 100.0;
            }
            string str2 = ((((((((((("=" + "totalqty=" + num3.ToString() + "|") + "totalwt=" + $"{num4:N}" + " lbs|") + "totalsell=" + $"{num:C}" + "|") + "totalnondiscsell=" + $"{nonDiscSell:C}" + "|") + "totalcomm=" + $"{num5:C}" + "|") + "commperc=" + $"{num10:N}" + " %|") + "totalcost=" + $"{num6:C}" + "|") + "beforemargin=" + $"{num7:C}" + "|") + "aftermargin=" + $"{num8:C}" + "|") + "beforemarginperc=" + $"{num11:N}" + " %|") + "aftermarginperc=" + $"{num12:N}" + " %|") + "approver=" + approverByRange;
            if (rownum >= 0)
            {
                DataRow row2 = table.Rows[rownum];
                str2 = (str2 + "|") + "rownum=" + rownum.ToString() + "|";
                str2 = row2.IsNull("Weight") ? (str2 + "rowwt=0|") : (str2 + "rowwt=" + $"{row2["Weight"]:N}" + "|");
                str2 = ((((((((str2 + "rowpartnum=" + row2["PartNo"].ToString() + "|") + "rowqty=" + row2["Quantity"].ToString() + "|") + "rowcost=" + $"{row2["Cost"]:C}" + "|") + "rowpc=" + row2["PriceCode"].ToString() + "|") + "rowrefsell=" + $"{row2["RefSell"]:C}" + "|") + "rowunitsell=" + $"{row2["UnitSell"]:C}" + "|") + "rowtotal=" + $"{row2["Total"]:C}" + "|") + "rowcommperc=" + $"{(((double)row2["Commission"]) * 100.0):N}" + " %|") + "rowcomm=" + $"{row2["CommissionAmount"]:C}";
            }
            return str2;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string str2;
            string s = "=";
            if ((base.Request.QueryString["fn"] != null) && ((str2 = base.Request.QueryString["fn"]) != null))
            {
                if (str2 == "d")
                {
                    this.DeleteRows(base.Request.QueryString["s"], int.Parse(base.Request.QueryString["r"]));
                    s = this.GetTotals(base.Request.QueryString["s"], -1);
                }
                else if (str2 == "q")
                {
                    this.UpdateQty(base.Request.QueryString["s"], int.Parse(base.Request.QueryString["r"]), int.Parse(base.Request.QueryString["n"]));
                    s = this.GetTotals(base.Request.QueryString["s"], int.Parse(base.Request.QueryString["r"]));
                }
                else if (str2 == "u")
                {
                    this.UpdateParams(base.Request.QueryString["p"], base.Request.QueryString["v"]);
                }
                else if (str2 == "desc")
                {
                    this.UpdateDesc(base.Request.QueryString["s"], int.Parse(base.Request.QueryString["r"]), base.Request.QueryString["v"]);
                }
                else if (str2 == "n")
                {
                    s = this.GetNotes(int.Parse(base.Request.QueryString["id"]));
                }
            }
            base.Response.Clear();
            base.Response.Write(s);
            base.Response.End();
        }

        protected void UpdateDesc(string sheetName, int rownum, string desc)
        {
            DataTable table = (DataTable)this.Session[sheetName];
            table.Rows[rownum]["Description"] = HttpUtility.UrlDecode(desc);
            this.Session[sheetName] = table;
        }

        protected void UpdateParams(string param, string value)
        {
            Hashtable hashtable = (Hashtable)this.Session["EstimateParams"];
            hashtable[param] = value;
            this.Session["EstimateParams"] = hashtable;
        }

        protected void UpdateQty(string sheetName, int rownum, int qty)
        {
            DataTable table = (DataTable)this.Session[sheetName];
            table.Rows[rownum]["Quantity"] = qty;
            table.Rows[rownum]["Total"] = (qty * double.Parse(table.Rows[rownum]["RefSell"].ToString())) * double.Parse(table.Rows[rownum]["Discount"].ToString());
            table.Rows[rownum]["TotalNonDisc"] = qty * double.Parse(table.Rows[rownum]["RefSell"].ToString());
            table.Rows[rownum]["CommissionAmount"] = double.Parse(table.Rows[rownum]["Commission"].ToString()) * double.Parse(table.Rows[rownum]["Total"].ToString());
            if (!table.Rows[rownum].IsNull("SingleWeight"))
            {
                table.Rows[rownum]["Weight"] = qty * double.Parse(table.Rows[rownum]["SingleWeight"].ToString());
            }
            this.Session[sheetName] = table;
        }

        // Properties
        protected DefaultProfile Profile =>
            ((DefaultProfile)this.Context.Profile);

        protected global_asax ApplicationInstance =>
            ((global_asax)this.Context.ApplicationInstance);
    }
}